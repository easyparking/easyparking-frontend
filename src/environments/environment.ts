// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const rootURL = 'https://easyparking-dev.herokuapp.com'
export const environment = {
  production: false,

  loginURL: rootURL + '/userSignIn',
  updateLoginStatusURL: rootURL + '/updateLoginStatus',
  getOwnersURL: rootURL + '/getOwners',
  verifyUserNameURL: rootURL + '/VerifyUserName/',
  createUsersURL: rootURL + '/createUser',
  changeOwnerStatus: rootURL + '/updateOwnerStatus',
  updateOwnerURL: rootURL + '/updateOwner',
  deleteUserURL: rootURL + '/deleteUser/',
  getDashboardDetails: rootURL + '/dashboard'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
