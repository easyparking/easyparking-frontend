import { ParkingTypeComponent } from './dashboardComponents/parking-type/parking-type.component';
import { PlanTypeComponent } from './dashboardComponents/plan-type/plan-type.component';
import { OwnerDashboardComponent } from './dashboardComponents/owner-dashboard/owner-dashboard.component';
import { DashboardLayoutComponent } from './layouts/dashboard-layout/dashboard-layout.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './landingPageComponents/products/products.component';
import { HelpComponent } from './landingPageComponents/help/help.component';
import { HomeComponent } from './landingPageComponents/home/home.component';
import { OursectorsComponent } from './landingPageComponents/oursectors/oursectors.component';
import { SignInComponent } from './landingPageComponents/sign-in/sign-in.component';
import { SignUpComponent } from './landingPageComponents/sign-up/sign-up.component';
import { ForgotComponent } from './landingPageComponents/forgot/forgot.component';
import { LandingLayoutComponent } from './layouts/landing-layout/landing-layout.component';
import { ParkingsComponent } from './dashboardComponents/parkings/parkings.component';
import { ParkingsearchComponent } from './dashboardComponents/parkingsearch/parkingsearch.component';
import { WantedVehiclesComponent } from './dashboardComponents/wanted-vehicles/wanted-vehicles.component';
import { MonthlycardsComponent } from './dashboardComponents/monthlycards/monthlycards.component';
import { OnlinecardsComponent } from './dashboardComponents/onlinecards/onlinecards.component';
import { VehicleNamesComponent } from './dashboardComponents/vehicle-names/vehicle-names.component';
import { ApplicationsComponent } from './dashboardComponents/applications/applications.component';
import { DemoRequestsComponent } from './dashboardComponents/demo-requests/demo-requests.component';
import { SettingsComponent } from './dashboardComponents/settings/settings.component';
import { OnlinePaymentReportComponent } from './dashboardComponents/online-payment-report/online-payment-report.component';
import { MinireportsComponent } from './dashboardComponents/minireports/minireports.component';
import { ReportsComponent } from './dashboardComponents/reports/reports.component';
import { ForceLogoutComponent } from './dashboardComponents/force-logout/force-logout.component';
import { AdminDashboardComponent } from './dashboardComponents/admin-dashboard/admin-dashboard.component';
import { ViewStandsComponent } from './dashboardComponents/view-stands/view-stands.component';
import { AccountsComponent } from './dashboardComponents/accounts/accounts.component';
import { OfflinecardsComponent } from './dashboardComponents/offlinecards/offlinecards.component';
import { OfflinePaymentsReportComponent } from './dashboardComponents/offline-payments-report/offline-payments-report.component';
import { AppregistrationsComponent } from './dashboardComponents/appregistrations/appregistrations.component';
import { EmpSessionHistoryComponent } from './dashboardComponents/emp-session-history/emp-session-history.component';
import { SlipLostVehiclesComponent } from './dashboardComponents/slip-lost-vehicles/slip-lost-vehicles.component';
import { AnalyticsComponent } from './dashboardComponents/analytics/analytics.component';
import { EmployeeDashboardComponent } from './dashboardComponents/employee-dashboard/employee-dashboard.component';
import { PriceRulesComponent } from './dashboardComponents/price-rules/price-rules.component';
import { EmploysComponent } from './dashboardComponents/employs/employs.component';
import { SettlementsComponent } from './dashboardComponents/settlements/settlements.component';
import { LoginActivityComponent } from './dashboardComponents/login-activity/login-activity.component';
import { OwnerMonthlyCardsComponent } from './dashboardComponents/owner-monthly-cards/owner-monthly-cards.component';
import { AuthGuard } from './authgaurd/auth.gaurd';
import { MyAccountComponent } from './customerComponents/my-account/my-account.component';
import { ProfileComponent } from './dashboardComponents/profile/profile.component';
import { GetStandComponent } from './landingPageComponents/get-stand/get-stand.component';
import { HelpRequestComponent } from './dashboardComponents/help-request/help-request.component';
import { ResetPasswordComponent } from './dashboardComponents/reset-password/reset-password.component';


const routes: Routes = [

  {
    path: '',
    component: LandingLayoutComponent,
    children: [
      {
        path: '',
        pathMatch: "full",
        redirectTo: "/home"
        // component: HomeComponent
      },
      {
        path: "home",
        component: HomeComponent
      },
      {
        path: "oursectors",
        component: OursectorsComponent
      },
      {
        path: "products",
        component: ProductsComponent
      },
      {
        path: "help",
        component: HelpComponent
      },
      {
        path: 'login',
        component: SignInComponent
      },
      {
        path: 'forgot',
        component: ForgotComponent
      },
      {
        path: 'register',
        component: SignUpComponent
      },
      {
        path: 'my-account',
        component: MyAccountComponent
      },
      {
        path: "viewStand",
        component: GetStandComponent
      },
    ]
  },
  {
    path: "",
    component: DashboardLayoutComponent,
    children: [
      {
        path: "profile",
        component: ProfileComponent,
        // canActivate: [AuthGuard]
      },
      {
        path: "admin-dashboard",
        component: AdminDashboardComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "help-request",
        component: HelpRequestComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "owner-dashboard",
        component: OwnerDashboardComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "accounts",
        component: AccountsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "appregistrations",
        component: AppregistrationsComponent,
        canActivate: [AuthGuard]
      },

      {
        path: "parkings",
        component: ParkingsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "vehicleSearch",
        component: ParkingsearchComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "wanted-vehicles",
        component: WantedVehiclesComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "monthlycards",
        component: MonthlycardsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "cardsales",
        component: OnlinecardsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "ownercardsales",
        component: OwnerMonthlyCardsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "offlinecards",
        component: OfflinecardsComponent,
        canActivate: [AuthGuard]
      },
      // {
      //   path: "locations",
      //   component: LocationsComponent
      // },
      {
        path: "vehicle-names",
        component: VehicleNamesComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "applications",
        component: ApplicationsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "demo-requests",
        component: DemoRequestsComponent,
        // canActivate: [AuthGuard]
      },
      {
        path: "locations",
        component: SettingsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "plan-type",
        component: PlanTypeComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "parking-type",
        component: ParkingTypeComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "online-payment-report",
        component: OnlinePaymentReportComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "offline-payment-report",
        component: OfflinePaymentsReportComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "reports",
        component: ReportsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "minireports",
        component: MinireportsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "force-logout",
        component: ForceLogoutComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "view-stands",
        component: ViewStandsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "session-history",
        component: EmpSessionHistoryComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "slip-lost-vehicles",
        component: SlipLostVehiclesComponent,
        canActivate: [AuthGuard]
      },

      {
        path: "analytics",
        component: AnalyticsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "employee-dashboard",
        component: EmployeeDashboardComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "price-rules/:id",
        component: PriceRulesComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "employs",
        component: EmploysComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "settlements",
        component: SettlementsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "login-activity",
        component: LoginActivityComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "reset-password",
        component: ResetPasswordComponent,
        canActivate: [AuthGuard]
      }

    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
