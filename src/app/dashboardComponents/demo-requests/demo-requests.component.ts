import { MatAccordion } from '@angular/material/expansion';
// import { MatAccordion } from '@angular/material';
import { Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { NgbdSortableHeader } from 'src/app/directives/tableDirective';
import { environment } from 'src/environments/environment.prod';
import { debounceTime } from 'rxjs/operators';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from 'src/app/shared/services/data.service';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';
import { Router } from '@angular/router';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import * as moment from 'moment';
import { ExportToCsv } from 'export-to-csv';


@Component({
  selector: 'app-demo-requests',
  templateUrl: './demo-requests.component.html',
  styleUrls: ['./demo-requests.component.css']
})
export class DemoRequestsComponent implements OnInit {
  pageSize = 10;
  active = 1;
  searchForm: any;
  demoRequestsList: any = [];
  count: any = 0;
  pager: any;
  page = 1;
  results$: any;
  loader: boolean = false;
  excelDemoRequestsList: any;
  constructor(private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private _fb: FormBuilder,
    private _dataService: DataService,
    private _decryptService: EncryptDecryptService,
    private _router: Router) {
    this.searchForm! = this._fb.group({
      searchKey: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.getDemoRequestsList(1, true);

    this.results$ = this.searchForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe((data: any) => this.getDemoRequestsList(1));
  }

  getDemoRequestsList(pageNum: number, isPagination?: any) {
    this.loader = true;
    let searchKey = this.searchForm.controls.searchKey.value;
    this._dataService.getApi(environment.getDemoRequestsListURL + `?size=${this.pageSize}&pageNo=${pageNum}&searchText=${searchKey}`)
      .subscribe((res: any) => {
        this.loader = false;
        if (res && res.status.toLowerCase() == 'success') {
          // this.getExcelDemoRequestsList(1)
          this.demoRequestsList = res.data;
          this.page = pageNum;
          if (pageNum == 1) {
            this.count = res.totalCount;
          }
          this.pager = this._dataService.getPager(this.count, pageNum, this.pageSize);
        } else {
          this.demoRequestsList = [];
        }
      }, (error: Error) => {
        this.demoRequestsList = [];
      })
  }

  getExcelDemoRequestsList(type: any) {
    this._dataService.getApi(environment.getDemoRequestsListURL + `?size=1000&pageNo=1&searchText=`)
      .subscribe((res: any) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.excelDemoRequestsList = res.data;
          this.excelDemoRequestsList.forEach((element: any, i: number) => {
            delete element._id;
            delete element.__v;
            // if(i==this.excelDemoRequestsList.length){
            //   this.excelDemoRequestsList =  Object.keys(element);
            // }
          });
          if (type == 'copy') {
            this.copyTable()
          }
          else if (type == 'pdf') {
            this.printPDF()
          }
          else if (type == 'excel') {
            this.exportAsXLSX()
          }
          else if (type == 'csv') {
            this.getCSV()
          }
        } else {
          this.excelDemoRequestsList = [];
        }
      }, (error: Error) => {
        this.excelDemoRequestsList = [];
      })
  }

  copyTable() {
    var text = "S.No." +
      "\tAddress" +
      "\t\tLocation" +
      "\t\tName of requestee" +
      "\t\tPhone" +
      "\t\tEmail" +
      "\t\tDate of request" +
      "\t\tMessage"
    var textData = "";
    for (let i = 0; i < this.demoRequestsList.length; i++) {
      textData += i + 1 +
        "\t\t" + (this.demoRequestsList[i].address ? this.demoRequestsList[i].address : 'N/A')
        + "\t\t" + (this.demoRequestsList[i].location ? this.demoRequestsList[i].location : 'N/A')
        + "\t" + (this.demoRequestsList[i].name ? this.demoRequestsList[i].name : 'N/A')
        + "\t" + (this.demoRequestsList[i].phone ? this.demoRequestsList[i].phone : 'N/A')
        + "\t" + (this.demoRequestsList[i].email ? this.demoRequestsList[i].email : 'N/A')
        + "\t" + (this.demoRequestsList[i].created_date ? this.getDate(this.demoRequestsList[i].created_date) : 'N/A')
        + "\t" + (this.demoRequestsList[i].message ? this.demoRequestsList[i].message : 'N/A') + "\n";
    }
    let selBox = document.createElement('textarea');

    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = text + '\n' + textData;

    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    if (selBox) {
      // window.print()
    }
    // this.MessageService.add({ severity: 'success', summary: '', detail: 'Link copied to Clipboard' });
    else {
      // this.MessageService.add({ severity: 'warning', summary: '', detail: 'Unable to copy the link to Clipboard' });
    }
  }

  async printPDF() {
    var doc = new jsPDF('p', 'pt');
    var col = [["S.No.", "Address", "Location", "Name of Requestor", "Mobile No", "Email", "Date of Request", "Message"]];
    var rows: any = [];
    await this.demoRequestsList.forEach((element: any, i: number) => {
      var temp = [i + 1,
      (element.address ? element.address : 'N/A'),
      (element.location ? element.location : 'N/A'),
      (element.name ? element.name : 'N/A'),
      (element.phone ? element.phone : 'N/A'),
      (element.email ? element.email : 'N/A'),
      (element.created_date ? this.getDate(element.created_date) : 'N/A'),
      (element.message ? element.message : 'N/A')];
      rows.push(temp);
    });
    await autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (data) => { },
    });
    doc.setProperties({ title: "Demo Request" });
    // window.open(doc.output('bloburl'), '_blank')
    window.open(URL.createObjectURL(doc.output('blob')), '_blank')
  }
  getCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Login Activity',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };
    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(this.excelDemoRequestsList);
  }

  exportAsXLSX(): void {
    this._dataService.exportAsExcelFile(this.excelDemoRequestsList, 'sample');
  }
  getDate(date: any) {
    return moment(date).format("DD/MM/YYYY HH:mm:ss")
  }

}
