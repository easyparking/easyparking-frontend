import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoRequestsComponent } from './demo-requests.component';

describe('DemoRequestsComponent', () => {
  let component: DemoRequestsComponent;
  let fixture: ComponentFixture<DemoRequestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoRequestsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
