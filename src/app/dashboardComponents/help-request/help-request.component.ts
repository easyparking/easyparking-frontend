import { DataService } from './../../shared/services/data.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import * as moment from 'moment';
import { ExportToCsv } from 'export-to-csv';

@Component({
  selector: 'app-help-request',
  templateUrl: './help-request.component.html',
  styleUrls: ['./help-request.component.css']
})
export class HelpRequestComponent implements OnInit {

  pageNum = 1;
  pageSize = 10;
  requestsList: any = []
  loader: boolean = false
  pager: any
  searchText: any = '';
  excelRequestsList: any;
  count: any = 0;
  constructor(private _dataService: DataService) {
  }

  ngOnInit(): void {
    this.getRequestsList(1);
  }
  getRequestsList(pageNum: any) {
    this.pageNum = pageNum;
    this.loader = true;
    this._dataService.getApi(environment.getRequestsURL + '?pageNo=' + pageNum + '&size=' + this.pageSize + '&searchText=' + this.searchText)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.requestsList = res.data;
          this.count = res.totalCount;
          // this.getExcelApplicationsList(1)
          this.pager = this._dataService.getPager(res.totalCount, pageNum, this.pageSize)
          this.loader = false
        } else {
          this.requestsList = []
          this.loader = false
        }
      })
  }


  getExcelApplicationsList(type: any) {
    // this.loader = true
    this._dataService.getApi(environment.getRequestsURL + '?pageNo=' + 1 + '&size=' + 1000 + '&searchText=' + this.searchText)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.excelRequestsList = res.data;
          this.excelRequestsList.forEach((element: any, i: number) => {
            delete element._id;
            delete element.__v;
          });
          if (type == 'copy') {
            this.copyTable()
          }
          else if (type == 'pdf') {
            this.printPDF()
          }
          else if (type == 'excel') {
            this.exportAsXLSX()
          }
          else if (type == 'csv') {
            this.getCSV()
          }
        } else {
          this.excelRequestsList = [];
        }
      })
  }

  setPage(event: any) {
    this.pageNum = event
    this.getRequestsList(1);
  }
  copyTable() {
    var text = "S.No." +
      "\tName" +
      "\t\tEmail" +
      "\t\tDate of Request" +
      "\t\tSubject" +
      "\t\tMessage"

    var textData = "";
    for (let i = 0; i < this.requestsList.length; i++) {
      textData += i + 1 +
        + "\t\t" + (this.requestsList[i].name ? this.requestsList[i].name : 'N/A')
        + "\t" + (this.requestsList[i].email ? this.requestsList[i].email : 'N/A')
        + "\t" + (this.requestsList[i].createdDate ? this.getTime(this.requestsList[i].createdDate) : 'N/A')
        + "\t" + (this.requestsList[i].subject ? this.requestsList[i].subject : 'N/A')
        + "\t" + (this.requestsList[i].message ? this.requestsList[i].message : 'N/A') + "\n"
    }
    let selBox = document.createElement('textarea');

    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = text + '\n' + textData;

    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  async printPDF() {
    var doc = new jsPDF('p', 'pt');
    var col = [["S.No.",
      "Name",
      "Email",
      "Date of Request",
      "Subject",
      "Message"]];
    var rows: any = [];
    await this.requestsList.forEach((element: any, i: number) => {
      var temp = [i + 1,
      (element.name ? element.name : 'N/A'),
      (element.email ? element.email : 'N/A'),
      (element.createdDate ? this.getTime(element.createdDate) : 'N/A'),
      (element.subject ? element.subject : 'N/A'),
      (element.message ? element.message : 'N/A')];
      rows.push(temp);
    });
    await autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (data) => { },
    });
    // doc.output('dataurlnewwindow')
      doc.setProperties({ title: "Help Requests" });
      // window.open(doc.output('bloburl'), '_blank')
      window.open(URL.createObjectURL(doc.output('blob')), '_blank')
  }
  getCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Login Activity',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };
    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(this.excelRequestsList);
  }

  exportAsXLSX(): void {
    this._dataService.exportAsExcelFile(this.excelRequestsList, 'sample');
  }
  getTime(date: any) {
    if (date) {
      return moment(date).format("DD-MM-YYYY HH:mm:ss");
    } else {
      return 'N/A'
    }
  }

}
