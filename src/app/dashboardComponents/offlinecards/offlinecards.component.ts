
import { environment } from './../../../environments/environment.prod';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatAccordion } from '@angular/material/expansion';
// import { MatAccordion } from '@angular/material';
import { Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { NgbdSortableHeader } from 'src/app/directives/tableDirective';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

interface Country {
  id: number;
  name: string;
  flag: string;
  area: number;
  population: number;
}

const COUNTRIES: Country[] = [
  {
    id: 1,
    name: 'Russia',
    flag: 'f/f3/Flag_of_Russia.svg',
    area: 17075200,
    population: 146989754
  },
  {
    id: 2,
    name: 'Canada',
    flag: 'c/cf/Flag_of_Canada.svg',
    area: 9976140,
    population: 36624199
  },
  {
    id: 3,
    name: 'United States',
    flag: 'a/a4/Flag_of_the_United_States.svg',
    area: 9629091,
    population: 324459463
  },
  {
    id: 4,
    name: 'China',
    flag: 'f/fa/Flag_of_the_People%27s_Republic_of_China.svg',
    area: 9596960,
    population: 1409517397
  }
];


export type SortColumn = keyof Country | '';
export type SortDirection = 'asc' | 'desc' | '';
const rotate: { [key: string]: SortDirection } = { 'asc': 'desc', 'desc': '', '': 'asc' };

const compare = (v1: string | number, v2: string | number) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
  column: SortColumn;
  direction: SortDirection;
}

@Component({
  selector: 'app-offlinecards',
  templateUrl: './offlinecards.component.html',
  styleUrls: ['./offlinecards.component.css']
})
export class OfflinecardsComponent implements OnInit {
  @ViewChild(MatAccordion)
  public accordion!: MatAccordion;
  page = 1;
  pageSize: any = 10;
  collectionSize = COUNTRIES.length;
  addEditOwnerForm!: FormGroup;
  pageNum: any;
  itemsPerPage: any = 10
  searchKey: any = ''
  pager: any;
  ownersData: any = []
  spinner: boolean = false
  count: any = 0
  ownerStatus: any = 0;
  showEdit: boolean = false;
  currentOwnerData: any = {}
  constructor(private _fb: FormBuilder,
    private _router: Router) {
    this.refreshCountries();
    this.addEditOwnerForm = this._fb.group({
      name: ['', [Validators.required]],
      userName: ['', [Validators.required]],
      password: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]],
      onlinePercentage: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(1), Validators.maxLength(2)]]
    })
  }

  ngOnInit(): void {
  }

  countries = COUNTRIES;

  @ViewChildren(NgbdSortableHeader)
  headers!: QueryList<NgbdSortableHeader>;

  onSort({ column, direction }: SortEvent) {

    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    // sorting countries
    if (direction === '' || column === '') {
      this.countries = COUNTRIES;
    } else {
      this.countries = [...COUNTRIES].sort((a, b) => {
        const res = compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  }

  refreshCountries() {
    this.countries = COUNTRIES
      .map((country, i) => ({ ...country }))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  showEmployeeDetails(data: any) {
    this._router.navigate(["/view-employees", data.ownerId])
  }
}
