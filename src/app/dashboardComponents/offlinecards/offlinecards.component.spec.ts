import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfflinecardsComponent } from './offlinecards.component';

describe('OfflinecardsComponent', () => {
  let component: OfflinecardsComponent;
  let fixture: ComponentFixture<OfflinecardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfflinecardsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfflinecardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
