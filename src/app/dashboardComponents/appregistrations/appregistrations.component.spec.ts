import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppregistrationsComponent } from './appregistrations.component';

describe('AppregistrationsComponent', () => {
  let component: AppregistrationsComponent;
  let fixture: ComponentFixture<AppregistrationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppregistrationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppregistrationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
