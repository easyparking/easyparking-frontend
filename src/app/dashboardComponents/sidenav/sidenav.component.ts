import { DataService } from '../../shared/services/data.service';
import { Component, OnInit } from '@angular/core';
import { OnlineOfflineService } from 'src/app/shared/services/online-offline.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  public isCollapsed = false;
  isCollapsableValue: boolean = false
  sideNavData: any = []
  isOnlineStatus: any = true;
  constructor(private _dataService: DataService,
    private _offlineService: OnlineOfflineService,) { }

  ngOnInit(): void {
    this._offlineService.networkConnectivity$.subscribe((res:any) => {
      // console.log("res",res)
      this.isOnlineStatus = res;      
    })
    this.sideNavData = []
    if ((localStorage.getItem("accessByRole")) == 'admin') {
      this.sideNavData.push(
        {
          title: 'Dashboard',
          icon: 'fa fa-tachometer',
          route: '/admin-dashboard',
          hasSubmenu: []
        },
        {
          title: 'Analytics',
          icon: 'fa fa-line-chart',
          route: '/analytics',
          hasSubmenu: []
        },
        {
          title: 'Accounts',
          icon: 'fa fa-user-o',
          route: '/accounts',
          hasSubmenu: []
        },
        {
          title: 'Stands',
          icon: 'fa fa-map-signs',
          route: '/view-stands',
          hasSubmenu: []
        },
        {

          title: 'Vehicles Search',
          icon: 'fa fa-search',
          route: '/vehicleSearch',
          hasSubmenu: []
        },
        {
          title: 'App Registrations',
          icon: 'fa fa-mobile',
          route: '/appregistrations',
          hasSubmenu: []
        }, {
        title: 'Wanted vehicles',
        icon: 'fa fa-mobile',
        route: '/wanted-vehicles',
        hasSubmenu: []

      },
        // {

        //   title: 'Customer Report',
        //   icon: 'fa fa-users',
        //   route: '/reports',
        //   hasSubmenu: []
        // },
        {

          title: 'Monthly Cards',
          icon: 'fa fa-desktop',
          route: '/cardsales',
          hasSubmenu: []
        },
        {

          title: 'Payment Reports',
          icon: 'fa fa-table',
          route: '/online-payment-report',
          hasSubmenu: []
        },
        {

          title: 'Job Applications',
          icon: 'fa fa-file-text-o',
          route: '/applications',
          hasSubmenu: []
        },
        {
          title: 'Help Request',
          icon: 'fa fa-question',
          route: '/help-request',
          hasSubmenu: []
        },

        {

          title: 'Requests for Demo',
          icon: 'fa fa-play',
          route: '/demo-requests',
          hasSubmenu: []
        },
        {

          title: 'Force logout',
          icon: 'fa fa-power-off',
          route: '/force-logout',
          hasSubmenu: []
        },
        {

          title: 'Settings',
          icon: 'fa fa-cogs',
          route: '/settings',
          hasSubmenu: [
            {
              title: 'Locations',
              icon: 'fa fa-map-marker',
              route: '/locations',
            },
            {
              title: 'Vehicle Names',
              icon: 'fa fa-list',
              route: '/vehicle-names',
            },
            {
              title: 'Plan Type',
              icon: 'fa fa-list',
              route: '/plan-type',
            },
            {
              title: 'Parking Type',
              icon: 'fa fa-list',
              route: '/parking-type',
            }
          ]
        },
        {
          title: 'Login Activity',
          icon: 'fa fa-sign-in',
          route: '/login-activity',
          hasSubmenu: []
        }
        // {

        //   title: 'Parkings Entry',
        //   icon: 'fa fa-search',
        //   route: '/parkings-entry',
        //   hasSubmenu: []
        // },
        // {

        //   title: 'Customers',
        //   icon: 'fa fa-users',
        //   route: '/customers',
        //   hasSubmenu: []
        // },

        // {

        //   title: ' Wanted Vehicles',
        //   icon: 'fa fa-car',
        //   route: '/wanted-vehicles',
        //   hasSubmenu: []
        // },


        // {

        //   title: 'Cards Users',
        //   icon: 'fa fa-desktop',
        //   route: '/onlinecardsusers',
        //   hasSubmenu: []
        // },


        // {

        //   title: 'Vehicle Names',
        //   icon: 'fa fa-users',
        //   route: '/vehicle-names',
        //   hasSubmenu: []
        // },


        // {

        //   title: 'Duplicate Vehicle Numbers',
        //   icon: 'fa fa-table',
        //   route: '/duplicatevehiclenumbers',
        //   hasSubmenu: []
        // }
      )
    }
    else if ((localStorage.getItem("accessByRole")) == 'owner') {
      this.sideNavData.push(
        {
          title: 'Dashboard',
          icon: 'fa fa-tachometer',
          route: '/owner-dashboard',
          hasSubmenu: []
        },
        // {
        //   title: 'Employs',
        //   icon: 'fa fa-users',
        //   route: '/employs'
        // },
        {
          title: 'Emp Session History',
          icon: 'fa fa-clock-o',
          route: '/session-history',
          hasSubmenu: []
        },
        {

          title: 'Force Logout',
          icon: 'fa fa-power-off',
          route: '/force-logout',
          hasSubmenu: []
        },
        {

          title: 'Monthly Cards',
          icon: 'fa fa-credit-card',
          route: '/ownercardsales',
          hasSubmenu: []
        },

        {

          title: 'Reports',
          icon: 'fa fa-file-text-o',
          route: '/reports',
          hasSubmenu: []
        },

        {
          title: 'Slip lost vehicles',
          icon: 'fa fa-car',
          route: '/slip-lost-vehicles',
          hasSubmenu: []
        },
        {
          title: 'Login Activity',
          icon: 'fa fa-sign-in',
          route: '/login-activity',
          hasSubmenu: []
        }


      )
    }
    else if ((localStorage.getItem("accessByRole")) == 'employee') {
      this.sideNavData.push(

        {
          title: 'Dashboard',
          icon: 'fa fa-tachometer',
          route: '/employee-dashboard',
          hasSubmenu: []
        },
        {
          title: 'Parkings',
          icon: 'fa fa-map-signs',
          route: '/parkings',
          hasSubmenu: []
        },
        {

          title: 'Monthly Cards',
          icon: 'fa fa-credit-card',
          route: '/monthlycards',
          hasSubmenu: []
        },
        {

          title: 'Reports',
          icon: 'fa fa-file-text-o',
          route: '/reports',
          hasSubmenu: []
        },
        {

          title: 'Mini Reports',
          icon: 'fa fa-file-text-o',
          route: '/minireports',
          hasSubmenu: []
        },
        {
          title: 'Slip lost vehicles',
          icon: 'fa fa-car',
          route: '/slip-lost-vehicles',
          hasSubmenu: []
        },
        {
          title: 'Session History',
          icon: 'fa fa-clock-o',
          route: '/session-history',
          hasSubmenu: []
        },
        {
          title: 'Login Activity',
          icon: 'fa fa-sign-in',
          route: '/login-activity',
          hasSubmenu: []
        }
      )
    }
    this._dataService.isCollapsableValue.subscribe((result) => {
      // if (result) {
      this.isCollapsableValue = result
      // }
    })
  }

}
