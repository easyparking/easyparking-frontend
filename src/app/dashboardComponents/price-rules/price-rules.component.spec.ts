import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceRulesComponent } from './price-rules.component';

describe('PriceRulesComponent', () => {
  let component: PriceRulesComponent;
  let fixture: ComponentFixture<PriceRulesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PriceRulesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceRulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
