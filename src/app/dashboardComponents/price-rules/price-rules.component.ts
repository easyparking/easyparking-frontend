import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { DataService } from 'src/app/shared/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';
import swal from 'sweetalert2';
import { Component, OnInit } from '@angular/core';




@Component({
  selector: 'app-price-rules',
  templateUrl: './price-rules.component.html',
  styleUrls: ['./price-rules.component.css']
})
export class PriceRulesComponent implements OnInit {
  spinner: boolean = false
  standId: any
  dynamicPriceRulesForm!: FormGroup
  loader: boolean = false
  vehicleTypesLength: any;
  hideInput: boolean = false
  currentIndex: any;
  constructor(private route: ActivatedRoute,
    private _router: Router,
    private _dataService: DataService,
    private _fb: FormBuilder) {
    this.standId = this.route.snapshot.paramMap.get('id');
    this.dynamicPriceRulesForm = this._fb.group({
      vehicleData: new FormArray([])
    })
  }
  // convenience getters for easy access to form fields
  get f() {
    return this.dynamicPriceRulesForm.controls;
  }

  get t() {
    return this.f['vehicleData'] as FormArray;
  }

  ngOnInit(): void {
    this.getPriceRulesDetails()
  }


  addRowIntable(data?: any) {
    if (data) {

      this.t.push(this._fb.group({
        vehicletype: data.vehicletype,
        amount: [data.amount, [Validators.pattern("^[0-9]+$")]],
        duration: [data.duration, [Validators.pattern("^[0-9]+$")]],
        fixedPriceFor1stHour: [data.fixedPriceFor1stHour, [Validators.pattern("^[0-9]+$")]],
        forEveryNextHour: [data.forEveryNextHour, [Validators.pattern("^[0-9]+$")]],
        cardActualPrice: [data.cardActualPrice, [Validators.pattern("^[0-9]+$")]],
        cardOfferPrice: [data.cardOfferPrice, [Validators.pattern("^[0-9]+$")]],
        cardMaxDiscount: [data.cardMaxDiscount, [Validators.pattern("^[0-9]+$")]],
        showInput: [false]
      }));
    }
    else {
      this.t.push(this._fb.group({
        vehicletype: [''],
        amount: [0, [Validators.pattern("^[0-9]+$")]],
        duration: [0, [Validators.pattern("^[0-9]+$")]],
        fixedPriceFor1stHour: [0, [Validators.pattern("^[0-9]+$")]],
        forEveryNextHour: [0, [Validators.pattern("^[0-9]+$")]],
        cardActualPrice: [0, [Validators.pattern("^[0-9]+$")]],
        cardOfferPrice: [0, [Validators.pattern("^[0-9]+$")]],
        cardMaxDiscount: [0, [Validators.pattern("^[0-9]+$")]],
        showInput: [true]
      }));
    }
  }

  getFormGroup(index: any) {
    this.currentIndex = index;
    return this.t.controls[index] as FormGroup
  }

  deleteRowIntable(index: any) {
    this.t.removeAt(index)
  }

  clickedBack() {
    this._router.navigateByUrl('/view-stands')
  }

  getPriceRulesDetails() {
    this.loader = true
    this.dynamicPriceRulesForm.setControl('vehicleData', new FormArray([]))
    this._dataService.getApi(environment.getPriceRulesURL + this.standId)
      .subscribe((res) => {
        if (res && res.status == 'success' && res.data) {
          this.vehicleTypesLength = res.data[0].vehicleTypes.length
          for (let i = 0; i < res.data[0].vehicleTypes.length; i++) {
            this.addRowIntable(res.data[0].vehicleTypes[i])
          }
          this.loader = false
        }
      })
  }

  cancelTable() {
    this.getPriceRulesDetails()
  }

  savePriceRules() {
    this.spinner = true
    let dataToAPI = {
      "standId": this.standId,
      "vehicleTypes": this.dynamicPriceRulesForm.value.vehicleData
    }
    this._dataService.postApi(environment.addPriceRulesURL, dataToAPI)
      .subscribe((res) => {
        if (res && (res.status == 'success' || res.status == 'Sucess')) {
          swal.fire("", res.message, "success");
          setTimeout(() => {
            swal.close()
          }, 3000);
          this.spinner = false;
          this.getPriceRulesDetails()
        }
        else {
          this.spinner = false
          swal.fire("Warning!", res.message, "warning");
          setTimeout(() => {
            swal.close()
          }, 3000);
        }

      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }

  validationZeroValue(formControlValue: any) {
    if (formControlValue.value.length == 0) {
      formControlValue.setValue(0)
      // formControlValue.clearValidators()
      // formControlValue.setErrors(null)
    }
  }

  validationRules(formData: any) {
    formData.cardMaxDiscount.setValidators(Validators.max(formData.cardActualPrice.value))
    formData.cardMaxDiscount.updateValueAndValidity()
    let minOfferPrice = formData.cardActualPrice.value - formData.cardMaxDiscount.value;
    formData.cardOfferPrice.setValidators([Validators.min(minOfferPrice), Validators.max(formData.cardActualPrice.value)])
    formData.cardOfferPrice.updateValueAndValidity()
    // let minOfferPrice=
  }

}
