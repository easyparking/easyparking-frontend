import { MatAccordion } from '@angular/material/expansion';
// import { MatAccordion } from '@angular/material';
import { Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { NgbdSortableHeader } from 'src/app/directives/tableDirective';
import { environment } from 'src/environments/environment.prod';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';
import { DataService } from 'src/app/shared/services/data.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-employee-dashboard',
  templateUrl: './employee-dashboard.component.html',
  styleUrls: ['./employee-dashboard.component.css']
})
export class EmployeeDashboardComponent implements OnInit {
  loader: boolean = false
  userData: any
  selectedStand: any
  standsData: any;
  tableData: any = []
  pendingCount: any = 0;
  entryCount: any = 0;
  exitedCount: any = 0;
  canclledCount: any = 0;
  offlineCardsCount: any = 0;
  totalCollectionCount: any = 0
  bicycleData: any;
  twoWheelerData: any;
  threeWheelerData: any;
  fourWheelerData: any;
  standDetails: any;
  cardsData: any;
  pendingOnLogout: any = 0 ;
  pendingVehiclesOnLogin: any;
  constructor(private _decryptService: EncryptDecryptService,
    private _dataService: DataService,
    private router: Router) {
  }

  ngOnInit(): void {
    this.loader = true
    this.userData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    if(!this.userData.data){
    this.router.navigateByUrl('/login');
  }

    if (localStorage.getItem("standSelected")) {
      let standsArray = JSON.parse(localStorage.getItem("standsArray") || "");
      let selectedStand = standsArray.filter((item: any) => item.standId == localStorage.getItem("standSelected"))
      this.selectedStand = selectedStand[0].standName;
      this.standDetails = selectedStand[0]
    }
    this.pendingVehiclesOnLogin = localStorage.getItem("pendingVehiclesOnLogin") ? JSON.parse(localStorage.getItem("pendingVehiclesOnLogin") || '') : {};
    this.getStandsArray()
  }

  getStandsArray() {
    this._dataService.getApi(environment.getStandsByOwnerURL + this.userData.data.ownerId)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == "success") {
          // this.standsData = res.data
          // this.selectedStand = res.data[0]
          this.getDashboardDetails()
        }
        else {
          this.standsData = []
        }
      }, (error: Error) => {
        this.standsData = []
      })
  }

  // getDashboardDetails() {
  //   this.loader = true
  //   this._dataService.getApi(environment.getEmpDashboardURL + '?empId=' + this.userData.data.employeeId + '&standId=' + this.standDetails.standId
  //     + '&ownerId=' + this.userData.data.ownerId)
  //     .subscribe((res: any) => {
  //       if (res && res.status.toLowerCase() == 'success') {
  //         this.loader = false
  //         this.cardsData = res.data
  //       }
  //       else {
  //         this.cardsData = []
  //         this.loader = false
  //       }
  //     })
  // }

  getDashboardDetails() {
    this.loader = true
    this.pendingCount = 0;
    this.entryCount = 0;
    this.exitedCount = 0;
    this.canclledCount = 0;
    this.offlineCardsCount = 0;
    this.totalCollectionCount = 0
    this._dataService.getApi(environment.getEmpDashboardURL + '?empId=' + this.userData.data.employeeId + '&standId=' + this.standDetails.standId
      + '&ownerId=' + this.userData.data.ownerId)
      .subscribe((res: any) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.loader = false;
          for (let i = 0; i < res.data.length; i++) {
            this.entryCount += res.data[i].total;
            this.offlineCardsCount += res.data[i].offlineCards;
            this.totalCollectionCount += (res.data[i].collection+res.data[i].cardsAmount);

            // res.data[i].pending = res.data[i].total - (res.data[i].exit + res.data[i].cancelled);
            // res.data[i].pending = res.data[i].total - res.data[i].exit
            console.log(this.pendingVehiclesOnLogin)
            this.pendingCount += this.pendingVehiclesOnLogin ? this.pendingVehiclesOnLogin[i].total : 0;
            this.exitedCount += res.data[i].exit;
            this.canclledCount += res.data[i].cancelled;
            this.pendingOnLogout += res.data[i].pendingOnLogout;

            this.cardsData = res.data

          }
        }
        else {
          this.cardsData = []
          this.loader = false
        }
      })
  }

  standOptionChanged($event: any) {
    this.getDashboardDetails()
  }

  getCss(i: any) {

    return ((i % 8 == 0) ? "" :
      ((i % 8 == 1) ? "rose" :
        ((i % 8 == 2) ? "success" :
          ((i % 8 == 3) ? "info" :
            ((i % 8 == 4) ? 'brown' :
              ((i % 8 == 5) ? 'blue' :
                ((i % 8 == 6) ? 'orange' : 'purple')))))))

  }

}
