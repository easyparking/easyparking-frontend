import { MatAccordion } from '@angular/material/expansion';
// import { MatAccordion } from '@angular/material';
import { Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren, ViewEncapsulation } from '@angular/core';
import { NgbdSortableHeader } from 'src/app/directives/tableDirective';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from 'src/app/shared/services/data.service';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';
import { environment } from 'src/environments/environment.prod';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import { ExportToCsv } from 'export-to-csv';
import { Router } from '@angular/router';

@Component({
  selector: 'app-emp-session-history',
  templateUrl: './emp-session-history.component.html',
  styleUrls: ['./emp-session-history.component.css']
})

export class EmpSessionHistoryComponent implements OnInit {
  pageSize = 5;
  selectedStand: any;
  userData: any;
  employeeList: any;
  employee: any = "";
  searchForm!: FormGroup;
  count: number = 0;
  page = 1;
  pager: any;
  results$: any;
  loader: boolean = false;
  historyList: any = [];
  totalDetails = {
      entries: 0,
      exits:0,
      cancelled:0,
      pendingonLogout:0,
      parkingsAmount:0,
      cardAmount:0
    };
  @Input() ownerId: any;
  @Input() standId: any;
  excelHistoryList: any;
  constructor(private modalService: NgbModal,
    private _dataService: DataService,
    private _fb: FormBuilder,
    private _decryptService: EncryptDecryptService,
    private router: Router) {
    this.searchForm! = this._fb.group({
      searchKey: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    if (localStorage.getItem("standSelected")) {
      let standsArray = JSON.parse(localStorage.getItem("standsArray") || "");
      let selectedStand = standsArray.filter((item: any) => item.standId == localStorage.getItem("standSelected"))
      this.selectedStand = selectedStand ? selectedStand[0] : ""
    }
    this.userData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    if(!this.userData.data){
    this.router.navigateByUrl('/login');
  }
    this.getEmployeeList(true);

    this.results$ = this.searchForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe((data: any) => this.getHistory(1));

    // if(this.ownerId){
    //   this.getHistory(1)
    // }
  }

  getEmployeeList(isPagination?: any) {
    this._dataService.getApi(environment.getEmpList + (this.selectedStand ? this.selectedStand.ownerId : (this.userData.data.ownerId ? this.userData.data.ownerId : this.ownerId)))
      .subscribe((res) => {
        if (res && res.status == 'success') {
          if (res.data && res.data.length > 0) {
            this.employeeList = res.data;
            // if (isPagination) {
            // this.getExcelHistoryData();
            // }
          }
        }
      })
  }

  getHistory(pageNum: any) {
    this.page = pageNum;
    this.loader = true;
    let data = {
      "ownerId": this.employee.ownerId ? this.employee.ownerId : this.ownerId,
      "standId": this.employee.standId ? this.employee.standId : this.standId,
      "startDate": this.userData.data.role == "owner" ? moment().subtract(7, 'days') : moment().subtract(2, 'days'),
      "endDate": moment(),
      "size": this.pageSize,
      "pageNo": this.page,
      "searchText": this.searchForm.controls.searchKey.value,
      "role": this.userData.data.role
    }
    this._dataService.postApi(environment.getSessionHistoryForEmployeeAndOwner + (this.employee.employeeId ? this.employee.employeeId : ''), data)
      .subscribe((res) => {
        this.loader = false;
        if (res && res.status.toLowerCase() == 'success') {
          this.historyList = res.sessionResult;
          if(this.userData.data.role != 'employee'){
            this.totalDetails = res.total;
          } else {
            for(let i =0; i< res.sessionResult.length; i++){
              console.log("res.sessionResult[i].entries",res.sessionResult[i].entries)
                this.totalDetails.exits += res.sessionResult[i].exits ? res.sessionResult[i].exits : 0;
                this.totalDetails.entries += res.sessionResult[i].entries ?  res.sessionResult[i].entries : 0;
                this.totalDetails.cancelled += res.sessionResult[i].cancelled ? res.sessionResult[i].cancelled :0;
                this.totalDetails.pendingonLogout += res.sessionResult[i].pendingonLogout ?  res.sessionResult[i].pendingonLogout : 0;
                this.totalDetails.parkingsAmount += res.sessionResult[i].parkingsAmount ? res.sessionResult[i].parkingsAmount : 0;
                this.totalDetails.cardAmount += res.sessionResult[i].cardAmount? res.sessionResult[i].cardAmount : 0;    
            }
          }
          if (pageNum == 1) {
            this.count = res.totalCount;
          }
          this.pager = this._dataService.getPager(this.count, pageNum, this.pageSize);
        } else {
          this.historyList = [];
        }
      }, (error) => {
        console.log({error});
        
        this.historyList = [];
      })
  }

  getExcelHistoryData(type: any) {
    let data = {
      "ownerId": this.employee.ownerId ? this.employee.ownerId : this.ownerId,
      "standId": this.employee.standId ? this.employee.standId : this.standId,
      "startDate": this.userData.data.role == "owner" ? moment().subtract(3, 'days') : moment().subtract(1, 'days'),
      "endDate": moment(),
      "size": 1000,
      "pageNo": 1,
      "searchText": this.searchForm.controls.searchKey.value,
      "role":this.userData.data.role
    }
    this._dataService.postApi(environment.getSessionHistoryForEmployeeAndOwner + (this.employee.employeeId ? this.employee.employeeId : ''), data)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.excelHistoryList = res.sessionResult;
          this.excelHistoryList.forEach((element: any, i: number) => {
            delete element._id;
            delete element.createdDate;
            delete element.employeeId;
            delete element.ipaddress;
            delete element.ownerId;
            delete element.phone;
            delete element.role;
            delete element.standId;
            delete element.standName;
            delete element.userName;
            delete element.__v;
          });
          if (type == 'copy') {
            this.copyTable()
          }
          else if (type == 'pdf') {
            this.printPDF()
          }
          else if (type == 'excel') {
            this.exportAsXLSX()
          }
          else if (type == 'csv') {
            this.getCSV()
          }
        } else {
          this.excelHistoryList = [];
        }
      }, (error) => {
        this.excelHistoryList = [];
      })
  }

  openModal(content: any) {
    this.modalService.open(content, { size: 'lg', centered: true });
  }
  getDate(date: any) {
    return moment(date).format("DD/MM/YYYY HH:mm:ss")
  }
  copyTable() {
    var text = "S.No." +
      "\t\tEmp Name" +
      "\t\tStart Time" +
      "\t\tEnd Time" +
      "\t\tPending" +
      "\t\tEntry" +
      "\t\tExit" +
      "\t\tCancel" +
      "\t\tCollection" +
      "\t\tCard Amount" +
      "\t\tTotal Amount" + "\n"
    var textData = "";
    for (let i = 0; i < this.historyList.length; i++) {
      textData += i + 1 +
        "\t\t" + (this.historyList[i].name ? this.historyList[i].name : 'N/A')
        + "\t" + (this.historyList[i].start_time ? this.getDate(this.historyList[i].start_time) : "N/A")
        + "\t" + (this.historyList[i].end_time ? this.getDate(this.historyList[i].end_time) : "N/A")
        + "\t" + (this.historyList[i].pendingonLogout ? this.historyList[i].pendingonLogout : 'N/A')
        + "\t" + (this.historyList[i].entries ? this.historyList[i].entries : 'N/A')
        + "\t" + (this.historyList[i].exits ? this.historyList[i].exits : 'N/A')
        + "\t" + (this.historyList[i].cancelled ? this.historyList[i].cancelled : 'N/A')
        + "\t" + (this.historyList[i].parkingsAmount ? this.historyList[i].parkingsAmount : 'N/A')
        + "\t" + (this.historyList[i].cardAmount ? this.historyList[i].cardAmount : 'N/A')
        + "\t" + (this.historyList[i].parkingsAmount ? (this.historyList[i].parkingsAmount + this.historyList[i].cardAmount) : 'N/A')

    }
    let selBox = document.createElement('textarea');

    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = text + '\n' + textData;

    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    if (selBox) {
      // window.print()
    }
    // this.MessageService.add({ severity: 'success', summary: '', detail: 'Link copied to Clipboard' });
    else {
      // this.MessageService.add({ severity: 'warning', summary: '', detail: 'Unable to copy the link to Clipboard' });
    }
  }

  async printPDF() {
    var doc = new jsPDF('p', 'pt');
    var col = [["S.No.",
      "Emp Name",
      "Start Time",
      "End Time",
      "Pending",
      "Entry",
      "Exit",
      "Cancel",
      "Collection",
      "Card Amount",
      "Total Amount"]];
    var rows: any = [];
    await this.historyList.forEach((element: any, i: number) => {
      var temp = [i + 1
        , (this.historyList[i].name ? this.historyList[i].name : 'N/A')
        , (this.historyList[i].start_time ? this.getDate(this.historyList[i].start_time) : "N/A")
        , (this.historyList[i].end_time ? this.getDate(this.historyList[i].end_time) : "N/A")
        , (this.historyList[i].pendingonLogout ? this.historyList[i].pendingonLogout : 'N/A')
        , (this.historyList[i].entries ? this.historyList[i].entries : 'N/A')
        , (this.historyList[i].exits ? this.historyList[i].exits : 'N/A')
        , (this.historyList[i].cancelled ? this.historyList[i].cancelled : 'N/A')
        , (this.historyList[i].parkingsAmount ? this.historyList[i].parkingsAmount : 'N/A')
        , (this.historyList[i].cardAmount ? this.historyList[i].cardAmount : 'N/A')
        , (this.historyList[i].parkingsAmount ? (this.historyList[i].parkingsAmount + this.historyList[i].cardAmount) : 'N/A')];
      rows.push(temp);
    });
    await autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (data) => { },
    });
    // doc.output('dataurlnewwindow')
      doc.setProperties({ title: "Employee Session History" });
      // window.open(doc.output('bloburl'), '_blank')
      window.open(URL.createObjectURL(doc.output('blob')), '_blank')
  }

  getCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Login Activity',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };
    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(this.excelHistoryList);
  }
  exportAsXLSX(): void {
    this._dataService.exportAsExcelFile(this.excelHistoryList, 'sample');
  }
  getTotalAmount(cardAmount: number = 0, collection: number = 0) {
    return cardAmount + collection;
  }
}
