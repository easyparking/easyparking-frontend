import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpSessionHistoryComponent } from './emp-session-history.component';

describe('EmpSessionHistoryComponent', () => {
  let component: EmpSessionHistoryComponent;
  let fixture: ComponentFixture<EmpSessionHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmpSessionHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpSessionHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
