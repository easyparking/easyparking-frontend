import { Component, OnInit } from '@angular/core';
import { NgbdSortableHeader } from 'src/app/directives/tableDirective';
import { NgbActiveModal, NgbDate, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/shared/services/data.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import * as moment from 'moment';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import { ExportToCsv } from 'export-to-csv';

@Component({
  selector: 'app-online-payment-report',
  templateUrl: './online-payment-report.component.html',
  styleUrls: ['./online-payment-report.component.css']
})
export class OnlinePaymentReportComponent implements OnInit {
  active = 1;
  loader = true;
  pageNum: any = 1
  cardsList: any = [];
  owners: { name: string, ownerId: string }[] = [];
  standsList: { standId: string, standName: string }[] = [];
  selectedOwner = "";
  selectedStand: any;
  // totalCounts:any = [];
  standsCount: any;
  pageSize = 10;
  results$: any;
  subject = new Subject()
  searchForm!: FormGroup;
  pager: any;
  count = 0;
  excelCardsList: any;

  constructor(private modalService: NgbModal, private _dataService: DataService,
    private _fb: FormBuilder) {
    this.searchForm! = this._fb.group({
      searchKey: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.getOwners();
    this.results$ = this.searchForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe((data: any) => this.getCardsList(this.active == 1 ? 'online' : 'offline', 1));
    this.search();
  }

  search() {
    const type = this.active == 1 ? 'online' : 'offline';
    this.getCardsList(type, 1);
  }
  
  disabledRenewal(card: any) {
    return (new Date(card?.endDate) > new Date())
  }

  getCardsList(cardType: string, pageNo: any) {
    this.loader = true;
    this.pageNum = pageNo
    this._dataService.getApi(environment.getPaymentReports + `?size=${this.pageSize}&pageNo=${pageNo}&searchText=${this.searchForm.controls.searchKey.value}&type=${cardType}&ownerId=${this.selectedOwner}`)
      .subscribe((res) => {
        this.loader = false;
        if (res && res.status.toLowerCase() == 'success') {
          this.getExcelCardsList(cardType)

          this.cardsList = res.data;
          this.count = res.totalCount;
          // if (pageNum == 1) {
          //   this.count = res.total;
          // }
          this.pager = this._dataService.getPager(this.count, pageNo, this.pageSize);
        }
      }, (error) => {
        this.loader = false;
      })
  }

  getExcelCardsList(cardType: string) {
    this._dataService.getApi(environment.getPaymentReports + `?size=1000&pageNo=${1}&searchText=${this.searchForm.controls.searchKey.value}&type=${cardType}&ownerId=${this.selectedOwner}`)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.excelCardsList = res.data;
          this.excelCardsList.forEach((element: any, i: number) => {
            delete element._id;
            delete element.cardHistory;
            delete element.createdBy;
            delete element.createdDate;
            delete element.employeeId;
            delete element.endDate;
            delete element.ownerId;
            delete element.renewedDate;
            delete element.role;
            delete element.standName;
            delete element.type;
            delete element.vehicleName;
            delete element.vehicleType;
            delete element.__v;
          });
        } else {
          this.excelCardsList = []
        }
      }, (error) => {
        this.excelCardsList = []
      })
  }

  getOwners() {
    this._dataService.getApi(environment.getOwnersListURL).subscribe(res => {
      if (res.status == 'success') {
        this.owners = res.data && res.data.length ? res.data : [];
        this.selectedOwner = this.owners.length ? this.owners[0].ownerId : '';
        // this.getStandsList(this.owners.length ? this.owners[0].ownerId : '')
      } else {
        this.owners = [];
      }
    })
  }

  getStandsList(ownerId: any) {
    this._dataService.getApi(environment.getStandsByOwnerURL + ownerId)
      .subscribe((response) => {
        if (response.status == 'success') {
          localStorage.setItem("standsArray", JSON.stringify(response.data))
          this.standsList = response.data;
          this.selectedStand = this.standsList.length ? this.standsList[0] : '';
        } else {
          this.standsList = []
        }
      })
  }

  openModal(content: any) {
    this.modalService.open(content, { size: 'lg', centered: true });
  }
  getDate(date: any) {
    return moment(date).format("DD/MM/YYYY HH:mm:ss");
  }
  copyTable() {
    var text = "S.No." +
      "\tStand ID" +
      "\t\tCard No" +
      "\t\tCustomer Name" +
      "\t\tPhone" +
      "\t\tVeh.No." +
      "\t\tDuration" +
      "\t\tPayment Date" +
      "\t\tPaied Amount" +
      "\t\tStatus"

    var textData = "";
    for (let i = 0; i < this.cardsList.length; i++) {
      textData += i + 1 + "\t\t" +
        + "\t\t" + (this.cardsList[i].standId ? this.cardsList[i].standId : 'N/A')
        + "\t" + (this.cardsList[i].cardId ? this.cardsList[i].cardId : 'N/A')
        + "\t" + (this.cardsList[i].customerName ? this.cardsList[i].customerName : 'N/A')
        + "\t" + (this.cardsList[i].customerPhone ? this.cardsList[i].customerPhone : 'N/A')
        + "\t" + (this.cardsList[i].vehicleNumber ? this.cardsList[i].vehicleNumber : 'N/A')
        + "\t" + (this.cardsList[i].duration ? this.cardsList[i].duration : 'N/A')
        + "\t" + (this.cardsList[i].renewedDate ? this.getDate(this.cardsList[i].renewedDate) : (this.cardsList[i].createdDate ? this.getDate(this.cardsList[i].createdDate) : "N/A"))
        + "\t" + (this.cardsList[i].amount ? this.cardsList[i].amount : 'N/A')
        + "\t" + (this.cardsList[i].status || "N/A") + "\n"
    }
    let selBox = document.createElement('textarea');

    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = text + '\n' + textData;

    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    if (selBox) {
      // window.print()
    }
    // this.MessageService.add({ severity: 'success', summary: '', detail: 'Link copied to Clipboard' });
    else {
      // this.MessageService.add({ severity: 'warning', summary: '', detail: 'Unable to copy the link to Clipboard' });
    }
  }

  async printPDF() {
    var doc = new jsPDF('p', 'pt');
    var col = [["S.No.",
      "Stand ID",
      "Card No",
      "Customer Name",
      "Phone",
      "Veh.No.",
      "Duration",
      "Payment Date",
      "Payable Amount",
      "Status"]];
    // ["S.No.",
    // "Stand ID",
    // "Card No",
    // "Customer Name",
    // "Phone",
    // "Veh.No.",
    // "Duration",
    // "Payment Date",
    // "Transaction ID",
    // "Payable Amount",
    // "Settled to Owner on",
    // "Transaction ID",
    // "Action"]
    var rows: any = [];
    await this.cardsList.forEach((element: any, i: number) => {
      var temp = [i + 1,
        , (this.cardsList[i].standId ? this.cardsList[i].standId : 'N/A')
        , (this.cardsList[i].cardId ? this.cardsList[i].cardId : 'N/A')
        , (this.cardsList[i].customerName ? this.cardsList[i].customerName : 'N/A')
        , (this.cardsList[i].customerPhone ? this.cardsList[i].customerPhone : 'N/A')
        , (this.cardsList[i].vehicleNumber ? this.cardsList[i].vehicleNumber : 'N/A')
        , (this.cardsList[i].duration ? this.cardsList[i].duration : 'N/A')
        , (this.cardsList[i].renewedDate ? this.getDate(this.cardsList[i].renewedDate) : (this.cardsList[i].createdDate ? this.getDate(this.cardsList[i].createdDate) : "N/A"))
        , (this.cardsList[i].amount ? this.cardsList[i].amount : 'N/A')
        , (this.cardsList[i].status || "N/A")];
      rows.push(temp);
    });
    await autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (data) => { },
    });
    // doc.output('dataurlnewwindow')
    
    doc.setProperties({ title: "Online Paymants Request" });
    // window.open(doc.output('bloburl'), '_blank')
    window.open(URL.createObjectURL(doc.output('blob')), '_blank')
  }

  getCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Parked Vehicles',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };
    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(this.excelCardsList);
  }
  exportAsXLSX(): void {
    this._dataService.exportAsExcelFile(this.excelCardsList, 'sample');
  }

}
