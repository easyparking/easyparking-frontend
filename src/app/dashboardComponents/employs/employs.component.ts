import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';
import { DataService } from 'src/app/shared/services/data.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment.prod';
import * as moment from 'moment';
import swal from 'sweetalert2';


@Component({
  selector: 'app-employs',
  templateUrl: './employs.component.html',
  styleUrls: ['./employs.component.css']
})
export class EmploysComponent implements OnInit {

  addEditEmployeeForm!: FormGroup
  isEdit: boolean = false
  employessListData: any = []
  pager: any;
  pageNum: any = 1;
  spinner: boolean = false
  standData: any
  empData: any
  loader: boolean = false
  constructor(private modalService: NgbModal,
    private route: ActivatedRoute,
    private _fb: FormBuilder,
    private _dataService: DataService,
    private _decryptService: EncryptDecryptService,
    private activeModal: NgbActiveModal) {
    this.standData = this._decryptService.decryptData(localStorage.getItem('StandData'))
    this.addEditEmployeeForm = this._fb.group({
      userName: ['', [Validators.required]],
      name: ['', [Validators.required]],
      pwd: ['', [Validators.required, Validators.minLength(6)]],
      phone: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]]
    })
  }

  ngOnInit(): void {
    this.getEmploeesList(1)
  }



  getEmploeesList(pageNum: any) {
    this.loader = true
    this._dataService.getApi(environment.getEmployeeListByStands + this.standData.ownerId)
      .subscribe((response) => {
        if (response.status.toLowerCase() == 'success' || response.status.toLowerCase() == 'sucess') {
          this.loader = false
          response.data.forEach((element: any) => {

            if (element.status == 'Active') {
              element.status = true
            }
            else {
              element.status = false
            }
          });
          this.employessListData = response.data
          // this.pager = this._dataService.getPager(response.totalCount, pageNum, 10)
        }
        else {
          this.employessListData = []
          this.loader = false
        }
      }, (error) => {
        // this.employessListData = []
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })

  }


  setPage(event: any) {
    this.pageNum = event
    this.getEmploeesList(this.pageNum)
  }

  getStartTime(date: any) {
    if (date) {
      return moment(date).format("DD-MM-YYYY HH:mm:ss");
    }
    else {
      return 'N/A'
    }
  }

  openModal(content: any, type: any, data?: any) {
    this.spinner = false
    this.addEditEmployeeForm.reset()
    if (type == 'add') {
      this.isEdit = false
    }
    else {
      setTimeout(() => {
        this.addEditEmployeeForm.controls.pwd.setErrors(null)
        this.addEditEmployeeForm.controls.pwd.clearValidators()
      }, 1);
      this.isEdit = true
    }
    if (data) {
      this.empData = data
      this.addEditEmployeeForm.setValue({
        userName: data.userName ? data.userName : '',
        name: data.name ? data.name : '',
        pwd: '',
        phone: data.phoneNumber ? data.phoneNumber : ''
      })
    }
    this.activeModal = this.modalService.open(content, { size: 'lg', centered: true });
  }

  checkUsername() {
    if (this.addEditEmployeeForm.controls.userName.value) {
      this._dataService.getApi(environment.verifyUserNameURL + this.addEditEmployeeForm.controls.userName.value)
        .subscribe((res) => {
          if (res && res.status.toLowerCase() == 'fail') {
            this.addEditEmployeeForm.controls.userName.setErrors({ invalid: true })
            this.addEditEmployeeForm.controls.userName.setErrors({ InvalidUserName: true })
          }
          else {
            this.addEditEmployeeForm.controls.userName.setErrors(null)
            this.addEditEmployeeForm.controls.userName.clearValidators()
          }
        })
    }
    else {
      this.addEditEmployeeForm.controls.userName.setValidators(Validators.required)
      this.addEditEmployeeForm.controls.userName.updateValueAndValidity()
    }

  }

  changeStatus(data: any) {
    this._dataService.putApi(environment.updateEmployeeStatusURL + data._id, {
      "status": data.status ? "Active" : "Inactive"
    }).subscribe((response) => {
      if (response && response.status == 'success') {
        this.getEmploeesList(1)
      }
      else {
        swal.fire("Warning!", response.message, "warning");
        setTimeout(() => {
          swal.close()
        }, 3000);
        this.getEmploeesList(1)
      }

    }, (error) => {
      swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
      setTimeout(() => {
        swal.close()
      }, 3000);
    })
  }

  deleteEmployee(data: any) {
    swal.fire({
      title: 'Are you sure to delete this record?',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this._dataService.deleteApi(environment.deleteEmployeeURL + data._id + '?standId=' + data.standId + "&employeeCount=" + (Number(this.standData.employeeCount) - 1))
          .subscribe((response) => {
            if (response && response.status == 'success') {
              this.getEmploeesList(1);
              this.standData.employeeCount -= 1
              localStorage.setItem('StandData', this.standData)
              swal.fire("", response.message, "success");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }
            else {
              swal.fire("Warning!", response.message, "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
              this.getEmploeesList(1)
            }

          }, (error) => {
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          })
      } else if (result.isDenied) {
      }
    })
  }
  addEditEmployeeData(type: any) {
    this.spinner = true
    if (type == 'add') {
      let dataToAPI = {
        "name": this.addEditEmployeeForm.value.name,
        "userName": this.addEditEmployeeForm.value.userName,
        "ownerId": this.standData.ownerId,
        "standId": this.standData.standId,
        "standName": this.standData.standName,
        "role": "employee",
        "phoneNumber": this.addEditEmployeeForm.value.phone,
        "password": this.addEditEmployeeForm.value.pwd,
        "employeeCount": this.standData.employeeCount + 1
      }
      this._dataService.postApi(environment.createUsersURL, dataToAPI)
        .subscribe((res) => {
          if (res && res.status == 'success') {
            swal.fire("", res.message, "success");
            setTimeout(() => {
              swal.close()
            }, 3000);
            this.spinner = false;
            this.standData.employeeCount += 1
            localStorage.setItem('StandData', this.standData)
            this.activeModal.close();
            this.getEmploeesList(1)
            this.addEditEmployeeForm.reset()
          }
          else {
            this.spinner = false
            swal.fire("Warning!", res.message, "warning");
            setTimeout(() => {
              swal.close()
            }, 3000);
          }

        }, (error) => {
          this.spinner = false
          swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
          setTimeout(() => {
            swal.close()
          }, 3000);
        })
    }
    else {
      let dataToAPI = {
        "name": this.addEditEmployeeForm.value.name,
        "userName": this.addEditEmployeeForm.value.userName,
        "id": this.empData._id,
        "phoneNumber": this.addEditEmployeeForm.value.phone,
        "password": this.addEditEmployeeForm.value.pwd
      }
      this._dataService.putApi(environment.updateEmployeeDataURL, dataToAPI)
        .subscribe((res) => {
          if (res && res.status == 'success') {
            swal.fire("", res.message, "success");
            setTimeout(() => {
              swal.close()
            }, 3000);
            this.spinner = false;
            this.activeModal.close();
            this.getEmploeesList(1)
            this.addEditEmployeeForm.reset()
          }
          else {
            this.spinner = false
            swal.fire("Warning!", res.message, "warning");
            setTimeout(() => {
              swal.close()
            }, 3000);
          }

        }, (error) => {
          this.spinner = false
          swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
          setTimeout(() => {
            swal.close()
          }, 3000);
          setTimeout(() => {
            swal.close()
          }, 3000);
        })
    }
  }
  removeSpace(form: any, formControlName: any) {
    if (formControlName != 'pwd') {
      form.controls[formControlName].setValue(form.controls[formControlName].value.split(" ").join(""))
    }
    else {
      if (form.controls[formControlName].value.indexOf(' ') >= 0) {
        form.controls[formControlName].setErrors({ specialCharacter: true })
      }
      else {
        form.controls[formControlName].setErrors(null)
      }
    }
  }
}
