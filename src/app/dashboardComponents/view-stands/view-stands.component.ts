import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';
import { MatAccordion } from '@angular/material/expansion';
// import { MatAccordion } from '@angular/material';
import { Component, Directive, ElementRef, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { NgbdSortableHeader } from 'src/app/directives/tableDirective';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/shared/services/data.service';
import { Router } from '@angular/router';
import { environment } from './../../../environments/environment.prod';
import swal from 'sweetalert2';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import * as moment from 'moment';



@Component({
  selector: 'app-view-stands',
  templateUrl: './view-stands.component.html',
  styleUrls: ['./view-stands.component.css']
})
export class ViewStandsComponent implements OnInit {
  // @ViewChild('Image', {static: false}) Image!:any;
  @ViewChild(MatAccordion)
  @ViewChildren(NgbdSortableHeader)
  headers!: QueryList<NgbdSortableHeader>;
  public accordion!: MatAccordion;
  page = 1;
  pageSize =5;
  standsForm!: FormGroup;
  searchForm!: FormGroup;
  ownersList: any;
  spinner = false;
  count: any;
  pager: any;
  statesList: any = [];
  districtsList: any = [];
  searchKey = '';
  standsList: any;
  base64: any;
  citiesList: any = [];
  currentStandData: any;
  showEdit: boolean = false;
  dataToCopy: any;
  fileName: any;
  online: any;
  status: any;
  standsOnlineCount = 0;
  standsOfflineCount = 0;
  fileError: boolean = false;
  url: any;
  file: any;
  results$: any;
  subject = new Subject()
  loader: boolean = false;
  sessionStand: any;
  parkingTypesList: any;
  constructor(private modalService: NgbModal,
    private _formBuilder: FormBuilder,
    private _dataService: DataService,
    private _router: Router,
    private activeModal: NgbActiveModal,
    private _encryptService: EncryptDecryptService) {
    this.standsForm! = this._formBuilder.group({
      state: ['', Validators.required],
      district: ['', Validators.required],
      city: ['', Validators.required],
      standName: ['', Validators.required],
      ownerName: ['', Validators.required],
      type: ['Parking', Validators.required],
      address: ['', Validators.required],
      addressLine1: ['', Validators.required],
      addressLine2: [''],
      addressLine3: [''],
      addressLine4: [''],
      notes: [''],
      phone: ['', [Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]],
      policeStationContactNum: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      nearPoliceStation: ['', Validators.required],
      latitude: ['', [Validators.required, Validators.pattern("^-?([1-8]?[1-9]|[1-9]0)\\.{1}\\d{1,6}")]],
      longitude: ['', [Validators.required, Validators.pattern("^-?([1-8]?[1-9]|[1-9]0)\\.{1}\\d{1,6}")]],
      standImage: [''],
      GSTIn: ['', Validators.required],
      initialGracePeriod: ['', [Validators.required]],
      lateGracePeriod: ['', Validators.required],
      twoWheelerTimePeriodAmount: ['', Validators.required],
      twoWheelerTaxAmount: ['', Validators.required],
      twoWheelertotalAmount: ['', Validators.required],
      bicycleTimePeriodAmount: ['', Validators.required],
      bicycleTaxAmount: ['', Validators.required],
      bicycletotalAmount: ['', Validators.required],
      threeWheelerTimePeriodAmount: ['', Validators.required],
      threeWheelerTaxAmount: ['', Validators.required],
      threeWheelertotalAmount: ['', Validators.required],
      fourWheelerTimePeriodAmount: ['', Validators.required],
      fourWheelerTaxAmount: ['', Validators.required],
      fourWheelertotalAmount: ['', Validators.required],
      showOnHomePage: [true]
    })
    this.searchForm! = this._formBuilder.group({
      searchKey: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.getOwnersList();
    this.getStandsList(1);
    this.getStates();
    this.getCounts();
    this.getAllParkingTypes();
    this.results$ = this.searchForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe((data: any) => this.getStandsList(1));
  }

  getAllParkingTypes() {
    this._dataService.getApi(environment.getAllParkingTypesUrl)
      .subscribe((res) => {
        if (res && res.status == 'Success') {
          this.parkingTypesList = res.plans;
        }
      }, (error) => {
      })
  }

  getCounts() {
    this._dataService.getApi(environment.getStandsCountURL)
      .subscribe((res) => {
        if (res && res.status == 'success') {
          this.count = res.onlineCardsCount.Total;
          this.standsOnlineCount = res.onlineCardsCount.Active;
          this.standsOfflineCount = res.onlineCardsCount.Total - res.onlineCardsCount.Active;
        }
      }, (error) => {
      })
  }

  getOwnersList() {
    this._dataService.getApi(environment.getOwnersListURL)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() != 'fail') {
          this.ownersList = res.data;
        } else {
          this.ownersList = [];
        }
      })
  }

  getStandsList(pageNum: any) {
    this.loader = true;
    this._dataService.getApi(environment.getStandsURL + `?size=${this.pageSize}&pageNo=${pageNum}&searchText=${this.searchForm.controls.searchKey.value}`)
      .subscribe((res) => {
        this.loader = false;
        if (res && res.status.toLowerCase() == 'success') {
          this.standsList = res.data;
          this.standsList.forEach((element: any) => {
            element.booleanStatus = (element.status == 'Active' ? true : false);
          });
          this.pager = this._dataService.getPager(this.count, pageNum, this.pageSize)
        }
        else {
          this.standsList = []
        }

      }, (error) => {
        this.standsList = []
      })
  }

  setPage(page: any) {
    this.page = page;
    this.getStandsList(page)
  }

  addStand(modal: any, type: any) {
    if (type == 'add') {
      let dataToAPI = {
        "ownerId": this.standsForm.controls.ownerName.value ? this.standsForm.controls.ownerName.value.ownerId : '',
        "ownerName": this.standsForm.controls.ownerName.value ? this.standsForm.controls.ownerName.value.name : '',
        "standName": this.standsForm.controls.standName.value,
        "standType": this.standsForm.controls.type.value,
        "address": this.standsForm.controls.address.value,
        "addressLine1": this.standsForm.controls.addressLine1.value,
        "addressLine2": this.standsForm.controls.addressLine2.value,
        "addressLine3": this.standsForm.controls.addressLine3.value,
        "addressLine4": this.standsForm.controls.addressLine4.value,
        "note": this.standsForm.controls.notes.value,
        "standPhone": this.standsForm.controls.phone.value,
        "policeStation": this.standsForm.controls.nearPoliceStation.value,
        "policeStationContact": this.standsForm.controls.policeStationContactNum.value,
        "latitude": this.standsForm.controls.latitude.value,
        "longtitue": this.standsForm.controls.longitude.value,
        "isPublic": !!this.standsForm.controls.showOnHomePage.value,
        "standImage": {
          "imgName": this.fileName,
          "imgData": this.base64
        },
        "location": {
          "state": {
            "id": this.standsForm.controls.state.value ? this.standsForm.controls.state.value._id : '',
            "state": this.standsForm.controls.state.value ? this.standsForm.controls.state.value.state : ''
          },
          "district": {
            "id": this.standsForm.controls.district.value ? this.standsForm.controls.district.value._id : '',
            "district": this.standsForm.controls.district.value ? this.standsForm.controls.district.value.district : '',
          },
          "city": {
            "id": this.standsForm.controls.city.value ? this.standsForm.controls.city.value._id : '',
            "city": this.standsForm.controls.city.value ? this.standsForm.controls.city.value.city : '',
          },
        },
        "GSTIn": this.standsForm.controls.GSTIn.value,
        "initialGracePeriod": this.standsForm.controls.initialGracePeriod.value,
        "lateGracePeriod": this.standsForm.controls.lateGracePeriod.value,
        "bicycle": {
          "timePeriodAmount": this.standsForm.controls.bicycleTimePeriodAmount.value,
          "taxAmount": this.standsForm.controls.bicycleTaxAmount.value,
          "totalAmount": this.standsForm.controls.bicycletotalAmount.value
        },
        "twoWheeler": {
          "timePeriodAmount": this.standsForm.controls.twoWheelerTimePeriodAmount.value,
          "taxAmount": this.standsForm.controls.twoWheelerTaxAmount.value,
          "totalAmount": this.standsForm.controls.twoWheelertotalAmount.value
        },
        "threeWheeler": {
          "timePeriodAmount": this.standsForm.controls.threeWheelerTimePeriodAmount.value,
          "taxAmount": this.standsForm.controls.threeWheelerTaxAmount.value,
          "totalAmount": this.standsForm.controls.threeWheelertotalAmount.value
        },
        "fourWheeler": {
          "timePeriodAmount": this.standsForm.controls.fourWheelerTimePeriodAmount.value,
          "taxAmount": this.standsForm.controls.fourWheelerTaxAmount.value,
          "totalAmount": this.standsForm.controls.fourWheelertotalAmount.value
        },
        "planType": this.standsForm.controls.ownerName.value ? this.standsForm.controls.ownerName.value.planType : '',
      }
      this.spinner = true;
      this._dataService.postApi(environment.createStandURL, dataToAPI)
        .subscribe((res) => {
          if (res && res.status.toLowerCase() == 'success') {
            this.spinner = false;
            modal.dismiss('Data saved')
            swal.fire("", res.message, "success");
            setTimeout(() => {
              swal.close()
            }, 3000);
            this.getStandsList(1);
            this.getCounts()
            this.standsForm.reset();

          }
          else {
            this.spinner = false
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          }
        });
    } else {
      let dataToAPI = {
        // "ownerId": this.standsForm.controls.ownerName.value,
        // "standName" : this.standsForm.controls.standName.value,
        // "ownerName": this.standsForm.controls.ownerName.value ? this.standsForm.controls.ownerName.value.name : '',
        "standType": this.standsForm.controls.type.value,
        "address": this.standsForm.controls.address.value,
        "addressLine1": this.standsForm.controls.addressLine1.value,
        "addressLine2": this.standsForm.controls.addressLine2.value,
        "addressLine3": this.standsForm.controls.addressLine3.value,
        "addressLine4": this.standsForm.controls.addressLine4.value,
        "note": this.standsForm.controls.notes.value,
        // "standPhone" : this.standsForm.controls.phone.value,
        "policeStation": this.standsForm.controls.nearPoliceStation.value,
        "policeStationContact": this.standsForm.controls.policeStationContactNum.value,
        "latitude": this.standsForm.controls.latitude.value,
        "longtitue": this.standsForm.controls.longitude.value,
        "isPublic": !!this.standsForm.controls.showOnHomePage.value,
        "standImage": {
          "imgName": this.fileName,
          "imgData": this.base64
        },
        "location": {
          "state": {
            "id": this.standsForm.controls.state.value ? this.standsForm.controls.state.value._id : '',
            "state": this.standsForm.controls.state.value ? this.standsForm.controls.state.value.state : ''
          },
          "district": {
            "id": this.standsForm.controls.district.value ? this.standsForm.controls.district.value._id : '',
            "district": this.standsForm.controls.district.value ? this.standsForm.controls.district.value.district : '',
          },
          "city": {
            "id": this.standsForm.controls.city.value ? this.standsForm.controls.city.value._id : '',
            "city": this.standsForm.controls.city.value ? this.standsForm.controls.city.value.city : '',
          },
        },
        "GSTIn": this.standsForm.controls.GSTIn.value,
        "initialGracePeriod": this.standsForm.controls.initialGracePeriod.value,
        "lateGracePeriod": this.standsForm.controls.lateGracePeriod.value
      }
      this.spinner = true;
      this._dataService.putApi(environment.updateStandsURL + this.currentStandData.standId, dataToAPI)
        .subscribe((res) => {
          if (res && res.status.toLowerCase() == 'success') {
            this.spinner = false;
            modal.dismiss('Data saved')
            swal.fire("", res.message, "success");
            setTimeout(() => {
              swal.close()
            }, 3000);
            this.getStandsList(1);
            this.standsForm.reset()
          }
          else {
            this.spinner = false
            swal.fire("Warning!", res.message, "warning");
            setTimeout(() => {
              swal.close()
            }, 3000);
          }

        }, (error) => {
          this.spinner = false
          swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
          setTimeout(() => {
            swal.close()
          }, 3000);
        })
    }
  }


  updateStatus(event: any, stand: any, key: any) {
    let data: any
    if (key == 'allowOnlineCards') {
      data = {
        allowOnlineCards: event ? true : false
      }
    } else {
      data = {
        status: event ? "Active" : "Inactive",
        expiryDate: event ? '' : new Date().toString()
      }
    }
    this._dataService.putApi(environment.updateStandsURL + stand.standId, data)
      .subscribe((res) => {
        if (res && res.status == 'success') {
          swal.fire("", res.message, "success");
          setTimeout(() => {
            swal.close()
          }, 3000);
          this.getStandsList(1)
          this.getCounts()
        }
        else {
          swal.fire("Warning!", res.message, "warning");
          setTimeout(() => {
            swal.close()
          }, 3000);
        }

      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }

  sendSMS(event: any, stand: any, type: any) {
    let data = {
      "SMS": {
        "entry": stand.SMS.entry,
        "exit": stand.SMS.exit,
        "monthlyCards": stand.SMS.monthlyCards
      }
    }
    this._dataService.putApi(environment.sendSmsUrl + stand.standId, data)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          swal.fire("", res.message, "success");
          setTimeout(() => {
            swal.close()
          }, 3000);
          this.getStandsList(1)
        }
        else {
          swal.fire("Warning!", res.message, "warning");
          setTimeout(() => {
            swal.close()
          }, 3000);
        }

      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }



  openModal(content: any, type: any, stand: any) {
    if (type == "add") {
      this.showEdit = false;
      this.standsForm.reset();
      this.standsForm.controls.showOnHomePage.setValue(true)
      this.standsForm.controls.state.setValue('');
      this.standsForm.controls.district.setValue('');
      this.standsForm.controls.city.setValue('');
      this.standsForm.controls.ownerName.setValue('');
      this.standsForm.controls.type.setValue('Parking');
      this.fileName = '';
      this.base64 = '';
      this.url = '';
      this.spinner = false;
      this.modalService.open(content, { size: 'lg', centered: true });
    } else {
      this.currentStandData = stand;
      this.url = '';
      this.showEdit = true;
      let state = this.statesList.filter((state: any) => state._id == this.currentStandData.location.state.id);
      let district: any = [];
      let city: any = []
      let owner = this.ownersList.filter((owner: any) => owner.ownerId == this.currentStandData.ownerId);
      if(state[0]) {
        this.getDistricts(state[0]);
      }
      setTimeout(() => {
        district = this.districtsList.filter((district: any) => district._id == this.currentStandData.location.district.id);
        this.getCities(district[0]);
        setTimeout(() => {
          city = this.citiesList.filter((city: any) => city._id == this.currentStandData.location.city.id);

        }, 1000);
      }, 1000);
      setTimeout(() => {
        this.fileName = this.currentStandData.standImage.imgName;
        this.base64 = this.currentStandData.standImage.imgData;
        this.standsForm.setValue({
          state: state && state.length > 0 ? state[0] : '',
          district: district && district.length > 0 ? district[0] : '',
          city: city && city.length > 0 ? city[0] : '',
          standName: this.currentStandData.standName,
          ownerName: owner && owner[0] ? owner[0] : '',
          type: this.currentStandData.standType ? this.currentStandData.standType : '',
          address: this.currentStandData.address,
          addressLine1: this.currentStandData.addressLine1,
          addressLine2: this.currentStandData.addressLine2,
          addressLine3: this.currentStandData.addressLine3,
          addressLine4: this.currentStandData.addressLine4,
          // price: this.currentStandData.price,
          phone: this.currentStandData.standPhone,
          policeStationContactNum: this.currentStandData.policeStationContact,
          nearPoliceStation: this.currentStandData.policeStation,
          latitude: this.currentStandData.latitude,
          longitude: this.currentStandData.longtitue,
          standImage: '',
          GSTIn: this.currentStandData.GSTIn,
          initialGracePeriod: this.currentStandData.initialGracePeriod,
          lateGracePeriod: this.currentStandData.lateGracePeriod,
          bicycleTimePeriodAmount: this.currentStandData.bicycle ? this.currentStandData.bicycle.timePeriodAmount : '',
          bicycleTaxAmount: this.currentStandData.bicycle ? this.currentStandData.bicycle.taxAmount : '',
          bicycletotalAmount: this.currentStandData.bicycle ? this.currentStandData.bicycle.totalAmount : '',
          twoWheelerTimePeriodAmount: this.currentStandData.twoWheeler ? this.currentStandData.twoWheeler.timePeriodAmount : '',
          twoWheelerTaxAmount: this.currentStandData.twoWheeler ? this.currentStandData.twoWheeler.taxAmount : '',
          twoWheelertotalAmount: this.currentStandData.twoWheeler ? this.currentStandData.twoWheeler.totalAmount : '',
          threeWheelerTimePeriodAmount: this.currentStandData.threeWheeler ? this.currentStandData.threeWheeler.timePeriodAmount : '',
          threeWheelerTaxAmount: this.currentStandData.threeWheeler ? this.currentStandData.threeWheeler.taxAmount : '',
          threeWheelertotalAmount: this.currentStandData.threeWheeler ? this.currentStandData.threeWheeler.totalAmount : '',
          fourWheelerTimePeriodAmount: this.currentStandData.fourWheeler ? this.currentStandData.fourWheeler.timePeriodAmount : '',
          fourWheelerTaxAmount: this.currentStandData.fourWheeler ? this.currentStandData.fourWheeler.taxAmount : '',
          fourWheelertotalAmount: this.currentStandData.fourWheeler ? this.currentStandData.fourWheeler.totalAmount : '',
          notes: this.currentStandData.note ? this.currentStandData.note : '',
          showOnHomePage: this.currentStandData.isPublic
        });
        this.standsForm.markAsPristine();
        this.standsForm.controls.standImage.setErrors(null);
        this.standsForm.controls.standImage.clearValidators();
        this.modalService.open(content, { size: 'lg', centered: true });
      }, 2050);
    }
  }
  test() {
    // // console.log(this.standsForm)
  }
  openSessionModal(sessionModal: any, stand: any) {
    this.sessionStand = stand;
    this.modalService.open(sessionModal, { size: 'lg', centered: true });
  }

  deleteStand(stand: any) {
    swal.fire({
      title: 'Are you sure to delete this record?',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this._dataService.deleteApi(environment.deleteStandes + stand.standId)
          .subscribe((res) => {
            if (res && res.status == 'success') {
              swal.fire("", res.message, "success");
              setTimeout(() => {
                swal.close()
              }, 3000);
              this.getStandsList(1)
              this.getCounts()
            }
            else {
              swal.fire("Warning!", res.message, "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }

          }, (error) => {
            this.spinner = false
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          })
      } else if (result.isDenied) {
      }
    })
  }

  uploadFile(event: any) {
    this.file = event.target.files[0];
    if (this.file.size > 100000) {
      this.fileError = true
      setTimeout(() => {
        this.fileError = false
      }, 10000);
    } else {
      this.fileName = this.file.name;
      this.fileError = false;
      setTimeout(() => {
        this.standsForm.controls.standImage.setValue(this.file.name);
        this.standsForm.controls.standImage.markAsDirty();
      }, 1);
      const reader = new FileReader();
      reader.readAsDataURL(this.file);
      reader.onload = (event: any) => {
        this.url = event.target.result
        this.base64 = reader.result
      };
    }
  }

  clearFiles() {
    this.base64 = '';
    this.file = undefined;
    this.url = '';
    this.fileError = false;
    this.fileName = "";
    setTimeout(() => {
      this.standsForm.controls.standImage.setValue(null);
      this.standsForm.controls.standImage.markAsDirty();
    }, 1);
  }

  getStates() {
    this._dataService.getApi(environment.getStatesURL)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.statesList = res.data
        } else {
          this.statesList = []
        }
      }, (err) => {
        this.statesList = []
      })
  }

  getDistricts(state: any) {
    if(state){
    this._dataService.getApi(environment.getDistrictsURL + "/" + state._id)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          setTimeout(() => {
            this.standsForm.controls.district.setValue("")
            this.standsForm.controls.city.setValue("")
          }, 1);
          this.districtsList = res.data;
          return this.districtsList;
        } else {
          this.districtsList = []
          return this.districtsList;
        }
      }, (err) => {
        this.districtsList = []
        return this.districtsList;
      })
    }
  }

  getCities(district: any) {
    if(district){
    this._dataService.getApi(environment.getCitiesURL + "/" + district._id)
      .subscribe((res) => {
        if (res && res && res.status.toLowerCase() == 'success') {
          setTimeout(() => {
            this.standsForm.controls.city.setValue("")
          }, 1);
          this.citiesList = res.data
        } else {
          this.citiesList = []
        }
      }, (err) => {
        this.citiesList = []
      })
    }
  }

  // routeToPriceRules(standId: any) {
  //   this._router.navigateByUrl('/price-rules/', standId)
  // }
  routeToEmplyee(stand: any) {
    var encryptedData = this._encryptService.encryptData(stand)
    localStorage.setItem('StandData', encryptedData)
  }

  getDate(date: any) {
    return moment(date).format("DD/MM/YYYY HH:mm:ss")
  }

}
