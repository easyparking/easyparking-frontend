import { MatAccordion } from '@angular/material/expansion';
// import { MatAccordion } from '@angular/material';
import { Component, Directive, EventEmitter, Injectable, Input, OnInit, Output, QueryList, ViewChild, ViewChildren, ViewEncapsulation } from '@angular/core';
import { NgbdSortableHeader } from 'src/app/directives/tableDirective';
import { NgbActiveModal, NgbDate, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/shared/services/data.service';
import { Router } from '@angular/router';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';
import { environment } from 'src/environments/environment.prod';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NgbDateParserFormatter, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { element } from 'protractor';
import * as moment from 'moment';
import { NgbDateFRParserFormatt } from 'src/app/Pipes/Dateparserformat';
import { OnlineOfflineService } from 'src/app/shared/services/online-offline.service';
import { OfflineDataService } from 'src/app/shared/services/offline-data.service';


@Component({
  selector: 'app-monthlycards',
  templateUrl: './monthlycards.component.html',
  styleUrls: ['./monthlycards.component.css'],
  providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatt }]
})
export class MonthlycardsComponent implements OnInit {

  datemodel!: NgbDateStruct;
  @ViewChild(MatAccordion)
  @ViewChildren(NgbdSortableHeader)
  headers!: QueryList<NgbdSortableHeader>;
  public accordion!: MatAccordion;
  page = 1;
  pageSize = 10;
  cardsForm!: FormGroup;
  searchForm!: FormGroup;
  cardsList: any;
  count: number = 0;
  pager: any;
  results$: any;
  subject = new Subject()
  selectedStand: any;
  userData: any;
  spinner: boolean = false;
  cardHistory: any = [];
  currentCard: any;
  showEdit: boolean = false;
  vehicleTypes: any;
  loader = false;
  amount: any;
  isPriceRuleAdded: boolean = false;
  isOnlineStatus: boolean = true;
  monthlyCardsEntryHistoryData: any;
  priceRulesDetails: any;
  cardOfferPrice: any;
  hasError: boolean = false;
  currentDate = { year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() }
  date = { year: 1987, month: 1, day: 1 }
  actualAmount: number = 0;
  sync: any;
  constructor(private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private _fb: FormBuilder,
    private _dataService: DataService,
    private _decryptService: EncryptDecryptService,
    private _ngbFormat: NgbDateParserFormatter,
    private _offlineService: OnlineOfflineService,
    private _offlineDataService: OfflineDataService,
    private router: Router) {
    this.cardsForm! = this._fb.group({
      startDate: [''],
      phone: ['', [Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]],
      vehicalType: ['Bicycle', Validators.required],
      duration: ['1', Validators.required],
      name: ['', Validators.required],
      vehicalNumber: ['', [Validators.required, Validators.pattern("^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$")]],
      amount: ['', Validators.required],
      cardNumber: [''],
      endDate: ''
    })
    this.searchForm! = this._fb.group({
      searchKey: ['', [Validators.required]],
    });
    //  this.date = ;
  }

  ngOnInit(): void {
    this._offlineService.networkConnectivity$.subscribe((res) => {
      this.isOnlineStatus = res;
      if (res) {
        this.getCardsList(1);
        if(this.selectedStand){
          this.getExpiredCards();
        }
      } else {
        if(localStorage.get("monthlyCardsHistoryData")){
          this.monthlyCardsEntryHistoryData = JSON.parse(localStorage.get("monthlyCardsHistoryData"))
        }
      }
    })
    if (localStorage.getItem("standSelected")) {
      let standsArray = JSON.parse(localStorage.getItem("standsArray") || "");
      let selectedStand = standsArray.filter((item: any) => item.standId == localStorage.getItem("standSelected"))
      this.selectedStand = selectedStand ? selectedStand[0] : ""
      this.getCardsList(1)
    }
    this.userData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    if(!this.userData.data){
    this.router.navigateByUrl('/login');
  }
  
  this.sync = this._dataService.isSyncing.subscribe((result: any) => {
    if(!result) {
      this.getCardsList(1)
    }
  });
    this.results$ = this.searchForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe((data: any) => this.getCardsList(1));
    this.getPriceRulesDetails();
    this.getExpiredCards();
  }
  
  ngOnDestroy(): void {
    if(this.sync){
    this.sync.unsubscribe();
  }
  }

  getExpiredCards() {
    this._dataService.getApi(environment.getexpiredMonthlyCardsUrl + `${this.selectedStand.standId}`)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          localStorage.setItem("monthlyCardsHistoryData", JSON.stringify(res.data))
          this.monthlyCardsEntryHistoryData = res.data;
        }
      })
  }

  getCardAmount() {
    this._dataService.getApi(environment.getPriceRulesURL + this.selectedStand.standId)
      .subscribe((res) => {
        if (res && res.status == 'success') {
          if (res.data && res.data.length > 0) {
            // this.vehicleTypes = res.data[0].vehicleTypes;
          }
        }
      })
  }

  getAmount(event: any) {
    if (this.vehicleTypes) {
      let vehicle = this.vehicleTypes.filter((element: any) => element.vehicletype == event);
      if (vehicle.length > 0) {
        this.amount = vehicle[0].cardActualPrice;
        this.cardOfferPrice = vehicle[0].cardOfferPrice;
        if (this.cardsForm.controls.duration.value) {
          this.actualAmount = vehicle[0].cardActualPrice * Number(this.cardsForm.controls.duration.value)
          setTimeout(() => {
            this.cardsForm.controls.amount.setValue(this.actualAmount)
            // this.cardsForm.controls.amount.setValidators(Validators.max(this.actualAmount))
          }, 1);
        } else {
          this.actualAmount = vehicle[0].cardActualPrice;
          setTimeout(() => {
            this.cardsForm.controls.amount.setValue(this.actualAmount)
            // this.cardsForm.controls.amount.setValidators(Validators.max(this.actualAmount))
          }, 1);
        }
      }
    } else {
      this.actualAmount = 0;
      this.cardsForm.controls.amount.setValue(0)
    }
  }
  changePrice(event: number) {
    if (event < this.cardOfferPrice) {
      this.hasError = true;
    } else {
      this.hasError = false;
    }
  }
  durationChanged(duration: any) {

    if (this.cardsForm.controls.startDate.value) {
      let endDate = new Date(this.cardsForm.controls.startDate.value.month + '/' + this.cardsForm.controls.startDate.value.day + '/' + this.cardsForm.controls.startDate.value.year)

      let date = new Date(endDate.setMonth((this.cardsForm.controls.startDate.value.month - 1) + parseInt(duration)))
      let date1 = moment(date).subtract(1, 'day')
      this.cardsForm.controls.endDate.setValue(date1.format('DD') + "/" + (date1.format('MM')) + "/" + date1.format('yyyy'))

    }
    setTimeout(() => {
      this.actualAmount = this.amount * Number(duration);
      this.cardsForm.controls.amount.setValue(this.amount * Number(duration))
    }, 1);
  }

  checkDate(endDate: any) {
    // endDate.split('/')[1] + '/' + endDate.split('/')[0]  + '/' + endDate.split('/')[2]
    if ((new Date(endDate) < new Date())) {
      return true;
    } else {
      return false
    }
  }


  getPriceRulesDetails() {
    this.vehicleTypes = []
    this.isPriceRuleAdded = false
    this._dataService.getApi(environment.getPriceRulesURL + this.selectedStand.standId)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success' && res.data) {
          this.priceRulesDetails = res.data;
          // this.vehicleTypes = res.data[0].vehicleTypes

          for (let i = 0; i < res.data[0].vehicleTypes.length; i++) {
            if (!(res.data[0].vehicleTypes[i].amount == 0 && (res.data[0].vehicleTypes[i].fixedPriceFor1stHour == 0 || res.data[0].vehicleTypes[i].forEveryNextHour == 0))) {
              this.isPriceRuleAdded = true;
              if (res.data[0].vehicleTypes[i].vehicletype != 'bicycle') {
                if (res.data[0].vehicleTypes[i].vehicletype == 'twoWheeler') {

                  res.data[0].vehicleTypes[i].vehicletypeValue = '2 W'
                  this.vehicleTypes.push(res.data[0].vehicleTypes[i])
                } else if (res.data[0].vehicleTypes[i].vehicletype == 'threeWheeler') {
                  // if(!(res.data[0].vehicleTypes[i].amount == 0 && (res.data[0].vehicleTypes[i].fixedPriceFor1stHour == 0 || res.data[0].vehicleTypes[i].forEveryNextHour == 0))){
                  res.data[0].vehicleTypes[i].vehicletypeValue = '3 W'
                  this.vehicleTypes.push(res.data[0].vehicleTypes[i])
                  // }
                } else if (res.data[0].vehicleTypes[i].vehicletype == 'fourWheeler') {
                  // if(!(res.data[0].vehicleTypes[i].amount == 0 && (res.data[0].vehicleTypes[i].fixedPriceFor1stHour == 0 || res.data[0].vehicleTypes[i].forEveryNextHour == 0))){
                  res.data[0].vehicleTypes[i].vehicletypeValue = '4 W'
                  this.vehicleTypes.push(res.data[0].vehicleTypes[i])
                  // }
                } else {
                  // if(!(res.data[0].vehicleTypes[i].amount == 0 && (res.data[0].vehicleTypes[i].fixedPriceFor1stHour == 0 || res.data[0].vehicleTypes[i].forEveryNextHour == 0))){
                  res.data[0].vehicleTypes[i].vehicletypeValue = res.data[0].vehicleTypes[i].vehicletype
                  this.vehicleTypes.push(res.data[0].vehicleTypes[i])
                  // }
                }
              } else {
                // if(!(res.data[0].vehicleTypes[i].amount == 0 && (res.data[0].vehicleTypes[i].fixedPriceFor1stHour == 0 || res.data[0].vehicleTypes[i].forEveryNextHour == 0))){
                res.data[0].vehicleTypes[i].vehicletypeValue = 'Bicycle';
                this.vehicleTypes.push(res.data[0].vehicleTypes[i])
                // }
              }
            }
            if ((res.data[0].vehicleTypes.length == i + 1 && !this.isPriceRuleAdded)) {
              // this.isPriceRuleAdded = false
              swal.fire("Warning!", "Price rules aren't added for the selected stand.Please contact admin", "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }
          }
        }
      })
  }

  getWantedVehiclesList(modal: any) {

    this.spinner = true;
    if (this.isPriceRuleAdded) {
      if (this._offlineService.isOnline && this.cardsForm.value.vehicalNumber) {

        this._dataService.getApi(environment.getWantedVehicleList + `?size=5&pageNo=1&searchText=${this.cardsForm.value.vehicalNumber.toUpperCase()}`)
          .subscribe((res) => {
            this.loader = false;
            if (res && res.status.toLowerCase() == 'success') {
              if (res.data.length > 0) {
                this.spinner = false;
                this.updateWantedVehicelStatus(res.data[0])
                this.addCard(modal);
                // swal.fire("Warning!", "Found this as wanted vehicel, please reach admin for details", "warning");
                // setTimeout(() => {
                //   swal.close()
                // }, 3000);
              } else {
                this.addCard(modal);
              }
            } else {
              this.addCard(modal);
            }
          }, (error) => {
          })

      } else {
        this.addCard(modal);
      }
    }
    else {
      swal.fire("Warning!", "Price rules aren't added for the selected stand.Please contact admin", "warning");
      setTimeout(() => {
        swal.close()
      }, 3000);
    }
  }
  // AP56AW3445
  updateWantedVehicelStatus(data: any) {
    this._dataService.putApi(environment.updateWantedVehicle + data._id, {
      standId: this.selectedStand[0].standId,
      vehicleData: data,
      standName: this.selectedStand[0].standName,
      location: this.selectedStand[0].location.city.city,
      isMatched: true
    })
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.spinner = false;
        }
      });
  }

  addCard(modal: any) {
    if (!this.showEdit) {
      let startDate = new Date(this.cardsForm.controls.startDate.value.month + '/' + this.cardsForm.controls.startDate.value.day + '/' + this.cardsForm.controls.startDate.value.year)
      let endDate = new Date(this.cardsForm.controls.startDate.value.month + '/' + this.cardsForm.controls.startDate.value.day + '/' + this.cardsForm.controls.startDate.value.year)
      let dataToAPI = {
        "isOfflineData" : false,
        "standId": this.selectedStand.standId,
        "standName": this.selectedStand.standName,
        "createdBy": this.userData.data.userName,
        "standCity": this.selectedStand ? this.selectedStand.location.city.city : '',
        "employeeId": this.userData.data.employeeId,
        "ownerId": this.selectedStand.ownerId,
        "vehicleType": this.cardsForm.controls.vehicalType.value,
        "vehicleName": "test",
        "customerName": this.cardsForm.controls.name.value,
        "customerPhone": this.cardsForm.controls.phone.value,
        "vehicleNumber": this.cardsForm.controls.vehicalNumber.value ? this.cardsForm.controls.vehicalNumber.value.toUpperCase() : '',
        "duration": this.cardsForm.controls.duration.value,
        "amount": this.cardsForm.controls.amount.value,
        "role": this.userData.data.role,
        // "startDate": this.cardsForm.controls.startDate.value ? this._ngbFormat.format(this.cardsForm.controls.startDate.value) : '',
        "startDate": this.cardsForm.controls.startDate.value ? startDate.toString() : '',
        "sendSMS": this.selectedStand.SMS.monthlyCards,
        "endDate": new Date(endDate.setMonth(endDate.getMonth() + parseInt(this.cardsForm.controls.duration.value))).toString(),
        "sessionId": JSON.parse(localStorage.getItem("sessionStartTime") || "")._id
      }
      this.spinner = true;
      if (this._offlineService.isOnline) {
        this._dataService.postApi(environment.addCardsUrl, dataToAPI)
          .subscribe((res) => {
            if (res && res.status.toLowerCase() == 'success') {
              this.spinner = false;
              modal.dismiss('Data saved')
              swal.fire("", res.message, "success");
              setTimeout(() => {
                swal.close()
              }, 3000);
              this.getCardsList(1);
              this.cardsForm.reset()
            } else {
              if (res.cardData) {
                this.currentCard = res.cardData;
                this.renewalCard(modal)
              } else {
                this.spinner = false
                swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
                setTimeout(() => {
                  swal.close()
                }, 3000);
              }

            }
          });
      } else {
        this._offlineDataService.addMonthlyCardsToIndexedDb(dataToAPI).then((res: any) => {
          this.spinner = false;
          this.monthlyCardsEntryHistoryData.unshift(dataToAPI)
          modal.dismiss('Data saved')
          swal.fire("", "Monthly Card Added Successfully", "success");
          setTimeout(() => {
            swal.close()
          }, 3000);
          // this.getCardsList(1);
          this.cardsForm.reset()

        }).catch((error: any) => {
          this.spinner = false;
          swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
          setTimeout(() => {
            swal.close()
          }, 3000);
        });
      }
    } else {
      this.renewalCard(modal)
    }
  }
  renewalCard(modal: any) {
    let date = this.cardsForm.controls.startDate.value ? moment(this.cardsForm.controls.startDate.value.month + '/' + this.cardsForm.controls.startDate.value.day + '/' + this.cardsForm.controls.startDate.value.year) : moment();
    this.currentCard.cardHistory.push({
      "startDate": date.toString(),
      "endDate": moment(date).add(parseInt(this.currentCard.duration), 'M').toString(),
      "duration": this.currentCard.duration,
      "amount": Number(this.currentCard.amount)
    })
    let dataToAPI = {
      "isOfflineData" : false,
      "cardId": this.currentCard.cardId,
      "startDate": date.toString(),
      "endDate": moment(date).add(parseInt(this.currentCard.duration), 'M').toString(),
      "renewalDate": moment().toString(),
      "cardHistory": this.currentCard.cardHistory,
      "sendSMS": this.selectedStand.SMS.monthlyCards,
      "sessionId": JSON.parse(localStorage.getItem("sessionStartTime") || "")._id,
      "amount": Number(this.cardsForm.controls.amount.value),
      "employeeId": this.userData.data.employeeId,
      "cardType": 'Offline',
    }
    this.spinner = true;
    if (this._offlineService.isOnline) {
      this._dataService.putApi(environment.cardRenewalUrl, dataToAPI)
        .subscribe((res) => {
          if (res && res.status.toLowerCase() == 'success') {
            this.spinner = false;
            modal.dismiss('Data saved')
            swal.fire("", res.message, "success");
            setTimeout(() => {
              swal.close()
            }, 3000);
            this.getCardsList(1);
            this.cardsForm.reset()
          }
          else {
            this.spinner = false;
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          }
        });
    } else {
      this._offlineDataService.renewalMonthlyCardsToIndexedDb(dataToAPI).then((res: any) => {
        this.spinner = false;
        modal.dismiss('Data saved')
        swal.fire("", "Monthly Card Renewed Successfully", "success");
        setTimeout(() => {
          swal.close()
        }, 3000);
        // this.getCardsList(1);
        this.cardsForm.reset()

      }).catch((error: any) => {
        this.spinner = false;
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      });
    }
  }

  getCardsList(pageNum: number) {
    this.page = pageNum;
    this.loader = true;
    this._dataService.getApi(environment.getCardsListURL + `?size=${this.pageSize}&pageNo=${pageNum}&searchText=${this.searchForm.controls.searchKey.value}&standId=${this.selectedStand.standId}`)
      .subscribe((res) => {
        this.loader = false;
        if (res && res.status.toLowerCase() == 'success') {
          this.cardsList = res.data;
          if (pageNum == 1) {
            this.count = res.totalCount;
          }
          this.pager = this._dataService.getPager(this.count, pageNum, this.pageSize);
        } else {
          this.cardsList = [];
        }
      }, (error) => {
        this.cardsList = [];
      })
  }

  setPage(page: any) {
    this.page = page;
    this.getCardsList(page)
  }
  //
  disabledRenewal(card: any) {
    return (new Date(card?.endDate) > new Date())
  }

  openModal(content: any, type: any, card?: any) {
    this.cardsForm.reset();
    this.spinner = false;
    if (type == "edit") {
      this.showEdit = true;
      this.currentCard = card;
      // let date = new Date(this.currentCard.endDate);
      let date = new Date(this.currentCard.endDate);

      let startDate = new Date(date).getDate() + '/' + (new Date(date).getMonth() + 1) + '/' + new Date(date).getFullYear();

      let getEndDate = new Date((date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear())
      let endDate = moment(getEndDate).add(Number(this.currentCard.duration), 'months').format("DD/MM/yyyy")
      this.getAmount(this.currentCard.vehicleType);
      if (this.currentCard.vehicleType == "bicycle") {
        setTimeout(() => {
          this.cardsForm.controls.vehicalNumber.setErrors(null);
        }, 1);
      } else {
        if(!this.currentCard.vehicleNumber) {
          setTimeout(() => {
            this.cardsForm.controls.vehicalNumber.setErrors({ require: true });
          }, 1);
        }
      }

      // new Date(getEndDate.getDate() + '/' + ((getEndDate.getMonth() + 1) + Number(this.currentCard.duration)) + '/' + getEndDate.getFullYear());
      this.cardsForm.setValue({
        startDate: this._ngbFormat.parse(startDate),
        phone: this.currentCard.customerPhone,
        vehicalType: this.currentCard.vehicleType,
        duration: this.currentCard.duration,
        cardNumber: this.currentCard.cardId,
        name: this.currentCard.customerName,
        vehicalNumber: this.currentCard.vehicleNumber,
        amount: this.currentCard.amount,
        endDate: endDate
      });

    } else {
      this.showEdit = false;
      this.cardsForm.controls.vehicalType.setValue("bicycle");
      this.getAmount("bicycle");
      if (this.cardsForm.controls.vehicalType.value == "bicycle") {
        setTimeout(() => {
          this.cardsForm.controls.vehicalNumber.setErrors(null);
        }, 1);
      } else {
        setTimeout(() => {
          this.cardsForm.controls.vehicalNumber.setErrors({ require: true });
        }, 1);
      }
      this.cardsForm.controls.duration.setValue("1")
    }
    this.modalService.open(content, { size: 'lg', centered: true });
  }

  openModal2(content2: any, history: any) {
    this.cardHistory = history;
    this.modalService.open(content2, { size: 'lg', centered: true });
  }
  getStatusColor(status: any) {
    switch (status) {
      case "Active":
        return "btn-success";
      case "Expired":
        return 'btn-danger';
      default:
        return "btn-primary";
    }
  }

  getDate(date: any) {
    // if(date){
    // let newDate = date.split('/')[1] + '/' + date.split('/')[0]  + '/' + date.split('/')[2]
    //   return moment(newDate).format("DD/MM/YYYY HH:mm:ss")
    // } else {
    return moment(date).format("DD/MM/YYYY HH:mm:ss")
    // }
  }

  getEndDate(date: any) {
    return moment(date).subtract(1, 'days').format("DD/MM/YYYY HH:mm:ss")
  }

  getEndDateByStartDate(event: any) {
    let endDate = new Date(event.month + '/' + event.day + '/' + event.year)
    let date = new Date(endDate.setMonth(endDate.getMonth() + parseInt(this.cardsForm.controls.duration.value)))
    this.cardsForm.controls.startDate.setValue({ year: date.getFullYear(), month: date.getMonth(), day: date.getDate() })
  }
  startDateChanged($event: any) {
    if ($event) {
      let endDate = new Date($event.month + '/' + $event.day + '/' + $event.year)
      let date = new Date(endDate.setMonth(($event.month - 1) + parseInt(this.cardsForm.controls.duration.value)))
      let date1 = moment(date).subtract(1, 'day')
      this.cardsForm.controls.endDate.setValue(date1.format('DD') + "/" + (date1.format('MM')) + "/" + date1.format('yyyy'))
    }
  }
  dateMethod(date: any) {
    if (date.length == 1) {
      return "0" + date
    }
    else {
      return date
    }
  }

  maskPhoneNumber(phone: string) {
    return phone.replace(phone.substring(2, 8), 'XXXXXX');
  }
}
