import { environment } from './../../../environments/environment.prod';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatAccordion } from '@angular/material/expansion';
// import { MatAccordion } from '@angular/material';
import { Component, Directive, EventEmitter, Inject, Input, OnInit, Output, QueryList, ViewChild, ViewChildren, ViewEncapsulation } from '@angular/core';
import { NgbdSortableHeader } from 'src/app/directives/tableDirective';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from 'src/app/shared/services/data.service';
import { DOCUMENT } from '@angular/common';
// import { ModalDirective } from 'ngx-bootstrap';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import { Observable, Subject } from 'rxjs';
import { debounce, debounceTime, map } from 'rxjs/operators';
import { ExportToCsv } from 'export-to-csv';
import * as moment from 'moment';
import { NgbDateParserFormatter, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateFRParserFormatt } from 'src/app/Pipes/Dateparserformat';

declare var window: any;
// declare var jsPDF: any;
@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatt }]
})
export class AccountsComponent implements OnInit {
  // @ViewChild(ModalDirective) modal: ModalDirective;

  pageSize: any = 10;
  addEditOwnerForm!: FormGroup;
  pageNum: any = 1;
  itemsPerPage: any = 10
  pager: any;
  ownersData: any = []
  spinner: boolean = false
  count: any = 0
  ownerStatus: any = 0;
  showEdit: boolean = false;
  currentOwnerData: any = {}
  dataToCopy: any = []
  print: boolean = false
  accountsStatus: any = {}
  inactiveCount: any = 0;
  activeCount: any = 0;
  results$: any;
  subject = new Subject()
  searchForm: any;
  loader: boolean = false;
  excelData: any;
  excelHedders: any = [];
  constructor(private modalService: NgbModal,
    private _fb: FormBuilder,
    private _dataService: DataService,
    private _router: Router, private activeModal: NgbActiveModal,
    private _ngbFormat: NgbDateParserFormatter
  ) {
    this.addEditOwnerForm! = this._fb.group({
      name: ['', [Validators.required]],
      userName: ['', [Validators.required]],
      password: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      planType: ['Rental', [Validators.required]],
      phoneNumber: ['', [Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]],
      onlinePercentage: ['', [Validators.pattern("^[0-9]*$"), Validators.minLength(1), Validators.maxLength(2)]],
      duration: ['30 Days'],
      trailStartDate: [''],
      trailEndDate: ['']
    });
    this.searchForm! = this._fb.group({
      searchKey: ['', [Validators.required]],
    });

  }

  ngOnInit(): void {
    this.getCounts();
    this.getOwners(1, 0);

    // this.getAllownersData();
    this.results$ = this.searchForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe((data: any) => this.getOwners(1, 0));
    this.addEditOwnerForm.controls.userName.valueChanges
      .pipe(debounceTime(500))
      .subscribe((data: any) => this.checkUsername());
  }

  getCounts() {
    this._dataService.getApi(environment.getAccountsCountURL)
      .subscribe((res) => {
        if (res && res.status == 'success') {
          this.count = res.count.Total;
          this.activeCount = res.count.Active;
          this.inactiveCount = res.count.Total - res.count.Active;
        }
      }, (error) => {
        this.ownersData = []
      })
  }

  getOwners(pageNum: any, count: any) {
    this.loader = true
    let dataTOAPI = {
      size: this.pageSize,
      pageNo: pageNum,
      count: count,
      searchText: this.searchForm.controls.searchKey.value
    }
    this._dataService.postApi(environment.getOwnersURL, dataTOAPI)
      .subscribe((res) => {
        if (res && res.status == 'success') {
          this.loader = false;
          res.data.forEach((element: any) => {
            let splittedData = element.createdDate.split(".")
            let splittedDate = (splittedData[0]).split("T")
            let formattedDate = (splittedDate[0]).split("-")
            element.createdDate = formattedDate[2] + "/" + formattedDate[1] + "/" + formattedDate[0] + " " + splittedDate[1]
            if (element.status == 'Active') {
              element.showStatus = true
            } else {
              element.showStatus = false
            }
          });

          this.ownersData = res.data;
          this.pager = this._dataService.getPager(this.count, pageNum, this.pageSize)
        } else {
          this.ownersData = []
          this.loader = false
        }

      }, (error) => {
        // this.ownersData = []
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }

  getAllownersData(type: any) {
    const data = {
      size: this.pageSize,
      pageNo: this.pageNum,
      searchText: this.searchForm.controls.searchKey.value
    }
    this._dataService.postApi(environment.getOwnersURL, data)
      .subscribe((res) => {
        if (res && res.status == 'success') {
          this.dataToCopy = res.data;
          this.excelData = res.data;
          this.excelData.forEach((element: any, i: number) => {
            delete element._id;
            // delete element.createdDate;
            delete element.employeeId;
            delete element.lastLoginSession;
            delete element.lastLogoutSession;
            delete element.loginStatus;
            delete element.onlinePaymentPercentage;
            delete element.planType;
            delete element.privileges;
            delete element.role;
            delete element.standId;
            delete element.standName;
            delete element.__v;
            // if(i==this.excelData.length){
            //   this.excelHedders =  Object.keys(element);
            // }
          });
          if (type == 'copy') {
            this.copyTable();
          }
          else if (type == 'pdf') {
            this.printPDF()
          }
          else if (type == 'excel') {
            this.exportAsXLSX()
          }
          else if (type == 'csv') {
            this.getCSV()
          }
        } else {
          this.dataToCopy = []
        }
      })

  }
  setPage(event: any) {
    this.pageNum = event
    this.getOwners(this.pageNum, 1)
  }

  checkUsername() {
    if (!this.showEdit && this.addEditOwnerForm.controls.userName.value) {
      this._dataService.getApi(environment.verifyUserNameURL + this.addEditOwnerForm.controls.userName.value)
        .subscribe((res) => {
          if (res && res.status.toLowerCase() == 'fail') {
            this.addEditOwnerForm.controls.userName.setErrors({ invalid: true })
            this.addEditOwnerForm.controls.userName.setErrors({ InvalidUserName: true })
          } else {
            this.addEditOwnerForm.controls.userName.setErrors(null)
            this.addEditOwnerForm.controls.userName.clearValidators()
          }
        })
    } else {
      this.addEditOwnerForm.controls.userName.setValidators(Validators.required)
      this.addEditOwnerForm.controls.userName.updateValueAndValidity()
    }

  }

  changePlanType($event: any) {
    if ($event.target.value == "Rental") {
      this.addEditOwnerForm.controls.planType.clearValidators()
      this.addEditOwnerForm.controls.planType.setErrors(null)
      this.addEditOwnerForm.controls.duration.setValue("30 Days")
      this.addEditOwnerForm.controls.duration.setValidators(Validators.required)
      this.addEditOwnerForm.controls.duration.updateValueAndValidity()
      this.addEditOwnerForm.controls.trailStartDate.clearValidators()
      this.addEditOwnerForm.controls.trailStartDate.setErrors(null)
      this.addEditOwnerForm.controls.trailEndDate.clearValidators()
      this.addEditOwnerForm.controls.trailEndDate.setErrors(null)
    } else if ($event.target.value == "Trial Mode") {
      this.addEditOwnerForm.controls.planType.clearValidators()
      this.addEditOwnerForm.controls.planType.setErrors(null)
      this.addEditOwnerForm.controls.trailStartDate.setValue({ year: new Date().getFullYear(), month: new Date().getMonth()+1, day: new Date().getDate() })
      this.addEditOwnerForm.controls.trailStartDate.setValidators(Validators.required)
      this.addEditOwnerForm.controls.trailStartDate.updateValueAndValidity()
      this.addEditOwnerForm.controls.trailEndDate.setValidators(Validators.required)
      this.addEditOwnerForm.controls.trailEndDate.updateValueAndValidity()
      this.addEditOwnerForm.controls.duration.clearValidators()
      this.addEditOwnerForm.controls.duration.setErrors(null)
    } else if ($event.target.value == "Card Sales") {
      this.addEditOwnerForm.controls.planType.setValidators(Validators.required)
      this.addEditOwnerForm.controls.planType.updateValueAndValidity()
      this.addEditOwnerForm.controls.duration.clearValidators()
      this.addEditOwnerForm.controls.duration.setErrors(null)
      this.addEditOwnerForm.controls.trailStartDate.clearValidators()
      this.addEditOwnerForm.controls.trailStartDate.setErrors(null)
      this.addEditOwnerForm.controls.trailEndDate.clearValidators()
      this.addEditOwnerForm.controls.trailEndDate.setErrors(null)
    }
  }
  AddEditOwnerData(type: any) {
    this.spinner = true;
    let expiryDate;
    if (this.addEditOwnerForm.controls.planType.value == "Rental") {
      if (this.addEditOwnerForm.controls.duration.value == "30 Days") {
        expiryDate = moment().add(30, 'days').toString()
      } else if (this.addEditOwnerForm.controls.duration.value == "90 Days") {
        expiryDate = moment().add(90, 'days').toString()
      } else if (this.addEditOwnerForm.controls.duration.value == "180 Days") {
        expiryDate = moment().add(180, 'days').toString()
      } else if (this.addEditOwnerForm.controls.duration.value == "365 Days") {
        expiryDate = moment().add(365, 'days').toString()
      }
    } else if (this.addEditOwnerForm.controls.planType.value == "Trial Mode") {
      expiryDate = moment(this.addEditOwnerForm.controls.trailEndDate.value.month + '/' + (this.addEditOwnerForm.controls.trailEndDate.value.day) + '/' + this.addEditOwnerForm.controls.trailEndDate.value.year).toString();
    } else {
      expiryDate = ''
    }
    if (type == 'add') {
      let dataToAPI = {
        "name": this.addEditOwnerForm.controls.name.value,
        "userName": this.addEditOwnerForm.controls.userName.value,
        "role": "owner",
        "phoneNumber": this.addEditOwnerForm.controls.phoneNumber.value,
        "password": this.addEditOwnerForm.controls.password.value,
        "onlinePaymentPercentage": this.addEditOwnerForm.controls.onlinePercentage.value ? this.addEditOwnerForm.controls.onlinePercentage.value : '',
        "planType": this.addEditOwnerForm.controls.planType.value,
        "emailId": this.addEditOwnerForm.controls.email.value,
        "expiryDate": expiryDate,
        "rentalDuration": this.addEditOwnerForm.controls.duration.value
      }
      this._dataService.postApi(environment.createUsersURL, dataToAPI)
        .subscribe((res) => {
          if (res && res.status == 'success') {
            swal.fire("", res.message, "success");
            setTimeout(() => {
              swal.close()
            }, 3000);
            this.spinner = false;
            this.activeModal.close();
            this.getOwners(1, 0);
            this.getCounts();
            this.addEditOwnerForm.reset()
          }
          else {
            this.spinner = false
            swal.fire("Warning!", res.message, "warning");
            setTimeout(() => {
              swal.close()
            }, 3000);
          }

        }, (error) => {
          this.spinner = false
          swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
          setTimeout(() => {
            swal.close()
          }, 3000);
        })
    }
    else {
      let dataToAPI = {
        "name": this.addEditOwnerForm.controls.name.value,
        "userName": this.addEditOwnerForm.controls.userName.value,
        "role": "owner",
        "phoneNumber": this.addEditOwnerForm.controls.phoneNumber.value,
        "password": this.addEditOwnerForm.controls.password.value,
        "onlinePaymentPercentage": this.addEditOwnerForm.controls.onlinePercentage.value ? this.addEditOwnerForm.controls.onlinePercentage.value : '',
        "ownerId": this.currentOwnerData.ownerId,
        "planType": this.addEditOwnerForm.controls.planType.value,
        "emailId": this.addEditOwnerForm.controls.email.value,
        "rentalDuration": this.addEditOwnerForm.controls.duration.value,
        "expiryDate": expiryDate,
      }
      this._dataService.postApi(environment.updateOwnerURL, dataToAPI)
        .subscribe((res) => {
          if (res && res.status == 'success') {
            swal.fire("", res.message, "success");
            setTimeout(() => {
              swal.close()
            }, 3000);
            this.spinner = false;
            this.activeModal.close();
            this.getOwners(1, 0)
            this.addEditOwnerForm.reset()
          }
          else {
            this.spinner = false
            swal.fire("Warning!", res.message, "warning");
            setTimeout(() => {
              swal.close()
            }, 3000);
          }

        }, (error) => {
          this.spinner = false
          swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
          setTimeout(() => {
            swal.close()
          }, 3000);
        })
    }

  }
  cancelAddEditForm() {
    this.addEditOwnerForm.reset()
    this.spinner = false
  }
  changeStatus(data: any) {
    this._dataService.postApi(environment.changeOwnerStatus, {
      "ownerId": data.ownerId,
      "status": data.showStatus ? "Active" : "Inactive"
    }).subscribe((response) => {
      if (response && response.status == 'success') {
        this.getOwners(1, 1)
        this.getCounts();
      }
      else {
        swal.fire("Warning!", response.message, "warning");
        setTimeout(() => {
          swal.close()
        }, 3000);
        // this.getOwners(1, 1)
      }

    }, (error) => {
      this.spinner = false
      swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
      setTimeout(() => {
        swal.close()
      }, 3000);
    })
  }
  showEditData(data: any) {
    this.currentOwnerData = data
    this.showEdit = true
    this.addEditOwnerForm.setValue({
      name: data.name,
      userName: data.userName,
      password: '',
      phoneNumber: data.phoneNumber,
      onlinePercentage: data.onlinePaymentPercentage,
      duration: data.rentalDuration ? data.rentalDuration : '',
    })
  }
  deleteOwner(data: any) {
    swal.fire({
      title: 'Are you sure to delete this record?',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        // swal.fire('Saved!', '', 'success')
        this._dataService.deleteApi(environment.deleteUserURL + data._id + "?ownerId=" + data.ownerId + "&role=" + data.role)
          .subscribe((res) => {
            if (res && res.status == 'success') {
              swal.fire("", res.message, "success");
              setTimeout(() => {
                swal.close()
              }, 3000);
              this.getOwners(1, 1)
              this.getCounts();
            }
            else {
              swal.fire("Warning!", res.message, "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }

          }, (error) => {
            this.spinner = false
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          })
      } else if (result.isDenied) {
      }
    })

  }

  openModal(content: any, type: any, ownerData?: any) {
    if (type == 'update') {
      this.currentOwnerData = ownerData
      this.showEdit = true;
      let date = moment(ownerData.expiryDate).subtract(20, 'days');
      let date1 = moment(ownerData.expiryDate);
      let startDate = date.format('D') + '/' + (date.format('M')) + '/' + date.format('YYYY');
      let expiryDate = date1.format('D') + '/' + (date1.format('M')) + '/' + date1.format('YYYY');
      this.addEditOwnerForm.setValue({
        name: ownerData.name ? ownerData.name : '',
        userName: ownerData.userName ? ownerData.userName : '',
        password: '',
        phoneNumber: ownerData.phoneNumber ? ownerData.phoneNumber : '',
        onlinePercentage: ownerData.onlinePaymentPercentage ? ownerData.onlinePaymentPercentage : '',
        email: ownerData.emailId ? ownerData.emailId : '',
        planType: ownerData.planType ? ownerData.planType : '',
        duration: ownerData.rentalDuration ? ownerData.rentalDuration : '',
        trailEndDate: this._ngbFormat.parse(expiryDate),
        trailStartDate: this._ngbFormat.parse(startDate)
      });
      setTimeout(() => {
        this.addEditOwnerForm.controls.password.setErrors(null)
        this.addEditOwnerForm.controls.password.clearValidators();
      }, 1);
    }
    else {
      this.showEdit = false
      this.addEditOwnerForm.reset()
      this.addEditOwnerForm.controls['planType'].setValue('Rental')
    }
    this.activeModal = this.modalService.open(content, { size: 'lg', centered: true });
  }

  copyTable() {

    var text = "S.No." +
      "\tAccount ID" +
      "\t\tContractor" +
      "\t\tUser Name" +
      "\t\tMobile No" +
      "\t\tEmail" +
      "\t\tPlan" +
      "\t\tAccount"
    var textData = "";
    for (let i = 0; i < this.dataToCopy.length; i++) {
      textData += i + 1 + "\t\t" + (this.dataToCopy[i].ownerId ? this.dataToCopy[i].ownerId : 'N/A') + "\t\t" + (this.dataToCopy[i].name ? this.dataToCopy[i].name : 'N/A') + "\t" + (this.dataToCopy[i].userName ? this.dataToCopy[i].userName : 'N/A') + "\t" + (this.dataToCopy[i].phoneNumber ? this.dataToCopy[i].phoneNumber : 'N/A') + "\t" +
        (this.dataToCopy[i].emailId ? this.dataToCopy[i].emailId : 'N/A') + "\t" + (this.dataToCopy[i].planType ? this.dataToCopy[i].planType : 'N/A') + "\t" + (this.dataToCopy[i].status ? this.dataToCopy[i].status : 'N/A') + "\n"
    }
    let selBox = document.createElement('textarea');

    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = text + '\n' + textData;

    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  async printPDF() {
    var doc = new jsPDF('p', 'pt');
    var col = [["S.No.", "Account ID", "Contractor", "User Name", "Mobile No", "Email", "Plan", "Account"]];
    var rows: any = [];
    await this.dataToCopy.forEach((element: any, i: number) => {
      var temp = [i + 1, (this.dataToCopy[i].ownerId ? this.dataToCopy[i].ownerId : 'N/A'), (this.dataToCopy[i].name ? this.dataToCopy[i].name : 'N/A'), (this.dataToCopy[i].userName ? this.dataToCopy[i].userName : 'N/A'), (this.dataToCopy[i].phoneNumber ? this.dataToCopy[i].phoneNumber : 'N/A'), (this.dataToCopy[i].emailId ? this.dataToCopy[i].emailId : 'N/A'), (this.dataToCopy[i].planType ? this.dataToCopy[i].planType : 'N/A'), (this.dataToCopy[i].status ? this.dataToCopy[i].status : 'N/A')];
      rows.push(temp);
    });
    await autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (data) => { },
    });
    doc.output('dataurlnewwindow')
  }

  getCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Owners CSV',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
      // headers: ['Owner Id', 'Name', "UserName", 'Phone Number', "Email Id", "Plan Type", "Show Status"]
    };

    const csvExporter = new ExportToCsv(options);

    csvExporter.generateCsv(this.excelData);
  }

  getDate(date: any) {
    //   if(date){
    //   let newDate = date.split('/')[1] + '/' + date.split('/')[0]  + '/' + date.split('/')[2]
    //   return moment(newDate).format("DD/MM/YYYY HH:mm:ss")
    // } else {
    return moment(date).format("DD/MM/YYYY HH:mm:ss")
    // }
  }


  exportAsXLSX(): void {
    this._dataService.exportAsExcelFile(this.excelData, 'sample');
  }
  removeSpace(form: any, formControlName: any,) {
    if (formControlName != 'password') {
      form.controls[formControlName].setValue(form.controls[formControlName].value.split(" ").join(""))
    }
    else {
      if (form.controls[formControlName].value.indexOf(' ') >= 0) {
        form.controls[formControlName].setErrors({ specialCharacter: true })
      }
      else {
        form.controls[formControlName].setErrors(null)
      }
    }
  }

}
