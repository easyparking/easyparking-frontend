import { Component, Directive, EventEmitter, Injectable, Input, OnInit, Output, QueryList, ViewChild, ViewChildren, ViewEncapsulation } from '@angular/core';
import { NgbdSortableHeader } from 'src/app/directives/tableDirective';
import { NgbActiveModal, NgbDate, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/shared/services/data.service';
import { Router } from '@angular/router';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';
import { environment } from 'src/environments/environment.prod';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NgbDateParserFormatter, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { element } from 'protractor';
import * as moment from 'moment';
import { NgbDateFRParserFormatt } from 'src/app/Pipes/Dateparserformat';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import { ExportToCsv } from 'export-to-csv';

@Component({
  selector: 'app-login-activity',
  templateUrl: './login-activity.component.html',
  styleUrls: ['./login-activity.component.css'],
  providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatt }]
})
export class LoginActivityComponent implements OnInit {

  searchForm!: FormGroup;
  userData: any;
  results$: any;
  employeeList: any = [];
  page: number = 1;
  loader: boolean = false;
  count: number = 0;
  pager: any;
  pageSize: number = 10;
  loginActivityList: any = [];
  today;
  fromDate: any;
  toDate: any;
  employee: any = '';
  maxDate = { year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() }
  excelLoginActivityList: any;
  ownersList: any;
  owner: any = "";
  standsList: any;
  stand: any = "";
  constructor(private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private _fb: FormBuilder,
    private _dataService: DataService,
    private _decryptService: EncryptDecryptService,
    private router: Router,
    private _ngbFormat: NgbDateParserFormatter) {

    this.today = new Date();
    this.today.setDate(this.today.getDate() - 14)
    this.fromDate = { year: this.today.getFullYear(), month: this.today.getMonth() + 1, day: this.today.getDate() };
    this.toDate = { year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() };
    this.searchForm! = this._fb.group({
      searchKey: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.userData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    if(!this.userData.data){
    this.router.navigateByUrl('/login');
  }
    this.results$ = this.searchForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe((data: any) => this.getLoginActivityList(1));
    if (this.userData.data.role != "admin") {
      this.getEmployeeList(this.userData.data.ownerId);
      this.getStandsList(this.userData.data.ownerId);
      this.owner = this.userData.data.ownerId;
    }
    this.getLoginActivityList(1);

    this.getOwnersList();
  }

  getStandsList(event: any) {
    this._dataService.getApi(environment.getStandsByOwnerURL + event)
      .subscribe((response) => {
        if (response.status == 'success') {
          this.standsList = response.data
        } else {
          this.standsList = []
        }
      })
  }

  getEmployeeList(event: any) {
    this._dataService.getApi(environment.getAllEmployeeList + '/' + event)
      .subscribe((res) => {
        if (res && res.status == 'success') {
          if (res.result && res.result.length > 0) {
            this.employeeList = res.result;
          }
        }
      })
  }

  getExcelLoginActivityList(type: any) {
    let data = {
      "size": 1000,
      "pageNo": 1,
      "searchText": this.searchForm.controls.searchKey.value,
      "ownerId": this.owner ? this.owner : this.userData.data.ownerId,
      "userName": this.employee ? this.employee : '',
      "startDate": this.fromDate ? new Date(this.fromDate.year + '/' + (this.fromDate.month) + '/' + this.fromDate.day).toString() : '',
      "endDate": this.toDate ? new Date(this.toDate.year + '/' + (this.toDate.month) + '/' + this.toDate.day + " 23:59:59").toString() : '',
      "standId": this.stand
    }
    this._dataService.postApi(environment.getLoginActivityListUrl, data)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.excelLoginActivityList = res.data;
          this.excelLoginActivityList.forEach((element: any, i: number) => {
            delete element._id;
            delete element.cancelled;
            delete element.cardAmount;
            delete element.createdDate;
            delete element.end_time;
            delete element.entries;
            delete element.exits;
            delete element.ownerId;
            delete element.parkingsAmount;
            delete element.pendingonLogin;
            delete element.pendingonLogout;
            delete element.role;
            delete element.standId;
            delete element.standName;
            delete element.start_time;
            delete element.__v;
          });
          if (type == 'copy') {
            this.copyTable()
          }
          else if (type == 'pdf') {
            this.printPDF()
          }
          else if (type == 'excel') {
            this.exportAsXLSX()
          }
          else if (type == 'csv') {
            this.getCSV()
          }
        } else {
          this.excelLoginActivityList = [];
        }
      }, (error) => {
        this.excelLoginActivityList = [];
      })
  }

  getOwnersList() {
    this._dataService.getApi(environment.getOwnersListURL)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() != 'fail') {
          this.ownersList = res.data;
        } else {
          this.ownersList = [];
        }
      })
  }

  getLoginActivityList(pageNum: number) {
    this.page = pageNum;
    this.loader = true;
    let data = {
      "size": this.pageSize,
      "pageNo": this.page,
      "searchText": this.searchForm.controls.searchKey.value,
      "ownerId": this.owner ? this.owner : this.userData.data.ownerId,
      "userName": this.employee ? this.employee : '',
      "startDate": this.fromDate ? new Date(this.fromDate.year + '/' + (this.fromDate.month) + '/' + this.fromDate.day).toString() : '',
      "endDate": this.toDate ? new Date(this.toDate.year + '/' + (this.toDate.month) + '/' + this.toDate.day + " 23:59:59").toString() : '',
      "standId": this.stand
    }
    this._dataService.postApi(environment.getLoginActivityListUrl, data)
      .subscribe((res) => {
        this.loader = false;
        if (res && res.status.toLowerCase() == 'success') {
          this.loginActivityList = res.data;
          // if (res.data.length != 0 && isPagination) {
          // this.getExcelLoginActivityList();
          // }
          if (pageNum == 1) {
            this.count = res.total;
          }
          this.pager = this._dataService.getPager(this.count, pageNum, this.pageSize);
        } else {
          this.loginActivityList = [];
        }
      }, (error) => {
        this.loginActivityList = [];
      })
  }

  openModal(content: any) {
    this.modalService.open(content, { size: 'lg', centered: true });
  }

  getDate(date: any) {
    return moment(date).format("DD/MM/YYYY HH:mm:ss") == "Invalid date" ? 'Session Running' : moment(date).format("DD/MM/YYYY HH:mm:ss")
  }

  copyTable() {
    var text = "S.No." +
      "\t\tEmploy ID" +
      "\t\tEmploy Name" +
      "\t\tUser Name" +
      "\t\tPhone " +
      "\t\tLogin time" +
      "\t\tIP Address" + '\n'
    var textData = "";
    for (let i = 0; i < this.loginActivityList.length; i++) {
      textData += i + 1 +
        "\t\t" + (this.loginActivityList[i].employeeId ? this.loginActivityList[i].employeeId : 'N/A')
        + "\t\t" + (this.loginActivityList[i].name ? this.loginActivityList[i].name : 'N/A')
        + "\t" + (this.loginActivityList[i].userName ? this.loginActivityList[i].userName : 'N/A')
        + "\t" + (this.loginActivityList[i].phone ? this.loginActivityList[i].phone : 'N/A')
        + "\t" + (this.loginActivityList[i].vehicleNumber ? this.loginActivityList[i].vehicleNumber : 'N/A') //
        + "\t" + (this.loginActivityList[i].start_time ? this.getDate(this.loginActivityList[i].start_time) : "N/A")
        + "\t" + (this.loginActivityList[i].ipaddress ? this.loginActivityList[i].ipaddress : 'N/A') + "\n"
    }
    let selBox = document.createElement('textarea');

    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = text + '\n' + textData;

    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    if (selBox) {
      // window.print()
    }
    // this.MessageService.add({ severity: 'success', summary: '', detail: 'Link copied to Clipboard' });
    else {
      // this.MessageService.add({ severity: 'warning', summary: '', detail: 'Unable to copy the link to Clipboard' });
    }
  }

  async printPDF() {
    var doc = new jsPDF('p', 'pt');
    var col = [["S.No.",
      "Employee ID",
      "Employee Name",
      "User Name",
      "Phone ",
      "Login time",
      "IP Address"]];
    var rows: any = [];
    await this.loginActivityList.forEach((element: any, i: number) => {
      var temp = [i + 1,
        , (this.loginActivityList[i].employeeId ? this.loginActivityList[i].employeeId : 'N/A')
        , (this.loginActivityList[i].name ? this.loginActivityList[i].name : 'N/A')
        , (this.loginActivityList[i].userName ? this.loginActivityList[i].userName : 'N/A')
        , (this.loginActivityList[i].phone ? this.loginActivityList[i].phone : 'N/A')
        , (this.loginActivityList[i].vehicleNumber ? this.loginActivityList[i].vehicleNumber : 'N/A')
        //  , (this.loginActivityList[i].duration ? this.loginActivityList[i].duration : 'N/A')
        , (this.loginActivityList[i].start_time ? this.getDate(this.loginActivityList[i].start_time) : "N/A")
        , (this.loginActivityList[i].ipaddress ? this.loginActivityList[i].ipaddress : 'N/A')];
      rows.push(temp);
    });
    await autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (data) => { },
    });
    // doc.output('dataurlnewwindow')
      doc.setProperties({ title: "Login Activity" });
      // window.open(doc.output('bloburl'), '_blank')
      window.open(URL.createObjectURL(doc.output('blob')), '_blank')
  }

  getCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Login Activity',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };
    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(this.excelLoginActivityList);
  }
  exportAsXLSX(): void {
    this._dataService.exportAsExcelFile(this.excelLoginActivityList, 'sample');
  }
}
