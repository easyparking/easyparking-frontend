import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from 'src/app/shared/services/data.service';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';
import { environment } from 'src/environments/environment.prod';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import * as moment from 'moment';
import swal from 'sweetalert2';
import { NgbDateFRParserFormatt } from 'src/app/Pipes/Dateparserformat';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css'],
  providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatt }]
})
export class ReportsComponent implements OnInit {
  userData: any;
  selectedStand: any;
  employeeList: any;
  loginSessionsList: any;
  employee: any = '';
  loginSession: any = '';
  reportTypes = '0'
  loginSessionData: any
  employeeData: any
  standsList: any = [];
  stand: any = ''
  standData: any
  hoveredDate: NgbDate | null = null;

  fromDate: NgbDate | null = null;
  toDate: NgbDate | null = null;
  datedisplay: boolean = false;
  showReset: boolean = false;
  @ViewChild('dateInput')
  dateInput!: { nativeElement: { contains: (arg0: any) => any; }; };
  reportsList: any;
  showError: boolean = false;
  type: string = '';
  constructor(private modalService: NgbModal,
    private _dataService: DataService,
    private _decryptService: EncryptDecryptService,
    private _ngbFormat: NgbDateParserFormatter,
    private router: Router) { }

  ngOnInit(): void {
    if (localStorage.getItem("standSelected")) {
      let standsArray = JSON.parse(localStorage.getItem("standsArray") || "");
      let selectedStand = standsArray.filter((item: any) => item.standId == localStorage.getItem("standSelected"))
      this.selectedStand = selectedStand ? selectedStand[0] : ""
    }
    this.userData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    if(!this.userData.data){
    this.router.navigateByUrl('/login');
  }
    this.getEmployeeList();
    this.getStands()
  }

  getEmployeeList() {
    this._dataService.getApi(environment.getEmpList + (this.selectedStand ? this.selectedStand.ownerId : this.userData.data.ownerId))
      .subscribe((res) => {
        if (res && res.status == 'success') {
          if (res.data && res.data.length > 0) {
            this.employeeList = res.data;
          }
          else {
            this.employeeList
          }
        }
      })
  }
  getStands() {
    this._dataService.getApi(environment.getStandsByOwnerURL + (this.selectedStand ? this.selectedStand.ownerId : this.userData.data.ownerId))
      .subscribe((res) => {
        if (res && res.status == 'success') {
          if (res.data && res.data.length > 0) {
            this.standsList = res.data;
          }
          else {
            this.standsList = []
          }
        }
      })

  }
  loginSessionChanged($event: any) {
    this.loginSessionData = $event
  }

  changeStandData($event: any) {
    this.standData = $event
  }
  getLoginSessionsList(employee: any) {
    this.employeeData = employee
    this._dataService.getApi(environment.getEmpSessionList + employee.userName+'?role='+this.userData.data.role)
      .subscribe((res) => {
        if (res && res.status == 'success') {
          if (res.data && res.data.length > 0) {
            this.loginSessionsList = res.data;
          } else {
            this.loginSessionsList = [];
          }
        }
      })
  }

  getReports() {
    let startDate = this.fromDate ? new Date(this.fromDate.month + '/' + this.fromDate.day + '/' + this.fromDate.year) : ''
    let endDate = this.toDate ? new Date(this.toDate.month + '/' + this.toDate.day + '/' + this.toDate.year) : ''
    if(((this.reportTypes == '0' ||this.reportTypes == '1' || this.reportTypes == '2') && (this.employee && this.loginSession)) || (this.reportTypes == '3' && this.stand) || (this.reportTypes == "4" && this.stand && (this.fromDate && this.toDate)) || (this.userData.data.role!='owner' && this.reportTypes == '3')){
      this.type = '';
      if(this.reportTypes == '0'){
        this.type = 'logoutReport';
      } else if(this.reportTypes == '1'){
        this.type = 'PendingVehiclesReport';
      } else if(this.reportTypes == '2'){
        this.type = 'PendingLogoutVehiclesReport';
      } else if(this.reportTypes == '3'){
        this.type = 'PendingCurrentVehiclesReport';
      } else if(this.reportTypes == '4'){
        this.type = 'cardsReport';
      }
      if(this.reportTypes == '4') {
        let data = {
          "startTime": (this.loginSession ? this.loginSession.start_time : startDate),
          "endTime": this.loginSession ? this.loginSession.end_time : endDate,
          "standId": this.selectedStand ? this.selectedStand.standId : this.employee.standId,
          // "ownerId": this.selectedStand ? this.selectedStand.ownerId : '',
          // "employeeId": this.employee ? this.employee.employeeId : '',
          // "reportType": type,
          // "sessionId":this.loginSessionData ? this.loginSessionData._id : ''
        }
      
        this._dataService.postApi(environment.getCardSalesReports, data)
          .subscribe((res) => {
            this.showError = false;
            if (res && res.status == 'success') {
              this.reportsList = res;
              // if (this.reportsList.cardsList?.length > 0) {
                this.printPDF(res, this.reportTypes, data)
              // } else {
              //   swal.fire("Warning!", "No Reports Found", "warning");
              //   setTimeout(() => {
              //     swal.close()
              //   }, 3000);
              // }
            } else {
              swal.fire("Warning!", "No Reports Found", "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }
          })
      } else {
        let data = {
          "start_time": this.reportTypes == "3" ? new Date() : (this.loginSession ? this.loginSession.start_time : startDate),
          "end_time": this.loginSession ? this.loginSession.end_time : endDate,
          "standId": this.selectedStand ? this.selectedStand.standId : this.employee.standId,
          "ownerId": this.selectedStand ? this.selectedStand.ownerId : '',
          "employeeId": this.employee ? this.employee.employeeId : '',
          "reportType": this.type,
          "sessionId":this.loginSessionData ? this.loginSessionData._id : ''
        }
      
        this._dataService.postApi(environment.getReportsURL, data)
          .subscribe((res) => {
            this.showError = false;
            if (res && res.status == 'success' && res.report) {
              this.reportsList = res.report[this.type];
              // if(this.reportTypes == '1' || this.reportTypes == '2' || this.reportTypes == '3' && res.report && res.report[type]){
                this.printPDF(res.report[this.type], this.reportTypes, data)
              // } else if ((this.reportsList.parkingsList && this.reportsList.parkingsList?.length > 0) || (this.reportsList.cardsList && this.reportsList.cardsList?.length > 0)) {
              //   this.printPDF(res.report[type], this.reportTypes, data)
              // } else {
              //   swal.fire("Warning!", "No Reports Found", "warning");
              //   setTimeout(() => {
              //     swal.close()
              //   }, 3000);
              // }
            } else {
              swal.fire("Warning!", "No Reports Found", "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }
          })
      }
     
    } else {
      this.showError = true;
    }
  }

  reportTypeChanged() {
    this.stand = "";
    this.employee = "";
    this.loginSession = "";
    this.fromDate = null;
    this.toDate = null;
  }

  async printPDF(data: any, type: any, dataToAPI?: any) {
    var doc = new jsPDF('landscape', 'pt');
    doc.setFontSize(12);
    if (parseInt(type) == 0 || parseInt(type) == 1) {
      doc.text("Owner Name:" + (this.selectedStand ? this.selectedStand.ownerName : (this.standData ? this.standData.ownerName : this.employee.ownerId)), 10, 20);
      doc.text("Stand Name:" + (this.selectedStand ? this.selectedStand.standName : (this.standData ? this.standData.standName : this.employee.standName)), 10, 40);
      doc.text("Location:" + (this.selectedStand ? this.selectedStand.location.city.city : (this.standData ? this.standData.location.city.city : '')), 10, 60);

      doc.text("Employee Name:" + this.employee.name, 300, 20);
      doc.text("Mobile Number:" + this.employee.phoneNumber, 300, 40);
      doc.text("Salary: --", 300, 60);

      doc.text("Login Time:" + (this.getDate(this.loginSessionData.start_time)), 500, 20);
      doc.text("Logout Time:" + (this.getDate(this.loginSessionData.end_time)), 500, 40);
      doc.text("Advance: --", 500, 60);

      this.showReportTypePdf(doc, data, type)
    } else if (parseInt(type) == 2) {
      doc.text("Owner Name:" + (this.selectedStand ? this.selectedStand.ownerName : (this.standData ? this.standData.ownerName : '')), 30, 20);
      doc.text("Stand Name:" + (this.selectedStand ? this.selectedStand.standName : (this.standData ? this.standData.standName : '')), 30, 40);
      doc.text("Location:" + (this.selectedStand ? this.selectedStand.location.city.city : (this.standData ? this.standData.location.city.city : '')), 30, 60);
      
      doc.text("Employee Name:" + this.employee.name, 300, 20);
      doc.text("Mobile Number:" + this.employee.phoneNumber, 300, 40);
      doc.text("Salary: --", 300, 60);

      doc.text("Login Time:" + (this.getDate(this.loginSessionData.start_time)), 500, 20);
      doc.text("Logout Time:" + (this.getDate(this.loginSessionData.end_time)), 500, 40);
      doc.text("Total Vehicles:" + this.reportsList.length, 500, 60);

      this.showReportTypePdf(doc, '', type)
    } else if (parseInt(type) == 3) {
      doc.text("Owner Name:" + (this.selectedStand ? this.selectedStand.ownerName : (this.standData ? this.standData.ownerName : '')), 10, 20);
      doc.text("Stand Name:" + (this.selectedStand ? this.selectedStand.standName : (this.standData ? this.standData.standName : '')), 10, 40);
      doc.text("Location:" + (this.selectedStand ? this.selectedStand.location.city.city : (this.standData ? this.standData.location.city.city : '')), 10, 60);
      
      doc.text("Employee Name:" + this.employee.name, 300, 20);
      doc.text("Mobile Number:" + this.userData.data.phoneNumber, 300, 40);

      doc.text("Total Vehicles:" + this.reportsList.length, 500, 20);
      doc.text("Date:" + moment().format("DD/MM/YYYY HH:mm"), 500, 40);
      this.showReportTypePdf(doc, '', type)
    } else if (parseInt(type) == 4) {
      this.showReportTypePdf(doc, data, type, dataToAPI)
    }

  }

  async showReportTypePdf(doc: any, data: any, type: any, dataToAPI?: any) {
    let start_time = this.loginSessionData.start_time;
    let end_time = this.loginSessionData.end_time;    
    if (parseInt(type) == 0) {
      let col = [["S.No.",
      "Parking Id",
        "Type",
        "Reg.No.",
        "Date & Time of Entry",
        "Date & Time of Exit",
        "Payable Amount",
        "Discount Amount",
        "Collected amount",
        "Remark"]];
      let rows: any = [];
      let totalPayableAmount = 0;
      let totalDiscountAmount = 0;
      let totalCollectedAmount = 0;
      let cardAmount =(data.cardAmount && data.cardAmount.length) ? data.cardAmount[0].SUM : 0;
      await data.parkingsList.forEach((element: any, i: number) => {
        var temp = [i + 1
          , (element.parkingId ? element.parkingId : '--')
          , (element.vehicleType ? this.getVehicleType(element.vehicleType) : '--')
          , (element.vehicleNumber ? element.vehicleNumber : (element.vehicleType == 'bicycle' ? 'BICYCLE' :'--'))
          , (element.dateAndTimeOfParking ? this.getDate(element.dateAndTimeOfParking) : '--')
          , (element.dateAndTimeOfExit ? this.getDate(element.dateAndTimeOfExit) : '--')
          , (element.actualPrice ? element.actualPrice : '0')
          , (element.offerPrice ? element.offerPrice : '0')
          , (element.finalPrice ? element.finalPrice : '0')
          , '--'];
          if(element.dateAndTimeOfExit >= start_time && element.dateAndTimeOfExit <= end_time){
            totalPayableAmount += element.actualPrice;
            totalDiscountAmount += element.offerPrice;
            totalCollectedAmount += element.finalPrice;
          }
        
        rows.push(temp);
      });
      let col1 = [["Pending Vehicles on login",
        "No of Entries",
        "No of Exits",
        "Cancelled",
        "No of Card Vehicles",
        // "No of Numberless Vehicles",
        "Pending vehicles on logout",
        "Total Payable amount",
        "Total discount amount",
        "Total collected amount",
        "Cards Amount",
        "Grand Total"]];
      let row1: any = [];
      let temp1 = [
        (data.total.pendingVehiclesLogin ? data.total.pendingVehiclesLogin : '0')
        , (data.total.entries ? data.total.entries : '0')
        , (data.total.exits ? data.total.exits : '0')
        , (data.total.cancelled ? data.total.cancelled : '0')
        , (data.total.cardVehicles ? data.total.cardVehicles : '0')
        // , (data.total.numberLessVehicles ? data.total.numberLessVehicles : '0')
        , (data.total.pendingVehiclesLogout ? data.total.pendingVehiclesLogout : '0')
        , totalPayableAmount
        , totalDiscountAmount
        , totalCollectedAmount
        , cardAmount
        , totalCollectedAmount + cardAmount 
      ];
      row1.push(temp1);
      await autoTable(doc, {
        head: col1,
        body: row1,
        margin: { top: 80, left: 20, right: 20, bottom: 0 },
        didDrawCell: (data) => { },
      });
      await autoTable(doc, {
        head: col,
        body: rows,
        margin: { top: 80, left: 20, right: 20, bottom: 0 },
        didDrawCell: (data) => { },
      });
      doc.setProperties({ title: this.type });
      // doc.output('dataurlnewwindow')
      // doc.save("logout_report-" + moment().format("DD/MM/YYYY HH:mm:ss"))
      // window.open(doc.output('bloburl'), '_blank')
      
      window.open(URL.createObjectURL(doc.output('blob')), '_blank')
    } else if (parseInt(type) == 1) {

      let col1 = [["S.No",
        "Tkt ID",
        "Type",
        "Reg.No.",
        "Date & Time of Entry"
      ]];
      let row1: any = [];
      await data.forEach((element: any, i: number) => {
        let temp = [i + 1
          , (element.parkingId ? element.parkingId : '--')
          , (element.vehicleType ? this.getVehicleType(element.vehicleType) : '--')
          , (element.vehicleNumber ? element.vehicleNumber : (element.vehicleType == 'bicycle' ? 'BICYCLE' :'--'))
          , (element.dateAndTimeOfParking ? this.getDate(element.dateAndTimeOfParking) : '--')];

        row1.push(temp);
      });
      await autoTable(doc, {
        head: col1,
        body: row1,
        margin: { top: 80, left: 20, right: 20, bottom: 0 },
        didDrawCell: (data) => { },
      });
      // doc.output('dataurlnewwindow')
      // doc.save("pending_vehicles_on_logout-" + moment().format("DD/MM/YYYY HH:mm:ss"))
      
      doc.setProperties({ title: this.type });
      // window.open(doc.output('bloburl'), '_blank')
      window.open(URL.createObjectURL(doc.output('blob')), '_blank')
    } else if (parseInt(type) == 2) {
      var col1 = [["0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "Other"
      ]];
      let row1: any = [];
        let temp1: any = ["     ", "     ", "     ", "     ", "     ", "     ", "     ", "     ", "     ", "     ", "     "];
        this.reportsList.sort((a:any, b:any) => {
          if (a.vehicleNumber.substring(6, a.vehicleNumber.length) < b.vehicleNumber.substring(6, b.vehicleNumber.length)) return -1
          return a.vehicleNumber.substring(6, a.vehicleNumber.length) > b.vehicleNumber.substring(6, b.vehicleNumber.length) ? 1 : 0
        })
      await this.reportsList.forEach((element: any) => {
        let stand = element.vehicleNumber[6];
        if (Number(stand) != NaN) {
          temp1[stand] = (temp1[stand]=="     "? '' : temp1[stand]+ '\n') + element.vehicleNumber.substring(6, element.vehicleNumber.length) + '-' + element.vehicleNumber.substring(0, 6) + (element.vehicleType == 'fourWheeler' ? '(4W)' : '') + (element.vehicleType == 'threeWheeler' ? '(3W)' : '')
        } else {
          temp1[10] = (temp1[10] =="     " ? '' : temp1[10]+ '\n') + element.vehicleNumber
        }
      });
      await temp1.forEach((element:any) => {
        element = element ? element : '     '
      });
      row1.push(temp1);
      await autoTable(doc, {
        head: col1,
        body: row1,
        margin: { top: 100, left: 20, right: 20, bottom: 0 },
        didDrawCell: (data) => { },
      });
      // doc.output('dataurlnewwindow')
      // doc.save("logout_vehicle_numbers_report-" + moment().format("DD/MM/YYYY HH:mm:ss"))
      doc.setProperties({ title: this.type });
      // window.open(doc.output('bloburl'), '_blank')
      window.open(URL.createObjectURL(doc.output('blob')), '_blank')
    } else if (parseInt(type) == 3) {
      var col1 = [["0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "Other"
        // ,
        // "Image"
      ]];
      let row1: any = [];
      let temp1: any = ["     ", "     ", "     ", "     ", "     ", "     ", "     ", "     ", "     ", "     ", "     "];
      this.reportsList.sort((a:any, b:any) => {
  if (a.vehicleNumber.substring(6, a.vehicleNumber.length) < b.vehicleNumber.substring(6, b.vehicleNumber.length)) return -1
  return a.vehicleNumber.substring(6, a.vehicleNumber.length) > b.vehicleNumber.substring(6, b.vehicleNumber.length) ? 1 : 0
})
      await this.reportsList.forEach((element: any) => {
        let stand = element.vehicleNumber[6];
        if (Number(stand) != NaN) {
          temp1[stand] = (temp1[stand]=="     "? '' : temp1[stand]+ '\n' )  + element.vehicleNumber.substring(6, element.vehicleNumber.length) + '-' + element.vehicleNumber.substring(0, 6) + (element.vehicleType == 'fourWheeler' ? '(4W)' : '') + (element.vehicleType == 'threeWheeler' ? '(3W)' : '')
        } else {
          temp1[10] = (temp1[10] =="     " ? '' : temp1[10]+ '\n') + element.vehicleNumber
        }
      });
      await temp1.forEach((element:any) => {
        element = element ? element : ''
      });
      
      row1.push(temp1);
      await autoTable(doc, {
        head: col1,
        body: row1,
        margin: { top: 100, left: 20, right: 20, bottom: 0 },
        didDrawCell: (data) => { },
      });
      // doc.output('dataurlnewwindow')
      // doc.save("vehicle_numbers_report-" + moment().format("DD/MM/YYYY HH:mm:ss"))
      doc.setProperties({ title: this.type });
      // window.open(doc.output('bloburl'), '_blank')
      window.open(URL.createObjectURL(doc.output('blob')), '_blank')
    } else if (parseInt(type) == 4) {
      var col1 = [["S.No",
        "Type",
        "Card Num",
        "Name",
        "Veh Num",
        "PayDate",
        "Duration",
        "Amount",
        "Start Date",
        "End Date",
        "Status"
      ]];
      let row1: any = [];
      let onlineCardsCount = 0;
      let totalCollection = 0;
      let onlineCardsCollection = 0;
      // console.log(data)
      await data.cardsList.forEach((element: any, i: number) => {
        let temp = [i + 1
          , (element.vehicleType ? this.getVehicleType(element.vehicleType) : '--')
          , (element.cardId ? element.cardId : '--')
          , (element.customerName ? element.customerName : '--')
          , (element.vehicleNumber ? element.vehicleNumber : (element.vehicleType == 'bicycle' ? 'BICYCLE' :'--'))
          , (element.createdDate ? this.getDate(element.createdDate) : '--')
          , (element.duration ? element.duration : '--')
          , (element.amount ? element.amount : '--')
          , (element.startDate ? this.getDate(element.startDate) : '--')
          , (element.endDate ? this.getDate(element.endDate) : '--')
          , (element.status ? element.status : '--')];
        totalCollection += element.amount;
        if (element.type != "offline") {
          onlineCardsCollection += element.amount;
          onlineCardsCount += 1;
        }
        row1.push(temp);
      });
      doc.text("Owner Name:" + (this.selectedStand ? this.selectedStand.ownerName : (this.standData ? this.standData.ownerName : '')), 10, 20);
      doc.text("Stand Name:" + (this.selectedStand ? this.selectedStand.standName : (this.standData ? this.standData.standName : '')), 10, 40);
      doc.text("Location:" + (this.selectedStand ? this.selectedStand.location.city.city : (this.standData ? this.standData.location.city.city : '')), 10, 60);

      doc.text("From Date:" + this.getDate(dataToAPI.startTime), 10, 80);

      doc.text("Total Cards:" + data.cardsList?.length, 300, 20);
      doc.text("Online Cards:" + onlineCardsCount, 300, 40);
      doc.text("Offline Cards: " + (data.cardsList?.length - onlineCardsCount), 300, 60);
      doc.text("To Date:" + this.getDate(dataToAPI.endTime), 300, 80);

      doc.text("Total Collection:" + totalCollection, 500, 20);
      doc.text("Online Collection:" + onlineCardsCollection, 500, 40);
      doc.text("Offline Collection:" + (totalCollection - onlineCardsCollection), 500, 60);
      doc.text("Print Date:" + moment().format("DD/MM/YYYY HH:mm"), 500, 80);
      // row1.push(temp1);
      await autoTable(doc, {
        head: col1,
        body: row1,
        margin: { top: 100, left: 20, right: 20, bottom: 0 },
        didDrawCell: (data) => { },
      });
      // doc.output('dataurlnewwindow')
      // doc.save("card_sales_report-" + moment().format("DD/MM/YYYY HH:mm:ss"))
      doc.setProperties({ title: this.type });
      // window.open(doc.output('bloburl'), '_blank')
      window.open(URL.createObjectURL(doc.output('blob')), '_blank')
    }
  }

  getDate(date: any) {
    return moment(date).format("DD/MM/YYYY HH:mm:ss") == "Invalid date" ? 'N/A' : moment(date).format("DD/MM/YYYY HH:mm:ss")
  }
  
  getVehicleType(type: string) {
    if (type == "bicycle") {
      return "Bicycle";
    } else if (type == "twoWheeler") {
      return "2 W";
    } else if (type == "threeWheeler") {
      return "3 W";
    } else if (type == "fourWheeler") {
      return "4 W";
    } else {
      return type;
    }
  }

  ResetDates() {
    this.showReset = false
    this.fromDate = null
    this.toDate = null
  }

  datepick() {
    this.datedisplay = !this.datedisplay;
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
      this.datedisplay = !this.datedisplay;
      this.showReset = true;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }
  //   isFrom  (date:NgbDate) {
  // return  equals(date, this.fromDate);
  //   }
  //   isTo  (date:NgbDate){
  //     return equals(date, this.toDate);
  //   }
  isDisabled(date: NgbDate) {
    // alert(date)
    const d = new Date(date.year, date.month - 1, date.day);
    if (d >= new Date())
      return d
    else
      return null
  }

  isEndDisabled(date: NgbDateStruct) {
    // alert(date)
    const d = new Date(date.year, date.month - 1, date.day);
    if (d >= new Date())

      return d
    else
      return null
  }

  onDateclickOutside(event: any) {
    if (!this.dateInput.nativeElement.contains(event.target)) {// or some s
      this.datedisplay = !this.datedisplay;
      // alert('outside');
    }
    // else if (event.target.offsetParent.nodeName != 'NGB-DATEPICKER') {
    //   alert(event.target.offsetParent.nodeName);
    //   this.datedisplay = !this.datedisplay;
    // }
  }

  thermalPrinting(){
    let popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    if(popupWin){
      popupWin.document.open();
      popupWin.document.write(`
    <html>
    <head>
      <meta charset="utf-8">
      <title></title>
    </head>
    <body onload="window.print();window.close()">
    <table width="100%" border="0" align="center" style="font-family: arial; font-size: 15px;">

    <tr>
        <td colspan="3" align="center"><h4 style="margin: 0;">Stand  :${(this.selectedStand ? this.selectedStand.standName : (this.standData ? this.standData.standName : this.employee.standName))}</h4></td>
      </tr>
      <tr>
        <td colspan="3" align="center"><h4 style="margin: 0;">Location  :${(this.selectedStand ? this.selectedStand.location.city.city : (this.standData ? this.standData.location.city.city : ''))}</h4></td>
      </tr>
      <tr>
        <td colspan="3" align="center"><hr style="margin:0px 0"></td>
      </tr>
      <tr>
        <td colspan="3" align="center"><h4 style="margin: 0;">Owner  :${(this.selectedStand ? this.selectedStand.ownerName : (this.standData ? this.standData.ownerName : this.employee.ownerId))}</h4></td>
      </tr>
      <tr>
        <td colspan="3" align="center"><h4 style="margin: 0;">Emp  :${this.employee.name}</h4></td>
      </tr>
      <tr>
        <td colspan="3" align="center"><hr style="margin:0px 0"></td>
      </tr>
      <tr>
        <td colspan="3" align="center"><h4 style="margin: 0;">Report Type  :${this.type}</h4></td>
      </tr>
      <tr>
        <td colspan="3" align="center"><h4 style="margin: 0;">Login  :${this.getDate(this.loginSessionData.start_time)}</h4></td>
      </tr>
      <tr>
        <td colspan="3" align="center"><h4 style="margin: 0;">Logout  :${this.getDate(this.loginSessionData.end_time)}</h4></td>
      </tr>
       
    </table>
    </body>
    </html>
  
    `);
    }
  }


}
