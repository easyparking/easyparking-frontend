import { DataService } from 'src/app/shared/services/data.service';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';
import { MatAccordion } from '@angular/material/expansion';
// import { MatAccordion } from '@angular/material';
import { Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { Console } from 'console';
import * as moment from 'moment';
import { Router } from '@angular/router';


@Component({
  selector: 'app-owner-dashboard',
  templateUrl: './owner-dashboard.component.html',
  styleUrls: ['./owner-dashboard.component.scss']
})
export class OwnerDashboardComponent implements OnInit {


  userData: any
  selectedStand: any
  standsData: any;
  cardsData: any = []
  tableData: any = []
  pendingCount: any = 0;
  entryCount: any = 0;
  exitedCount: any = 0;
  canclledCount: any = 0;
  offlineCardsCount: any = 0;
  totalCollectionCount: any = 0
  pendingOnLogoutCount = 0;
  loader: boolean = false;
  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'Parkings Count' },
  ];
  public lineChartData1: ChartDataSets[] = [
    { data: [], label: 'Collection' },
  ];
  public lineChartLabels: Label[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];
  // public lineChartOptions: (ChartOptions & { annotation: any }) = {
  //   responsive: true,
  // };
  public lineChartColors: Color[] = [
    { // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType :ChartType  = 'line';
  public lineChartPlugins = [];

  constructor(private _decryptService: EncryptDecryptService,
    private _dataService: DataService,
    private router: Router) {
  }

  ngOnInit(): void {
    this.loader = true
    this.userData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    if(!this.userData.data){
    this.router.navigateByUrl('/login');
  }
    this.getDashboardDetails();
  }

  getStandsArray() {
    this.loader = true
    this._dataService.getApi(environment.getStandsByOwnerURL + this.userData.data.ownerId)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == "success") {
          // this.loader = false
          this.standsData = res.data
          this.selectedStand = res.data[0]
          this.getStandByIdData()
        }
        else {
          this.standsData = []
          // this.loader = false
        }
      }, (error) => {
        this.standsData = []
        // this.loader = false
      })
  }

  getDashboardDetails() {
    this.loader = true
    this._dataService.getApi(environment.getOwnerDashboardURL + this.userData.data.ownerId)
      .subscribe((res) => {
        this.getStandsArray()
        if (res && res.status.toLowerCase() == 'success') {
          this.cardsData = res.data
        }
        else {
          this.cardsData = []
        }
      })
  }
  getStandByIdData() {
    this.loader = true
    this.pendingCount = 0;
    this.pendingOnLogoutCount = 0;
    this.entryCount = 0;
    this.exitedCount = 0;
    this.canclledCount = 0;
    this.offlineCardsCount = 0;
    this.totalCollectionCount = 0
    this._dataService.getApi(environment.getOwnerDashboardDetailsURL + "?standId=" + this.selectedStand.standId + "&ownerId=" + this.selectedStand.ownerId)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.getGraphData(res.sessionResult);
          for (let i = 0; i < res.data.length; i++) {
            this.entryCount += res.data[i].total
            this.offlineCardsCount += res.data[i].offlineCards
            this.totalCollectionCount += res.data[i].collection

            // res.data[i].pending = res.data[i].total - (res.data[i].exit + res.data[i].cancelled)
            this.pendingCount += res.data[i].pending
            this.pendingOnLogoutCount += res.data[i].pendingOnLogout
            this.exitedCount += res.data[i].exit;
            this.canclledCount += res.data[i].cancelled

            this.tableData = res.data
            
          }
          this.loader = false
        }
        else {
          this.tableData = []
          this.loader = false
        }
      })
  }
  getGraphData(res:any) {
    // let data = {
    // "startDate":  res ? res[0].start_time : moment().startOf('month'),
    // "endDate": res ? moment() : moment().endOf('month'),
    // }
    let data = {
      "startDate": moment().startOf('month').format('YYYY-MM-DD hh:mm'),
      "endDate": moment().format('YYYY-MM-DD hh:mm'),
      }
      console.log(data)
    this.lineChartData = [
      { data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], label: 'Parkings Count' },
    ];
    this.lineChartData1 = [
      { data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], label: 'Collection' },
    ];
    this._dataService.postApi(environment.getGraphDataURL + this.userData.data.ownerId, data)
      .subscribe((res) => {
        this.loader = false
        if (res && res.status.toLowerCase() == 'success') {
          res.parkingsCount.forEach((element:any) => {
            if(this.lineChartData[0].data){
              this.lineChartData[0].data[Number(moment(element.dateAndTimeOfParking).format('D'))-1] = element.count
              this.lineChartData = this.lineChartData.filter((ele:any)=>ele)  
            }
          }); 
          res.collectionCount.forEach((element:any) => {
            if(this.lineChartData1[0].data){
              this.lineChartData1[0].data[Number(moment(element.dateAndTimeOfExit).format('D'))-1] = element.totalPrice
              this.lineChartData1 = this.lineChartData1.filter((ele:any)=>ele)  
            }
          });
          
        }
      })
  }
  standOptionChanged($event: any) {
    this.getStandByIdData()
  }

  getCss(i: any) {

    return ((i % 8 == 0) ? "overview-item--c1" :
      ((i % 8 == 1) ? "overview-item--c2" :
      ((i % 8 == 2) ? "overview-item--c3" :
      ((i % 8 == 3) ? "overview-item--c4" :
      ((i % 8 == 4) ? 'overview-item--c5' :
      ((i % 8 == 5) ? 'overview-item--c6' :
      ((i % 8 == 6) ? 'overview-item--c7' : 'overview-item--c8')))))))

  }
}
