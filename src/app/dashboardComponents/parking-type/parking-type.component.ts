
import { Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment.prod';
import swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/shared/services/data.service';

@Component({
  selector: 'app-parking-type',
  templateUrl: './parking-type.component.html',
  styleUrls: ['./parking-type.component.css']
})
export class ParkingTypeComponent implements OnInit {
  page = 1;
  pageSize = 10;
  parkingsList: any = []
  searchKey: any = '';
  pager: any
  isEdit: boolean = false
  addEditParkingForm!: FormGroup
  spinner: boolean = false
  loader: boolean = false
  currentData: any;
  count: any = 0;
  constructor(private modalService: NgbModal,
    private _dataService: DataService,
    private _fb: FormBuilder,
    private activeModal: NgbActiveModal) {
    this.addEditParkingForm = this._fb.group({
      parkingName: ['', [Validators.required]]
    })
  }

  ngOnInit(): void {
    this.searchParkingsList(1)
  }

  searchParkingsList(pageNum: any) {
    this.loader = true
    this._dataService.getApi(environment.getParkringTypesListURL + '?size=' + this.pageSize + '&pageNo=' + pageNum + '&searchText=' + this.searchKey)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.loader = false
          this.parkingsList = res.data;
          this.count = res.totalCount;
          this.pager = this._dataService.getPager(res.totalCount, pageNum, this.pageSize)
        }
        else {
          this.loader = false
          this.parkingsList = []
        }
      }, (error) => {
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }

  setPage(event: any) {

    this.page = event
    this.searchParkingsList(this.page)
  }

  openModal(content: any, type: any, data?: any) {
    this.currentData = data
    this.addEditParkingForm.reset()
    this.spinner = false
    if (type == 'add') {
      this.isEdit = false
    }
    else {
      this.isEdit = true
      this.addEditParkingForm.controls.parkingName.setValue(data.type)
    }
    this.activeModal = this.modalService.open(content, { size: 'lg', centered: true });
  }


  addParking() {
    this.spinner = true
    let dataToAPI = {
      "type": this.addEditParkingForm.value.parkingName,
    }
    this._dataService.postApi(environment.addParkingTypeURL, dataToAPI)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          swal.fire("", res.message, "success");
          setTimeout(() => {
            swal.close()
          }, 3000);
          this.spinner = false;
          this.activeModal.close()
          this.searchParkingsList(1)
        }
        else {
          this.spinner = false
          swal.fire("Warning!", res.message, "warning");
          setTimeout(() => {
            swal.close()
          }, 3000);
        }

      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }
  updateParking() {
    this.spinner = true
    let dataToAPI = {
      "type": this.addEditParkingForm.value.parkingName,
      "id": this.currentData._id
    }
    this._dataService.putApi(environment.updateParkingTypeURL, dataToAPI)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          swal.fire("", res.message, "success");
          setTimeout(() => {
            swal.close()
          }, 3000);
          this.spinner = false;
          this.activeModal.close()
          this.searchParkingsList(1)
        }
        else {
          this.spinner = false
          swal.fire("Warning!", res.message, "warning");
          setTimeout(() => {
            swal.close()
          }, 3000);
        }

      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }

  deleteParking(planType: any) {
    swal.fire({
      title: 'Are you sure to delete this record?',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        // swal.fire('Saved!', '', 'success')
        this._dataService.deleteApi(environment.deleteParkingTypeURL + '/' + planType._id)
          .subscribe((res) => {
            if (res && res.status.toLowerCase() == 'success') {
              swal.fire("", res.message, "success");
              setTimeout(() => {
                swal.close()
              }, 3000);
              this.spinner = false;
              this.activeModal.close()
              this.searchParkingsList(1)
            }
            else {
              this.spinner = false
              swal.fire("Warning!", res.message, "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }

          }, (error) => {
            this.spinner = false
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          })
      } else if (result.isDenied) {
      }
    })
  }

}
