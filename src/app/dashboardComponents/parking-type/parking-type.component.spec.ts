import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkingTypeComponent } from './parking-type.component';

describe('ParkingTypeComponent', () => {
  let component: ParkingTypeComponent;
  let fixture: ComponentFixture<ParkingTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParkingTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkingTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
