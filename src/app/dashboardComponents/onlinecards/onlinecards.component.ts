import { MatAccordion } from '@angular/material/expansion';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/shared/services/data.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-onlinecards',
  templateUrl: './onlinecards.component.html',
  styleUrls: ['./onlinecards.component.css']
})
export class OnlinecardsComponent implements OnInit {
  active = 1;
  loader = true;
  cardsList: any = [];
  owners: { name: string, ownerId: string }[] = [];
  standsList: { standId: string, standName: string }[] = [];
  selectedOwner = "";
  selectedStand:any;
  totalCounts:any = [];
  standsCount: any;

  constructor(private _dataService: DataService) {
  }

  ngOnInit(): void {
    this.search();
    this.getOwners();
  }

  search() {
    const type = this.active == 1 ? 'online' : 'offline';
    this.getCardsList(type);
  }

  getCardsList(cardType: string) {
    this.loader = true;
    this._dataService.getApi(environment.getOfflineCardsByOwner + `?ownerId=${this.selectedOwner}&cardType=${cardType}`)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.totalCounts = res.totalCards;
          this.standsCount = res.standsCount;
          this.getCardDetailsByStand(cardType);
        }
      }, (error) => {
        this.loader = false;
      })
  }

  getCardDetailsByStand(cardType: string) {
    this._dataService.getApi(environment.getOfflineCardsByStand + `?ownerId=${this.selectedOwner}&standId=${this.selectedStand ? this.selectedStand.standId : ''}&cardType=${cardType}`)
      .subscribe((res) => {
        this.loader = false;
        if (res && res.status.toLowerCase() == 'success') {
          this.cardsList = [res.data];
        } else {
          this.cardsList = [];
        }
      }, (error) => {
        this.loader = false;
        this.cardsList = [];
      })
  }

  getOwners() {
    this._dataService.getApi(environment.getOwnersListURL).subscribe(res => {
      if (res.status == 'success') {
        this.owners = res.data && res.data.length ? res.data : [];
        this.selectedOwner = this.owners.length ? this.owners[0].ownerId : '';
        this.getStandsList(this.owners.length ? this.owners[0].ownerId : '')
      } else {
        this.owners = [];
      }
    })
  }

  getStandsList(ownerId:any) {
    this._dataService.getApi(environment.getStandsByOwnerURL + ownerId)
      .subscribe((response) => {
        if (response.status == 'success') {
          localStorage.setItem("standsArray", JSON.stringify(response.data))
          this.standsList = response.data;
          this.selectedStand = this.standsList.length ? this.standsList[0] : '';
        } else {
          this.standsList = []
        }
      })
  }
  copyTable() {
  //   var text = "S.No." +
  //     "\tVehicle No" +
  //     "\t\tType" +
  //     "\t\tMobile No." +
  //     "\t\tCustomer Name" +
  //     "\t\tRequest Date" +
  //     "\t\tMissing Date" +
  //     "\t\tRequest By" +
  //     "\t\tPolice Complaint logged?" +
  //     "\t\tLocated At" +
  //     "\t\tLocated On" +
  //     "\t\tRemarks"
  //   var textData = "";
  //   for (let i = 0; i < this.cardsList.length; i++) {
  //     textData += i + 1 +
  //       "\t\t" + (this.cardsList[i].vehicleNumber ? this.cardsList[i].vehicleNumber : 'N/A')
  //       + "\t\t" + (this.cardsList[i].type ? this.getVehicleType(this.cardsList[i].type) : 'N/A')
  //       + "\t" + (this.cardsList[i].phoneNumber ? this.cardsList[i].phoneNumber : 'N/A')
  //       + "\t" + (this.cardsList[i].insuranceValidty ? this.cardsList[i].insuranceValidty : 'N/A')
  //       + "\t" + (this.cardsList[i].vehicleNumber ? this.cardsList[i].vehicleNumber : 'N/A')
  //       + "\t" + (this.cardsList[i].missingDate ? this.getDate(this.cardsList[i].missingDate) : 'N/A')
  //       + "\t" + (this.cardsList[i].requestDate ? this.getDate(this.cardsList[i].requestDate) : "N/A")
  //       + "\t" + (this.cardsList[i].requestBy ? this.cardsList[i].requestBy : 'N/A')
  //       + "\t" + (this.cardsList[i].policeComplaintMade ? this.cardsList[i].policeComplaintMade : 'N/A')
  //       + "\t" + ('N/A')
  //       + "\t" + ('N/A')
  //       + "\t" + (this.cardsList[i].remarks ? this.cardsList[i].remarks : 'N/A') + "\n"
  //   }
  //   let selBox = document.createElement('textarea');

  //   selBox.style.position = 'fixed';
  //   selBox.style.left = '0';
  //   selBox.style.top = '0';
  //   selBox.style.opacity = '0';
  //   selBox.value = text + '\n' + textData;

  //   document.body.appendChild(selBox);
  //   selBox.focus();
  //   selBox.select();
  //   document.execCommand('copy');
  //   document.body.removeChild(selBox);
  //   if (selBox) {
  //     // window.print()
  //   }
  //   // this.MessageService.add({ severity: 'success', summary: '', detail: 'Link copied to Clipboard' });
  //   else {
  //     // this.MessageService.add({ severity: 'warning', summary: '', detail: 'Unable to copy the link to Clipboard' });
  //   }
  }

  async printPDF() {
  //   var doc = new jsPDF('p', 'pt');
  //   var col = [["S.No.",
  //     "Vehicle No",
  //     "Type",
  //     "Mobile No.",
  //     "Insurance Validity",
  //     "Missing Date",
  //     "Request Date",
  //     "Request By",
  //     "Police Complaint logged?",
  //     "Located At",
  //     "Located On",
  //     "Remarks"]];
  //   var rows: any = [];
  //   await this.wantedVehicleList.forEach((element: any, i: number) => {
  //     var temp = [i + 1,
  //     element.vehicleNumber || 'N/A',
  //     this.getVehicleType(element.type) || 'N/A',
  //     element.phoneNumber || 'N/A',
  //     element.insuranceValidty || 'N/A',
  //     element.missingDate ? this.getDate(element.missingDate) : 'N/A',
  //     element.requestDate ? this.getDate(element.requestDate) : 'N/A',
  //     element.requestBy || 'N/A',
  //     element.policeComplaintMade || 'N/A',
  //       "N/A",
  //       "N/A",
  //     element.remarks || 'N/A'];
  //     rows.push(temp);
  //   });
  //   await autoTable(doc, {
  //     head: col,
  //     body: rows,
  //     didDrawCell: (data) => { },
  //   });
  //   doc.output('dataurlnewwindow')
  }
}
