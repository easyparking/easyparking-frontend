import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlinecardsComponent } from './onlinecards.component';

describe('OnlinecardsComponent', () => {
  let component: OnlinecardsComponent;
  let fixture: ComponentFixture<OnlinecardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnlinecardsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlinecardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
