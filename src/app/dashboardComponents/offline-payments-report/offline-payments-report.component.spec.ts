import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfflinePaymentsReportComponent } from './offline-payments-report.component';

describe('OfflinePaymentsReportComponent', () => {
  let component: OfflinePaymentsReportComponent;
  let fixture: ComponentFixture<OfflinePaymentsReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfflinePaymentsReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfflinePaymentsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
