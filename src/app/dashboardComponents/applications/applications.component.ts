import { DataService } from './../../shared/services/data.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import * as moment from 'moment';
import { ExportToCsv } from 'export-to-csv';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.css']
})
export class ApplicationsComponent implements OnInit {

  pageNum = 1;
  pageSize = 10;
  applicationsList: any = []
  loader: boolean = false
  pager: any
  searchText: any = ''
  excelApplicationsList: any;
  count: any;
  constructor(private _dataService: DataService) {
  }

  ngOnInit(): void {
    this.getApplicationsList(1);

  }
  getApplicationsList(pageNum: any) {
    this.loader = true
    this._dataService.getApi(environment.getApplicationsURL + '?pageNo=' + pageNum + '&size=' + this.pageSize + '&searchText=' + this.searchText)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.applicationsList = res.data;
          this.count = res.totalCount
          this.pager = this._dataService.getPager(res.totalCount, pageNum, this.pageSize)
          this.loader = false
        }
        else {
          this.applicationsList = []
          this.loader = false
        }
      })
  }

  getExcelApplicationsList(type: any) {
    // this.loader = true
    this._dataService.getApi(environment.getApplicationsURL + '?pageNo=' + 1 + '&size=' + 1000 + '&searchText=' + this.searchText)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.excelApplicationsList = res.data;
          // this.loader = false
          this.excelApplicationsList.forEach((element: any, i: number) => {
            delete element._id;
            // delete element.createdDate;
            // delete element.employeeId;
            // delete element.lastLoginSession;
            // delete element.lastLogoutSession;
            // delete element.loginStatus;
            // delete element.onlinePaymentPercentage;
            // delete element.planType;
            // delete element.privileges;
            // delete element.role;
            // delete element.standId;
            delete element.applicantId;
            delete element.__v;
            // if(i==this.excelApplicationsList.length){
            //   this.excelApplicationsHedders =  Object.keys(element);
            // }
          });

          if (type == 'copy') {
            this.copyTable()
          }
          else if (type == 'pdf') {
            this.printPDF()
          }
          else if (type == 'excel') {
            this.exportAsXLSX()
          }
          else if (type == 'csv') {
            this.getCSV()
          }
        } else {
          this.excelApplicationsList = []
          // this.loader = false
        }

      })
  }

  setPage(event: any) {
    this.pageNum = event
    this.getApplicationsList(1);
  }
  copyTable() {
    var text = "S.No." +
      "\tName" +
      "\t\tQualification" +
      "\t\tAge" +
      "\t\tEmail" +
      "\t\tPhone" +
      "\t\tState" +
      "\t\tCity" +
      "\t\tTown" +
      "\t\tAddress" +
      "\t\tDate of Application" +
      "\t\tMessage"

    var textData = "";
    for (let i = 0; i < this.applicationsList.length; i++) {
      textData += i + 1 +
        + "\t\t" + (this.applicationsList[i].name ? this.applicationsList[i].name : 'N/A')
      "\t\t" + (this.applicationsList[i].qualification ? this.applicationsList[i].qualification : 'N/A')
        + "\t" + (this.applicationsList[i].age ? this.applicationsList[i].age : 'N/A')
        + "\t" + (this.applicationsList[i].email ? this.applicationsList[i].email : 'N/A')
        + "\t" + (this.applicationsList[i].phone ? this.applicationsList[i].phone : 'N/A')
        + "\t" + (this.applicationsList[i].state ? this.applicationsList[i].state : 'N/A')
        + "\t" + (this.applicationsList[i].city ? this.applicationsList[i].city : 'N/A')
        + "\t" + (this.applicationsList[i].town ? this.applicationsList[i].town : 'N/A')
        + "\t" + (this.applicationsList[i].address ? this.applicationsList[i].address : 'N/A')
        + "\t" + (this.applicationsList[i].createdDate ? this.getTime(this.applicationsList[i].createdDate) : 'N/A')
        + "\t" + (this.applicationsList[i].message ? this.applicationsList[i].message : 'N/A') + "\n"
    }
    let selBox = document.createElement('textarea');

    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = text + '\n' + textData;

    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    if (selBox) {
      // window.print()
    }
    // this.MessageService.add({ severity: 'success', summary: '', detail: 'Link copied to Clipboard' });
    else {
      // this.MessageService.add({ severity: 'warning', summary: '', detail: 'Unable to copy the link to Clipboard' });
    }
  }

  async printPDF() {
    var doc = new jsPDF('landscape', 'pt');
    var col = [["S.No.",
      "Name",
      "Qualification",
      "Age",
      "Email",
      "Phone",
      "State",
      "City",
      "Town",
      "Address",
      "Date of Application",
      "Message"]];
    var rows: any = [];
    await this.applicationsList.forEach((element: any, i: number) => {
      var temp = [i + 1,
      (element.name ? element.name : 'N/A'),
      (element.qualification ? element.qualification : 'N/A'),
      (element.age ? element.age : 'N/A'),
      (element.email ? element.email : 'N/A'),
      (element.phone ? element.phone : 'N/A'),
      (element.state ? element.state : 'N/A'),
      (element.city ? element.city : 'N/A'),
      (element.town ? element.town : 'N/A'),
      (element.address ? element.address : 'N/A'),
      (element.createdDate ? this.getTime(element.createdDate) : 'N/A'),
      (element.message ? element.message : 'N/A')];
      rows.push(temp);
    });
    await autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (data) => { },
    });
    // doc.output('dataurlnewwindow')
      window.open(URL.createObjectURL(doc.output('blob')), '_blank')
  }
  getCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Login Activity',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };
    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(this.excelApplicationsList);
  }

  exportAsXLSX(): void {
    this._dataService.exportAsExcelFile(this.excelApplicationsList, 'sample');
  }
  getTime(date: any) {
    if (date) {
      return moment(date).format("DD-MM-YYYY HH:mm:ss");
    } else {
      return 'N/A'
    }
  }

}
