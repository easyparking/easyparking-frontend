import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from 'src/app/shared/services/data.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';
import * as moment from 'moment';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
declare var window: any;
@Component({
  selector: 'app-slip-lost-vehicles',
  templateUrl: './slip-lost-vehicles.component.html',
  styleUrls: ['./slip-lost-vehicles.component.css']
})
export class SlipLostVehiclesComponent implements OnInit {
  searchForm!: FormGroup
  filteredPhoneNumbers!: Observable<any[]>;
  filteredVehicles!: Observable<any[]>;
  isLoading: boolean = false;
  userData: any;
  selectedStand: any;
  parkingsList: any = []
  spinner1: boolean = false;
  standData: any
  spinner2: boolean = false;
  loader: boolean = false
  entryVehicleData: any;
  showOTP: boolean = false;
  otpValue: any
  constructor(private _dataService: DataService,
    private _fb: FormBuilder,
    private _decryptService: EncryptDecryptService,
    private router: Router) {
    this.searchForm = this._fb.group({
      vehicleNumber: ['', Validators.pattern("^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$")],
      phNumber: ['', [Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]],
      altPhNumber: ['', [Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]],
      otp: ['']
    })


  }

  ngOnInit(): void {
    this.userData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    if(!this.userData.data){
    this.router.navigateByUrl('/login');
  }
    // this.searchParkingData()
    if (localStorage.getItem("standSelected")) {
      let standsArray = JSON.parse(localStorage.getItem("standsArray") || "")
      this.selectedStand = standsArray.filter((item: any) => item.standId == localStorage.getItem("standSelected"))
    }
    this.filteredPhoneNumbers = this.searchForm.controls.phNumber.valueChanges
      .pipe(
        startWith(''),
        debounceTime(1000),
        distinctUntilChanged(),
        switchMap(val => {
          // this.isLoading = true
          return this.filter(val, environment.getDetailsByPhURL || '')
        })
      );
    // getParkingDetailsByOwner
    this.filteredVehicles = this.searchForm.controls.vehicleNumber.valueChanges
      .pipe(
        startWith(''),
        debounceTime(1000),
        distinctUntilChanged(),
        switchMap(val => {
          // this.isLoading = true
          return this.filter(val,
            (localStorage.getItem("standSelected") ? (environment.searchParkingListURL + this.selectedStand[0].standId + "&vehicleNumber=")
              : (environment.getParkingDetailsByOwnerURL + this.userData.data.ownerId + "&vehicleNumber="))
            || '')
        })
      );
    if (this.userData.data.role == 'owner') {
      this.getSlipLostVehicleData()
    }
  }
  filter(val: string, dataURL: any): Observable<any[]> {

    // call the service which makes the http-request
    return this._dataService.getApi(dataURL + val)
      .pipe(
        map((response) => {
          this.isLoading = false
          // this.isEntry = true
          return response.data
        }
        ))


  }
  getSlipLostVehicleData() {
    this.loader = true
    // if (this.searchForm.controls.vehicleNumber.value && this.searchForm.controls.phNumber.value) {

    // (localStorage.getItem("standSelected") ? (environment.searchParkingListURL + this.selectedStand[0].standId + "&vehicleNumber=")
    // : (environment.getParkingDetailsByOwnerURL + this.userData.data.ownerId + "&vehicleNumber=")


    this._dataService.getApi(((this.userData.data.role != 'owner') ? environment.getSlipLostVehiclesURL : (environment.getSlipLostVehiclesbyOwnerURL + this.userData.data.ownerId))
      + '?vehicleNumber=' + (this.searchForm.controls.vehicleNumber.value ? this.searchForm.controls.vehicleNumber.value : '')
      + '&phone=' + (this.searchForm.controls.phNumber.value ? this.searchForm.controls.phNumber.value : ''))
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.loader = false
          this.parkingsList = res.data
        }
        else {
          // swal.fire("Warning!", "No search data found", "warning");
          this.parkingsList = []
          this.loader = false
        }
      }, (error) => {
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
    // }
    // else {
    //   this.loader = false
    //   this.parkingsList = []
    // }

  }
  selectedOption($event: any, type: any) {
    this.entryVehicleData = $event.option.value
    if (type == 'vehicle') {

      this.searchForm.controls.vehicleNumber.setValue($event.option.value.vehicleNumber)
      this.searchForm.controls.phNumber.setValue($event.option.value.phone)
      // this.getSlipLostVehicleData()
    }

    else {
      // this.searchForm.controls.phNumber.setValue($event.option.value.phone)
      this.searchForm.controls.phNumber.markAsDirty()
      this.searchForm.controls.phNumber.markAsTouched()
      // this.getSlipLostVehicleData()
    }
    if (this.userData.data.role == 'owner') {
      this.getSlipLostVehicleData()
    }
  }

  searchPhNumber() {
    if (this.userData.data.role != 'owner') {
      if (this.searchForm.controls.vehicleNumber.value) {
        this.searchForm.controls.vehicleNumber.setErrors(null)
        if (this.searchForm.controls.phNumber.value && this.searchForm.controls.phNumber.value.length == 10 && this.searchForm.controls.phNumber.value != this.entryVehicleData.phone) {
          this.searchForm.controls.phNumber.setErrors({ invalidPh: true })
        }
        else if (this.searchForm.controls.phNumber.value.length == 0 || !this.searchForm.controls.phNumber.value) {
          this.searchForm.controls.phNumber.setErrors({ invalidPh: false })
          this.searchForm.controls.phNumber.setErrors({ required: true })
        }
      }
      else {

        this.searchForm.controls.vehicleNumber.setErrors({ invalid: true })
        this.searchForm.controls.vehicleNumber.setErrors({ required: true })
      }
    }
    // else{
    //   this.getSlipLostVehicleData()
    // }
  }
  sendOTP() {
    this._dataService.getApi(environment.sendOTPURL + '/' + this.searchForm.controls.altPhNumber.value ? this.searchForm.controls.altPhNumber.value : this.searchForm.controls.phNumber.value)
      .subscribe((res) => {
        if (res.status.toLowerCase() == 'success') {
          this.showOTP = true
          this.otpValue = res.otp
          // this.searchForm.controls.otp.setValue(res.otp)
          this.searchForm.controls.otp.setErrors({ required: true })
          // this.searchForm.controls.otp.markAsDirty()
          // this.searchForm.controls.otp.markAsTouched()
        }
        else {
          this.searchForm.controls.otp.setErrors(null)
          swal.fire("Warning!", res.message, "warning");
          setTimeout(() => {
            swal.close()
          }, 3000);
        }
      }, (error) => {
        this.searchForm.controls.otp.setErrors(null)
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })

  }

  enterOTPValue() {
    if (this.searchForm.controls.otp.value && this.searchForm.controls.otp.value.length == 6) {
      if (this.searchForm.controls.otp.value != this.otpValue) {
        this.searchForm.controls.otp.setErrors({ invalidOTP: true })
      }
      else {
        this.searchForm.controls.otp.setErrors(null)
      }
    }
    else if (this.searchForm.controls.otp.value.length == 0) {
      this.searchForm.controls.otp.setErrors({ required: true })
    }
  }
  getStartTime(date: any) {
    if (date) {
      return moment(date).format("DD-MM-YYYY HH:mm:ss");
    }
    else {
      return 'N/A'
    }
  }

  addExitEntry(data?: any) {
    this.spinner1 = true;
    let dataToAPI = {
      "parkingId": data.parkingId,
      "status": "Exit",
      "closedEmpId": this.userData.data.employeeId,
      "closedEmpName": this.userData.data.name,
      "closedEmpMobile": this.userData.data.phoneNumber,
      "standId": data.standId,
      "dateAndTimeOfParking": data.dateAndTimeOfParking.toString(),
      "dateAndTimeOfExit": new Date().toString(),
      "vehicleType": data.vehicleType,
      "cardNumber": data.cardNumber,
      "isSlipLost": true
      //     "parkingId": "P3",
      // "status": "Exit",
      // "closedEmpName": "anand",
      //   "closedEmpMobile": "9988998899"
    }
    this._dataService.putApi(environment.updateParkingURL, dataToAPI)
      .subscribe((res) => {
        if (res && res.status == 'success') {
          // swal.fire("", res.message, "success");

          swal.fire({
            title: res.message,
            icon: 'success',
            confirmButtonText: `Ok`,
          }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
              this.getStandData(res.data)
              // this.exitPrintPreview()
            } else if (result.isDenied) {
            }
          })
          this.spinner1 = false
          this.getSlipLostVehicleData()
          this.searchForm.reset()
        }
        else {
          this.spinner1 = false
          swal.fire("Warning!", res.message, "warning");
          setTimeout(() => {
            swal.close()
          }, 3000);
        }

      }, (error) => {
        this.spinner1 = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }

  getStandData(parkingData: any) {
    this._dataService.getApi(environment.getStandDataByIdURL + parkingData.standId)
      .subscribe((res) => {
        if (res && res.data && res.status.toLowerCase() == 'success') {
          this.standData = res.data
          this.exitPrintPreview(parkingData)
        }
        else {
          swal.fire("Warning!", res.message, "warning");
          setTimeout(() => {
            swal.close()
          }, 3000);
        }
      }, (error) => {
        this.spinner2 = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }

  exitPrintPreview(data: any) {
    let exitTime;
    let entryTime;
    let text1 = this.standData.addressLine1 ? this.standData.addressLine1 : ''
    let text2 = this.standData.addressLine2 ? this.standData.addressLine2 : ''
    let text3 = this.standData.addressLine3 ? this.standData.addressLine3 : ''
    let text4 = this.standData.addressLine4 ? this.standData.addressLine4 : ''
    if (data.dateAndTimeOfParking) {
      entryTime = moment(data.dateAndTimeOfParking).format("DD-MM-YYYY HH:mm:ss");
    }
    else {
      entryTime = '--'
    }
    if (data.dateAndTimeOfExit) {
      exitTime = moment(data.dateAndTimeOfExit).format("DD-MM-YYYY HH:mm:ss");
    }
    else {
      exitTime = "--"
    }



    let popupWin;
    // let printContents = document.getElementById('print-section').innerHTML
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
  <html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body onload="window.print();window.close()">
  <table width="100%" cellspacing="3" cellpadding="3" border="0" align="center" style="font-family: arial; font-size: 14px;">
    <tr>
      <td colspan="3" align="center"><h3 style="margin: 0;">--Vehicle Check out--</h3></td>
    </tr>
    <tr>
      <td colspan="3" align="center"><h4 style="margin: 0;">${data.standName}</h4></td>
    </tr>
    <tr>
      <td colspan="3" align="center"><h4 style="margin: 0;">${text1}</h4></td>
    </tr>
    <tr>
    <td colspan="3" align="center"><h4 style="margin: 0;">${text2}</h4></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><h4 style="margin: 0;">${text3}</h4></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><h4 style="margin: 0;">${text4}</h4></td>
  </tr>
    <tr>
    <td>Parking ID:</td>
    <td>${data.parkingId}</td>
  </tr>
  <tr>
  <td>Veh No:</td>
  <td>${data.vehicleNumber}</td>
</tr>
    <tr>
      <td>Entry:</td>
      <td>${entryTime}</td>
    </tr>
    <tr>
      <td>Exit:</td>
      <td>${exitTime}</td>
    </tr>

    <tr>
      <td>Duration:</td>
      <td>1</td>
    </tr>
    <tr>
      <td>Bill Amount:</td>
      <td>${data.amount}</td>
    </tr>
  </table>
  </body>
  </html>

  `);
    // ${finalData}
    popupWin.document.close();
  }
}
