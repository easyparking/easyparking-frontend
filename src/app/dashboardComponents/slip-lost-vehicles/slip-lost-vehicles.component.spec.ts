import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SlipLostVehiclesComponent } from './slip-lost-vehicles.component';

describe('SlipLostVehiclesComponent', () => {
  let component: SlipLostVehiclesComponent;
  let fixture: ComponentFixture<SlipLostVehiclesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SlipLostVehiclesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SlipLostVehiclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
