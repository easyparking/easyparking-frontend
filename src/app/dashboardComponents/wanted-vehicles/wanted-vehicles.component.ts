import { MatAccordion } from '@angular/material/expansion';
// import { MatAccordion } from '@angular/material';
import { Component, Directive, EventEmitter, Injectable, Input, OnInit, Output, QueryList, ViewChild, ViewChildren, ViewEncapsulation } from '@angular/core';
import { NgbdSortableHeader } from 'src/app/directives/tableDirective';
import { NgbActiveModal, NgbDate, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/shared/services/data.service';
import { Router } from '@angular/router';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';
import { environment } from 'src/environments/environment.prod';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NgbDateParserFormatter, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { element } from 'protractor';
import * as moment from 'moment';
import { NgbDateFRParserFormatt } from 'src/app/Pipes/Dateparserformat';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable';
import { Observable, of, OperatorFunction } from 'rxjs';
import { catchError, distinctUntilChanged, map, tap, switchMap, startWith } from 'rxjs/operators';
import { ExportToCsv } from 'export-to-csv';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-wanted-vehicles',
  templateUrl: './wanted-vehicles.component.html',
  styleUrls: ['./wanted-vehicles.component.css'],
  providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatt }]
})
export class WantedVehiclesComponent implements OnInit {
  datemodel!: NgbDateStruct;
  @ViewChild(MatAccordion)
  @ViewChildren(NgbdSortableHeader)
  headers!: QueryList<NgbdSortableHeader>;
  public accordion!: MatAccordion;
  isLoading: boolean = false
  page = 1;
  pageSize = 10;
  vehicleForm!: FormGroup;
  searchForm!: FormGroup;
  count: number = 0;
  pager: any;
  results$: any;
  subject = new Subject()
  selectedStand: any;
  userData: any;
  spinner: boolean = false;
  currentVehicleData: any;
  showEdit: boolean = false;
  vehicleTypes: any;
  loader = false;
  wantedVehicleList: any = [];
  filteredVehicles!: Observable<any[]>;
  isPriceRuleAdded: boolean = false;
  excelWantedVehicleList: any;
  currentDate = { year: new Date().getFullYear(), month: new Date().getMonth(), day: new Date().getDate() }
  matchedVehicle: any;
  constructor(private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private _fb: FormBuilder,
    private _dataService: DataService,
    private _decryptService: EncryptDecryptService,
    private _ngbFormat: NgbDateParserFormatter,
    private router: Router) {
    this.vehicleForm! = this._fb.group({
      vehicleNumber: ['', [Validators.required, Validators.pattern("^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$")]],
      type: ['Bicycle', [Validators.required]],
      mobileNumber: ['', [Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]],
      insuranceTo: ['', Validators.required],
      insuranceFrom: ['', Validators.required],
      missingDate: ['', Validators.required],
      requestedDate: ['', Validators.required],
      requestedBy: ['', Validators.required],
      isPoliceComplaintLogged: ['No', Validators.required],
      remarks: ['', Validators.required],
    })
    this.searchForm! = this._fb.group({
      searchKey: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.userData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    if(!this.userData.data){
    this.router.navigateByUrl('/login');
  }
    this.results$ = this.searchForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe((data: any) => this.getVehiclesList(1));

    this.filteredVehicles = this.vehicleForm.controls.vehicleNumber.valueChanges
      .pipe(
        startWith(''),
        debounceTime(1000),
        distinctUntilChanged(),
        switchMap(val => {
          // this.isLoading = true
          return this.filter(val, (environment.searchParkingListURL + this.selectedStand[0].standId + "&vehicleNumber=") || '')
        })
      );
    this.getVehiclesList(1);
  }

  filter(val: string, dataURL: any): Observable<any[]> {
    // call the service which makes the http-request
    return this._dataService.getApi(dataURL + val)
      .pipe(
        map((response) => {
          // this.isLoading = false
          // this.isEntry = true
          return response.data
        }
        )
      )
  }

  selectedOption($event: any) {
    // this.entryVehicleData = $event.option.value

  }

  checkDate(endDate: any) {
    if ((new Date(endDate) < new Date())) {
      return true;
    } else {
      return false
    }
  }

  getVehicleType(type: string) {
    if (type.trim() == "bicycle") {
      return "Bicycle";
    } else if (type.trim() == "twoWheeler") {
      return "2 W";
    } else if (type.trim() == "threeWheeler") {
      return "3 W";
    } else if (type.trim() == "fourWheeler") {
      return "4 W";
    } else {
      return type.trim();
    }
  }

  getVehicles(vehicelNum: any, content1: any) {
    // environment.getVehiclesList + '?size=10&pageNo=1&searchText='
    this._dataService.getApi(environment.getParkingListUrl + '?size=10&pageNo=1&searchText=' + vehicelNum)
      .subscribe((res) => {
        this.loader = false;
        if (res && res.status.toLowerCase() == 'success') {
          if (res.data.length > 0) {
            this.matchedVehicle = res.data[0]
            this.modalService.open(content1, { size: 'lg', centered: true });
            // swal.fire("", "Vehice matched", "success");
            // setTimeout(() => {
            //   swal.close()
            // }, 3000);
          } else {
            swal.fire("", "Added vehicle succefully, No vehicle matched with existing records", "success")
            setTimeout(() => {
              swal.close()
            }, 3000);
          }
        }
      }, (error) => {
      })
  }

  addVehicle(modal: any, content1: any) {
    if (!this.showEdit) {
      let missingDate = new Date(this.vehicleForm.controls.missingDate.value.month + '/' + this.vehicleForm.controls.missingDate.value.day + '/' + this.vehicleForm.controls.missingDate.value.year)
      let startDate = new Date(this.vehicleForm.controls.requestedDate.value.month + '/' + this.vehicleForm.controls.requestedDate.value.day + '/' + this.vehicleForm.controls.requestedDate.value.year)
      let insuranceFrom: any = '';
      if (this.vehicleForm.controls.insuranceFrom.value && this.vehicleForm.controls.insuranceFrom.value.month) {
        insuranceFrom = new Date(this.vehicleForm.controls.insuranceFrom.value.month + '/' + this.vehicleForm.controls.insuranceFrom.value.day + '/' + this.vehicleForm.controls.insuranceFrom.value.year)
      }
      let insuranceTo: any = '';
      if (this.vehicleForm.controls.insuranceTo.value && this.vehicleForm.controls.insuranceTo.value.month) {
        insuranceTo = new Date(this.vehicleForm.controls.insuranceTo.value.month + '/' + this.vehicleForm.controls.insuranceTo.value.day + '/' + this.vehicleForm.controls.insuranceTo.value.year)
      }
      let dataToAPI = {
        "vehicleNumber": this.vehicleForm.controls.vehicleNumber.value.toUpperCase(),
        "type": this.vehicleForm.controls.type.value,
        "phoneNumber": this.vehicleForm.controls.mobileNumber.value,
        // "insuranceValidty": this.vehicleForm.controls.insuranceValidity.value,
        "missingDate": this.vehicleForm.controls.missingDate.value ? missingDate.toString() : '',
        // "missingDate":this.vehicleForm.controls.missingDate.value ? this._ngbFormat.format(this.vehicleForm.controls.missingDate.value) : '',
        "requestBy": this.vehicleForm.controls.requestedBy.value,
        "requestDate": this.vehicleForm.controls.requestedDate.value ? startDate.toString() : '',
        // "requestDate":this.vehicleForm.controls.requestedDate.value ? this._ngbFormat.format(this.vehicleForm.controls.requestedDate.value) : '',
        "policeComplaintMade": this.vehicleForm.controls.isPoliceComplaintLogged.value,
        "remarks": this.vehicleForm.controls.remarks.value,
        "insuranceFrom": this.vehicleForm.value.insuranceFrom ? insuranceFrom.toString() : '',
        "insuranceTo": this.vehicleForm.value.insuranceTo ? insuranceTo.toString() : '',
      }
      this.spinner = true;
      this._dataService.postApi(environment.addWantedVehicle, dataToAPI)
        .subscribe((res) => {
          if (res && res.status.toLowerCase() == 'success') {
            this.spinner = false;
            modal.dismiss('Data saved')
            this.getVehicles(this.vehicleForm.controls.vehicleNumber.value.toUpperCase(), content1)
            // swal.fire("", res.message, "success");
            setTimeout(() => {
              swal.close()
            }, 3000);
            this.getVehiclesList(1);
            this.vehicleForm.reset()
          }
          else {
            this.spinner = false
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          }
        });
    } else {
      let missingDate = new Date(this.vehicleForm.controls.missingDate.value.month + '/' + this.vehicleForm.controls.missingDate.value.day + '/' + this.vehicleForm.controls.missingDate.value.year)
      let startDate = new Date(this.vehicleForm.controls.requestedDate.value.month + '/' + this.vehicleForm.controls.requestedDate.value.day + '/' + this.vehicleForm.controls.requestedDate.value.year)
      let insuranceFrom: any = '';
      if (this.vehicleForm.controls.insuranceFrom.value && this.vehicleForm.controls.insuranceFrom.value.month) {
        insuranceFrom = new Date(this.vehicleForm.controls.insuranceFrom.value.month + '/' + this.vehicleForm.controls.insuranceFrom.value.day + '/' + this.vehicleForm.controls.insuranceFrom.value.year)
      }
      let insuranceTo: any = '';
      if (this.vehicleForm.controls.insuranceTo.value && this.vehicleForm.controls.insuranceTo.value.month) {
        insuranceTo = new Date(this.vehicleForm.controls.insuranceTo.value.month + '/' + this.vehicleForm.controls.insuranceTo.value.day + '/' + this.vehicleForm.controls.insuranceTo.value.year)
      }
      let dataToAPI = {
        "vehicleNumber": this.vehicleForm.controls.vehicleNumber.value.toUpperCase(),
        "type": this.vehicleForm.controls.type.value,
        "phoneNumber": this.vehicleForm.controls.mobileNumber.value,
        // "insuranceValidty": this.vehicleForm.controls.insuranceValidity.value,
        "missingDate": this.vehicleForm.controls.missingDate.value ? missingDate.toString() : '',
        // "missingDate":this.vehicleForm.controls.missingDate.value ? this._ngbFormat.format(this.vehicleForm.controls.missingDate.value) : '',
        "requestBy": this.vehicleForm.controls.requestedBy.value,
        "requestDate": this.vehicleForm.controls.requestedDate.value ? startDate.toString() : '',
        // "requestDate":this.vehicleForm.controls.requestedDate.value ? this._ngbFormat.format(this.vehicleForm.controls.requestedDate.value) : '',
        "policeComplaintMade": this.vehicleForm.controls.isPoliceComplaintLogged.value,
        "remarks": this.vehicleForm.controls.remarks.value,
        "insuranceFrom": this.vehicleForm.value.insuranceFrom ? insuranceFrom.toString() : '',
        "insuranceTo": this.vehicleForm.value.insuranceTo ? insuranceTo.toString() : '',
      }
      this.spinner = true;
      this._dataService.putApi(environment.updateWantedVehicle + this.currentVehicleData._id, dataToAPI)
        .subscribe((res) => {
          if (res && res.status.toLowerCase() == 'success') {
            this.spinner = false;
            // this.getVehicles(this.vehicleForm.controls.vehicleNumber.value.toUpperCase())
            modal.dismiss('Data saved')
            swal.fire("", res.message, "success");
            setTimeout(() => {
              swal.close()
            }, 3000);
            this.getVehiclesList(1);
            this.vehicleForm.reset()
          }
          else {
            this.spinner = false
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          }
        });
    }
  }


  getVehiclesList(pageNum: number) {
    this.page = pageNum;
    this.loader = true;
    this._dataService.getApi(environment.getWantedVehicleList + `?size=${this.pageSize}&pageNo=${pageNum}&searchText=${this.searchForm.controls.searchKey.value}`)
      .subscribe((res) => {
        this.loader = false;
        if (res && res.status.toLowerCase() == 'success') {
          this.wantedVehicleList = (res.data || []);
          // this.getExcelVehiclesList();
          if (pageNum == 1) {
            this.count = res.total;
          }
          this.pager = this._dataService.getPager(this.count, pageNum, this.pageSize);
        } else {
          this.wantedVehicleList = [];
        }
      }, (error) => {
        this.wantedVehicleList = [];
      })
  }

  getExcelVehiclesList(type: any) {
    this._dataService.getApi(environment.getWantedVehicleList + `?size=${1000}&pageNo=${1}&searchText=${this.searchForm.controls.searchKey.value}`)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.excelWantedVehicleList = (res.data || []);
          this.excelWantedVehicleList.forEach((element: any, i: number) => {
            delete element._id;
            delete element.cBook;
            delete element.chassisNumber;
            delete element.engineNumber;
            delete element.isMatched;
            delete element.stolenDate;
            delete element.__v;
          });
          if (type == 'copy') {
            this.copyTable()
          }
          else if (type == 'pdf') {
            this.printPDF()
          }
          else if (type == 'excel') {
            this.exportAsXLSX()
          }
          else if (type == 'csv') {
            this.getCSV()
          }

        } else {
          this.excelWantedVehicleList = [];
        }
      }, (error) => {
        this.excelWantedVehicleList = [];
      })
  }

  setPage(page: any) {
    this.page = page;
    this.getVehiclesList(page)
  }
  //
  disabledRenewal(card: any) {
    return (new Date(card?.endDate) > new Date())
  }

  openModal(content: any, type?: any, vehicle?: any) {
    this.vehicleForm.reset();
    this.spinner = false;
    let date: any = new Date()
    this.vehicleForm.controls.requestedDate.setValue({ year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() })
    if (type == "edit") {
      this.vehicleForm.controls.type.setErrors(null)
      this.showEdit = true;
      this.currentVehicleData = vehicle;
      let missingDate = new Date(this.currentVehicleData.missingDate).getDate() + '/' + (new Date(this.currentVehicleData.missingDate).getMonth() + 1) + '/' + new Date(this.currentVehicleData.missingDate).getFullYear();
      let requestedDate = new Date(this.currentVehicleData.requestDate).getDate() + '/' + (new Date(this.currentVehicleData.requestDate).getMonth() + 1) + '/' + new Date(this.currentVehicleData.requestDate).getFullYear();
      let insuranceFrom = new Date(this.currentVehicleData.insuranceFrom).getDate() + '/' + (new Date(this.currentVehicleData.insuranceFrom).getMonth() + 1) + '/' + new Date(this.currentVehicleData.missingDate).getFullYear();
      let insuranceTo = new Date(this.currentVehicleData.insuranceTo).getDate() + '/' + (new Date(this.currentVehicleData.insuranceTo).getMonth() + 1) + '/' + new Date(this.currentVehicleData.insuranceTo).getFullYear();
      this.vehicleForm.setValue({
        vehicleNumber: this.currentVehicleData.vehicleNumber.toUpperCase(),
        type: this.currentVehicleData.type ? this.currentVehicleData.type : '',
        mobileNumber: this.currentVehicleData.phoneNumber,
        insuranceFrom: this._ngbFormat.parse(insuranceFrom),
        insuranceTo: this._ngbFormat.parse(insuranceTo),
        missingDate: this._ngbFormat.parse(missingDate),
        requestedDate: this._ngbFormat.parse(requestedDate),
        requestedBy: this.currentVehicleData.requestBy,
        isPoliceComplaintLogged: this.currentVehicleData.policeComplaintMade,
        remarks: this.currentVehicleData.remarks,
      })

    } else {
      this.showEdit = false;
      this.vehicleForm.controls.type.setValue("bicycle");
      this.vehicleForm.controls.isPoliceComplaintLogged.setValue("No");
    }
    this.modalService.open(content, { size: 'lg', centered: true });
  }


  deleteVehicle(vehicle: any) {
    this._dataService.deleteApi(environment.deleteWantedVehicle + vehicle._id)
      .subscribe((res) => {
        if (res && res.status == 'success') {
          swal.fire("", res.message, "success");
          setTimeout(() => {
            swal.close()
          }, 3000);
          this.getVehiclesList(1);
        }
        else {
          swal.fire("Warning!", res.message, "warning");
          setTimeout(() => {
            swal.close()
          }, 3000);
        }

      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }

  getStatusColor(status: any) {
    switch (status) {
      case "Active":
        return "btn-success";
      case "Expired":
        return 'btn-danger';
      default:
        return "btn-primary";
    }
  }

  getDate(date: any) {
    //   if(date){
    //   let newDate = date.split('/')[1] + '/' + date.split('/')[0]  + '/' + date.split('/')[2]
    //   return moment(newDate).format("DD/MM/YYYY HH:mm:ss")
    // } else {
    return moment(date).format("DD/MM/YYYY HH:mm:ss")
    // }
  }


  copyTable() {
    var text = "S.No." +
      "\tVehicle No" +
      "\t\tType" +
      "\t\tMobile No." +
      "\t\tCustomer Name" +
      "\t\tRequest Date" +
      "\t\tMissing Date" +
      "\t\tRequest By" +
      "\t\tPolice Complaint logged?" +
      "\t\tLocated At" +
      "\t\tLocated On" +
      "\t\tRemarks"
    var textData = "";
    for (let i = 0; i < this.wantedVehicleList.length; i++) {
      textData += i + 1 +
        "\t\t" + (this.wantedVehicleList[i].vehicleNumber ? this.wantedVehicleList[i].vehicleNumber : 'N/A')
        + "\t\t" + (this.wantedVehicleList[i].type ? this.getVehicleType(this.wantedVehicleList[i].type) : 'N/A')
        + "\t" + (this.wantedVehicleList[i].phoneNumber ? this.wantedVehicleList[i].phoneNumber : 'N/A')
        + "\t" + (this.wantedVehicleList[i].insuranceValidty ? this.wantedVehicleList[i].insuranceValidty : 'N/A')
        + "\t" + (this.wantedVehicleList[i].vehicleNumber ? this.wantedVehicleList[i].vehicleNumber : 'N/A')
        + "\t" + (this.wantedVehicleList[i].missingDate ? this.getDate(this.wantedVehicleList[i].missingDate) : 'N/A')
        + "\t" + (this.wantedVehicleList[i].requestDate ? this.getDate(this.wantedVehicleList[i].requestDate) : "N/A")
        + "\t" + (this.wantedVehicleList[i].requestBy ? this.wantedVehicleList[i].requestBy : 'N/A')
        + "\t" + (this.wantedVehicleList[i].policeComplaintMade ? this.wantedVehicleList[i].policeComplaintMade : 'N/A')
        + "\t" + ('N/A')
        + "\t" + ('N/A')
        + "\t" + (this.wantedVehicleList[i].remarks ? this.wantedVehicleList[i].remarks : 'N/A') + "\n"
    }
    let selBox = document.createElement('textarea');

    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = text + '\n' + textData;

    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    if (selBox) {
      // window.print()
    }
    // this.MessageService.add({ severity: 'success', summary: '', detail: 'Link copied to Clipboard' });
    else {
      // this.MessageService.add({ severity: 'warning', summary: '', detail: 'Unable to copy the link to Clipboard' });
    }
  }

  async printPDF() {
    var doc = new jsPDF('p', 'pt');
    var col = [["S.No.",
      "Vehicle No",
      "Type",
      "Mobile No.",
      "Insurance Validity",
      "Missing Date",
      "Request Date",
      "Request By",
      "Police Complaint logged?",
      "Located At",
      "Located On",
      "Remarks"]];
    var rows: any = [];
    await this.wantedVehicleList.forEach((element: any, i: number) => {
      var temp = [i + 1,
      element.vehicleNumber || 'N/A',
      this.getVehicleType(element.type) || 'N/A',
      element.phoneNumber || 'N/A',
      element.insuranceValidty || 'N/A',
      element.missingDate ? this.getDate(element.missingDate) : 'N/A',
      element.requestDate ? this.getDate(element.requestDate) : 'N/A',
      element.requestBy || 'N/A',
      element.policeComplaintMade || 'N/A',
        "N/A",
        "N/A",
      element.remarks || 'N/A'];
      rows.push(temp);
    });
    await autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (data) => { },
    });
    doc.output('dataurlnewwindow')
  }

  getCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Wanted Vehicles',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
      // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
    };

    const csvExporter = new ExportToCsv(options);

    csvExporter.generateCsv(this.excelWantedVehicleList);
  }
  exportAsXLSX(): void {
    this._dataService.exportAsExcelFile(this.excelWantedVehicleList, 'sample');
  }
}
