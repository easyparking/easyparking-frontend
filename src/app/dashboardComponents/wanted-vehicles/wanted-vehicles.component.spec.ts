import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WantedVehiclesComponent } from './wanted-vehicles.component';

describe('WantedVehiclesComponent', () => {
  let component: WantedVehiclesComponent;
  let fixture: ComponentFixture<WantedVehiclesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WantedVehiclesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WantedVehiclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
