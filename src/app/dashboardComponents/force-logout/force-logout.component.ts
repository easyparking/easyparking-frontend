import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbDate, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/shared/services/data.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import swal from 'sweetalert2';
import * as moment from 'moment';
import { AuthService } from 'src/app/shared/services/auth.service';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';

@Component({
  selector: 'app-force-logout',
  templateUrl: './force-logout.component.html',
  styleUrls: ['./force-logout.component.css']
})
export class ForceLogoutComponent implements OnInit {
  page = 1;
  pageSize = 10;
  logoutsList: any = [];
  loader = false;
  count: any;
  pager: any;
  results$: any;
  subject = new Subject();
  searchForm!: FormGroup;
  historyDetails: any;
  spinner: boolean = false;
  currentIndex: any;
  modelLoader: boolean = false;
  ownersList: any = [];
  standsList: any = [];
  owner: any = '';
  stand: any = '';
  currentUser: any;
  userData: any;
  constructor(private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private _fb: FormBuilder,
    private _dataService: DataService,
    private _authService: AuthService,
    private _decryptService: EncryptDecryptService,
    private router: Router) {
    this.searchForm! = this._fb.group({
      searchKey: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    // this.getLogoutUsersList(1);
    this.userData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    if(!this.userData.data){
    this.router.navigateByUrl('/login');
  }
    this.getOwnersList();

    if (this.userData.data.role == "owner") {
      this.getStandsList(this.userData.data.ownerId);
      this.owner = this.userData.data.ownerId;
    }
    this.results$ = this.searchForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe((data: any) => this.getLogoutUsersList(1));
  }

  getStandsList(id: any) {
    this._dataService.getApi(environment.getStandsByOwnerURL + id)
      .subscribe((response) => {
        if (response.status == 'success') {
          localStorage.setItem("standsArray", JSON.stringify(response.data))
          this.standsList = response.data
        }
        else {
          this.standsList = []
        }
      })
  }

  getOwnersList() {
    this._dataService.getApi(environment.getOwnersListURL)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() != 'fail') {
          this.ownersList = res.data;
        } else {
          this.ownersList = [];
        }
      })
  }

  getLogoutUsersList(pageNum: number) {
    this.page = pageNum;
    this.loader = true;
    this._dataService.getApi(environment.getLogoutList + `?size=${this.pageSize}&pageNo=${pageNum}&searchText=${this.searchForm.controls.searchKey.value}&ownerId=${this.owner}&standId=${this.stand}`)
      .subscribe((res) => {
        this.loader = false;
        if (res && res.status.toLowerCase() == 'success') {
          this.logoutsList = res.result;
          if (pageNum == 1) {
            this.count = res.total;
          }
          this.pager = this._dataService.getPager(this.count, pageNum, this.pageSize);
        } else {
          this.logoutsList = [];
        }
      }, (error) => {
        this.logoutsList = [];
      })
  }

  getDate(date: any) {
    return moment(date).format("DD/MM/YYYY HH:mm:ss")
  }

  openModal(content: any, data: any) {
    this.modelLoader = true;
    this.currentUser = data;
    this._dataService.getApi(environment.getLoginHistory + data.name).subscribe((res: any) => {
      if (res && res.status.toLowerCase() == "success") {
        this.historyDetails = res.result;
        this.modelLoader = false
      }
    });
    this.modalService.open(content, { size: 'lg', centered: true });
  }



  logout(data: any) {
    let standDetails: any;
    if (localStorage.getItem("standsArray")) {
      standDetails = JSON.parse(localStorage.getItem("standsArray") || "")
    }
    this._dataService.putApi(environment.endSessionURL, {
      "end_time": new Date(Date.now()).toString(),
      "sessionId": data._id,
      "start_time": data.lastLoginSession,
      "userName": data.userName,
      "employeeName": data.name,
      "employeeId": data.employeeId,
      "standId": data.standId,
      "standName": data.standName,
      "role": "employee",
      // "standId": data.standId,
      "ownerId": data.ownerId
    })
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          swal.fire("", res.message, "success");
          setTimeout(() => {
            swal.close()
          }, 3000);
          this.spinner = false;
          this.getLogoutUsersList(1);
        } else {
          this.spinner = false;
          swal.fire("Warning!", res.message, "warning");
          setTimeout(() => {
            swal.close()
          }, 3000);
        }

      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }

}
