import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkingsearchComponent } from './parkingsearch.component';

describe('ParkingsearchComponent', () => {
  let component: ParkingsearchComponent;
  let fixture: ComponentFixture<ParkingsearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParkingsearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkingsearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
