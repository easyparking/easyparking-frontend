import { DataService } from 'src/app/shared/services/data.service';
import { MatAccordion } from '@angular/material/expansion';
// import { MatAccordion } from '@angular/material';
import { Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { NgbdSortableHeader } from 'src/app/directives/tableDirective';
import { environment } from 'src/environments/environment.prod';
import * as moment from 'moment';
import swal from 'sweetalert2';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable';
import { ExportToCsv } from 'export-to-csv';
import { element } from 'protractor';

@Component({
  selector: 'app-parkingsearch',
  templateUrl: './parkingsearch.component.html',
  styleUrls: ['./parkingsearch.component.css']
})
export class ParkingsearchComponent implements OnInit {

  page = 1;
  pageSize = 10;
  searchText: any = ''
  vehiclesListArray: any = []
  pager: any
  loader: boolean = false
  excelVehiclesListArray: any;
  count: any =0;
  constructor(private _dataService: DataService) {

  }

  ngOnInit(): void {
    this.searchVehiclesData(1)
  }

  searchVehiclesData(pageNum: any) {
    this.loader = true
    this._dataService.getApi(environment.getParkingsListURL + "?size=" + this.pageSize + "&pageNo=" + pageNum + "&searchText=" + this.searchText)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.vehiclesListArray = res.data;
          this.loader = false;
          this.count = res.totalCount;
          this.pager = this._dataService.getPager(res.totalCount, pageNum, this.pageSize)
        }
        else {
          this.loader = false
          this.vehiclesListArray = []
        }
      }, (err) => {
        // swal.fire('Oops!', 'Something went wrong. Please try again', 'error');

      })
  }

  getExcelSearchVehiclesData(type: any) {
    this._dataService.getApi(environment.getParkingsListURL + "?size=" + 1000 + "&pageNo=" + 1 + "&searchText=" + this.searchText)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.excelVehiclesListArray = res.data;
          if (type == 'copy') {
            this.copyTable()
          }
          else if (type == 'pdf') {
            this.printPDF()
          }
          else if (type == 'excel') {
            this.exportAsXLSX()
          }
          else if (type == 'csv') {
            this.getCSV()
          }
        } else {
          this.excelVehiclesListArray = [];
        }
      }, (err) => {
        this.excelVehiclesListArray = [];
      })
  }

  setPage(event: any) {
    this.page = event
    this.searchVehiclesData(this.page)
  }
  getStringLength(stringData: any) {
    // if (stringData.length < 6) {
    return stringData
    // }
    // else {
    //   let subString = stringData.substr(0, 5)
    //   return subString + '...'
    // }
  }

  getVehicleType(type: string) {
    if (type == "bicycle") {
      return "Bicycle";
    } else if (type == "twoWheeler") {
      return "2 W";
    } else if (type == "threeWheeler") {
      return "3 W";
    } else if (type == "fourWheeler") {
      return "4 W";
    } else {
      return type;
    }
  }

  getStartTime(date: any) {
    if (date) {
      return moment(date).format("DD-MM-YYYY HH:mm:ss");
    }
    else {
      return 'N/A'
    }
  }


  copyTable() {
    var text = "S.No." +
      "\tState" +
      "\t\tDistrict" +
      "\t\tLocation" +
      "\t\tStand ID" +
      "\t\tStand Name" +
      "\t\tParking ID" +
      "\t\tVehicle No." +
      "\t\tPh.No" +
      "\t\tType of Vehicle" +
      "\t\tDate & Time of Parking" +
      "\t\tDate & Time of Exit" +
      "\t\tParked emp Name & Mob. No." +
      "\t\tExited Emp Name & Mobile No." +
      "\t\tStand Owner" +
      "\t\tStand Owner Mobile Number"
    var textData = "";
    for (let i = 0; i < this.vehiclesListArray.length; i++) {
      textData += i + 1 +
        "\t\t" + (this.vehiclesListArray[i].state ? this.vehiclesListArray[i].state : 'N/A')
        + "\t\t" + (this.vehiclesListArray[i].district ? this.getVehicleType(this.vehiclesListArray[i].district) : 'N/A')
        + "\t" + (this.vehiclesListArray[i].location ? this.vehiclesListArray[i].location : 'N/A')
        + "\t" + (this.vehiclesListArray[i].standId ? this.vehiclesListArray[i].standId : 'N/A')
        + "\t" + (this.vehiclesListArray[i].standName ? this.vehiclesListArray[i].standName : 'N/A')
        + "\t" + (this.vehiclesListArray[i].standId ? this.getDate(this.vehiclesListArray[i].standId) : 'N/A')
        + "\t" + (this.vehiclesListArray[i].vehicleNumber ? this.getDate(this.vehiclesListArray[i].vehicleNumber) : "N/A")
        + "\t" + (this.vehiclesListArray[i].phone ? this.vehiclesListArray[i].phone : 'N/A')
        + "\t" + (this.vehiclesListArray[i].vehicleType ? this.getVehicleType(this.vehiclesListArray[i].vehicleType) : 'N/A')
        + "\t" + (this.vehiclesListArray[i].dateAndTimeOfParking ? this.getStartTime(this.vehiclesListArray[i].dateAndTimeOfParking) : 'N/A')
        + "\t" + (this.vehiclesListArray[i].dateAndTimeOfExit ? this.getStartTime(this.vehiclesListArray[i].dateAndTimeOfExit) : 'N/A')
        + "\t" + (this.vehiclesListArray[i].openEmpName ? (this.vehiclesListArray[i].openEmpName + ',' + this.vehiclesListArray[i].openEmpMobile) : 'N/A')
        + "\t" + (this.vehiclesListArray[i].closedEmpName ? (this.vehiclesListArray[i].closedEmpName + ',' + this.vehiclesListArray[i].closedEmpMobile) : 'N/A')
        + "\t" + (this.vehiclesListArray[i].ownerName ? this.vehiclesListArray[i].ownerName : 'N/A')
        + "\t" + (this.vehiclesListArray[i].standMobileNumber ? this.vehiclesListArray[i].standMobileNumber : 'N/A') + "\n"
    }
    let selBox = document.createElement('textarea');

    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = text + '\n' + textData;

    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    if (selBox) {
      // window.print()
    }
    // this.MessageService.add({ severity: 'success', summary: '', detail: 'Link copied to Clipboard' });
    else {
      // this.MessageService.add({ severity: 'warning', summary: '', detail: 'Unable to copy the link to Clipboard' });
    }
  }

  async printPDF() {
    var doc = new jsPDF('p', 'pt');
    var col = [["S.No.",
      "State",
      "District",
      "Location",
      "Stand ID",
      "Stand Name",
      "Parking ID",
      "Vehicle No.",
      "Ph.No",
      "Type of Vehicle",
      "Date & Time of Parking",
      "Date & Time of Exit",
      "Parked emp Name & Mob. No.",
      "Exited Emp Name & Mobile No.",
      "Stand Owner",
      "Stand Owner Mobile Number"]];
    var rows: any = [];
    await this.vehiclesListArray.forEach((element: any, i: number) => {
      var temp = [i + 1,
      (element.state ? element.state : 'N/A')
        , (element.district ? this.getVehicleType(element.district) : 'N/A')
        , (element.location ? element.location : 'N/A')
        , (element.standId ? element.standId : 'N/A')
        , (element.standName ? element.standName : 'N/A')
        , (element.standId ? this.getDate(element.standId) : 'N/A')
        , (element.vehicleNumber ? this.getDate(element.vehicleNumber) : "N/A")
        , (element.phone ? element.phone : 'N/A')
        , (element.vehicleType ? this.getVehicleType(element.vehicleType) : 'N/A')
        , (element.dateAndTimeOfParking ? this.getStartTime(element.dateAndTimeOfParking) : 'N/A')
        , (element.dateAndTimeOfExit ? this.getStartTime(element.dateAndTimeOfExit) : 'N/A')
        , (element.openEmpName ? (element.openEmpName + ',' + element.openEmpMobile) : 'N/A')
        , (element.closedEmpName ? (element.closedEmpName + ',' + element.closedEmpMobile) : 'N/A')
        , (element.ownerName ? element.ownerName : 'N/A')
        , (element.standMobileNumber ? element.standMobileNumber : 'N/A') + "\n"];
      rows.push(temp);
    });
    await autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (data) => { },
    });
    // doc.output('dataurlnewwindow')
      doc.setProperties({ title: "Parkings Search" });
      // window.open(doc.output('bloburl'), '_blank')
      window.open(URL.createObjectURL(doc.output('blob')), '_blank')
  }

  getCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Parked Vehicles',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };
    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(this.excelVehiclesListArray);
  }
  exportAsXLSX(): void {
    this.excelVehiclesListArray.forEach((element:any)=>{
      delete element.actualPrice;
      delete element.cardEndDate;
      delete element.cardNumber;
      delete element.cardStartDate;
      delete element.createdDate;
      delete element.customerAddress;
      delete element.customerName;
      delete element.expectedDuration;
      delete element.finalPrice;
      delete element.image;
      delete element.isEntryOnOffline;
      delete element.isExitOnOffline;
      delete element.isSlipLost;
      delete element.offerPrice;
      delete element.openEmpId;
      delete element.ownerId;
      delete element.parkingId;
      delete element.remarks;
      delete element.__v;
      delete element._id;
      delete element.closedEmpId;
      delete element.vehicleName;
      
      delete Object.assign(element, {['State']: element['state'] })['state'];
      delete Object.assign(element, {['District']: element['district'] })['district'];
      delete Object.assign(element, {['Location']: element['location'] })['location'];
      delete Object.assign(element, {['Stand ID']: element['standId'] })['standId'];
      delete Object.assign(element, {['Stand Name']: element['standName'] })['standName'];
      delete Object.assign(element, {['Parking ID']: element['parkingId'] })['parkingId'];
      delete Object.assign(element, {['Status']: element['status'] })['status'];
      delete Object.assign(element, {['Vehicle No.']: element['vehicleNumber'] })['vehicleNumber'];
      delete Object.assign(element, {['Ph.No']: element['phone'] })['phone'];
      delete Object.assign(element, {['Type of Vehicle']: this.getVehicleType(element['vehicleType']) })['vehicleType'];
      delete Object.assign(element, {['Date & Time of Parking']: this.getStartTime(element['dateAndTimeOfParking']) })['dateAndTimeOfParking'];
      delete Object.assign(element, {['Date & Time of Exit']: this.getStartTime(element['dateAndTimeOfExit']) })['dateAndTimeOfExit'];
      delete Object.assign(element, {['Parked emp Name & Mob. No.']: element['openEmpName'] + ' ' + element['openEmpMobile'] })['openEmpName'];
      delete element.openEmpMobile;
      delete Object.assign(element, {['Exited Emp Name & Mobile No.']: element['closedEmpName'] + ' ' + element['closedEmpMobile'] })['closedEmpName'];
      delete element.closedEmpMobile;
      delete Object.assign(element, {['Stand Owner']: element['ownerName'] })['ownerName'];
      delete Object.assign(element, {['Stand Owner Mobile Number']: element['standMobileNumber'] })['standMobileNumber'];
      // 
      // 
    })
    this._dataService.exportAsExcelFile(this.excelVehiclesListArray, 'sample');
  }

  getDate(date: any) {
    return moment(date).format("DD/MM/YYYY HH:mm:ss")
  }
}
