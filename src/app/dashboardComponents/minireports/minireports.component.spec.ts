import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MinireportsComponent } from './minireports.component';

describe('MinireportsComponent', () => {
  let component: MinireportsComponent;
  let fixture: ComponentFixture<MinireportsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MinireportsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MinireportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
