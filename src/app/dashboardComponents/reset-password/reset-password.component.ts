import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment.prod';
import swal from 'sweetalert2';
import { DataService } from 'src/app/shared/services/data.service';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  resetPasswordForm!:FormGroup;
  spinner = false;
  userData: any;
  constructor(private formBuilder: FormBuilder,
    private _dataService: DataService,
    private _decryptService: EncryptDecryptService,) {
    this.resetPasswordForm = this.formBuilder.group({
      currentPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    })
   }

  ngOnInit(): void {
    this.userData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    // console.log("******",this.userData)
  }
  resetPassword(){
    this.spinner = true;
    let data = {
      "userName": this.userData.data.userName,
      "currentPassword":this.resetPasswordForm.controls.currentPassword.value,
      "newPassword": this.resetPasswordForm.controls.newPassword.value
    }
    
    this._dataService.postApi(environment.resetPassword, data)
    .subscribe((res) => {
      this.spinner = false;
      if (res && res.status == 'success') {
        this.resetPasswordForm.reset();
        swal.fire("success!", res.message, "success");
        setTimeout(() => {
          swal.close()
        }, 3000);
        } else {
          swal.fire("Warning!", res.message.message, "warning");
          setTimeout(() => {
            swal.close()
          }, 3000);
        }
    })
  }

}
