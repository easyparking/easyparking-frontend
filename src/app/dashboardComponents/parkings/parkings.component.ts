import { OfflineDataService } from './../../shared/services/offline-data.service';
import { OnlineOfflineService } from './../../shared/services/online-offline.service';
import { DataService } from './../../shared/services/data.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChildren } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of, OperatorFunction } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import * as moment from 'moment';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap, startWith } from 'rxjs/operators';
import jsPDF from 'jspdf';
import * as QRCode from 'qrcode'
import { element } from 'protractor';
// import 'rxjs/add/operator/take';
import { take } from 'rxjs/operators';

declare var window: any;
@Component({
  selector: 'app-parkings',
  templateUrl: './parkings.component.html',
  styleUrls: ['./parkings.component.css']
})
export class ParkingsComponent implements OnInit {
  @ViewChildren('scannerInput')
  vc!: { first: { nativeElement: { focus: () => void; }; }; };
  addParkingForm: FormGroup
  parkingSearchData: any = []
  isEntry: boolean = true
  parkingsList: any = []
  historyData: any = []
  pager: any;
  pageNum = 1
  selectedStand: any
  userData: any
  spinner: boolean = false
  isLoading: boolean = false
  vehicleTypes: any = []
  isPriceRuleAdded: boolean = false;
  filteredPhoneNumbers!: Observable<any[]>;
  filteredVehicles!: Observable<any[]>;
  filteredMonthlyCards!: Observable<any[]>;
  filteredVehicleCards!: Observable<any[]>;
  entryVehicleData: any
  loader: boolean = false
  entryCardData: any;
  isVehicleNameSelected = false;
  parkingsEntryHistoryData: any = []
  isOnlineStatus: boolean = true
  priceRulesDetails: any;
  editDetails: any;
  parkingTime: any;
  count: any;
  pageSize = 5;
  qrCodeUrlValue: any;
  isCardValid: boolean = false;

  defaultVehicelType: any = "twoWheeler";
  vehicleNumberList: any = [];
  cardNumbersList: any = [];
  phoneNumberList: any = [];
  searchForm!: FormGroup;
  isCardLoading: boolean = false;
  isPhoneLoading: boolean = false;
  sync: any;
  constructor(private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private _fb: FormBuilder,
    private _dataService: DataService,
    private _decryptService: EncryptDecryptService,
    private _router: Router,
    private _offlineService: OnlineOfflineService,
    private _offlineDataService: OfflineDataService,
    private router: Router) {
    this.addParkingForm = this._fb.group({
      vehicleNumber: ['', [Validators.required, Validators.pattern("^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$")]],
      vehicleName: [''],
      vehicleType: [this.defaultVehicelType, [Validators.required]],
      cardNumber: ['', [Validators.minLength(6)]],
      customerName: [''],
      customerAdress: [''],
      phNumber: ['', [Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]],
      expectedParkingDays: [0],
      qrCode: [''],
      isDefault: [false],
      "cardStartDate": [''],
      "cardEndDate": [''],
    })
    this.searchForm! = this._fb.group({
      searchKey: ['', [Validators.required]],
    });
  }

  ngAfterViewInit() {
    this.vc.first.nativeElement.focus();
  }
//   ngOnDestroy() {
//     this._dataService.isSyncing.
// }

  ngOnInit(): void {
    this.pageNum = 1;
    this.userData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    if(!this.userData.data){
      this.router.navigateByUrl('/login');
    }
    if (localStorage.getItem("standSelected")) {
      let standsArray = JSON.parse(localStorage.getItem("standsArray") || "")
      this.selectedStand = standsArray.filter((item: any) => item.standId == localStorage.getItem("standSelected"))
      this.getParkingsList(1);
    }
    this.isOnlineStatus = this._offlineService.isOnline
    if (!this._offlineService.isOnline) {
      if (localStorage.getItem("parkingsHistoryData")) {
        this.parkingsEntryHistoryData = JSON.parse(localStorage.getItem("parkingsHistoryData") || '')
      }
    } else {
      if (localStorage.getItem("standSelected")) {
        this.getparkingsEntryHistory();
      }
    }
    
   this._dataService.isSyncing.subscribe((result: any) => {
    console.log("value changed",result)
    if(!result) {
      // console.log("value changed",result)
      this.getParkingsList(1)
    }
  });
    this._offlineService.networkConnectivity$.subscribe((res) => {
      console.log("res",res)
      this.isOnlineStatus = res;
      if (res) {
        if(this.selectedStand){
          this.getparkingsEntryHistory()
        }
      } else { 
        this.loader = false;
        if (localStorage.getItem("parkingsHistoryData")) {
        this.parkingsEntryHistoryData = JSON.parse(localStorage.getItem("parkingsHistoryData") || "")
      }
    }
      
    })
    this.searchForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe((data: any) => this.getParkingsList(1));
    this.addParkingForm.controls.phNumber.valueChanges
      .pipe(
        startWith(''),
        debounceTime(1000),
        // distinctUntilChanged()
      ).subscribe(value => {
        this.filteredPhoneNumbers = this.filter(value, environment.getDetailsByPhURL + this.selectedStand[0].standId + '&phoneNumber=', 'phone' || '')
      });
     
    this.addParkingForm.controls.vehicleNumber.valueChanges
      .pipe(
        startWith(''),
        debounceTime(1000),
        // distinctUntilChanged()
      ).subscribe(value => {
        this.filteredVehicles = this.filter(value, (environment.searchParkingListURL + this.selectedStand[0].standId + "&vehicleNumber="), 'vehicle' || '')
      });

    this.addParkingForm.controls.cardNumber.valueChanges
      .pipe(
        startWith(''),
        debounceTime(500),
        // distinctUntilChanged()
      ).subscribe((value) => {
        this.filteredMonthlyCards = this.filterCards(value, environment.getCardsForParking + '?standId=' + this.selectedStand[0].standId + '&ownerId=' + this.selectedStand[0].ownerId + '&cardId=')
    });
        
    this.addParkingForm.controls.vehicleName.valueChanges
      .pipe(
        startWith(''),
        debounceTime(500),
        // distinctUntilChanged()
      ).subscribe((value) => {
        this.filteredVehicleCards = this.filter(value, (environment.getVehiclesList + '?size=10&pageNo=1&searchText='), 'vehicleName')
    });
    this.getPriceRulesDetails();
    if(localStorage.getItem("defaultVehicelType")){
      this.defaultVehicelType = localStorage.getItem("defaultVehicelType");
      this.addParkingForm.controls.vehicleType.setValue(this.defaultVehicelType);
      this.addParkingForm.controls.isDefault.setValue(true)
    } else {
     
      this.addParkingForm.controls.isDefault.setValue(false) 
    }
  }
  ngOnDestroy(): void {
    if(this.sync){
    this.sync.unsubscribe();
  }
  }

  filter(val: string, dataURL: any, searchKey?: any): Observable<any[]> {
    // call the service which makes the http-request
    if (this._offlineService.isOnline) {
      if (val) {
       if (searchKey == 'vehicle') {
        this.isLoading = true;
      } else if (searchKey == 'phone') {
        this.isPhoneLoading = true;
      }
        return this._dataService.getApi(dataURL + val)
          .pipe(
            map((response) => {
              this.isLoading = false;
              this.isPhoneLoading = false;
              this.isCardLoading = false
              this.vehicleNumberList = (response.data || []);
              return response.data
            }
            )
          )
      }
      else {
        this.isLoading = false;
        this.isPhoneLoading = false;
        this.isCardLoading = false
        let data: any = []
        return of(data)
      }

    } else {
      this.isLoading = false;
      this.isPhoneLoading = false;
      this.isCardLoading = false
      let searchData = JSON.parse(localStorage.getItem("parkingsHistoryData") || '')
      if (searchKey == 'vehicle') {
        this.vehicleNumberList = searchData.filter((searchItem: any) => searchItem.vehicleNumber.toLowerCase().includes(val));
        return of(searchData.filter((searchItem: any) => searchItem.vehicleNumber.toLowerCase().includes(val)))
      } else if (searchKey == 'phone') {
        this.vehicleNumberList = searchData.filter((searchItem: any) => searchItem.phone.toLowerCase().includes(val));
        return of(searchData.filter((searchItem: any) => searchItem.phone.toLowerCase().includes(val)))
      } else if (searchKey == 'vehicleName') {
        this.vehicleNumberList = searchData.filter((searchItem: any) => searchItem.vehicleName.toLowerCase().includes(val));
        return of(searchData.filter((searchItem: any) => searchItem.vehicleName.toLowerCase().includes(val)))
      } else {
        return of([])
      }
    }
  }

  filterCards(val: string, dataURL: any): Observable<any[]> {

    // call the service which makes the http-request
    if (this._offlineService.isOnline) {
      return this._dataService.getApi(dataURL + encodeURI(val))
        .pipe(
          map((response) => {
            let filterValue = []
            this.isLoading = false;
            this.isPhoneLoading = false;
            this.isCardLoading = false
            this.cardNumbersList = response.cardData;
            return response.cardData
          }
          )
        )
    } else {
      this.isLoading = false;
      this.isPhoneLoading = false;
      this.isCardLoading = false
      return of([])
    }

  }

  setAsDefult(event: any) {
    console.log(event)
    if (event.srcElement.checked && this.addParkingForm.controls.vehicleType.value) {
      localStorage.setItem("defaultVehicelType",this.addParkingForm.controls.vehicleType.value)
      this.defaultVehicelType = this.addParkingForm.controls.vehicleType.value 
      this.addParkingForm.controls.isDefault.setValue(true)
    } else {
      this.addParkingForm.controls.isDefault.setValue(false) 
      localStorage.removeItem("defaultVehicelType")
    }
  }

  typeChanged(event: any) {
    if (['bicycle','Rickshaw','ElectricBike'].indexOf(event)>-1) {
      setTimeout(() => {
        this.addParkingForm.controls.vehicleNumber.setErrors(null);
        this.addParkingForm.controls.vehicleNumber.clearValidators();
      }, 1);
    } else {
      setTimeout(() => {
        if (!this.addParkingForm.controls.vehicleNumber.value) {
          this.addParkingForm.controls.vehicleNumber.setValidators([Validators.required, Validators.pattern("^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$")]);
          this.addParkingForm.controls.vehicleNumber.updateValueAndValidity();
        }
      }, 1);
    }
  }

  resetForm() {
    let defaultVehicelType = '';
    if (this.addParkingForm.controls.isDefault.value || localStorage.getItem("defaultVehicelType")) {
      defaultVehicelType = localStorage.getItem("defaultVehicelType") || this.defaultVehicelType;
      this.addParkingForm.controls.isDefault.setValue(true);
      // defaultVehicelType
    } else {
      this.defaultVehicelType = '';
    }

    this.addParkingForm.controls.vehicleNumber.setValue("", { emitEvent: false })
    this.addParkingForm.controls.vehicleType.setValue(defaultVehicelType)
    this.addParkingForm.controls.expectedParkingDays.setValue(0)
    this.addParkingForm.controls.vehicleName.setValue("")
    this.addParkingForm.controls.cardNumber.setValue("")
    this.addParkingForm.controls.cardStartDate.setValue("")
    this.addParkingForm.controls.cardEndDate.setValue("")
    this.addParkingForm.controls.customerName.setValue("")
    this.addParkingForm.controls.customerAdress.setValue("")
    this.addParkingForm.controls.phNumber.setValue("", { emitEvent: false })
    this.addParkingForm.controls.qrCode.setValue("");

    this.isEntry = true;
    setTimeout(() => {
      this.addParkingForm.setErrors(null);
      this.addParkingForm.controls.vehicleNumber.setValidators([Validators.required, Validators.pattern("^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$")]);
      this.addParkingForm.controls.vehicleNumber.updateValueAndValidity();
      this.addParkingForm.controls.phNumber.setValidators([Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)])
      this.addParkingForm.controls.vehicleNumber.updateValueAndValidity();
      this.addParkingForm.markAsPristine();
    }, 1);
  }

  getPriceRulesDetails() {
    this.vehicleTypes = []
    this.isPriceRuleAdded = false
    this._dataService.getApi(environment.getPriceRulesURL + this.selectedStand[0].standId)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success' && res.data) {
          this.priceRulesDetails = res.data;
          // this.vehicleTypes = res.data[0].vehicleTypes

          for (let i = 0; i < res.data[0].vehicleTypes.length; i++) {
            if (!(res.data[0].vehicleTypes[i].amount == 0 && (res.data[0].vehicleTypes[i].fixedPriceFor1stHour == 0 || res.data[0].vehicleTypes[i].forEveryNextHour == 0))) {
              this.isPriceRuleAdded = true;
              if (['bicycle','Rickshaw','ElectricBike'].indexOf(res.data[0].vehicleTypes[i].vehicletype)==-1) {
                if (res.data[0].vehicleTypes[i].vehicletype == 'twoWheeler') {

                  res.data[0].vehicleTypes[i].vehicletypeValue = '2 W'
                  this.vehicleTypes.push(res.data[0].vehicleTypes[i])
                } else if (res.data[0].vehicleTypes[i].vehicletype == 'threeWheeler') {
                  // if(!(res.data[0].vehicleTypes[i].amount == 0 && (res.data[0].vehicleTypes[i].fixedPriceFor1stHour == 0 || res.data[0].vehicleTypes[i].forEveryNextHour == 0))){
                  res.data[0].vehicleTypes[i].vehicletypeValue = '3 W'
                  this.vehicleTypes.push(res.data[0].vehicleTypes[i])
                  // }
                } else if (res.data[0].vehicleTypes[i].vehicletype == 'fourWheeler') {
                  // if(!(res.data[0].vehicleTypes[i].amount == 0 && (res.data[0].vehicleTypes[i].fixedPriceFor1stHour == 0 || res.data[0].vehicleTypes[i].forEveryNextHour == 0))){
                  res.data[0].vehicleTypes[i].vehicletypeValue = '4 W'
                  this.vehicleTypes.push(res.data[0].vehicleTypes[i])
                  // }
                } else {
                  // if(!(res.data[0].vehicleTypes[i].amount == 0 && (res.data[0].vehicleTypes[i].fixedPriceFor1stHour == 0 || res.data[0].vehicleTypes[i].forEveryNextHour == 0))){
                  res.data[0].vehicleTypes[i].vehicletypeValue = res.data[0].vehicleTypes[i].vehicletype
                  this.vehicleTypes.push(res.data[0].vehicleTypes[i])
                  // }
                }
              } else {
                // if(!(res.data[0].vehicleTypes[i].amount == 0 && (res.data[0].vehicleTypes[i].fixedPriceFor1stHour == 0 || res.data[0].vehicleTypes[i].forEveryNextHour == 0))){
                res.data[0].vehicleTypes[i].vehicletypeValue = res.data[0].vehicleTypes[i].vehicletype == 'bicycle'?'Bicycle':res.data[0].vehicleTypes[i].vehicletype;
                this.vehicleTypes.push(res.data[0].vehicleTypes[i])
                // }
              }
            }
            if ((res.data[0].vehicleTypes.length == i + 1 && !this.isPriceRuleAdded)) {
              // this.isPriceRuleAdded = false
              swal.fire("Warning!", "Price rules aren't added for the selected stand.Please contact admin", "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }
          }
        }
      })
  }

  getParkingsList(pageNum: any) {
    this.loader = true;
    this.pageNum = pageNum;
    let startDate = localStorage.getItem("sessionStartTime") ? JSON.parse(localStorage.getItem("sessionStartTime") || "").start_time : new Date()
    this._dataService.getApi(environment.getParkingsListURL + "?standId=" + this.selectedStand[0].standId + '&startDate=' + startDate + "&size=" + this.pageSize + "&pageNo=" + pageNum + '&searchText=' + this.searchForm.value.searchKey + '&rev=' + Math.random())
      .subscribe((response) => {
        if (response.status == 'success') {
          this.loader = false
          this.parkingsList = response.data;
          this.count = response.totalCount
          this.pager = this._dataService.getPager(response.totalCount, pageNum, this.pageSize)
        } else {
          this.loader = false
          this.count = 0
          this.parkingsList = []
        }
      }, (error) => {
        this.loader = false
        this.count = 0
        this.parkingsList = []
      })
  }

  setPage(event: any) {
    this.pageNum = event;
    this.getParkingsList(this.pageNum)
  }

  openHistoryModal(content: any, data: any) {
    this.activeModal = this.modalService.open(content, { size: 'lg', centered: true });
    this.showHistory(data)
  }

  checkValue(value:any, formControlName:string){
    if(Number(value) == 0 && value!=''){
      // setTimeout(() => {
        this.addParkingForm.controls[formControlName].setErrors({ format: true });
      // }, 1);
    } else {
    setTimeout(() => {
      this.addParkingForm.controls[formControlName].setErrors({ format: null });
      this.addParkingForm.controls[formControlName].updateValueAndValidity();
    }, 1);
    }
  }

  selectedOption($event: any) {
    this.entryVehicleData = $event.option.value;
    if ($event.option.value.status == "Entry") {
      this.isEntry = false
    } else {
      this.isEntry = true
    }
    this.addParkingForm.controls.vehicleNumber.setValue($event.option.value.vehicleNumber, { emitEvent: false })
    this.addParkingForm.controls.vehicleName.setValue($event.option.value.vehicleName)
    this.addParkingForm.controls.vehicleType.setValue($event.option.value.vehicleType)
    this.addParkingForm.controls.cardNumber.setValue($event.option.value.cardNumber, { emitEvent: false })
    this.addParkingForm.controls.cardStartDate.setValue($event.option.value.renewedDate ? $event.option.value.renewedDate : $event.option.value.createdDate)
    this.addParkingForm.controls.cardEndDate.setValue($event.option.value.endDate)
    this.addParkingForm.controls.customerName.setValue($event.option.value.customerName)
    this.addParkingForm.controls.customerAdress.setValue($event.option.value.customerAddress)
    this.addParkingForm.controls.phNumber.setValue($event.option.value.phone, { emitEvent: false })
    this.addParkingForm.controls.phNumber.markAsDirty()
    this.addParkingForm.controls.phNumber.markAsTouched()

  }

  selectedCardNumber($event: any) {
    //
    this.entryCardData = $event.option.value;
    this.addParkingForm.controls.cardNumber.setValue($event.option.value.cardId)
    this._dataService.getApi(environment.checkParkingByCard + '?standId=' + this.selectedStand[0].standId + '&ownerId=' + this.selectedStand[0].ownerId + '&cardId=' + $event.option.value.cardId).subscribe(res => {
      if (res.parkings.length > 0) {
        this.isEntry = false;
        this.entryVehicleData = res.parkings[0];
        setTimeout(() => {
          this.addParkingForm.controls.vehicleNumber.setValue(res.parkings[0].vehicleNumber, { emitEvent: false })
          this.addParkingForm.controls.vehicleName.setValue(res.parkings[0].vehicleName)
          this.addParkingForm.controls.vehicleType.setValue(res.parkings[0].vehicleType)
          this.addParkingForm.controls.cardNumber.setValue(res.parkings[0].cardNumber)
          this.addParkingForm.controls.cardStartDate.setValue(res.parkings[0].renewedDate ? res.parkings[0].renewedDate : res.parkings[0].createdDate)
          this.addParkingForm.controls.cardEndDate.setValue(res.parkings[0].endDate)
          this.addParkingForm.controls.customerName.setValue(res.parkings[0].customerName)
          this.addParkingForm.controls.customerAdress.setValue(res.parkings[0].customerAddress)
          this.addParkingForm.controls.phNumber.setValue(res.parkings[0].phone, { emitEvent: false })
        }, 1);
      } else {
        this.isEntry = true;

        this.entryVehicleData = this.entryCardData;
        setTimeout(() => {
          this.addParkingForm.controls.vehicleNumber.setValue(this.entryCardData.vehicleNumber, { emitEvent: false })
          this.addParkingForm.controls.vehicleName.setValue(this.entryCardData.vehicleName)
          this.addParkingForm.controls.vehicleType.setValue(this.entryCardData.vehicleType)
          this.addParkingForm.controls.cardNumber.setValue(this.entryCardData.cardId)
          this.addParkingForm.controls.cardStartDate.setValue(this.entryCardData.renewedDate ? this.entryCardData.renewedDate : this.entryCardData.createdDate)
          this.addParkingForm.controls.cardEndDate.setValue(this.entryCardData.endDate)
          this.addParkingForm.controls.customerName.setValue(this.entryCardData.customerName)
          this.addParkingForm.controls.customerAdress.setValue('')
          this.addParkingForm.controls.phNumber.setValue(this.entryCardData.customerPhone, { emitEvent: false })
        }, 1);
      }

    })

  }

  getVehicleType(type: string) {
    if (type == "bicycle") {
      return "Bicycle";
    } else if (type == "twoWheeler") {
      return "2 W";
    } else if (type == "threeWheeler") {
      return "3 W";
    } else if (type == "fourWheeler") {
      return "4 W";
    } else {
      return type;
    }
  }

  searchVehicle(formControl: any) {
    if (this.entryVehicleData && !this.entryCardData) {
      this.isEntry = true;
      this.entryVehicleData = null;
    }
  }

  vehicleNumberSelected() {
    if (this.vehicleNumberList.length > 0) {
      let vehicleData = this.vehicleNumberList.filter((ele: any) => (ele.vehicleNumber.toLowerCase().includes(this.addParkingForm.controls.vehicleNumber.value.toLowerCase()) || ele.parkingId.toLowerCase().includes(this.addParkingForm.controls.vehicleNumber.value.toLowerCase())));
      if (vehicleData && vehicleData.length > 0) {
        this.entryVehicleData = vehicleData[0];
        this.addParkingForm.controls.vehicleNumber.setValue(vehicleData[0].vehicleNumber, { emitEvent: false })
        this.addParkingForm.controls.vehicleName.setValue(vehicleData[0].vehicleName)
        this.addParkingForm.controls.vehicleType.setValue(vehicleData[0].vehicleType)
        this.addParkingForm.controls.cardNumber.setValue(vehicleData[0].cardNumber, { emitEvent: false })
        this.addParkingForm.controls.cardStartDate.setValue(vehicleData[0].cardStartDate)
        this.addParkingForm.controls.cardEndDate.setValue(vehicleData[0].cardEndDate)
        this.addParkingForm.controls.customerName.setValue(vehicleData[0].customerName)
        this.addParkingForm.controls.customerAdress.setValue(vehicleData[0].customerAddress)
        this.addParkingForm.controls.phNumber.setValue(vehicleData[0].phone, { emitEvent: false })
        this.addParkingForm.controls.phNumber.markAsDirty();
        this.addParkingForm.controls.phNumber.markAsTouched();
        if (vehicleData[0].status == "Entry") {
          this.addExitEntry(this.entryVehicleData)
        } else {
          this.getWantedVehiclesList()
        }
      } else {
        if(this.addParkingForm.valid){
          if(!this.isEntry){
            this.addExitEntry(this.entryVehicleData)
          } else {
            this.getWantedVehiclesList()
          }
        }
      }
    } else {
        if(this.addParkingForm.valid){
          if(!this.isEntry){
            this.addExitEntry(this.entryVehicleData)
          } else {
            this.getWantedVehiclesList()
          }
      } else {
        this.addParkingForm.controls.phNumber.markAsDirty();
        this.addParkingForm.controls.vehicleNumber.markAsDirty();
        this.addParkingForm.controls.vehicleType.markAsDirty();
      }
    }
  }

  phoneNumberSelected() {

    if (this.addParkingForm.controls.phNumber.valid && this.vehicleNumberList.length == 1) {
      if (this.vehicleNumberList && this.vehicleNumberList.length > 0) {
        this.entryVehicleData = this.vehicleNumberList[0];
        this.addParkingForm.controls.vehicleNumber.setValue(this.vehicleNumberList[0].vehicleNumber, { emitEvent: false })
        this.addParkingForm.controls.vehicleName.setValue(this.vehicleNumberList[0].vehicleName)
        this.addParkingForm.controls.vehicleType.setValue(this.vehicleNumberList[0].vehicleType)
        this.addParkingForm.controls.cardNumber.setValue(this.vehicleNumberList[0].cardNumber, { emitEvent: false })
        this.addParkingForm.controls.cardStartDate.setValue(this.vehicleNumberList[0].cardStartDate)
        this.addParkingForm.controls.cardEndDate.setValue(this.vehicleNumberList[0].cardEndDate)
        this.addParkingForm.controls.customerName.setValue(this.vehicleNumberList[0].customerName)
        this.addParkingForm.controls.customerAdress.setValue(this.vehicleNumberList[0].customerAddress)
        this.addParkingForm.controls.phNumber.setValue(this.vehicleNumberList[0].phone, { emitEvent: false })
        this.addParkingForm.controls.phNumber.markAsDirty();
        this.addParkingForm.controls.phNumber.markAsTouched();
        if (this.vehicleNumberList[0].status == "Entry") {
          this.addExitEntry(this.entryVehicleData)
        } else {
          this.getWantedVehiclesList()
        }
      } else {
        if(this.addParkingForm.valid){
          if(!this.isEntry){
            this.addExitEntry(this.entryVehicleData)
          } else {
            this.getWantedVehiclesList()
          }
        }
      }
    } else {
      if(this.addParkingForm.valid){
        if(!this.isEntry){
            this.addExitEntry(this.entryVehicleData)
          } else {
            this.getWantedVehiclesList()
          }
      }
    }
  }
  cardNumberSelected() {
    if (this.addParkingForm.controls.cardNumber.valid && this.cardNumbersList.length > 0) {
      let cardData = this.cardNumbersList.filter((ele: any) => ele.cardId.toLowerCase() == this.addParkingForm.controls.cardNumber.value.toLowerCase());
      if (cardData && cardData.length > 0) {
        this.entryCardData = cardData[0];
        this._dataService.getApi(environment.searchParkingListURL + this.selectedStand[0].standId + "&vehicleNumber=" + cardData[0].vehicleNumber).subscribe(res => {
          if (res.status.toLowerCase() == "success") {
            if (res.data) {
              this.entryVehicleData = res.data[0];
              this.addParkingForm.controls.vehicleNumber.setValue(res.data[0].vehicleNumber, { emitEvent: false })
              this.addParkingForm.controls.vehicleName.setValue(res.data[0].vehicleName)
              this.addParkingForm.controls.vehicleType.setValue(res.data[0].vehicleType)
              this.addParkingForm.controls.cardNumber.setValue(cardData[0].cardId, { emitEvent: false })
              this.addParkingForm.controls.cardStartDate.setValue(cardData[0].renewedDate ? cardData[0].renewedDate : cardData[0].createdDate)
              this.addParkingForm.controls.cardEndDate.setValue(cardData[0].endDate)
              this.addParkingForm.controls.customerName.setValue(res.data[0].customerName)
              this.addParkingForm.controls.customerAdress.setValue(res.data[0].customerAddress)
              this.addParkingForm.controls.phNumber.setValue(res.data[0].phone, { emitEvent: false })
              if (res.data[0].status == "Entry") {
                this.addExitEntry(this.entryVehicleData)
              } else {
                this.getWantedVehiclesList()
              }
            } else {
              this.entryVehicleData = cardData[0];
              this.addParkingForm.controls.vehicleNumber.setValue(cardData[0].vehicleNumber, { emitEvent: false })
              this.addParkingForm.controls.vehicleName.setValue(cardData[0].vehicleName)
              this.addParkingForm.controls.vehicleType.setValue(cardData[0].vehicleType)
              this.addParkingForm.controls.cardNumber.setValue(cardData[0].cardId, { emitEvent: false })
              this.addParkingForm.controls.cardStartDate.setValue(cardData[0].renewedDate ? cardData[0].renewedDate : cardData[0].createdDate)
              this.addParkingForm.controls.cardEndDate.setValue(cardData[0].endDate)
              this.addParkingForm.controls.customerName.setValue(cardData[0].customerName)
              this.addParkingForm.controls.customerAdress.setValue(cardData[0].customerAddress)
              this.addParkingForm.controls.phNumber.setValue(cardData[0].customerPhone, { emitEvent: false })

              this.getWantedVehiclesList()

            }
            this.addParkingForm.controls.phNumber.markAsDirty()
            this.addParkingForm.controls.phNumber.markAsTouched();
          }
        })
      }
    }
  }

  addVehicle() {
    if(this.addParkingForm.value.vehicleName){
      let dataToAPI = {
        "vehicleName": this.addParkingForm.value.vehicleName,
        "vehicleType": this.addParkingForm.value.vehicleType
      }
      if (this._offlineService.isOnline) {
        this._dataService.postApi(environment.addVehicleURL, dataToAPI)
          .subscribe((res) => {
          }, (error) => {
          })
      }
    }
  }

  async getWantedVehiclesList() {
    if (this.isPriceRuleAdded) {
      if( !this.spinner && !this.isCardLoading && !this.isPhoneLoading && !this.isLoading) {
      this.spinner = true;
      if (this._offlineService.isOnline) {
        if (['bicycle','rickshaw','electricbike'].indexOf(this.addParkingForm.value.vehicleType.toLowerCase())==-1 && this.addParkingForm.value.vehicleNumber) {
          this._dataService.getApi(environment.getWantedVehicleList + `?size=5&pageNo=1&searchText=${this.addParkingForm.value.vehicleNumber.toUpperCase()}`)
            .subscribe((res) => {
              this.loader = false;
              if (res && res.status.toLowerCase() == 'success') {
                if (res.data.length > 0) {
                  this.updateWantedVehicelStatus(res.data[0]);
                  this.addParkingEntry();
                } else {
                  this.addParkingEntry();
                }
              } else {
                this.addParkingEntry();
              }
            }, (error) => {
            })
        } else {
          this.addParkingEntry()
        }
      } else {
        this.addParkingEntry()
      }
    }
    } else {
        swal.fire("Warning!", "Price rules aren't added for the selected stand.Please contact admin", "warning");
        setTimeout(() => {
          swal.close()
        }, 3000);
    }
  }

  updateWantedVehicelStatus(data: any) {
    this._dataService.putApi(environment.updateWantedVehicle + data._id,
      {
        standId: this.selectedStand[0].standId,
        vehicleData: data,
        standName: this.selectedStand[0].standName,
        location: this.selectedStand[0].location.city.city
      })
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.spinner = false;
        }
      });
  }

  addParkingEntry() {
    let dataToAPI = {
      "ownerId": this.selectedStand[0].ownerId,
      "ownerName": this.selectedStand[0].ownerName,
      "vehicleNumber": this.addParkingForm.value.vehicleNumber ? this.addParkingForm.value.vehicleNumber.toUpperCase() : '',
      "phone": this.entryVehicleData ? (this.entryVehicleData.phone ? this.entryVehicleData.phone : this.entryVehicleData.customerPhone) : this.addParkingForm.value.phNumber,
      "vehicleType": this.addParkingForm.value.vehicleType,
      "vehicleName": this.addParkingForm.value.vehicleName,
      "customerName": this.addParkingForm.value.customerName ? this.addParkingForm.value.customerName.toUpperCase() : '',
      "customerAddress": this.addParkingForm.value.customerAdress ? this.addParkingForm.value.customerAdress.toUpperCase() : '',
      "cardNumber": this.entryCardData ? (this.addParkingForm.value.cardNumber ? this.addParkingForm.value.cardNumber.toUpperCase() : '') : '',
      "cardStartDate": this.addParkingForm.controls.cardStartDate.value,
      "cardEndDate": this.addParkingForm.controls.cardEndDate.value,
      "dateAndTimeOfParking": new Date(Date.now()).toString(),
      "dateAndTimeOfExit": "",
      "standName": this.selectedStand[0].standName,
      "standId": this.selectedStand[0].standId,
      "openEmpName": this.userData.data.name,
      "openEmpMobile": this.userData.data.phoneNumber,
      "closedEmpName": "",
      "closedEmpMobile": "",
      "standMobileNumber": this.selectedStand[0].standPhone,
      "standCode": this.selectedStand[0].standCode,
      "parkingId": "",
      "status": "Entry",
      "expectedDuration": this.addParkingForm.value.expectedParkingDays,
      "state": this.selectedStand[0].location.state.state,
      "district": this.selectedStand[0].location.district.district,
      "location": this.selectedStand[0].location.city.city,
      "openEmpId": this.userData.data.employeeId,
      "closedEmpId": "",
      "sendSMS": this.selectedStand[0].SMS.entry,
      "sessionId": JSON.parse(localStorage.getItem("sessionStartTime") || "")._id,
      "finalPrice": 0,
      "offerPrice":0,
      "actualPrice": 0,
      "isEntryOnOffline": false
      // "amount ": 0,
    }
    if (this._offlineService.isOnline) {
      if (!this.isVehicleNameSelected) {
        this.addVehicle();
      }
      if (this.addParkingForm.value.cardNumber) {
        this.checkCardValidity(this.addParkingForm.value.cardNumber)
      } else {
        this.isCardValid = false
      }
      this._dataService.postApi(environment.addParkingEntryURL, dataToAPI)
        .subscribe((res) => {
          if (res && res.status == 'success') {
            this.filteredVehicles = of([]);
            this.filteredMonthlyCards = of([]);
            this.filteredPhoneNumbers = of([]);
            this.isEntry = true;
            this.spinner = false;
            this.generateQRCode(res.data.parkingId);
            this.addParkingForm.reset();
            this.entryCardData = undefined
            setTimeout(() => {
              this.addParkingForm.setErrors(null);
              this.addParkingForm.markAsPristine();
              this.addParkingForm.controls.vehicleNumber.setValidators([Validators.required, Validators.pattern("^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$")]);
              this.addParkingForm.controls.vehicleNumber.updateValueAndValidity();
              this.addParkingForm.controls.phNumber.setValidators([Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)])
              this.addParkingForm.controls.vehicleNumber.updateValueAndValidity();
            }, 1);
            this.getParkingsList(1)
            this.getparkingsEntryHistory();
            this.addParkingForm.controls.vehicleType.setValue(this.defaultVehicelType);
             if (localStorage.getItem("defaultVehicelType")) {
              this.addParkingForm.controls.isDefault.setValue(true);
              // defaultVehicelType
            } else {
              this.addParkingForm.controls.isDefault.setValue(true);
            }
            this.addParkingForm.controls.expectedParkingDays.setValue(0)
            swal.fire("", res.message, "success");
            setTimeout(() => {
              swal.close()
              if (!this.isCardValid) {
                this.entryPrintPreview(res.data)
              }
            }, 1000);
          } else {
            this.addParkingForm.reset();
            this.entryCardData = undefined
            setTimeout(() => {
              this.addParkingForm.setErrors(null);
              this.addParkingForm.controls.vehicleType.setValue(this.defaultVehicelType);
              if (localStorage.getItem("defaultVehicelType")) {
               this.addParkingForm.controls.isDefault.setValue(true);
               // defaultVehicelType
             } else {
               this.addParkingForm.controls.isDefault.setValue(true);
             }
              this.addParkingForm.markAsPristine();
              this.addParkingForm.controls.vehicleNumber.setValidators([Validators.required, Validators.pattern("^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$")]);
              this.addParkingForm.controls.vehicleNumber.updateValueAndValidity();
              this.addParkingForm.controls.phNumber.setValidators([Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)])
              this.addParkingForm.controls.vehicleNumber.updateValueAndValidity();
            }, 1);
            this.spinner = false
            swal.fire("Warning!", res.message, "warning");
            setTimeout(() => {
              swal.close()
            }, 3000);
          }

        }, (error) => {
          this.spinner = false
          swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
          setTimeout(() => {
            swal.close()
          }, 3000);
        })
    } else {
      let vehicle = this.parkingsEntryHistoryData.filter((ele:any)=>ele.vehicleNumber == dataToAPI.vehicleNumber);
      if(vehicle && vehicle.length > 0){
        swal.fire("Warning!", "Vehicle is already parked", "warning"); 
        this.spinner = false;
        setTimeout(() => {
          swal.close()
        }, 3000);
      } else {
        this._offlineDataService.addParkingsToIndexedDb(dataToAPI).then((res: any) => {
          this.generateQRCode(dataToAPI.vehicleNumber)
          this.isEntry = true
          this.addParkingForm.reset();
          this.entryCardData = undefined
          setTimeout(() => {
            this.addParkingForm.setErrors(null);
            this.addParkingForm.markAsPristine();
            this.addParkingForm.controls.vehicleNumber.setValidators([Validators.required, Validators.pattern("^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$")]);
            this.addParkingForm.controls.vehicleNumber.updateValueAndValidity();
            this.addParkingForm.controls.phNumber.setValidators([Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)])
            this.addParkingForm.controls.vehicleNumber.updateValueAndValidity();
          }, 1);
          this.spinner = false;
          swal.fire("", "Succcessfully added parking in offline", "success");
          setTimeout(() => {
            swal.close()
            this.entryPrintPreview(dataToAPI)
            this.parkingsEntryHistoryData.unshift(dataToAPI)
            localStorage.setItem("parkingsHistoryData", JSON.stringify(this.parkingsEntryHistoryData))
            // this.addParkingForm.controls.vehicleType.setValue(this.defaultVehicelType)
            this.addParkingForm.controls.vehicleType.setValue(this.defaultVehicelType);
            if (localStorage.getItem("defaultVehicelType")) {
             this.addParkingForm.controls.isDefault.setValue(true);
             // defaultVehicelType
           } else {
             this.addParkingForm.controls.isDefault.setValue(true);
           }
            this.addParkingForm.controls.expectedParkingDays.setValue(0)
          }, 1000);
        }).catch((error: any) => {
          this.spinner = false;
          if(error.message && error.message.includes("Key already exists")){
            swal.fire("Warning!", "Vehicle is already parked", "warning");
          } else {
            swal.fire("Warning!", "Something went wrong", "warning");
          }
          setTimeout(() => {
            swal.close()
          }, 3000);
        });
      }
    }
  }

  addExitEntry(data?: any) {
    data = data ? data : this.entryVehicleData;
    swal.fire({
      title: 'Exit the vehicle?\n\nParking Id : ' + (data.parkingId ? data.parkingId : data.vehicleNumber) + '\nDuration : ' + this.getDuration(data.dateAndTimeOfParking, 'Entry','' , 'hm') + '\nAmount :' + this.calculateAmount(data) + '/-',
      showDenyButton: true,
      showCancelButton: false,
      allowOutsideClick: false,
      confirmButtonText: 'Exit',
      denyButtonText: `Print & Exit`,
      cancelButtonColor: '#d33',

    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.exitParking(data, 'exit')
      } else if (result.isDenied) {
        if (this.addParkingForm.value.cardNumber || (data && data.cardNumber)) {
        this.checkCardValidity((data && data.cardNumber) ? data.cardNumber : this.addParkingForm.value.cardNumber)
      } else {
        this.isCardValid = false
      }
        this.exitParking(data, 'print')
      }
    })
  }

  exitParking(data?: any, type?: any) {
    let dataToAPI: any = {
      "parkingId": data ? data.parkingId : (this.entryVehicleData.parkingId ? this.entryVehicleData.parkingId : ''),
      "status": (moment().diff(moment(data ? data.dateAndTimeOfParking : this.entryVehicleData.dateAndTimeOfParking), 'minutes') <= this.selectedStand[0].initialGracePeriod) ? "Cancelled" : "Exit",
      "closedEmpId": this.userData.data.employeeId,
      "closedEmpName": this.userData.data.name,
      "closedEmpMobile": this.userData.data.phoneNumber,
      "standId": data ? data.standId : this.entryVehicleData.standId,
      "dateAndTimeOfParking": data ? data.dateAndTimeOfParking.toString() : this.entryVehicleData.dateAndTimeOfParking.toString(),
      "dateAndTimeOfExit": new Date().toString(),
      "vehicleType": data ? data.vehicleType : this.addParkingForm.value.vehicleType,
      "cardNumber": data ? data.cardNumber : (this.addParkingForm.value.cardNumber ? this.addParkingForm.value.cardNumber : ''),
      "cardStartDate": this.addParkingForm.controls.cardStartDate.value,
      "cardEndDate": this.addParkingForm.controls.cardEndDate.value,
      "sendSMS": this.selectedStand[0].SMS.exit,
      "vehicleNumber": data ? data.vehicleNumber : this.entryVehicleData.vehicleNumber,
      "sessionId": JSON.parse(localStorage.getItem("sessionStartTime") || "")._id,
      "phone": data ? data.phone : this.entryVehicleData.phone,
      "ownerId": this.selectedStand[0].ownerId,
      "ownerName": this.selectedStand[0].ownerName,
      "standCode": this.selectedStand[0].standCode,
      "finalPrice": this.calculateAmount(data),
      "offerPrice":0,
      "actualPrice": this.calculateAmount(data),
      "isExitOnOffline": false
    }
    if(!this.spinner && !this.isCardLoading && !this.isPhoneLoading && !this.isLoading) {
      this.spinner = true;
    if (this._offlineService.isOnline) {
      
      this._dataService.putApi(environment.updateParkingURL, dataToAPI)
        .subscribe((res) => {
          if (res && res.status == 'success') {
              this.isEntry = true;
              this.filteredVehicles = of([]);
              this.filteredMonthlyCards = of([]);
              this.filteredPhoneNumbers = of([]);
              this.addParkingForm.reset();
              this.entryCardData = undefined
              // this.addParkingForm.controls.vehicleType.setValue(this.defaultVehicelType)
              this.addParkingForm.controls.vehicleType.setValue(this.defaultVehicelType);
              if (localStorage.getItem("defaultVehicelType")) {
               this.addParkingForm.controls.isDefault.setValue(true);
               // defaultVehicelType
             } else {
               this.addParkingForm.controls.isDefault.setValue(true);
             }
              this.addParkingForm.controls.expectedParkingDays.setValue(0)
            setTimeout(() => {
              this.addParkingForm.setErrors(null);
               this.addParkingForm.markAsPristine();
               this.addParkingForm.controls.vehicleNumber.setValidators([Validators.required, Validators.pattern("^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$")]);
               this.addParkingForm.controls.vehicleNumber.updateValueAndValidity();
               this.addParkingForm.controls.phNumber.setValidators([Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)])
               this.addParkingForm.controls.vehicleNumber.updateValueAndValidity();
            }, 1);

            this.parkingTime = this.getDuration(dataToAPI.dateAndTimeOfParking, 'Entry', '', 'hm')
            swal.fire("", res.message, "success");
            setTimeout(() => {
              swal.close()
              if (type == 'print') {
                if (!this.isCardValid) {
                  this.exitPrintPreview(dataToAPI);
                }
              }
            }, 1000);
            this.spinner = false;
            this.getParkingsList(1);
            this.getparkingsEntryHistory();
          } else {
            this.addParkingForm.reset();
            this.entryCardData = undefined
            // this.addParkingForm.controls.vehicleType.setValue(this.defaultVehicelType)
            this.addParkingForm.controls.vehicleType.setValue(this.defaultVehicelType);
            if (localStorage.getItem("defaultVehicelType")) {
             this.addParkingForm.controls.isDefault.setValue(true);
             // defaultVehicelType
           } else {
             this.addParkingForm.controls.isDefault.setValue(true);
           }
            this.addParkingForm.controls.expectedParkingDays.setValue(0)
            this.spinner = false
            swal.fire("Warning!", res.message, "warning");
            setTimeout(() => {
              swal.close()
            }, 3000);
          }

        }, (error) => {
          this.spinner = false
          swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
          setTimeout(() => {
            swal.close()
          }, 3000);
        })

    } else {
      this._offlineDataService.addExitsToIndexedDb(dataToAPI).then((res: any) => {
        if (dataToAPI.parkingId == "") {
          this._offlineDataService.removeAddParkingData(dataToAPI)
        }
        this.parkingTime = this.getDuration(dataToAPI.dateAndTimeOfParking, 'Entry')
        dataToAPI.finalPrice = this.calculateAmount(dataToAPI)
        this.isEntry = true
        this.spinner = false;
        this.addParkingForm.reset();
        this.entryCardData = undefined
        setTimeout(() => {
          this.addParkingForm.setErrors(null);
          // this.addParkingForm.controls.vehicleType.setValue(this.defaultVehicelType)
              this.addParkingForm.controls.vehicleType.setValue(this.defaultVehicelType);
              if (localStorage.getItem("defaultVehicelType")) {
               this.addParkingForm.controls.isDefault.setValue(true);
               // defaultVehicelType
             } else {
               this.addParkingForm.controls.isDefault.setValue(true);
             }
          this.addParkingForm.controls.vehicleNumber.setValidators([Validators.required, Validators.pattern("^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$")]);
          this.addParkingForm.controls.vehicleNumber.updateValueAndValidity();
          this.addParkingForm.controls.phNumber.setValidators([Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)])
          this.addParkingForm.controls.vehicleNumber.updateValueAndValidity();
          this.addParkingForm.markAsPristine();
        }, 1);
        swal.fire("", "Succcessfully updated parking in offline", "success");
        setTimeout(() => {
          swal.close()
          if (type == 'print') {
            this.exitPrintPreview(dataToAPI)
          }
          this.parkingsEntryHistoryData.forEach((element: any) => {
            if (element.vehicleNumber == dataToAPI.vehicleNumber && element.status.toLowerCase() == 'entry') {
              element.status = "Exit";
              element.dateAndTimeOfExit = dataToAPI.dateAndTimeOfExit;
              element.finalPrice = dataToAPI.finalPrice
            }
          })
          localStorage.setItem("parkingsHistoryData", JSON.stringify(this.parkingsEntryHistoryData))
          // this.addParkingForm.controls.vehicleType.setValue(this.defaultVehicelType);
          this.addParkingForm.controls.vehicleType.setValue(this.defaultVehicelType);
          if (localStorage.getItem("defaultVehicelType")) {
           this.addParkingForm.controls.isDefault.setValue(true);
           // defaultVehicelType
         } else {
           this.addParkingForm.controls.isDefault.setValue(true);
         }
          this.addParkingForm.controls.expectedParkingDays.setValue(0)
        }, 1000);
      }).catch((error: any) => {
        this.spinner = false;
        swal.fire("Warning!", "Something went wrong", "warning");
        setTimeout(() => {
          swal.close()
        }, 3000);
      });
    }
  }
  }


  checkCardValidity(data: any) {
    this._dataService.postApi(environment.getCardByIDURL, {
      cardId: encodeURI(this.addParkingForm.controls.cardNumber.value),
      standId: this.selectedStand[0].standId
    })
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.addParkingForm.controls.cardNumber.setErrors(null)
          let splitteDDate = res.data.endDate.split("-")
          if (new Date() > new Date(splitteDDate[2], splitteDDate[1] - 1, splitteDDate[0])) {
            this.isCardValid = false
          } else {
            this.isCardValid = true
          }
        } else {
          this.isCardValid = false
        }
      })
  }
  showHistory(data: any) {
    this._dataService.getApi(environment.showParkingHistoryURL + data.vehicleNumber)
      .subscribe((response) => {
        if (response.status == 'success') {
          this.historyData = response.data
        } else {
          this.historyData = []
        }
      })
  }

  getStartTime(date: any) {
    if (date) {
      return moment(date).format("DD-MM-YYYY HH:mm:ss");
    } else {
      return 'Pending'
    }
  }

  entryPrintPreview(data: any, type?: any) {
    let reprint = ''
    if (type) {
      reprint = '(Re-printed)'
    }
    else {
      reprint = ''
    }
    let entryTime = moment(data.dateAndTimeOfParking).format("DD-MM-YYYY HH:mm:ss");
    let text1 = this.selectedStand[0].addressLine1;
    let text2 = this.selectedStand[0].addressLine2 ? ", \n" + this.selectedStand[0].addressLine2 : ''
    let text3 = this.selectedStand[0].addressLine3 ? ", \n" + this.selectedStand[0].addressLine3 : ''
    let text4 = this.selectedStand[0].addressLine4 ? ", \n" + this.selectedStand[0].addressLine4 : ''
    let note = this.selectedStand[0].note ? this.selectedStand[0].note : ''
    let currentDate = moment().format("DD-MM-YYYY HH:mm:ss")
    let popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
  //   <tr>
  //   <td colspan="2" style="white-space:nowrap">${currentDate}</td>

  //   <td style="text-align:right;">Parking Slip</td>
  // </tr>
    popupWin.document.write(`
  <html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body onload="window.print();window.close()">


  <table width="100%" border="0" align="center" style="font-family: arial; font-size: 15px;">
  
  <tr>
  <td colspan="3"><hr style="margin:0px 0"></td>
</tr>
  <tr>
      <td colspan="3" align="center"><h4 style="margin: 0;">--Vehicle Entry Slip ${reprint}--</h4></td>
    </tr>
    <tr>
      <td colspan="3" align="center" style="-webkit-user-modify: read-write-plaintext-only;"><h5 style="margin: 0;">${text1}${text2}${text3}</h5></td>
    </tr>
    <tr>
      <td colspan="3"><hr style="margin:0px 0"></td>
    </tr>
    <tr>
      <td width="15%">Entry</td>
      <td style="white-space:nowrap">:<b> ${entryTime}</b></td>
      <td rowspan="6"><img width="100" height="100" style="margin-top:-2px;" src="${this.qrCodeUrlValue}"></td>
    </tr>
    <tr>
      <td style="white-space:nowrap">ID No</td>
      <td>:<b> ${data.parkingId ? data.parkingId : data.vehicleNumber}</b></td>
    </tr>
    <tr>
      <td style="white-space:nowrap">Veh Type</td>
      <td>: ${this.getVehicleType(data.vehicleType)}</td>
    </tr>
    <tr>
      <td style="white-space:nowrap">Veh No</td>
      <td>:<b> ${data.vehicleNumber}</b></td>
    </tr>
    <tr>
    <td></td>
    <td></td>
    </tr>
    <tr>
    <td></td>
    <td></td>
    </tr>
    <tr>
  <td colspan="3"><hr style="margin:0px 0"></td>
</tr>
    <tr>
    <td colspan="3"  style="-webkit-user-modify: read-write-plaintext-only;">${note}</td>
    </tr>

  </table>
  </body>
  </html>

  `);
    popupWin.document.close();
  }

  exitPrintPreview(data: any, type?: any) {
    let reprint = '';
    if (type) {
      reprint = '(Re-printed)'
    }
    else {
      reprint = ''
    }
    if (!this._offlineService.isOnline) {
    this.parkingTime = this.getDuration(data.dateAndTimeOfParking, 'Entry')
    data.finalPrice = this.calculateAmount(data)
    }
    let exitTime;
    let entryTime;
    let text1 =  this.selectedStand[0].addressLine1
    let text2 = this.selectedStand[0].addressLine2 ? ", \n" + this.selectedStand[0].addressLine2 : ''
    let text3 = this.selectedStand[0].addressLine3 ? ", \n" + this.selectedStand[0].addressLine3 : ''
    let text4 =  this.selectedStand[0].addressLine4 ? this.selectedStand[0].addressLine4 : ''
    let note = this.selectedStand[0].note ? this.selectedStand[0].note : ''
    let currentDate = moment().format("DD-MM-YYYY HH:mm:ss")
    if (data.dateAndTimeOfParking) {
      entryTime = moment(data.dateAndTimeOfParking).format("DD-MM-YYYY HH:mm:ss");
    } else {
      entryTime = '--'
    }
    if (data.dateAndTimeOfExit) {
      exitTime = moment(data.dateAndTimeOfExit).format("DD-MM-YYYY HH:mm:ss");
    } else {
      exitTime = "Pending"
    }
//     <tr>
//   <td colspan="2" style="white-space:nowrap">${currentDate}</td>

//   <td style="text-align:right;">Parking Bill</td>
// </tr>
    let popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
  <html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body onload="window.print();window.close()">
  <table width="100%" border="0" align="center" style="font-family: arial; font-size: 15px;">
  
<tr>
  <td colspan="3"><hr style="margin:0px 0"></td>
</tr>
  <tr>
      <td colspan="3" align="center"><h4 style="margin: 0;">--Vehicle Exit Slip ${reprint}--</h4></td>
    </tr>
    <tr>
      <td colspan="3" align="center"  style="-webkit-user-modify: read-write-plaintext-only;"><h5 style="margin: 0;">${text1}${text2}${text3}</h5></td>
    </tr>
    <tr>
      <td colspan="3" align="center"><hr style="margin:0px 0"></td>
    </tr>
    <tr>
    <td style="width:100px;white-space:nowrap">Parking ID</td>
    <td colspan="2"  style="white-space:nowrap">:<b> ${data.parkingId ? data.parkingId : data.vehicleNumber}</b></td>
  </tr>
  <tr>
  <td style="white-space:nowrap">Veh No</td>
  <td colspan="2" style="white-space:nowrap">:<b> ${data.vehicleNumber}</b></td>
</tr>
    <tr>
      <td style="white-space:nowrap">Entry</td>
      <td colspan="2" style="white-space:nowrap">: ${entryTime}</td>
    </tr>
    <tr>
      <td style="white-space:nowrap">Exit</td>
      <td colspan="2" style="white-space:nowrap">: ${exitTime}</td>
    </tr>

    <tr>
      <td style="white-space:nowrap">Duration</td>
      <td colspan="2" style="white-space:nowrap">: ${this.parkingTime}</td>
    </tr>
    <tr>
      <td style="white-space:nowrap">Bill Amount</td>
      <td colspan="2" style="white-space:nowrap">: Rs<b> ${data.finalPrice}/-</b></td>
    </tr>
    <tr>
  <td colspan="3"><hr style="margin:0px 0"></td>
</tr>
    <tr>
    <td colspan="3" style="-webkit-user-modify: read-write-plaintext-only;">${text4}</td>
  </tr>

  </table>
  </body>
  </html>

  `);
    popupWin.document.close();
  }


  getparkingsEntryHistory() {
    this._dataService.getApi(environment.getParkingsEntryHistoryURL + '?ownerId=' + this.selectedStand[0].ownerId + '&standId=' + this.selectedStand[0].standId + '&status=Entry')
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          localStorage.setItem("parkingsHistoryData", JSON.stringify(res.data))
          this.parkingsEntryHistoryData = res.data
        } else {
          this.parkingsEntryHistoryData = []
        }
      })
  }

  calculateAmount(parkingData: any) {
    try {console.log(parkingData)
      let priceRulesData = this.priceRulesDetails[0].vehicleTypes.filter((item: any) => item.vehicletype == parkingData.vehicleType)[0];
      let parkingTimeInMins = moment(new Date(parkingData.dateAndTimeOfExit ? parkingData.dateAndTimeOfExit : new Date())).diff(moment(new Date(parkingData.dateAndTimeOfParking)), 'minutes');
      if(moment(new Date(parkingData.cardEndDate)).isBefore(new Date()) && moment(new Date(parkingData.cardEndDate)).isAfter(new Date(parkingData.dateAndTimeOfParking))){console.log('a')
          parkingTimeInMins = moment(new Date(parkingData.dateAndTimeOfExit ? parkingData.dateAndTimeOfExit : new Date())).diff(moment(new Date(parkingData.cardEndDate)), 'minutes');
      }
      else{
        console.log('b')
      }
      let finalAmount = 0;
      let offerPrice = 0;
      if(this.selectedStand[0].initialGracePeriod < parkingTimeInMins){
        let numberOfSlots = Math.floor((parkingTimeInMins/(60* priceRulesData.duration)));
        let extraTimeThanASlot = 0;
        if(numberOfSlots>=1){
          extraTimeThanASlot = (parkingTimeInMins % (60*priceRulesData.duration)) > this.selectedStand[0].lateGracePeriod ? 1 : 0;
        }
        if (numberOfSlots + extraTimeThanASlot > 0) {
          if (priceRulesData.fixedPriceFor1stHour == 0 && priceRulesData.forEveryNextHour == 0) {
            finalAmount = (numberOfSlots + extraTimeThanASlot) * Number(priceRulesData.amount);
          } else {
            let forEveryNextSlot = (numberOfSlots + extraTimeThanASlot - 1) * Number(priceRulesData.forEveryNextHour)
            finalAmount = Number(priceRulesData.fixedPriceFor1stHour) + forEveryNextSlot;
            // offerPrice = Math.abs(Number(priceRulesData.fixedPriceFor1stHour) - Number(priceRulesData.forEveryNextHour)) * (numberOfSlots + extraTimeThanASlot - 1);
          }
        } else {
          if (priceRulesData.fixedPriceFor1stHour == 0 && priceRulesData.forEveryNextHour == 0) {
            finalAmount =  Number(priceRulesData.amount);
          } else {
            finalAmount = Number(priceRulesData.fixedPriceFor1stHour);
            // offerPrice = Math.abs(Number(priceRulesData.fixedPriceFor1stHour) - Number(priceRulesData.forEveryNextHour));
          }
          }
      }
      return this.isValidCard(parkingData) == "Valid Monthly Card" ? 0 : Math.floor(finalAmount)
    }
    catch (e) {
      return 0;
    }
  }

  getDuration(startTime: any, status: any, endTime?: any, type?:any) {
    if (status == "Exit" || status == "Cancelled") {
      console.log(status,endTime)
      let parkingTimeInMins = moment(new Date(endTime)).diff(moment(new Date(startTime)), 'minutes');
      // return Math.ceil((parkingTimeInMins - Number(this.selectedStand[0].lateGracePeriod)) / 60);
      console.log(parkingTimeInMins)
      return Math.floor(parkingTimeInMins / 60) + ' Hr ' + parkingTimeInMins % 60 + ' Min';
    } else {
      let parkingTimeInMins = moment(new Date()).diff(moment(new Date(startTime)), 'minutes');
      // if(type == 'hm'){
        return Math.floor(parkingTimeInMins / 60) + ' Hr ' + parkingTimeInMins % 60 + ' Min';
      // }
      // return Math.ceil((parkingTimeInMins - Number(this.selectedStand[0].lateGracePeriod)) / 60) > 0 ? Math.ceil((parkingTimeInMins - Number(this.selectedStand[0].lateGracePeriod)) / 60) 
    }
  }

  generateQRCode(data: any) {
    // With promises
    QRCode.toDataURL(data, { version: 2 })
      .then((url: any) => {
        this.qrCodeUrlValue = url
      })
      .catch((err: any) => {
        console.error(err)
      })
  }

  onKey(event: any) {
    if (event.target.value.includes('PKT') && event.target.value.length > 5) {
      if (this.isOnlineStatus) {
        this._dataService.getApi(environment.searchParkingListURL + this.selectedStand[0].standId + "&vehicleNumber=" + event.target.value)
          .subscribe((res) => {
            if (res.status.toLowerCase() == 'success') {
              if (res.data.length != 0) {
                res.data.forEach((element: any) => {
                  if (element.parkingId == event.target.value) {
                    this.bindScannedPakingIdValues(element)
                  }
                });
              }
            }
          })
      }
      else {
        this.parkingsEntryHistoryData.forEach((element: any) => {
          if (element.parkingId == event.target.value) {
            this.bindScannedPakingIdValues(element)
          }
        });
      }
    }
  }

  bindScannedPakingIdValues(element: any) {
    this.entryVehicleData = element
    if (element.status == "Entry") {
      this.isEntry = false
    } else {
      this.isEntry = true
    }
    this.addParkingForm.controls.vehicleNumber.setValue(element.vehicleNumber)
    this.addParkingForm.controls.vehicleName.setValue(element.vehicleName)
    this.addParkingForm.controls.vehicleType.setValue(element.vehicleType)
    this.addParkingForm.controls.cardNumber.setValue(element.cardNumber)
    this.addParkingForm.controls.cardStartDate.setValue(element.cardStartDate)
    this.addParkingForm.controls.cardEndDate.setValue(element.cardEndDate)
    this.addParkingForm.controls.customerName.setValue(element.customerName)
    this.addParkingForm.controls.customerAdress.setValue(element.customerAddress)
    this.addParkingForm.controls.phNumber.setValue(element.phone)
    this.addParkingForm.controls.phNumber.markAsDirty()
    this.addParkingForm.controls.phNumber.markAsTouched()
  }

  getParkingId(data: any) {
    if (data.cardNumber) {
      if (new Date(data.cardEndDate) >= new Date()) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }
  isValidCard(data: any) {
    if (data.cardNumber) {
      if (new Date(data.cardEndDate ? data.cardEndDate : this.entryCardData.endDate) >= new Date()) {
        return "Valid Monthly Card";
      } else {
        return "Expired Monthly Card";
      }
    } else {
      return "N/A";
    }
  }

  maskPhoneNumber(phone: string) {
    return phone.replace(phone.substring(2, 8), 'XXXXXX');
  }

}
