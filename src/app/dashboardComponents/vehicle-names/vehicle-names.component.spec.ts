import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleNamesComponent } from './vehicle-names.component';

describe('VehicleNamesComponent', () => {
  let component: VehicleNamesComponent;
  let fixture: ComponentFixture<VehicleNamesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehicleNamesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleNamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
