import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from 'src/app/shared/services/data.service';
import { MatAccordion } from '@angular/material/expansion';
// import { MatAccordion } from '@angular/material';
import { Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { NgbdSortableHeader } from 'src/app/directives/tableDirective';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment.prod';
import swal from 'sweetalert2';


@Component({
  selector: 'app-vehicle-names',
  templateUrl: './vehicle-names.component.html',
  styleUrls: ['./vehicle-names.component.css']
})
export class VehicleNamesComponent implements OnInit {

  page = 1;
  pageSize = 10;
  vehiclesList: any = []
  searchKey: any = '';
  pager: any
  isEdit: boolean = false
  addEditVehicleForm!: FormGroup
  spinner: boolean = false
  loader: boolean = false
  count: any = 0;
  constructor(private modalService: NgbModal,
    private _dataService: DataService,
    private _fb: FormBuilder,
    private activeModal: NgbActiveModal) {
    this.addEditVehicleForm = this._fb.group({
      vehicleName: ['', [Validators.required]],
      vehicleType: ['2Wheeler', [Validators.required]]
    })
  }

  ngOnInit(): void {
    this.searchVehicleList(1)
  }

  searchVehicleList(pageNum: any) {
    this.loader = true
    this._dataService.getApi(environment.getVehiclesList + '?size=' + this.pageSize + '&pageNo=' + pageNum + '&searchText=' + this.searchKey)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.loader = false
          this.vehiclesList = res.data;
          this.count = res.totalCount;
          this.pager = this._dataService.getPager(res.totalCount, pageNum, this.pageSize)
        }
        else {
          this.loader = false
          this.vehiclesList = []
        }
      }, (error) => {
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }

  setPage(event: any) {
    this.page = event
    this.searchVehicleList(this.page)
  }

  openModal(content: any, type: any, data?: any) {
    this.addEditVehicleForm.reset()
    this.spinner = false
    if (type == 'add') {
      this.addEditVehicleForm.controls.vehicleType.setValue('2Wheeler')
      this.isEdit = false
    }
    else {
      this.isEdit = true
      this.addEditVehicleForm.controls.vehicleType.setValue(this.getVehicleType(data.type))
      this.addEditVehicleForm.controls.vehicleName.setValue(data.name)
    }
    this.activeModal = this.modalService.open(content, { size: 'lg', centered: true });
  }

  getVehicleType(type: string) {
    if (type == "bicycle") {
      return "Bicycle";
    } else if (type == "twoWheeler") {
      return "2 W";
    } else if (type == "threeWheeler") {
      return "3 W";
    } else if (type == "fourWheeler") {
      return "4 W";
    } else {
      return type;
    }
  }

  addVehicle() {
    this.spinner = true
    let dataToAPI = {
      "vehicleName": this.addEditVehicleForm.value.vehicleName,
      "vehicleType": this.addEditVehicleForm.value.vehicleType
    }
    this._dataService.postApi(environment.addVehicleURL, dataToAPI)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          swal.fire("", res.message, "success");
          setTimeout(() => {
            swal.close()
          }, 3000);
          this.spinner = false;
          this.activeModal.close()
          this.searchVehicleList(1)
        }
        else {
          this.spinner = false
          swal.fire("Warning!", res.message, "warning");
          setTimeout(() => {
            swal.close()
          }, 3000);
        }

      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }
  updateVehicle() {
    this.spinner = true
    let dataToAPI = {
      "vehicleName": this.addEditVehicleForm.value.vehicleName,
      "vehicleType": this.addEditVehicleForm.value.vehicleType
    }
    this._dataService.putApi(environment.updateVehicleURL, dataToAPI)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          swal.fire("", res.message, "success");
          setTimeout(() => {
            swal.close()
          }, 3000);
          this.spinner = false;
          this.activeModal.close()
          this.searchVehicleList(1)
        }
        else {
          this.spinner = false
          swal.fire("Warning!", res.message, "warning");
          setTimeout(() => {
            swal.close()
          }, 3000);
        }

      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }
  vehicleDelete(vehicle: any) {
    swal.fire({
      title: 'Are you sure to delete this record?',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this._dataService.deleteApi(environment.deleteVehicleURL + '/' + vehicle._id)
          .subscribe((res) => {
            if (res && res.status.toLowerCase() == 'success') {
              swal.fire("", res.message, "success");
              setTimeout(() => {
                swal.close()
              }, 3000);
              this.spinner = false;
              this.activeModal.close()
              this.searchVehicleList(1)
            }
            else {
              this.spinner = false
              swal.fire("Warning!", res.message, "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }

          }, (error) => {
            this.spinner = false
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          })
      } else if (result.isDenied) {
      }
    })
  }
}
