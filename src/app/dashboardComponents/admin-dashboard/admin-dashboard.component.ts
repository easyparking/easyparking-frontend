import { Component, OnInit } from '@angular/core';
import { CustomAdapter } from 'src/app/shared/datepicker/custom-adapter.service';
import { CustomDateParserFormatter } from 'src/app/shared/datepicker/custom-date-parser-formatter.service';
import { NgbDateAdapter, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../../shared/services/data.service';
import { environment } from '../../../environments/environment.prod'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss'],
  providers: [
    { provide: NgbDateAdapter, useClass: CustomAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter }
  ]
})
export class AdminDashboardComponent implements OnInit {
  page = 1;
  pageSize = 10;
  model1: string | undefined;
  model2: string | undefined;

  dashboardAccountsData: any;
  dashboardStandsData: any;
  dashboardSMSData: any;
  smsSent: any;
  standsOnlineCount: any;
  standsOfflineCount: number = 0;
  vehicalDetails: any;
  dashboardData: any;
  wantedVehicleList: any;
  count: any;
  pager: any;
  searchForm!: FormGroup;
  results$: any;
  loader: boolean = false;
  constructor(public dataservice: DataService,
    private _fb: FormBuilder,) {
    this.searchForm! = this._fb.group({
      searchKey: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.getDashboardData();
    this.getVehiclesList(1);
    this.getCounts();
    this.results$ = this.searchForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe((data: any) => this.getVehiclesList(1));
  }

  getStatusColor(status: any) {
    switch (status) {
      case "Active":
        return "btn-success";
      case "Expired":
        return 'btn-danger';
      default:
        return "btn-primary";
    }
  }

  getVehicleType(type: string) {
    if (type == "bicycle") {
      return "Bicycle";
    } else if (type == "twoWheeler") {
      return "2 W";
    } else if (type == "threeWheeler") {
      return "3 W";
    } else if (type == "fourWheeler") {
      return "4 W";
    } else {
      return type;
    }
  }

  getDate(date: any) {
    return moment(date).format("DD/MM/YYYY HH:mm:ss")
  }

  getVehiclesList(pageNum: number) {
    this.page = pageNum;
    this.loader = true;
    this.dataservice.getApi(environment.getWantedVehicleList + `?size=${this.pageSize}&pageNo=${pageNum}&searchText=${this.searchForm.controls.searchKey.value}`)
      .subscribe((res) => {
        this.loader = false;
        if (res && res.status.toLowerCase() == 'success') {
          this.wantedVehicleList = (res.data || []);
          if (pageNum == 1) {
            this.count = res.total;
          }
          this.pager = this.dataservice.getPager(this.count, pageNum, this.pageSize);
        } else {
          this.wantedVehicleList = [];
        }
      }, (error) => {
        this.wantedVehicleList = [];
      })
  }

  setPage(page: any) {
    this.page = page;
    this.getVehiclesList(page)
  }

  getCounts() {
    this.dataservice.getApi(environment.getStandsCountURL)
      .subscribe((res) => {
        if (res && res.status == 'success') {
          this.standsOnlineCount = res.onlineCardsCount.Active;
          this.standsOfflineCount = res.onlineCardsCount.Total - res.onlineCardsCount.Active;
        }
      }, (error) => {
      })
  }

  getDashboardData(): void {
    this.dataservice.getApi(environment.getDashboardDetails).subscribe(res => {
      if (res.status == "success") {
        this.dashboardData = res
        this.dashboardAccountsData = res.accountsCount;
        this.dashboardStandsData = res.standsCount;
        this.dashboardSMSData = res.SMS_Balance.sms || 0;
        this.smsSent = res.SMS_Sent;
        this.vehicalDetails = res.vehiclesCount
      }
    })
  }
}
