import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { DataService } from 'src/app/shared/services/data.service';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';
import { environment } from 'src/environments/environment.prod';
import { debounceTime } from 'rxjs/operators';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import * as moment from 'moment';
import { ExportToCsv } from 'export-to-csv';
import { Router } from '@angular/router';

@Component({
  selector: 'app-owner-monthly-cards',
  templateUrl: './owner-monthly-cards.component.html',
  styleUrls: ['./owner-monthly-cards.component.css']
})
export class OwnerMonthlyCardsComponent implements OnInit {
  pageSize = 10;
  active = 2;
  onlineSearchForm: any;
  offlineSearchForm: any;
  cardsList: any = [];
  count: any = 0;
  pager: any;
  page = 1;
  results$: any;
  loader = false;
  excelCardsList: any;
  userData: any;
  constructor(private modalService: NgbModal,
    private _fb: FormBuilder,
    private _dataService: DataService,
    private _decryptService: EncryptDecryptService,
    private router: Router) {
    this.onlineSearchForm! = this._fb.group({
      searchKey: ['', [Validators.required]],
    });
    this.offlineSearchForm! = this._fb.group({
      searchKey: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.userData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    if(!this.userData.data){
    this.router.navigateByUrl('/login');
  }
    this.getCardsList(1, "offline");
    this.results$ = this.onlineSearchForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe((data: any) => this.getCardsList(1, "online"));
    this.results$ = this.offlineSearchForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe((data: any) => this.getCardsList(1, "offline"));
  }

  getCardsList(pageNum: number, type: string) {
    this.loader = true;
    this.page = pageNum;
    let searchKey = (type == "online") ? this.onlineSearchForm.controls.searchKey.value : this.offlineSearchForm.controls.searchKey.value
    this._dataService.getApi(environment.getCardsListURL + `?size=${this.pageSize}&pageNo=${pageNum}&searchText=${searchKey}&type=${type}&ownerId=${this.userData.data.ownerId}`)
      .subscribe((res) => {
        this.loader = false;
        if (res && res.status.toLowerCase() == 'success') {
          this.cardsList = res.data;
          this.getExcelCardsList(type)
          if (pageNum == 1) {
            this.count = res.total;
          }
          this.pager = this._dataService.getPager(this.count, pageNum, this.pageSize);
        } else {
          this.cardsList = [];
        }
      }, (error) => {
        this.cardsList = [];
      })
  }

  getExcelCardsList(type: string) {
    let searchKey = (type == "online") ? this.onlineSearchForm.controls.searchKey.value : this.offlineSearchForm.controls.searchKey.value
    this._dataService.getApi(environment.getCardsListURL + `?size=1000&pageNo=${1}&searchText=${searchKey}&type=${type}`)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.excelCardsList = res.data;
          this.excelCardsList.forEach((element: any, i: number) => {
            delete element._id;
            delete element.cardHistory;
            delete element.createdBy;
            delete element.createdDate;
            delete element.employeeId;
            delete element.endDate;
            delete element.ownerId;
            delete element.renewedDate;
            delete element.role;
            delete element.standName;
            delete element.type;
            delete element.vehicleName;
            delete element.vehicleType;
            delete element.__v;
          });
        } else {
          this.excelCardsList = []
        }
      }, (error) => {
        this.excelCardsList = []
      })
  }


  copyTable() {
    var text = "S.No." +
      "\tStand ID" +
      "\t\tCard No" +
      "\t\tName of requestee" +
      "\t\tCustomer Name" +
      "\t\tPhone" +
      "\t\tVeh.No." +
      "\t\tPayment Date" +
      "\t\tPaied Amount"
    var textData = "";
    for (let i = 0; i < this.cardsList.length; i++) {
      textData += i + 1 +
        "\t\t" + (this.cardsList[i].standId ? this.cardsList[i].standId : 'N/A')
        + "\t\t" + (this.cardsList[i].cardId ? this.cardsList[i].cardId : 'N/A')
        + "\t" + (this.cardsList[i].customerName ? this.cardsList[i].customerName : 'N/A')
        + "\t" + (this.cardsList[i].customerPhone ? this.cardsList[i].customerPhone : 'N/A')
        + "\t" + (this.cardsList[i].vehicleNumber ? this.cardsList[i].vehicleNumber : 'N/A')
        //  + "\t" + (this.cardsList[i].duration ? this.cardsList[i].duration : 'N/A')
        + "\t" + (this.cardsList[i].renewedDate ? this.getDate(this.cardsList[i].renewedDate) : (this.cardsList[i].createdDate ? this.getDate(this.cardsList[i].createdDate) : "N/A"))
        + "\t" + (this.cardsList[i].amount ? this.cardsList[i].amount : 'N/A') + "\n"
    }
    let selBox = document.createElement('textarea');

    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = text + '\n' + textData;

    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    if (selBox) {
      // window.print()
    }
    // this.MessageService.add({ severity: 'success', summary: '', detail: 'Link copied to Clipboard' });
    else {
      // this.MessageService.add({ severity: 'warning', summary: '', detail: 'Unable to copy the link to Clipboard' });
    }
  }

  async printPDF() {
    var doc = new jsPDF('p', 'pt');
    var col = [["S.No.",
      "Stand ID",
      "Card No",
      "Name of requestee",
      "Customer Name",
      "Phone",
      "Veh.No.",
      "Payment Date",
      "Paied Amount"]];
    var rows: any = [];
    await this.cardsList.forEach((element: any, i: number) => {
      var temp = [i + 1,
        , (this.cardsList[i].standId ? this.cardsList[i].standId : 'N/A')
        , (this.cardsList[i].cardId ? this.cardsList[i].cardId : 'N/A')
        , (this.cardsList[i].customerName ? this.cardsList[i].customerName : 'N/A')
        , (this.cardsList[i].customerPhone ? this.cardsList[i].customerPhone : 'N/A')
        , (this.cardsList[i].vehicleNumber ? this.cardsList[i].vehicleNumber : 'N/A')
        //  , (this.cardsList[i].duration ? this.cardsList[i].duration : 'N/A')
        , (this.cardsList[i].renewedDate ? this.getDate(this.cardsList[i].renewedDate) : (this.cardsList[i].createdDate ? this.getDate(this.cardsList[i].createdDate) : "N/A"))
        , (this.cardsList[i].amount ? this.cardsList[i].amount : 'N/A')];
      rows.push(temp);
    });
    await autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (data) => { },
    });
    // doc.output('dataurlnewwindow')
      doc.setProperties({ title: "Owner Monthly Cards" });
      // window.open(doc.output('bloburl'), '_blank')
      window.open(URL.createObjectURL(doc.output('blob')), '_blank')
  }

  getCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Parked Vehicles',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };
    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(this.excelCardsList);
  }
  exportAsXLSX(): void {
    this._dataService.exportAsExcelFile(this.excelCardsList, 'sample');
  }
  openModal(content: any) {
    this.modalService.open(content, { size: 'lg', centered: true });
  }
  getDate(date: any) {
    return moment(date).format("DD/MM/YYYY HH:mm:ss")
  }

}
