import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerMonthlyCardsComponent } from './owner-monthly-cards.component';

describe('OwnerMonthlyCardsComponent', () => {
  let component: OwnerMonthlyCardsComponent;
  let fixture: ComponentFixture<OwnerMonthlyCardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OwnerMonthlyCardsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerMonthlyCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
