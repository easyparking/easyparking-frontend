
import { DataService } from 'src/app/shared/services/data.service';
// import { MatAccordion } from '@angular/material';
import { Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment.prod';
import swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-plan-type',
  templateUrl: './plan-type.component.html',
  styleUrls: ['./plan-type.component.css']
})
export class PlanTypeComponent implements OnInit {

  page = 1;
  pageSize = 10;
  plansList: any = []
  searchKey: any = '';
  pager: any
  isEdit: boolean = false
  addEditPlanForm!: FormGroup
  spinner: boolean = false
  loader: boolean = false
  currentData: any;
  count: any =0;
  constructor(private modalService: NgbModal,
    private _dataService: DataService,
    private _fb: FormBuilder,
    private activeModal: NgbActiveModal) {
    this.addEditPlanForm = this._fb.group({
      planName: ['', [Validators.required]]
    })
  }

  ngOnInit(): void {
    this.searchPlansList(1)
  }

  searchPlansList(pageNum: any) {
    this.loader = true
    this._dataService.getApi(environment.getPlansURL + '?size=' + this.pageSize + '&pageNo=' + pageNum + '&searchText=' + this.searchKey)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.loader = false
          this.plansList = res.data;
          this.count = res.totalCount;
          this.pager = this._dataService.getPager(res.totalCount, pageNum, this.pageSize)
        }
        else {
          this.loader = false
          this.plansList = []
        }
      }, (error) => {
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }

  setPage(event: any) {
    this.page = event
    this.searchPlansList(this.page)
  }

  openModal(content: any, type: any, data?: any) {
    this.currentData = data
    this.addEditPlanForm.reset()
    this.spinner = false
    if (type == 'add') {
      this.isEdit = false
    }
    else {
      this.isEdit = true
      this.addEditPlanForm.controls.planName.setValue(data.type)
    }
    this.activeModal = this.modalService.open(content, { size: 'lg', centered: true });
  }


  addPlan() {
    this.spinner = true
    let dataToAPI = {
      "type": this.addEditPlanForm.value.planName,
    }
    this._dataService.postApi(environment.addPlansURL, dataToAPI)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          swal.fire("", res.message, "success");
          setTimeout(() => {
            swal.close()
          }, 3000);
          this.spinner = false;
          this.activeModal.close()
          this.searchPlansList(1)
        }
        else {
          this.spinner = false
          swal.fire("Warning!", res.message, "warning");
          setTimeout(() => {
            swal.close()
          }, 3000);
        }

      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }
  updatePlan() {
    this.spinner = true
    let dataToAPI = {
      "type": this.addEditPlanForm.value.planName,
      "id": this.currentData._id
    }
    this._dataService.putApi(environment.updatePlansURL, dataToAPI)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          swal.fire("", res.message, "success");
          setTimeout(() => {
            swal.close()
          }, 3000);
          this.spinner = false;
          this.activeModal.close()
          this.searchPlansList(1)
        }
        else {
          this.spinner = false
          swal.fire("Warning!", res.message, "warning");
          setTimeout(() => {
            swal.close()
          }, 3000);
        }

      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }

  deletePlan(planType: any) {
    swal.fire({
      title: 'Are you sure to delete this record?',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        // swal.fire('Saved!', '', 'success')
        this._dataService.deleteApi(environment.deletePlanURL + '/' + planType._id)
          .subscribe((res) => {
            if (res && res.status.toLowerCase() == 'success') {
              swal.fire("", res.message, "success");
              setTimeout(() => {
                swal.close()
              }, 3000);
              this.spinner = false;
              this.activeModal.close()
              this.searchPlansList(1)
            }
            else {
              this.spinner = false
              swal.fire("Warning!", res.message, "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }

          }, (error) => {
            this.spinner = false
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          })
      } else if (result.isDenied) {
      }
    })
  }

}
