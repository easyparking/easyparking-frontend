import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbDate, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/shared/services/data.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import swal from 'sweetalert2';
import * as moment from 'moment';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {

  searchForm!: FormGroup;
  page = 1;
  pageSize = 10;
  ownersList: any = [];
  standsList: any = [];
  owner: any = '';
  stand: any = '';
  analyticsType = "1";
  analytics: any;
  exitData: any = [];
  entryData: any;
  totalData: any = [];
  totalExitCount: number = 0;
  totalEntryCount: number = 0;
  loader: boolean = false;
  currentStand: any;

  constructor(private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private _fb: FormBuilder,
    private _dataService: DataService,
    private _authService: AuthService,
    private _router: Router) {
    this.searchForm! = this._fb.group({
      searchKey: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.getOwnersList();
    this.getAnalyticsData()
  }

  getStandsList(id: any) {
    this._dataService.getApi(environment.getStandsByOwnerURL + id)
      .subscribe((response) => {
        if (response.status == 'success') {
          localStorage.setItem("standsArray", JSON.stringify(response.data))
          this.standsList = response.data;
          this.stand = this.standsList.length > 0 ? this.standsList[0].standId : '';
        } else {
          this.standsList = []
        }
      })
  }

  getOwnersList() {
    this._dataService.getApi(environment.getOwnersListURL)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() != 'fail') {
          this.ownersList = res.data;
          this.owner = this.ownersList.length > 0 ? this.ownersList[0].ownerId : '';
          this.getStandsList(this.owner)
        } else {
          this.ownersList = [];
        }
      })
  }

  getType(type: any) {
    if (type == "1") {
      return "All";
    } else if (type == "2") {
      return "Entry";
    } else if (type == "3") {
      return "Exit";
    } else if (type == "4") {
      return "Entry";
    } else {
      return "Exit";
    }
  }

  getAnalyticsData() {
    let data = {
      "startTime": (this.analyticsType != "4" && this.analyticsType != "5") ? moment().subtract(1, 'weeks').startOf('week').add(1, "days") : moment().format("yyyy-MM-DDT00:00:00"),
      "endTime": (this.analyticsType != "4" && this.analyticsType != "5") ? moment().subtract(1, 'weeks').endOf('week').add(1, "days") : moment().format("yyyy-MM-DDT23:59:59"),
      "type": this.getType(this.analyticsType),
      "ownerId": this.owner,
      "standId": this.stand
    }
    this.loader = true;
    this._dataService.postApi(environment.getAnalytics, data)
      .subscribe((res) => {

        if (res && res.status == 'success') {
          this.currentStand  = this.stand
          this.analytics = res.result;
          this.makeTableData(data)
          // swal.fire("", res.message, "success");
          this.loader = false;
        } else {
          this.loader = false
          // swal.fire("Warning!", res.message, "warning");
        }
      }, (error) => {
        this.loader = false
        // swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
      })
  }

  makeTableData(data: any) {
    if (this.analyticsType != "4" && this.analyticsType != "5") {
      this.exitData = [0, 0, 0, 0, 0, 0, 0];
      this.entryData = [0, 0, 0, 0, 0, 0, 0];
      this.totalData = [0, 0, 0, 0, 0, 0, 0];
      this.totalExitCount = 0;
      this.totalEntryCount = 0;
      this.analytics.forEach((element: any) => {

        if (element.status != "Entry") {
          if (moment(data.startTime).format("DD/MM/yyyy") == moment(element.dateAndTimeOfExit).format("DD/MM/yyyy")) {
            this.exitData[0] += 1
            this.totalData[0] += 1;
          } else if (moment(data.startTime).add(1, "day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfExit).format("DD/MM/yyyy")) {
            this.exitData[1] += 1
            this.totalData[1] += 1;
          } else if (moment(data.startTime).add(2, "day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfExit).format("DD/MM/yyyy")) {
            this.exitData[2] += 1
            this.totalData[2] += 1;
          } else if (moment(data.startTime).add(3, "day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfExit).format("DD/MM/yyyy")) {
            this.exitData[3] += 1
            this.totalData[3] += 1;
          } else if (moment(data.startTime).add(4, "day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfExit).format("DD/MM/yyyy")) {
            this.exitData[4] += 1
            this.totalData[4] += 1;
          } else if (moment(data.startTime).add(5, "day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfExit).format("DD/MM/yyyy")) {
            this.exitData[5] += 1
            this.totalData[5] += 1;
          } else if (moment(data.startTime).add(6, "day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfExit).format("DD/MM/yyyy")) {
            this.exitData[6] += 1
            this.totalData[6] += 1;
          }
          this.totalExitCount += 1
        }
        if (element.status == "Entry") {
          if (moment(data.startTime).format("DD/MM/yyyy") == moment(element.dateAndTimeOfParking).format("DD/MM/yyyy")) {
            this.entryData[0] += 1;
            this.totalData[0] += 1;
          } else if (moment(data.startTime).add(1, "day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfParking).format("DD/MM/yyyy")) {
            this.entryData[1] += 1
            this.totalData[1] += 1;
          } else if (moment(data.startTime).add(2, "day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfParking).format("DD/MM/yyyy")) {
            this.entryData[2] += 1;
            this.totalData[2] += 1;
          } else if (moment(data.startTime).add(3, "day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfParking).format("DD/MM/yyyy")) {
            this.entryData[3] += 1;
            this.totalData[3] += 1;
          } else if (moment(data.startTime).add(4, "day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfParking).format("DD/MM/yyyy")) {
            this.entryData[4] += 1;
            this.totalData[4] += 1;
          } else if (moment(data.startTime).add(5, "day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfParking).format("DD/MM/yyyy")) {
            this.entryData[5] += 1
            this.totalData[5] += 1;
          } else if (moment(data.startTime).add(6, "day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfParking).format("DD/MM/yyyy")) {
            this.entryData[6] += 1
            this.totalData[6] += 1;
          }
          this.totalEntryCount += 1;
        }
      });
    } else {
      this.exitData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

      this.analytics.forEach((element: any) => {
        if (element.status == "Entry") {
          if (moment(element.dateAndTimeOfParking).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 02:00:00")) {
            this.exitData[0] += 1
            this.totalData[0] += 1;
          } else if (moment(element.dateAndTimeOfParking).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 04:00:00")) {
            this.exitData[1] += 1
            this.totalData[1] += 1;
          } else if (moment(element.dateAndTimeOfParking).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 06:00:00")) {
            this.exitData[2] += 1
            this.totalData[2] += 1;
          } else if (moment(element.dateAndTimeOfParking).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 08:00:00")) {
            this.exitData[3] += 1
            this.totalData[3] += 1;
          } else if (moment(element.dateAndTimeOfParking).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 10:00:00")) {
            this.exitData[4] += 1
            this.totalData[4] += 1;
          } else if (moment(element.dateAndTimeOfParking).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 12:00:00")) {
            this.exitData[5] += 1
            this.totalData[5] += 1;
          } else if (moment(element.dateAndTimeOfParking).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 14:00:00")) {
            this.exitData[6] += 1
            this.totalData[6] += 1;
          } else if (moment(element.dateAndTimeOfParking).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 16:00:00")) {
            this.exitData[7] += 1
            this.totalData[7] += 1;
          } else if (moment(element.dateAndTimeOfParking).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 18:00:00")) {
            this.exitData[8] += 1
            this.totalData[8] += 1;
          } else if (moment(element.dateAndTimeOfParking).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 20:00:00")) {
            this.exitData[9] += 1
            this.totalData[9] += 1;
          } else if (moment(element.dateAndTimeOfParking).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 22:00:00")) {
            this.exitData[10] += 1
            this.totalData[10] += 1;
          } else if (moment(element.dateAndTimeOfParking).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 23:59:59")) {
            this.exitData[11] += 1
            this.totalData[11] += 1;
          }
          this.totalExitCount += 1
        }
        if (element.status != "Entry") {
          if (moment(element.dateAndTimeOfExit).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 02:00:00")) {
            this.exitData[0] += 1
            this.totalData[0] += 1;
          } else if (moment(element.dateAndTimeOfExit).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 04:00:00")) {
            this.exitData[1] += 1
            this.totalData[1] += 1;
          } else if (moment(element.dateAndTimeOfExit).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 06:00:00")) {
            this.exitData[2] += 1
            this.totalData[2] += 1;
          } else if (moment(element.dateAndTimeOfExit).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 08:00:00")) {
            this.exitData[3] += 1
            this.totalData[3] += 1;
          } else if (moment(element.dateAndTimeOfExit).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 10:00:00")) {
            this.exitData[4] += 1
            this.totalData[4] += 1;
          } else if (moment(element.dateAndTimeOfExit).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 12:00:00")) {
            this.exitData[5] += 1
            this.totalData[5] += 1;
          } else if (moment(element.dateAndTimeOfExit).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 14:00:00")) {
            this.exitData[6] += 1
            this.totalData[6] += 1;
          } else if (moment(element.dateAndTimeOfExit).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 16:00:00")) {
            this.exitData[7] += 1
            this.totalData[7] += 1;
          } else if (moment(element.dateAndTimeOfExit).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 18:00:00")) {
            this.exitData[8] += 1
            this.totalData[8] += 1;
          } else if (moment(element.dateAndTimeOfExit).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 20:00:00")) {
            this.exitData[9] += 1
            this.totalData[9] += 1;
          } else if (moment(element.dateAndTimeOfExit).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 22:00:00")) {
            this.exitData[10] += 1
            this.totalData[10] += 1;
          } else if (moment(element.dateAndTimeOfExit).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 23:59:59")) {
            this.exitData[11] += 1
            this.totalData[11] += 1;
          }
          this.totalExitCount += 1
        }

        // if(element.status == "Entry"){
        //   if(moment(element.dateAndTimeOfParking).format("DD/MM/yyyy HH:MM:ss") <= moment().format("DD/MM/yyyy 02:00:00")){
        //     this.entryData[0] += 1;
        //     this.totalData[0] += 1;
        //   } else if(moment(data.startTime).add(1,"day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfParking).format("DD/MM/yyyy")){
        //     this.entryData[1] += 1
        //     this.totalData[1] += 1;
        //   } else if(moment(data.startTime).add(2,"day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfParking).format("DD/MM/yyyy")){
        //     this.entryData[2] += 1;
        //     this.totalData[2] += 1;
        //   } else if(moment(data.startTime).add(3,"day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfParking).format("DD/MM/yyyy")){
        //     this.entryData[3] += 1;
        //     this.totalData[3] += 1;
        //   } else if(moment(data.startTime).add(4,"day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfParking).format("DD/MM/yyyy")){
        //     this.entryData[4] += 1;
        //     this.totalData[4] += 1;
        //   } else if(moment(data.startTime).add(5,"day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfParking).format("DD/MM/yyyy")){
        //     this.entryData[5] += 1
        //     this.totalData[5] += 1;
        //   } else if(moment(data.startTime).add(6,"day").format("DD/MM/yyyy") == moment(element.dateAndTimeOfParking).format("DD/MM/yyyy")){
        //     this.entryData[6] += 1
        //     this.totalData[6] += 1;
        //   }
        //   this.totalEntryCount += 1;
        // }
      });
    }
  }

}
