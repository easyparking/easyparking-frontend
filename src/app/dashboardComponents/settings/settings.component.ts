import { environment } from 'src/environments/environment.prod';
import { DataService } from './../../shared/services/data.service';
import { MatAccordion } from '@angular/material/expansion';
// import { MatAccordion } from '@angular/material';
import { Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { NgbdSortableHeader } from 'src/app/directives/tableDirective';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';


@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  showStates: boolean = true
  showDistricts: boolean = false
  showLocations: boolean = false
  spinner: boolean = false
  stateValue: any
  districtValue: any
  cityValue: any
  statePageSize: any = 10;
  districtPageSize: any = 10;
  cityPageSize: any = 10;
  statesList: any = []
  districtsList: any = []
  citiesList: any = []
  tempStatesList: any = []
  tempDistrictsList: any = []
  tempCitiesList: any = []
  searchStateValue: any = ''
  searchDistrictValue: any;
  searchCityValue: any;
  statesPager: any;
  statesPageNum: any = 1;
  districtsPager: any;
  districtsPageNum: any = 1;
  citiesPager: any;
  citiesPageNum: any = 1;
  currentStateData: any = {}
  currentDistrictData: any = {}
  currentStateDataBreadCrumb: any = {}
  currentDistrictDataBreadCrumb: any = {}
  loader: boolean = false;
  showEdit: boolean = false;
  currentData: any;
  constructor(private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private _dataService: DataService) {

  }

  ngOnInit(): void {
    this.showStates = true
    this.showDistricts = false
    this.showLocations = false
    this.getStates(1)
    // this.getDistricts(1)
    // this.getCities(1)
  }
  openModal(content: any, data?: any, type?: string) {
    if (type && type.includes("Edit") && data) {
      this.showEdit = true;
      this.currentData = data;
      if (type.includes("state")) {
        this.stateValue = data.state;
        this.showStates = true;
      } else if (type.includes("district")) {
        this.showDistricts = true;
        this.districtValue = data.district;
      } else if (type.includes("location")) {
        this.showLocations = true;
        this.cityValue = data.city;
      }
    } else {
      this.stateValue = ''
      this.districtValue = ''
      this.cityValue = ''
    }
    this.spinner = false
    this.activeModal = this.modalService.open(content, { size: 'lg', centered: true });
  }


  showDistrictsFn(data: any) {
    this.currentStateData = data
    this.currentStateDataBreadCrumb = data
    this.showStates = false
    this.showDistricts = true
    this.showLocations = false
  }
  showLocationsFn(data: any) {
    this.currentDistrictData = data
    this.showStates = false
    this.showDistricts = false
    this.showLocations = true
  }
  getStates(pageNum: any) {
    this.loader = true
    this._dataService.getApi(environment.getStatesURL)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.loader = false
          this.statesList = res.data
          this.tempStatesList = res.data
          this.statesPager = this._dataService.getPager(res.data.length, pageNum, this.statePageSize)
        }
        else {
          this.loader = false
          this.statesList = []
          this.tempStatesList = []
        }
      }, (err) => {
        this.loader = false
        // this.statesList = []
        // this.tempStatesList = []
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }
  setStatesPage(event: any) {
    this.statesPageNum = event
    this.getStates(this.statesPageNum)
  }

  getDistricts(pageNum: any, data: any) {
    this.currentDistrictData = data
    this.loader = true
    this._dataService.getApi(environment.getDistrictsURL + '/' + data._id)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.loader = false
          this.districtsList = res.data
          this.tempDistrictsList = res.data
          this.districtsPager = this._dataService.getPager(res.data.length, pageNum, this.districtPageSize)
        }
        else {
          this.loader = false
          this.districtsList = []
          this.tempDistrictsList = []
        }
      }, (err) => {
        // this.districtsList = []
        // this.tempDistrictsList = []
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }
  setDistrictsPage(event: any) {
    this.districtsPageNum = event
    this.getDistricts(this.districtsPageNum, this.currentDistrictData)
  }

  getCities(pageNum: any, data: any) {
    this.currentStateData = data
    this.loader = true
    this._dataService.getApi(environment.getCitiesURL + "/" + data._id)
      .subscribe((res) => {
        if (res && res && res.status.toLowerCase() == 'success') {
          this.loader = false
          this.citiesList = res.data
          this.tempCitiesList = res.data
          this.citiesPager = this._dataService.getPager(res.data.length, pageNum, this.cityPageSize)
        }
        else {
          this.loader = false
          this.citiesList = []
          this.tempCitiesList = []
        }
      }, (err) => {
        // this.citiesList = []
        // this.tempCitiesList = []
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
        setTimeout(() => {
          swal.close()
        }, 3000);
      })
  }
  setCitiesPage(event: any) {
    this.citiesPageNum = event
    this.getCities(this.citiesPageNum, this.currentStateData)
  }

  searchStates(pageNum: any) {
    this.loader = true;
    if (this.searchStateValue) {
      this.statesList = this.tempStatesList.filter((element: any) => element.state.toLowerCase().includes(this.searchStateValue.toLowerCase()))
      this.statesPager = this._dataService.getPager(this.statesList.length, pageNum, this.statePageSize)
      this.loader = false
    }
    else {
      this.statesList = this.tempStatesList
      this.statesPager = this._dataService.getPager(this.statesList.length, pageNum, this.statePageSize)
      this.loader = false
    }
  }

  searchDistricts(pageNum: any) {
    this.loader = true;
    if (this.searchDistrictValue) {
      this.districtsList = this.tempDistrictsList.filter((element: any) => element.district.toLowerCase().includes(this.searchDistrictValue.toLowerCase()))
      this.districtsPager = this._dataService.getPager(this.districtsList.length, pageNum, this.districtPageSize)
      this.loader = false
    } else {
      this.districtsList = this.tempDistrictsList
      this.districtsPager = this._dataService.getPager(this.districtsList.length, pageNum, this.districtPageSize)
      this.loader = false
    }
  }

  searchCities(pageNum: any) {
    this.loader = true;
    if (this.searchCityValue) {
      this.citiesList = this.tempCitiesList.filter((element: any) => element.city.toLowerCase().includes(this.searchCityValue.toLowerCase()))
      this.citiesPager = this._dataService.getPager(this.citiesList.length, pageNum, this.cityPageSize)
      this.loader = false
    } else {
      this.citiesList = this.tempCitiesList
      this.citiesPager = this._dataService.getPager(this.citiesList.length, pageNum, this.cityPageSize)
      this.loader = false
    }
  }

  addLocations(type: any) {
    this.spinner = true;
    if (this.showEdit) {
      this.updateLocations(type)
    } else {
      if (type == 'state') {
        this._dataService.postApi(environment.addStatesUrl, {
          "state": this.stateValue
        })
          .subscribe((res) => {
            if (res && (res.status.toLowerCase() == "success" || res.status.toLowerCase() == "sucess")) {
              swal.fire("", res.message, "success");
              setTimeout(() => {
                swal.close()
              }, 3000);
              this.spinner = false;
              this.activeModal.close();
              this.getStates(1)
            }
            else {
              this.spinner = false
              this.activeModal.close();
              swal.fire("Warning!", res.message, "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }
          }, (error) => {
            this.spinner = false
            this.activeModal.close();
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          })
      }
      else if (type == 'district') {
        this._dataService.postApi(environment.addDistrictURL, {
          "stateId": this.currentStateData._id,
          "districtsCount": this.currentStateData.districtsCount + 1,
          "district": this.districtValue
        })
          .subscribe((res) => {
            if (res && (res.status.toLowerCase() == "success" || res.status.toLowerCase() == "sucess")) {
              swal.fire("", res.message, "success");
              setTimeout(() => {
                swal.close()
              }, 3000);
              this.spinner = false;
              this.currentStateData.districtsCount += 1
              this.activeModal.close();
              this.getDistricts(1, this.currentDistrictData)
            }
            else {
              this.spinner = false
              this.activeModal.close();
              swal.fire("Warning!", res.message, "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }
          }, (error) => {
            this.spinner = false
            this.activeModal.close();
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          })
      }
      else if (type == 'location') {
        this._dataService.postApi(environment.addCityURL, {
          "districtId": this.currentDistrictData._id,
          "stateId": this.currentDistrictData.stateId,
          "citiesCount": this.currentDistrictData.citiesCount + 1,
          "city": this.cityValue
        })
          .subscribe((res) => {
            if (res && (res.status.toLowerCase() == "success" || res.status.toLowerCase() == "sucess")) {
              swal.fire("", res.message, "success");
              setTimeout(() => {
                swal.close()
              }, 3000);
              this.spinner = false;
              this.currentDistrictData.citiesCount += 1
              this.activeModal.close();
              this.getCities(1, this.currentStateData)
            }
            else {
              this.spinner = false
              this.activeModal.close();
              swal.fire("Warning!", res.message, "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }
          }, (error) => {
            this.spinner = false
            this.activeModal.close();
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          })
      }
    }
  }

  updateLocations(type: string) {
    this.spinner = true
    if (type == 'state') {
      this._dataService.putApi(environment.updateStateURL + this.currentData._id, {
        "state": this.stateValue
      })
        .subscribe((res) => {
          if (res && (res.status.toLowerCase() == "success" || res.status.toLowerCase() == "sucess")) {
            swal.fire("", res.message, "success");
            setTimeout(() => {
              swal.close()
            }, 3000);
            this.spinner = false;
            this.activeModal.close();
            this.getStates(1)
          }
          else {
            this.spinner = false
            this.activeModal.close();
            swal.fire("Warning!", res.message, "warning");
            setTimeout(() => {
              swal.close()
            }, 3000);
          }
        }, (error) => {
          this.spinner = false
          this.activeModal.close();
          swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
          setTimeout(() => {
            swal.close()
          }, 3000);
        })
    }
    else if (type == 'district') {
      this._dataService.putApi(environment.updateDistrictURL + this.currentData._id, {
        "district": this.districtValue
      })
        .subscribe((res) => {
          if (res && (res.status.toLowerCase() == "success" || res.status.toLowerCase() == "sucess")) {
            swal.fire("", res.message, "success");
            setTimeout(() => {
              swal.close()
            }, 3000);
            this.spinner = false;
            this.currentStateData.districtsCount += 1
            this.activeModal.close();
            this.getDistricts(1, this.currentDistrictData)
          }
          else {
            this.spinner = false
            this.activeModal.close();
            swal.fire("Warning!", res.message, "warning");
            setTimeout(() => {
              swal.close()
            }, 3000);
          }
        }, (error) => {
          this.spinner = false
          this.activeModal.close();
          swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
          setTimeout(() => {
            swal.close()
          }, 3000);
        })
    }
    else if (type == 'location') {
      this._dataService.putApi(environment.updateCityURL + this.currentData._id, {
        "city": this.cityValue
      })
        .subscribe((res) => {
          if (res && (res.status.toLowerCase() == "success" || res.status.toLowerCase() == "sucess")) {
            swal.fire("", res.message, "success");
            setTimeout(() => {
              swal.close()
            }, 3000);
            this.spinner = false;
            this.currentDistrictData.citiesCount = + 1
            this.activeModal.close();
            this.getCities(1, this.currentStateData)
          }
          else {
            this.spinner = false
            this.activeModal.close();
            swal.fire("Warning!", res.message, "warning");
            setTimeout(() => {
              swal.close()
            }, 3000);
          }
        }, (error) => {
          this.spinner = false
          this.activeModal.close();
          swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
          setTimeout(() => {
            swal.close()
          }, 3000);
        })
    }
  }

  deleteStateFn(id: any) {
    swal.fire({
      title: 'Are you sure to delete this record?',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this._dataService.deleteApi(environment.deleteStateURL + id)
          .subscribe((res) => {
            if (res && (res.status.toLowerCase() == "success" || res.status.toLowerCase() == "sucess")) {
              swal.fire("", res.message, "success");
              setTimeout(() => {
                swal.close()
              }, 3000);
              this.getStates(1)
            }
            else {
              swal.fire("Warning!", res.message, "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }
          }, (error) => {
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          })
      } else if (result.isDenied) {
      }
    })
  }
  deleteDistrictFn(id: any) {
    swal.fire({
      title: 'Are you sure to delete this record?',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this._dataService.deleteApi(environment.deleteDistrictURL + id + '?stateId=' + this.currentDistrictData._id + '&districtsCount=' + (this.currentStateData.districtsCount - 1))
          .subscribe((res) => {
            if (res && (res.status.toLowerCase() == "success" || res.status.toLowerCase() == "sucess")) {
              swal.fire("", res.message, "success");
              setTimeout(() => {
                swal.close()
              }, 3000);
              this.currentStateData.districtsCount = - 1
              this.getDistricts(1, this.currentDistrictData)
            }
            else {
              swal.fire("Warning!", res.message, "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }
          }, (error) => {
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          })
      } else if (result.isDenied) {
      }
    })
  }
  deleteLocationFn(id: any) {
    swal.fire({
      title: 'Are you sure to delete this record?',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this._dataService.deleteApi(environment.deleteLocationURL + id + "?districtId=" + this.currentDistrictData._id + "&citiesCount=" + (this.currentDistrictData.citiesCount - 1))
          .subscribe((res) => {
            if (res && (res.status.toLowerCase() == "success" || res.status.toLowerCase() == "sucess")) {
              this.currentDistrictData.citiesCount -= 1
              swal.fire("", res.message, "success");
              setTimeout(() => {
                swal.close()
              }, 3000);
              this.getCities(1, this.currentStateData)
            }
            else {
              swal.fire("Warning!", res.message, "warning");
              setTimeout(() => {
                swal.close()
              }, 3000);
            }
          }, (error) => {
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          })
      } else if (result.isDenied) {
      }
    })
  }
  backClicked(type: any) {
    if (type == 'showDisricts') {
      this.showDistricts = false;
      this.showStates = true;
      this.showLocations = false
    }
    else if (type == 'showLocations') {
      this.showDistricts = true;
      this.showStates = false;
      this.showLocations = false
    }
  }
}
