import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { DataService } from '../shared/services/data.service';

@Component({
  selector: 'app-showpdf',
  templateUrl: './showpdf.component.html',
  styleUrls: ['./showpdf.component.css']
})
export class ShowpdfComponent implements OnInit {
  selectedStand: any;
  data: any;
  exitTime: any;
  entryTime: any;
  text1: any;
  text2: any;
  text3: any;
  text4: any;
  note: any;
  currentDate: string = '';
  constructor(private _dataService: DataService) {

    this._dataService.exitParkingData.subscribe((result: any) => {
      this.data = result;
      this.currentDate = moment(new Date()).format("DD-MM-YYYY HH:mm:ss")
      if (this.data.dateAndTimeOfParking) {
        this.entryTime = moment(this.data.dateAndTimeOfParking).format("DD-MM-YYYY HH:mm:ss");
      } else {
        this.entryTime = '--'
      }
      if (this.data.dateAndTimeOfExit) {
        this.exitTime = moment(this.data.dateAndTimeOfExit).format("DD-MM-YYYY HH:mm:ss");
      } else {
        this.exitTime = "Pending"
      }
    })

    this._dataService.standDetails.subscribe((result: any) => {
      this.selectedStand = result;
      if(result){
        this.text1 = this.selectedStand[0].addressLine1 ? this.selectedStand[0].addressLine1 : ''
        this.text2 = this.selectedStand[0].addressLine2 ? this.selectedStand[0].addressLine2 : ''
        this.text3 = this.selectedStand[0].addressLine3 ? this.selectedStand[0].addressLine3 : ''
        this.text4 = this.selectedStand[0].addressLine4 ? this.selectedStand[0].addressLine4 : ''
        this.note = this.selectedStand[0].note ? this.selectedStand[0].note : ''
      }
    })
  }

  ngOnInit(): void {
    // this.exitTime;
    // this.entryTime;
  }

}
