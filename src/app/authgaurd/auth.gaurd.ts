import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { EncryptDecryptService } from '../shared/services/encrypt-decrypt.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router, private _decryptService: EncryptDecryptService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};;
    if (currentUser) {
      if( state.url == "/reset-password"){
        return true;
      }else if (currentUser.data.role == 'owner' && (
        state.url == "/owner-dashboard" ||
        state.url == "/session-history" ||
        state.url == "/force-logout" ||
        state.url == "/ownercardsales" ||
        state.url == "/minireports" ||
        state.url == "/reports" ||
        state.url == "/slip-lost-vehicles" ||
        state.url == "/login-activity"
      )) {
        return true;
      } else if (currentUser.data.role == 'admin' && (
        state.url == "/admin-dashboard" ||
        state.url == "/analytics" ||
        state.url == "/accounts" ||
        state.url == "/view-stands" ||
        state.url == "/employs" ||
        state.url.includes("/price-rules") ||
        state.url == "/vehicleSearch" ||
        state.url == "/appregistrations" ||
        state.url == "/wanted-vehicles" ||
        state.url == "/minireports" ||
        state.url == "/reports" ||
        state.url == "/cardsales" ||
        state.url == "/online-payment-report" ||
        state.url == "/demo-requests" ||
        state.url == "/force-logout" ||
        state.url == "/locations" ||
        state.url == "/vehicle-names" ||
        state.url == "/login-activity" ||
        state.url == "/applications" ||
        state.url == "/plan-type" ||
        state.url == "/parking-type" || 
        state.url == "/help-request")) {
        return true;
      } else if (currentUser.data.role == 'employee' && (
        state.url == "/employee-dashboard" ||
        state.url == "/parkings" ||
        state.url == "/monthlycards" ||
        state.url == "/reports" ||
        state.url == "/minireports" ||
        state.url == "/slip-lost-vehicles" ||
        state.url == "/session-history" ||
        state.url == "/login-activity")) {
        return true;
      }
    }

    // not logged in so redirect to login page with the return url
    localStorage.clear();
    this.router.navigateByUrl('/login');
    return false;
  }
}
