import { environment } from './../environments/environment.prod';
import { Component } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, NavigationStart } from '@angular/router';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';
import { DataService } from './shared/services/data.service';
import { AuthService } from './shared/services/auth.service';
import { EncryptDecryptService } from './shared/services/encrypt-decrypt.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'parking-web';
  idleState = 'Not started.';
  timedOut = false;
  // lastPing?: Date = ;
  constructor(private router: Router,
    private idle: Idle,
    private keepalive: Keepalive,
    private _authService: AuthService,
    private _dataService: DataService, private authservice: AuthService, private _decryptService: EncryptDecryptService,
    private _router: Router) {
    // if (_authService.isAuthenticated() == true && router.url != "/userSignIn") {
    //   // idle.setIdle(1200);
    //   // idle.setTimeout(360);
    //   idle.setIdle(10);
    //   idle.setTimeout(10);
    //   idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    //   idle.onTimeoutWarning.subscribe((countdown: number) => {
    //     alert('Timeout Warning - ' + countdown);
    //   });
    //   idle.onTimeout.subscribe(() => {
    //     this.logout()
    //     // this.router.navigate(['lockScreen', { url: this.router.url }]);
    //   });
    //   this.reset()
    // }
    if (this.authservice.isAuthenticated()) {

      let sessionData = localStorage.getItem("sessionStartTime") ? JSON.parse(localStorage.getItem("sessionStartTime") || '') : {};
      let tokenData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
      console.log("tokenData",tokenData);
      
      if(tokenData.data){
        let dataTOAPI = {
          "userName": tokenData.data.userName,
          "start_time": sessionData.start_time
        }
        this._dataService.postApi(environment.checkSessionUrl, dataTOAPI)
          .subscribe((res) => {
            console.log("res",res);
            if (!res.result.end_time) {
              if (tokenData.data.role == 'owner') {
                this._router.navigateByUrl('/owner-dashboard')
              } else if (tokenData.data.role == 'admin') {
                // localStorage.setItem('accessByRole', 'admin')
                this._router.navigateByUrl('/admin-dashboard')
              } else if (tokenData.data.role == 'employee') {
                if (localStorage.getItem("standSelected")) {
                  this._router.navigateByUrl('/employee-dashboard')
                }
              } else {
                this._router.navigateByUrl('/login')
              }
            } else {
              this._router.navigateByUrl('/login')
            }
          })
      } else {
        this.router.navigateByUrl('/login');
      }
    }
  }

  ngOnInit() {
    this._dataService.getIPAddress().subscribe(res => {
      if (res && res.ip) {
        this._dataService.setIpAddress(res.ip)
      }
    });
  }

  reset() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;

  }
  logout() {
    this._dataService.postApi(environment.updateLoginStatusURL, {
      "userName": this._authService.getToken().userName
    })
      .subscribe((res) => {
        localStorage.clear();
        this.router.navigateByUrl('/login')
        this._dataService.setLoginData('')
      })
  }
}
