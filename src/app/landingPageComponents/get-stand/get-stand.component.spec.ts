import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetStandComponent } from './get-stand.component';

describe('GetStandComponent', () => {
  let component: GetStandComponent;
  let fixture: ComponentFixture<GetStandComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetStandComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetStandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
