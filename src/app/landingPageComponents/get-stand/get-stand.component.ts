import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EncryptDecryptService } from './../../shared/services/encrypt-decrypt.service';
import { DataService } from './../../shared/services/data.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { NgbCalendar, NgbDateParserFormatter, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateFRParserFormatt } from 'src/app/Pipes/Dateparserformat';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-get-stand',
  templateUrl: './get-stand.component.html',
  styleUrls: ['./get-stand.component.css'],
  providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatt }]
})
export class GetStandComponent implements OnInit {
  // @ViewChild('cardFormModal') cardFormModal?: ElementRef;
  @ViewChild('content', { static: false }) private content: any;
  standData: any
  allowedVehicles: any = []
  userData: any
  isLoggedIn: boolean = false
  monthlyCardForm!: FormGroup;
  cDate!: NgbDateStruct;
  selectDisable: boolean = true
  spinner: boolean = false;
  constructor(private modalService: NgbModal,
    private _dataService: DataService,
    private _decryptService: EncryptDecryptService,
    private _fb: FormBuilder,
    calendar: NgbCalendar,
    private router: Router) {
    this.cDate = calendar.getToday()
    this.monthlyCardForm = this._fb.group({
      startDate: [new Date(), [Validators.required]],
      vehicleType: ['', [Validators.required]],
      duration: ['1', [Validators.required]],
      name: ['', [Validators.required]],
      vehicleNumber: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      finalAmount: ['', [Validators.required]],
      email: ['', [Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
    })

  }

  ngOnInit(): void {
    this.userData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    // if(!this.userData.data){
    // this.router.navigateByUrl('/login');
  // }
    this.standData = this._decryptService.decryptData(localStorage.getItem('homeStandData') || '')  
    if(localStorage.getItem("monthlyCardDetails")&& JSON.parse(localStorage.getItem("monthlyCardDetails") || '')) {
      this.buyCard(JSON.parse(localStorage.getItem("monthlyCardDetails") || ''));
      if(this.userData.data){
      this.isLoggedIn = true;
      }
    }
    this.getPriceRulesData()
  }

  getPriceRulesData() {
    this.allowedVehicles = []
    this._dataService.getApi(environment.getPriceRulesURL + this.standData.standId)
      .subscribe((res) => {
        if (res.status.toLowerCase() == 'success') {
          for (let i = 0; i < res.data[0].vehicleTypes.length; i++) {
            for (let j = 0; j < this.standData.allowedVehicles.length; j++) {
              if (res.data[0].vehicleTypes[i].vehicletype == this.standData.allowedVehicles[j]) {
                this.allowedVehicles.push(res.data[0].vehicleTypes[i])
              }
            }
          }

        }
      })
  }


  getVehicleType(type: string) {
    if (type == "bicycle") {
      return "Bicycle";
    } else if (type == "twoWheeler") {
      return "2 W";
    } else if (type == "threeWheeler") {
      return "3 W";
    } else if (type == "fourWheeler") {
      return "4 W";
    } else {
      return type;
    }
  }

  proceedPayment(modal:any) {
    this.spinner = true;
    let bodyToAPI = {
      "purpose": "testing",
      "amount": 100,
      "name": "pavan",
      "phone": "9701002329",
      "email": "saipavan.anand@gmail.com",
      "redirectUrl": "https://easyparking-dev.web.app/"
    }
    this._dataService.postApi(environment.paymentRequestUrl, bodyToAPI)
      .subscribe((res) => {
        if (res.status.toLowerCase() == 'success') {
          if (JSON.parse(res.response).success) {
            this.spinner = false
            // this.addCard(modal)
            window.open(JSON.parse(res.response).payment_request.longurl, '_blank');
          }
          else {
            swal.fire('Oops!', 'Something went wrong. Please try again', 'warning');
          }

        }
        else {
          this.spinner = false
          swal.fire("", res.message, "warning");
        }
      }, (err) => {
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
      })
  }
  buyCard(allowedVehicle: any) {
    this.spinner = false
    this.monthlyCardForm.reset()
    this.monthlyCardForm.controls['duration'].setValue('')
    this.monthlyCardForm.controls['vehicleType'].disable();
    if(!localStorage.getItem("monthlyCardDetails")) {
      this.modalService.open(this.content, { size: 'lg', centered: true });
      if (this.userData) {
        if (this.userData.data && this.userData.data.role == 'customer') {
          this.isLoggedIn = true
          setTimeout(() => {
            this.monthlyCardForm.controls['vehicleType'].setValue(allowedVehicle.vehicletype)
            this.monthlyCardForm.controls['name'].setValue(this.userData.data.name)
            this.monthlyCardForm.controls['vehicleNumber'].setValue(this.userData.data.vehicleNumber)
            this.monthlyCardForm.controls['phoneNumber'].setValue(this.userData.data.phoneNumber)
            this.monthlyCardForm.controls['amount'].setValue(allowedVehicle.cardOfferPrice)
            this.monthlyCardForm.controls['finalAmount'].setValue(allowedVehicle.cardOfferPrice + 10)
          }, 1);
        } else {
          localStorage.setItem("monthlyCardDetails",JSON.stringify(allowedVehicle))
          this.isLoggedIn = false;
        }
      } else {
        localStorage.setItem("monthlyCardDetails",JSON.stringify(allowedVehicle))
        this.isLoggedIn = false;
      }
    } else {
      allowedVehicle = JSON.parse(localStorage.getItem("monthlyCardDetails") || '')
      if (this.userData) {
        if (this.userData.data && this.userData.data.role == 'customer') {
          localStorage.removeItem("monthlyCardDetails")
          this.isLoggedIn = true
          setTimeout(() => {
            this.monthlyCardForm.controls['vehicleType'].setValue(allowedVehicle.vehicletype)
            this.monthlyCardForm.controls['name'].setValue(this.userData.data.name)
            this.monthlyCardForm.controls['vehicleNumber'].setValue(this.userData.data.vehicleNumber)
            this.monthlyCardForm.controls['phoneNumber'].setValue(this.userData.data.phoneNumber)
            this.monthlyCardForm.controls['amount'].setValue(allowedVehicle.cardOfferPrice)
            this.monthlyCardForm.controls['finalAmount'].setValue(allowedVehicle.cardOfferPrice + 10)
          }, 1);
        }
      }
    }
  }

  addCard(modal:any) {
    let startDate = new Date(this.monthlyCardForm.controls.startDate.value.month + '/' + this.monthlyCardForm.controls.startDate.value.day + '/' + this.monthlyCardForm.controls.startDate.value.year)
    let endDate = new Date(this.monthlyCardForm.controls.startDate.value.month + '/' + this.monthlyCardForm.controls.startDate.value.day + '/' + this.monthlyCardForm.controls.startDate.value.year)
    let dataToAPI = {
      "standId": this.standData.standId,
      "standName": this.standData.standName,
      "createdBy": this.userData.data.userName,
      "employeeId": this.userData.data.employeeId,
      "ownerId": this.standData.ownerId,
      "vehicleType": this.monthlyCardForm.controls.vehicleType.value,
      "vehicleName": "",
      "customerName": this.monthlyCardForm.controls.name.value,
      "customerPhone": this.monthlyCardForm.controls.phoneNumber.value,
      "vehicleNumber": this.monthlyCardForm.controls.vehicleNumber.value.toUpperCase(),
      "duration": this.monthlyCardForm.controls.duration.value,
      "amount": this.monthlyCardForm.controls.amount.value,
      "role": this.userData.data.role,
      "startDate": this.monthlyCardForm.controls.startDate.value ? startDate.toString() : '',
      "sendSMS": this.standData.SMS.monthlyCards,
      "endDate": new Date(endDate.setMonth(endDate.getMonth() + parseInt(this.monthlyCardForm.controls.duration.value))).toString()
    }
    this.spinner = true;
    this._dataService.postApi(environment.addCardsUrl, dataToAPI)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.spinner = false;
          swal.fire("", res.message, "success");
          setTimeout(() => {
            swal.close()
          }, 3000);

          modal.dismiss('Data saved')
        }
        else {
          this.spinner = false;
          
          swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
          setTimeout(() => {
            swal.close();
            modal.dismiss('Data saved')
          }, 3000);
        }
      });
  }


  getValue(value: any) {
    return parseInt(value)
  }
  mapClicked($event: MouseEvent) {

  }
}
