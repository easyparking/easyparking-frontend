import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { DataService } from 'src/app/shared/services/data.service';
import { environment } from 'src/environments/environment.prod';
import swal from 'sweetalert2';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {

  addRequrestForm!: FormGroup;
  spinner: boolean = false;
  constructor(private router: Router,
    private _dataService: DataService,
    private _authService: AuthService,
    private _decryptService: EncryptDecryptService,
    private _fb: FormBuilder,
    private _router: Router) { 
    this.addRequrestForm = this._fb.group({
      fullName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      subject: ['', [Validators.required]],
      message: ['', [Validators.required]]
    }) }

  ngOnInit(): void {
  }
  addHelpRequest(){
    this.spinner = true;
    let dataToAPI = {
      "name": this.addRequrestForm.value.fullName,
      "email": this.addRequrestForm.value.email,
      "subject": this.addRequrestForm.value.subject ? this.addRequrestForm.value.subject : '',
      "message": this.addRequrestForm.value.message ? this.addRequrestForm.value.message : ''
    }
    this._dataService.postApi(environment.addHelpRequestsURL, dataToAPI)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          swal.fire("", res.message, "success");
          this.spinner = false;
          this._router.navigateByUrl("/home");
        } else {
          this.spinner = false;
          swal.fire("Warning!", res.message, "warning");
        }
      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
      })
  }
}
