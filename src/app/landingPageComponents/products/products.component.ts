import { DataService } from 'src/app/shared/services/data.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  sendLinkForm!: FormGroup
  spinner: boolean = false
  constructor(private _fb: FormBuilder,
    private _dataService: DataService,
    private _router: Router) {
    this.sendLinkForm = this._fb.group({
      phoneNumber: ['', [Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]],
    })
  }

  ngOnInit(): void {
  }
  sendLink() {
    this.spinner = true
    this._dataService.getApi(environment.sendDownloadLinkURL + this.sendLinkForm.value.phoneNumber)
      .subscribe((res) => {
        if (res && res.status == 'success') {
          swal.fire("", res.message, "success");
          this.spinner = false;
          this.sendLinkForm.reset()
          this._router.navigateByUrl('/login')
        }
        else {
          this.spinner = false
          swal.fire("Warning!", res.message, "warning");
        }

      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
      })

  }
}
