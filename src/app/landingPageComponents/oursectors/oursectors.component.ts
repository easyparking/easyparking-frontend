import { EncryptDecryptService } from './../../shared/services/encrypt-decrypt.service';
import { DataService } from './../../shared/services/data.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-oursectors',
  templateUrl: './oursectors.component.html',
  styleUrls: ['./oursectors.component.scss']
})
export class OursectorsComponent implements OnInit, OnDestroy {
  state: any = ''
  city: any = ''
  statesList: any = []
  citiesList: any = []
  loader: boolean = false;
  searchData: any = [];
  // initial center position for the map
  lat: number = 0;
  lng: number = 0;
  label: string | undefined;
  constructor(private _router: Router,
    private _dataService: DataService,
    private _encryptService: EncryptDecryptService) { }

  ngOnInit(): void {
    this.loader = true;
    this.getStates();
    let searchDetails = this._encryptService.decryptData(localStorage.getItem('homeSearchDetails') || '')
    if (!searchDetails) {
      this.searchStandsAPICall();
    }
  }
  getStandDetails(data: any) {
    var encryptedData = this._encryptService.encryptData(data)
    localStorage.setItem('homeStandData', encryptedData)
    this._router.navigateByUrl('/viewStand')
  }
  changeState(event: any) {
    if (this.state == '' || this.state.length == 0) {
      this.city = ''
      this.citiesList = []
    } else {
      this.getAllCities(this.state)
    }

  }
  changeCity(event: any) {
  }
  getStates() {
    this._dataService.getApi(environment.getStatesURL)
      .subscribe((res) => {
        if (res.status.toLowerCase() == 'success') {
          this.statesList = res.data;
          let searchDetails = this._encryptService.decryptData(localStorage.getItem('homeSearchDetails') || '')
          if (searchDetails) {
            this.getAllCities(searchDetails.state)
          }
        }
      })
  }
  getAllCities(data: any) {
    this._dataService.getApi(environment.getAllCitiesURL + '/' + (data._id))
      .subscribe((res) => {
        if (res.status.toLowerCase() == 'success') {

          this.citiesList = res.result
          let searchDetails = this._encryptService.decryptData(localStorage.getItem('homeSearchDetails') || '')
          if (searchDetails) {
            setTimeout(() => {
              this.city = searchDetails.city;
              this.state = searchDetails.state;
              this.searchStandsAPICall()
            }, 1);
            localStorage.removeItem('homeSearchDetails')
          }
        }
      })
  }
  searchStandsAPICall() {
    this.loader = true
    this._dataService.postApi(environment.searchStandsHomeURL, {
      "vehicleType": "",
      "duration": "",
      "startDate": "",
      "state": (this.city != '' || this.city.length != 0) ? this.city.stateId : '',
      "city": (this.city != '' || this.city.length != 0) ? this.city._id : ''
    })
      .subscribe((res) => {
        if (res.status.toLowerCase() == 'success') {
          this.loader = false
          this.searchData = res.result
          this.lat = parseInt(this.searchData[0].latitude)
          this.lng = parseInt(this.searchData[0].longtitue)
        }
        else {
          this.loader = false
          this.searchData = []
        }
      })
  }

  ngOnDestroy() {
    localStorage.removeItem('homeSearchDetails')
  }
  clickedMarker(label: string) {
    this.label = label
  }

}
