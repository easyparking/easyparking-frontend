import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OursectorsComponent } from './oursectors.component';

describe('OursectorsComponent', () => {
  let component: OursectorsComponent;
  let fixture: ComponentFixture<OursectorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OursectorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OursectorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
