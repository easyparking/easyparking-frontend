import { Router } from '@angular/router';
import { DataService } from 'src/app/shared/services/data.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  show: boolean = false
  signUpForm!: FormGroup
  spinner: boolean = false
  otpValue: any;
  isVerified  = false;
  constructor(private _fb: FormBuilder,
    private _dataService: DataService,
    private _router: Router) {
    this.signUpForm = this._fb.group({
      userName: ['', [Validators.required]],
      name: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]],
      otp: ['', [Validators.required]],
      vehicleNumber: ['', [Validators.required,Validators.pattern("^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$")]],
      pwd: ['', [Validators.required]],
      reTypePwd: ['', [Validators.required]]
    })
  }

  ngOnInit(): void {
  }
  showHidePassword() {
    this.show = !this.show
  }
  submitSignUpForm() {    
    let dataToAPI = {
      "name": this.signUpForm.value.name,
      "userName": this.signUpForm.value.userName,
      "ownerId": "",
      "standId": "",
      "role": "customer",
      "phoneNumber": this.signUpForm.controls.phoneNumber.value,
      "vehicleNumber": this.signUpForm.value.vehicleNumber,
      "password": this.signUpForm.value.pwd,
    }
    this._dataService.postApi(environment.createUsersURL, dataToAPI)
      .subscribe((res) => {
        if (res && res.status == 'success') {
          swal.fire("", res.message, "success");
          this.spinner = false;
          this.signUpForm.reset()
          this._router.navigateByUrl('/login')
        }
        else {
          this.spinner = false
          swal.fire("Warning!", res.message, "warning");
        }

      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
      })
  }

  matchPasswords() {
    if (this.signUpForm.controls.pwd.value && this.signUpForm.controls.reTypePwd.value) {
      // if (this.signUpForm.controls.reTypePwd.value.length >= this.signUpForm.controls.pwd.value.length) {
      if (this.signUpForm.controls.pwd.value != this.signUpForm.controls.reTypePwd.value) {
        this.signUpForm.controls.reTypePwd.setErrors({ noMatch: true })
        // this.signUpForm.controls.reTypePwd.markAsDirty()
        // this.signUpForm.controls.reTypePwd.markAsTouched()
      }

      else {
        this.signUpForm.controls.reTypePwd.setErrors(null)
      }
      // }
      // else{

      // }

    }
    else if (this.signUpForm.controls.pwd.value) {
      if (this.signUpForm.controls.pwd.value.indexOf(' ') >= 0) {
        this.signUpForm.controls.pwd.setErrors({ specialCharacter: true })
      }
      else {
        this.signUpForm.controls.pwd.setErrors(null)
      }
    }
  }

  sendOTP() {
    if(!this.isVerified){
    this._dataService.getApi(environment.sendOTPURL + "/" + this.signUpForm.controls.phoneNumber.value)
      .subscribe((res) => {
        if (res.status.toLowerCase() == 'success') {
          this.otpValue = res.otp
        }
        else {
          swal.fire("Warning!", res.message, "warning");
        }
      }, (error) => {
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
      })
    }
  }
  enterOTPValue() {
    if (this.signUpForm.controls.otp.value && this.signUpForm.controls.otp.value.length >= 6) {
      if (this.signUpForm.controls.otp.value != this.otpValue) {
        this.signUpForm.controls.otp.setErrors({ invalidOTP: true })
      }
      else {
        this.signUpForm.controls.otp.setErrors(null);
        this.signUpForm.controls.phoneNumber.disable();
        this.isVerified = true;
      }
    }
    else if (this.signUpForm.controls.otp.value.length == 0) {
      this.signUpForm.controls.otp.setErrors({ required: true })
    }
  }
  removeSpace(form: any, formControlName: any) {
    form.controls[formControlName].setValue(form.controls[formControlName].value.split(" ").join(""))
  }
}
