import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { DataService } from 'src/app/shared/services/data.service';
import { environment } from 'src/environments/environment.prod';
import swal from 'sweetalert2';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  spinner: boolean = false
  isEmployee: boolean = false
  isCustomer: boolean = false
  addCareersForm!: FormGroup
  @ViewChild('closeCareersModal')
  closeCareersModal!: ElementRef;
  standSelected: boolean = true
  ipAddress: string = '';
  constructor(private router: Router,
    private _dataService: DataService,
    private _authService: AuthService,
    private _decryptService: EncryptDecryptService,
    private _fb: FormBuilder,
    private _router: Router) {
    this.addCareersForm = this._fb.group({
      fullName: ['', [Validators.required]],
      qualification: ['', [Validators.required]],
      age: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.maxLength(2)]],
      email: ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      phoneNumber: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]],
      state: ['', [Validators.required]],
      city: ['', [Validators.required]],
      district: ['', [Validators.required]],
      salary: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      CompleteAddress: [''],
      message: ['']
    })
  }

  ngOnInit(): void {
    this._dataService.ipAddressValue.subscribe((result: any) => {
      this.ipAddress = result;
    });
     this.standSelected=!localStorage.getItem("standSelected")
    this._dataService.loginData.subscribe((message) => {
      if ((message)) {
        this.getHeaderData(message)
      }
      else {
        this.getHeaderData(localStorage.getItem('accessByRole'))
      }

    })
    // this._dataService.standData.subscribe((message) => {
    //   if ((message)) {
    //     this.standSelected = false
    //   } else {
    //     this.standSelected = true
    //   }
    // })
  }

  getHeaderData(message: any) {
    if (message == 'employee') {
      this.isEmployee = true
      // this.isCustomer = false
    }
    else if (message == 'customer') {
      this.isCustomer = true
      // this.isEmployee = false
    }
    else {
      this.isCustomer = false
      this.isEmployee = false
    }
  }

  logout() {
    let standDetails: any;
    if (localStorage.getItem("standsArray")) {
      standDetails = JSON.parse(localStorage.getItem("standsArray") || "")
    }

    const userData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    if(!userData.data){
    this.router.navigateByUrl('/login');
  }
  const sessionDetails = localStorage.getItem("sessionStartTime");
    if (sessionDetails) {
      this._dataService.putApi(environment.endSessionURL, {
        "end_time": new Date(Date.now()).toString(),
        "start_time": JSON.parse(localStorage.getItem("sessionStartTime") || "").start_time,
        "sessionId": JSON.parse(sessionDetails)._id,
        // "start_time": localStorage.getItem("sessionStartTime"),
        "employeeId": userData.data.employeeId,
        "role": userData.data.role,
        "userName": this._authService.getToken().data.userName,
        "employeeName": this._authService.getToken().data.name,
        "standName": standDetails && standDetails.length > 0 ? standDetails[0].standName : '',
        "standId": standDetails && standDetails.length > 0 ? standDetails[0].standId : '',
        "ownerId": standDetails && standDetails.length > 0 ? standDetails[0].ownerId : ''
      })

        .subscribe(res => {
          localStorage.clear();
          this._dataService.setLoginData('')
          this.router.navigateByUrl('/login')
        })
    }
    else {
      localStorage.clear();
      this._dataService.setLoginData('')
      this.router.navigateByUrl('/home')
    }

  }

  careersAdded() {

    this.spinner = true
    // fullName: ['', [Validators.required]],
    // : ['', [Validators.required]],

    let dataToAPI = {
      "name": this.addCareersForm.value.fullName,
      "qualification": this.addCareersForm.value.qualification,
      "age": this.addCareersForm.value.age,
      "email": this.addCareersForm.value.email,
      "phone": this.addCareersForm.value.phoneNumber,
      "state": this.addCareersForm.value.state,
      "city": this.addCareersForm.value.city,
      "district": this.addCareersForm.value.district,
      "address": this.addCareersForm.value.CompleteAddress ? this.addCareersForm.value.CompleteAddress : '',
      "message": this.addCareersForm.value.message ? this.addCareersForm.value.message : '',
      "prefferedSalary": parseInt(this.addCareersForm.value.salary)
    }
    this._dataService.postApi(environment.addCareersURL, dataToAPI)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          swal.fire("", res.message, "success");
          this.spinner = false;
          this.closeCareersModal.nativeElement.click()
          this.addCareersForm.reset()
        }
        else {
          this.spinner = false
          swal.fire("Warning!", res.message, "warning");
        }

      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
      })
  }
  routeToEmpDashboard() {
    // this.createSession(JSON.parse(localStorage.getItem('empResponse') || ""), JSON.parse(localStorage.getItem('standLogin') || ""))
    this._router.navigateByUrl('/employee-dashboard')
  }

  createSession(dataResult: any, standData?: any) {
    let dataTOAPI = {
      "start_time": new Date(Date.now()).toString(),
      "end_time": null,
      "userName": dataResult.data.userName ? dataResult.data.userName : '',
      "standId": standData ? standData.standId : (dataResult.data.standId ? dataResult.data.standId : ''),
      "standName": standData ? standData.standName : (dataResult.data.standName ? dataResult.data.standName : ''),
      "ownerId": dataResult.data.ownerId ? dataResult.data.ownerId : '',
      "role": dataResult.data.role ? dataResult.data.role : '',
      "createdDate": new Date(Date.now()).toString(),
      "name": dataResult.data.name,
      "phone": dataResult.data.phoneNumber,
      "ipaddress": this.ipAddress,
      "employeeId": dataResult.data.employeeId
    }
    this._dataService.postApi(environment.createSessionURL, dataTOAPI)
      .subscribe((res) => {
        localStorage.setItem("sessionStartTime", JSON.stringify(res.data))

      })
  }

}
