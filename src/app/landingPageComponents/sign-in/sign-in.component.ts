import { environment } from './../../../environments/environment.prod';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { DataService } from 'src/app/shared/services/data.service';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SignInComponent implements OnInit {
  // horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  // verticalPosition: MatSnackBarVerticalPosition = 'top';
  loginForm: FormGroup;
  isEmployee: boolean = false
  showWarning: boolean = false
  warningMessage: any = ''
  standsArray: any = [];
  selectedStand: any = ''
  // config = new MatSnackBarConfig();

  date: any
  empResponse: any
  ipAddress: string = '';
  spinner: boolean = false;
  constructor(private _fb: FormBuilder,
    private _router: Router,
    private _dataService: DataService,
    private _snackBar: MatSnackBar,
    private _encryptService: EncryptDecryptService) {
    this.loginForm = this._fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    })
  }
  ngOnInit(): void {
    this.loginForm.reset();
    this._dataService.ipAddressValue.subscribe((result: any) => {
      this.ipAddress = result;
    });
    this.isEmployee = false
  }
  loginFormSubmitted() {
    // this.config.panelClass = ['mat-warn']
    // this.config.horizontalPosition = this.horizontalPosition,
    //   this.config.verticalPosition = this.verticalPosition,
    //   this.config.duration = 5000,
    this.spinner = true;
    let dataToAPI = {
      "userName": this.loginForm.value.username,
      "password": this.loginForm.value.password
    }
    // if (this.loginForm.value.username == 'customer') {

    // }
    // else {
    this._dataService.postApi(environment.loginURL, dataToAPI)
      .subscribe((result) => {
        this.spinner = false;
        // let decryptedData = btoa(JSON.stringify(result))
        var encryptedData = this._encryptService.encryptData(result)
        localStorage.setItem('UserData', encryptedData)
        if (result) {
          if (result.status == 'success') {
            this.date = new Date(Date.now()).toString()
            this.empResponse = result
            localStorage.setItem("currentDate", this.date)
            if (result.data.role == 'owner') {
              this.createSession(result)
              this.isEmployee = false
              localStorage.setItem('accessByRole', 'owner')
              this._router.navigateByUrl('/owner-dashboard')
            } else if (result.data.role == 'admin') {
              this.createSession(result)
              this.isEmployee = false
              localStorage.setItem('accessByRole', 'admin')
              this._router.navigateByUrl('/admin-dashboard')
            } else if (result.data.role == 'employee') {
              this._dataService.getApi(environment.getStandsByOwnerURL + result.data.ownerId)
                .subscribe((response) => {
                  if (response.status == 'success') {
                    localStorage.setItem("standsArray", JSON.stringify(response.data))
                    this.standsArray = response.data
                  } else {
                    this.standsArray = []
                  }
                })
              this.isEmployee = true
              localStorage.setItem('accessByRole', 'employee')
              this._dataService.setLoginData('employee')

            }
            else if (result.data.role == 'customer') {
              this._dataService.setLoginData('customer')
              localStorage.setItem('accessByRole', 'customer')
              if(!localStorage.getItem("monthlyCardDetails")) {
              this._router.navigateByUrl('/my-account')
              } else {
                this._router.navigateByUrl('/viewStand')
              }
            }
          }
          else {
            this.showWarning = true
            this.warningMessage = result.message
            setTimeout(() => {
              this.showWarning = false
            }, 5000);
            // this._snackBar.open(result.message, '', this.config)
          }
        }
        else {
          this.showWarning = true
          this.warningMessage = "Something went wrong. Please try again after sometime!!"
          setTimeout(() => {
            this.showWarning = false
          }, 5000);
        }
      })
    // }



  }
  standOptionChanged($event: any) {
    localStorage.setItem("standSelected", this.selectedStand)
    let selectedStand = this.standsArray.filter((item: any) => item.standId == this.selectedStand)
    localStorage.setItem("empResponse", JSON.stringify(this.empResponse))
    localStorage.setItem("standLogin", JSON.stringify(selectedStand[0]))
    this._dataService.setStandData($event)
  }

  loginClicked() {
    let selectedStand = this.standsArray.filter((item: any) => item.standId == this.selectedStand);
    // this._dataService.getApi(environment.checkSessionUrl+ '/'+(selectedStand[0] ? selectedStand[0].standId : (this.empResponse.data.standId ? this.empResponse.data.standId : '')))
    //   .subscribe((res) => {
    //     this.createSession(this.empResponse, selectedStand[0])
    //   },error=>{
    this.createSession(this.empResponse, selectedStand[0])
  // })
    localStorage.setItem("standSelected", this.selectedStand)
  }
  createSession(dataResult: any, standData?: any) {
    let dataTOAPI = {
      "start_time": new Date(Date.now()).toString(),
      "end_time": null,
      "userName": dataResult.data.userName ? dataResult.data.userName : '',
      "standId": standData ? standData.standId : (dataResult.data.standId ? dataResult.data.standId : ''),
      "standName": standData ? standData.standName : (dataResult.data.standName ? dataResult.data.standName : ''),
      "standCity": standData ? standData.location.city.city : '',
      "ownerId": dataResult.data.ownerId ? dataResult.data.ownerId : '',
      "role": dataResult.data.role ? dataResult.data.role : '',
      "createdDate": new Date(Date.now()).toString(),
      "name": dataResult.data.name,
      "phone": dataResult.data.phoneNumber,
      "ipaddress": this.ipAddress,
      "employeeId": dataResult.data.employeeId
    };
    this.spinner = true;
    this._dataService.postApi(environment.createSessionURL, dataTOAPI)
      .subscribe((res) => {
        localStorage.setItem("sessionStartTime", JSON.stringify(res.data));
        localStorage.setItem("pendingVehiclesOnLogin",JSON.stringify(res.pendingVehiclesOnLogin))
        if (this.empResponse.data.role == 'employee') {
          this.spinner = false;
          this.isEmployee = false;
          this._router.navigateByUrl('/employee-dashboard')
        }
      })
  }

}

