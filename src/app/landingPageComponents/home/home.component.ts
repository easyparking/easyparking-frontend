import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { DataService } from 'src/app/shared/services/data.service';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';
import { environment } from 'src/environments/environment.prod';
import swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  images = [33, 32].map((n) => `../../assets/images/${n}.jpg`);
  state: any = '';
  city: string = '';
  citiesList: any = [];
  statesList: any;
  date: any;
  vehicle: any = '';
  duration: any = '';
  constructor(private authservice: AuthService, private _decryptService: EncryptDecryptService,
    private _router: Router,
    private _dataService: DataService,) {

  }

  ngOnInit(): void {
    localStorage.removeItem('homeSearchDetails')
    this.getStates();
  }
  searchStand() {
    if(this.city&&this.state){
      localStorage.setItem('homeSearchDetails',this._decryptService.encryptData({
        date: this.date,
        city: this.city,
        state: this.state,
        vehicle : this.vehicle,
        duration : this.duration
        }))
      this._router.navigateByUrl('/oursectors')
    } else {
      swal.fire("Warning!", 'Please Select State and City', "warning");
    }
   
  }
  changeState(event: any) {
    if (this.state == '' || this.state.length == 0) {
      this.city = ''
      this.citiesList = []
    } else {
      this.getAllCities(this.state)
    }
  }

  getStates() {
    this._dataService.getApi(environment.getStatesURL)
      .subscribe((res) => {
        if (res.status.toLowerCase() == 'success') {
          this.statesList = res.data
        }
      })
  }
  getAllCities(data: any) {
    this._dataService.getApi(environment.getAllCitiesURL + '/' + (data._id))
      .subscribe((res) => {
        if (res.status.toLowerCase() == 'success') {
          this.citiesList = res.result
        }
      })
  }
}
