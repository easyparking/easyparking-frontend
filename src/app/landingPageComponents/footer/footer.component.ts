import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from 'src/app/shared/services/data.service';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';
import { environment } from 'src/environments/environment.prod';
import swal from 'sweetalert2';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  currentYear: any;
  requestDemoForm: FormGroup;
  spinner = false;
  constructor(private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private _fb: FormBuilder,
    private _dataService: DataService,
    private _decryptService: EncryptDecryptService,
    private _router: Router) {
    this.currentYear = (new Date()).getFullYear();
    this.requestDemoForm! = this._fb.group({
      fullName: ['', Validators.required],
      phone: ['', [Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]],
      location: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      completeAddress: ['', Validators.required],
      message: ['', Validators.required]
    })
  }

  ngOnInit(): void {
  }

  openModal(content: any) {
    this.modalService.open(content, { size: 'lg', centered: true });
  }

  requestDemo(modal: any) {
    let dataToAPI = {
      "name": this.requestDemoForm.controls.fullName.value,
      "location": this.requestDemoForm.controls.location.value,
      "email": this.requestDemoForm.controls.email.value,
      "phone": this.requestDemoForm.controls.phone.value,
      "address": this.requestDemoForm.controls.completeAddress.value,
      "message": this.requestDemoForm.controls.message.value
    }
    this.spinner = true;
    this._dataService.postApi(environment.requestDemoUrl, dataToAPI)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.spinner = false;
          modal.dismiss('Data saved')
          swal.fire("", res.message, "success");
          this.requestDemoForm.reset()
        }
        else {
          this.spinner = false
          swal.fire('Oops!', res.message, 'error');
        }
      });
  }

}
