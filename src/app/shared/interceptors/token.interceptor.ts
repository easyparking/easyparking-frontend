
import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private router: Router,
    private _authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let newRequest;
    const re = '/userSignIn';
    const headers1 = new HttpHeaders({
      // 'Authorization': `Bearer ${this._authService.getToken().accessToken}`,
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
    // if ((request.url.search(re) === -1)) {
    //   if (this._authService.isLoggedIn) {
    //     const headers = new HttpHeaders({
    //       'Authorization': `Bearer ${this._authService.getToken().accessToken}`,
    //       'Content-Type': 'application/json',
    //       'Accept': 'application/json'
    //     });

    //     newRequest = request.clone({ headers: headers });
    //     return next.handle(newRequest);

    //   } else {
    //     this.router.navigate(['/login']);
    //   }
    // }

    if ((request.url.includes('/payment-requests'))) {
      const headers = new HttpHeaders({
        'Access-Control-Allow-Origin': 'http://localhost:4200',
        'X-Api-Key': 'f373f539bd6c243eff012d7e7f1a6958',
        'X-Auth-Token': '75a76743aee32d4ee98ba7d3fa1fe833'
      });

      newRequest = request.clone({ headers: headers });
      return next.handle(newRequest);
    }
    return next.handle(request.clone({ headers: headers1 }));


  }
}
