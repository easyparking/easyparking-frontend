import { EncryptDecryptService } from './encrypt-decrypt.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  loggedInStatus: boolean = false
  constructor(private _decryptService: EncryptDecryptService) { }

  setLoggedIn(value: boolean) {
    this.loggedInStatus = value;
  }

  get isLoggedIn() {
    if (localStorage.getItem('UserData')) {

      let tokenData = this._decryptService.decryptData(localStorage.getItem('UserData'))
      if (tokenData && tokenData.accessToken) {
        return this.loggedInStatus = true;
      } else {
        return this.loggedInStatus = false;
      }
    }
    else {
      return this.loggedInStatus = false;
    }

  }

  isAuthenticated() {
    if (localStorage.getItem('UserData')) {
      return true
    }
    else {
      return false
    }

  }

  getToken(): any {
    return this._decryptService.decryptData(localStorage.getItem('UserData'))
  }

}
