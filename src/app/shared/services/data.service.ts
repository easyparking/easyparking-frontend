import { Injectable, EventEmitter } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, of, BehaviorSubject, Subject } from "rxjs";
import { map, catchError, tap, retry, shareReplay } from "rxjs/operators";
import { Router } from "@angular/router";
import * as _ from 'underscore';
var FileSaver = require('file-saver');
import * as XLSX from 'xlsx';
import { OnlineOfflineService } from "./online-offline.service";
import { environment } from "src/environments/environment.prod";

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private loginValue = new BehaviorSubject('');
  loginData = this.loginValue.asObservable();

  private exitParking = new BehaviorSubject('');
  exitParkingData = this.exitParking.asObservable();
  private selectedStand = new BehaviorSubject('');
  standDetails = this.selectedStand.asObservable();

  private ipAddress = new BehaviorSubject('');
  ipAddressValue = this.ipAddress.asObservable();

  private standValue = new BehaviorSubject('');
  standData = this.standValue.asObservable();

  private isCollapsed = new BehaviorSubject(false)
  isCollapsableValue = this.isCollapsed.asObservable()

  private dashboardsCountsData = new BehaviorSubject(false)
  dashboardsCountsValue = this.dashboardsCountsData.asObservable()
  
  private dataSyncing = new Subject()
  isSyncing = this.dataSyncing.asObservable().pipe(shareReplay())

  triggerNetwork = true;
  constructor(private http: HttpClient, private router: Router,private networkService: OnlineOfflineService) { }  
  
  setIsSyncing(message: any) {
    this.dataSyncing.next(message)
  }

  setSelectedStandDetails(message: any) {
    this.selectedStand.next(message)
  }

  setIpAddress(message: any) {
    this.ipAddress.next(message)
  }

  setExitParkingData(message: any) {
    this.exitParking.next(message)
  }

  setLoginData(message: any) {
    this.loginValue.next(message)
  }

  setStandData(message: any) {
    this.standValue.next(message)
  }

  isCollapsableMethod(message: any) {
    this.isCollapsed.next(message)
  }

  storeCountsData(data: any) {
    this.dashboardsCountsData.next(data)
  }

  getIPAddress(): Observable<any> {
    return this.http.get("https://api.ipify.org/?format=json").pipe(
      retry(3),
      map((uresponse: any) => {
        return uresponse;
      }),
      catchError(this.handleError<any>("Login"))
    );
  }

  getApi(endpoint: string): Observable<any> {
    if(endpoint.indexOf('?') == -1){
      endpoint += `?rev=${Math.random()}`
    } else {
      endpoint += `&rev=${Math.random()}`
    }
    return this.http.get(endpoint).pipe(
      // retry(3),
      map((uresponse: any) => {
        return uresponse;
      }),
      catchError(this.handleError<any>("Login"))
    );
  }

  getAPIWithoutRetry(endpoint: string): Observable<any> {
    if(endpoint.indexOf('?') == -1){
      endpoint += `?rev=${Math.random()}`
    } else {
      endpoint += `&rev=${Math.random()}`
    }
    return this.http.get(endpoint).pipe(
      map((uresponse: any) => {
        return uresponse;
      }),
      catchError(this.handleError<any>("Login"))
    );
  }

  search(term: string, url: any) {
    if (term === '') {
      return of([]);
    }
    return this.http
      .get(url + term).pipe(
        // retry(3),
        map((response: any) => response.data),
        catchError(this.handleError<any>("Login"))
      );
  }
  postApi(endpoint: string, data: any): Observable<any> {
    return this.http.post<any>(endpoint, JSON.stringify(data)).pipe(
      map((uresponse: any) => {
        return uresponse;
      }),
      catchError(this.handleError<any>("Login"))
    );
  }

  putApi(endpoint: string, data: any): Observable<any> {
    return this.http.put(endpoint, JSON.stringify(data)).pipe(
      map((uresponse: any) => {
        return uresponse;
      }),
      catchError(this.handleError<any>("Login"))
    );
  }

  deleteApi(endpoint: string): Observable<any> {
    return this.http.delete(endpoint).pipe(
      map((uresponse: any) => {
        return uresponse;
      }),
      catchError(this.handleError<any>("Login"))
    );
  }


  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      console.log({error});
      if(this.triggerNetwork){
        this.triggerNetwork = false;
        setTimeout(() => {
          this.triggerNetwork = true;
        }, 10000);
      // TODO: send the error to remote logging infrastructure
      // console.error(error); // log to // console instead
      if (
        (error.error.status == "401" &&
          error.error.message == "Unauthorized") ||
        error.status == 0
      ) {
        // this.router.navigate(["/"]);
      
        this.networkService.networkConnectivity$.next(false);
        const timer = setInterval(()=>{
          // console.log("interval started");
          this.getAPIWithoutRetry(environment.getStatesURL).subscribe(res => {
            console.log("interval cleared",res);
            if(res&&res.status == "Success"){
              this.networkService.networkConnectivity$.next(true);
              clearInterval(timer);
            } else {
              this.networkService.networkConnectivity$.next(false);
              clearInterval(timer);
              }
          },(error) => {
            this.networkService.networkConnectivity$.next(false);
            clearInterval(timer);
            })
        }, 10000)
      }
    }
      // TODO: better job of transforming error for user consumption
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getPager(totalItems: number, currentPage: number, pageSize: number) {
    // calculate total pages
    let startPage: number = 0, endPage: number = 0;
    let totalPages = Math.ceil(totalItems / pageSize);

    if (totalPages < 6) {
      // less than 10 total pages so show all

      if (currentPage < 6) {
        startPage = 1;
        endPage = totalPages;
      }
    }
    else if (totalPages > 5 && totalPages <= 10) {
      if (currentPage < 6) {
        startPage = 1;
        endPage = 5
      }
      else {
        startPage = currentPage - 1;
        endPage = totalPages;
      }
    } else {
      // more than 10 total pages so calculate start and end pages
      if (currentPage < 6) {
        startPage = 1;
        endPage = 5;
      } else if (currentPage + 4 > totalPages) {
        startPage = totalPages - 4;
        endPage = totalPages;
      } else {
        startPage = currentPage - 1;
        endPage = currentPage + 3;
      }
    }

    // calculate start and end item indexes
    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    // create an array of pages to ng-repeat in the pager control
    let pages = _.range(startPage, endPage + 1);

    // return object with all pager properties required by the view
    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages
    };
  }
  
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    // ,{header:["S","h","e","e_1","t","J","S_1"]}
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

}
