import { DataService } from './data.service';
import { OnlineOfflineService } from './online-offline.service';
import { Injectable } from '@angular/core';
import Dexie from 'dexie'; // wrapper for IndexedDB
import { resolve } from 'dns';
import { environment } from 'src/environments/environment.prod';


@Injectable({
  providedIn: 'root'
})
export class OfflineDataService {
  private addParkingsDb: any;
  private exitParkingsDb: any
  private donedb: any;
  private addMonthlyCards: any;
  private renewalMonthlyCards: any;
  constructor(private _offlineService: OnlineOfflineService,
    private _dataService: DataService) {
      // setTimeout(() => {
        this.registerToAddParkings()
        this.registerToExitParkings()
        this.createParkingsIndexedDatabase()
        this.createExitIndexedDatabase();
        this.registerToAddMonthlyCards();
        this.registerToRenewalMonthlyCards();
        this.createAddMonthlyCardsIndexedDatabase()
        this.createRenewalMonthlyCardsIndexedDatabase()
      // }, 5000);
  }

  ngOnInit() {
  }

  // ---------- create the indexedDB
  private createParkingsIndexedDatabase() {
    this.addParkingsDb = new Dexie("addParkingsDatabase");
    this.addParkingsDb.version(1).stores({
      parkings: "vehicleNumber,ownerId,phone,vehicleType,vehicleName,customerName,customerAddress,cardNumber,dateAndTimeOfParking,dateAndTimeOfExit,standName,standId,openEmpName,openEmpMobile,closedEmpName,closedEmpMobile,standMobileNumber,parkingId,status,expectedDuration,state,district,location,openEmpId,closedEmpId,sendSMS,ownerName"
    });
    this.addParkingsDb.open().catch(function (err: any) {
      console.error(err.stack || err);
    });
  }

  // ---------- add todo to the indexedDB on offline mode
  async addParkingsToIndexedDb(todo: any) {
    return this.addParkingsDb.parkings.add(todo)
  }

  private registerToAddParkings() {
    this._offlineService.networkConnectivity$.subscribe(online => {
      if (online) {
        //pass the items to the backend if the connetion is enabled
        setTimeout(() => {
          this.sendItemsFromAddParkingsIndexedDb();
        }, 5000);
      } else {
      }
    });
  }

  private async sendItemsFromAddParkingsIndexedDb() {
    const allOfflineAddedParkings: any[] = await this.addParkingsDb.parkings.toArray();
    if (allOfflineAddedParkings.length != 0) {
      this._dataService.setIsSyncing(true)
          this.addParkingDataToAPI(allOfflineAddedParkings[0]);
    } else {
      this._dataService.setIsSyncing(false)
      localStorage.removeItem("parkingsHistoryData")
    }
  }

  addParkingDataToAPI(dataToAPI: any) {
    this.addVehicle(dataToAPI)
    dataToAPI["isEntryOnOffline"] = true;
    this._dataService.postApi(environment.addParkingEntryURL, dataToAPI)
      .subscribe((res) => {
        // if (res && res.status == 'success') {
          this.addParkingsDb.parkings.delete(dataToAPI.vehicleNumber).then(() => {
            this.sendItemsFromAddParkingsIndexedDb()
          });
        // }
      }, (error) => {
      })
  }

  removeAddParkingData(data:any){
    this.addParkingsDb.parkings.delete(data.vehicleNumber).then(() => {
    });
  }

  addVehicle(data: any) {
    let vehicleData = {
      "vehicleName": data.vehicleName,
      "vehicleType": data.vehicleType
    }
    this._dataService.postApi(environment.addVehicleURL, vehicleData)
      .subscribe((res) => {

      }, (error) => {
      })
  }

  private createExitIndexedDatabase() {
    this.exitParkingsDb = new Dexie("exitParkingsDatabase");

    this.exitParkingsDb.version(1).stores({
      exits: "vehicleNumber,parkingId,status,closedEmpId,closedEmpName,closedEmpMobile,standId,dateAndTimeOfParking,dateAndTimeOfExit,vehicleType,cardNumber,sendSMS"
    });
    this.exitParkingsDb.open().catch(function (err: any) {
      console.error(err.stack || err);
    });
  }

  // ---------- add todo to the indexedDB on offline mode
  async addExitsToIndexedDb(todo: any) {
    return this.exitParkingsDb.exits.add(todo)
  }

  private registerToExitParkings() {
    this._offlineService.networkConnectivity$.subscribe(online => {
      if (online) {
        //pass the items to the backend if the connetion is enabled
        setTimeout(() => {
          this.sendItemsFromExitParkingsIndexedDb();
        }, 5000);
      } else {
      }
    });
  }

  private async sendItemsFromExitParkingsIndexedDb() {
    const allOfflineExitParkings: any[] = await this.exitParkingsDb.exits.toArray();
    if (allOfflineExitParkings.length != 0) {
      this._dataService.setIsSyncing(true);
        // send items to backend...
          this.exitParkingDataToAPI(allOfflineExitParkings[0])
    } else {
      this._dataService.setIsSyncing(false);
      localStorage.removeItem("parkingsHistoryData")
    }
  }

  exitParkingDataToAPI(dataToAPI: any) {
    if(dataToAPI.parkingId == ""){
      dataToAPI["isEntryOnOffline"] = true;
      this._dataService.postApi(environment.addParkingEntryURL, dataToAPI)
      .subscribe((res) => {
          this.exitParkingsDb.exits.delete(dataToAPI.vehicleNumber).then(() => {
            this.sendItemsFromExitParkingsIndexedDb();
          });
      }, (error) => {
      })
    } else {
      delete dataToAPI.ownerName;
      delete dataToAPI.ownerId;
      delete dataToAPI.standCode;
      dataToAPI["isExitOnOffline"] = true;
      this._dataService.putApi(environment.updateParkingURL, dataToAPI)
        .subscribe((res) => {
          // if (res && res.status == 'success') {
            this.exitParkingsDb.exits.delete(dataToAPI.vehicleNumber).then(() => {
              this.sendItemsFromExitParkingsIndexedDb();
            });
          // }
  
        }, (error) => {
  
        })
    }
  }
  
  // ---------- create the indexedDB for monthly cards add
  private createAddMonthlyCardsIndexedDatabase() {
    this.addMonthlyCards = new Dexie("addMonthlyCards");
    this.addMonthlyCards.version(1).stores({
      monthlyCards: "vehicleNumber,standId,standName,createdBy,employeeId,ownerId,vehicleType,vehicleName,customerName,customerPhone,duration,amount,role,startDate,sendSMS,endDate"
    });
    this.addMonthlyCards.open().catch(function (err: any) {
      console.error(err.stack || err);
    });
  }
  
  async addMonthlyCardsToIndexedDb(todo: any) {
    return this.addMonthlyCards.monthlyCards.add(todo)
  }

  private registerToAddMonthlyCards(){
    this._offlineService.networkConnectivity$.subscribe(online => {
      if (online) {
        //pass the items to the backend if the connetion is enabled
        setTimeout(() => {
          this.sendItemsFromAddMonthlyCardsIndexedDb();
        }, 5000);
      } else {
      }
    });
  } 

  private async sendItemsFromAddMonthlyCardsIndexedDb() {
    const allOfflineMonthlyCards: any[] = await this.addMonthlyCards.monthlyCards.toArray();
    if (allOfflineMonthlyCards.length != 0) {
      this._dataService.setIsSyncing(true);
        this.addMonthlyCardsToDb(allOfflineMonthlyCards[0])
    } else {
      this._dataService.setIsSyncing(false);
      localStorage.removeItem("monthlyCardsHistoryData")
    }
  }

  addMonthlyCardsToDb(dataToAPI: any) {
    dataToAPI["isOfflineData"] = true;
    this._dataService.postApi(environment.addCardsUrl, dataToAPI)
      .subscribe((res) => {
        // if (res && res.status.toLowerCase() == 'success') {
          this.addMonthlyCards.monthlyCards.delete(dataToAPI.vehicleNumber).then(() => {
            this.sendItemsFromAddMonthlyCardsIndexedDb()
          });
        // }
      })
  }

  // ---------- create the indexedDB for monthly cards renewal
  private createRenewalMonthlyCardsIndexedDatabase() {
    this.renewalMonthlyCards = new Dexie("renewalMonthlyCards");
    this.renewalMonthlyCards.version(1).stores({
      monthlyCards: "cardId,startDate,endDate,renewalDate,cardHistory,sendSMS"
    
    });
    this.renewalMonthlyCards.open().catch(function (err: any) {
      console.error(err.stack || err);
    });
  }
  
  async renewalMonthlyCardsToIndexedDb(todo: any) {
    return this.renewalMonthlyCards.monthlyCards.add(todo)
  }

  private registerToRenewalMonthlyCards(){
    this._offlineService.networkConnectivity$.subscribe(online => {
      if (online) {
        //pass the items to the backend if the connetion is enabled
        setTimeout(() => {
          this.sendItemsFromRenewalMonthlyCardsIndexedDb();
        }, 5000);
      } else {
      }
    });
  } 

  private async sendItemsFromRenewalMonthlyCardsIndexedDb() {
    const allOfflineRenewalMonthlycard: any[] = await this.renewalMonthlyCards.monthlyCards.toArray();
    if (allOfflineRenewalMonthlycard.length != 0) {
          this.renewalMonthlyCardsInDb(allOfflineRenewalMonthlycard[0]);
          this._dataService.setIsSyncing(true)
    } else {
      localStorage.removeItem("monthlyCardsHistoryData");
      this._dataService.setIsSyncing(false)
    }
  }

  renewalMonthlyCardsInDb(dataToAPI: any) {
    dataToAPI["isOfflineData"] = true;
    this._dataService.putApi(environment.cardRenewalUrl, dataToAPI)
      .subscribe((res) => {
        // if (res && res.status.toLowerCase() == 'success') {
          this.exitParkingsDb.monthlyCards.delete(dataToAPI.cardId).then(() => {
            this.sendItemsFromRenewalMonthlyCardsIndexedDb()
          });
        // }
      })
  }

}
