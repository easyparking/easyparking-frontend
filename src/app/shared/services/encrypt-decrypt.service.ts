import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class EncryptDecryptService {

  constructor(private router: Router) { }

  encryptData(result: any) {
    // Encrypt
    return CryptoJS.AES.encrypt(JSON.stringify(result), '123456$#@$^@1ERF').toString();
  }
  decryptData(data: any) {
    // Decrypt
    if(data){
      var bytes = CryptoJS.AES.decrypt(data, '123456$#@$^@1ERF');
      if(bytes.toString(CryptoJS.enc.Utf8)) {
        return (JSON.parse(bytes.toString(CryptoJS.enc.Utf8)));
      } else {
        localStorage.clear();
        this.router.navigateByUrl('/login');
      }
    } else {
      return null
    }

    // [{id: 1}, {id: 2}]
  }
}
