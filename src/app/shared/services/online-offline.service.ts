import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { Observable, fromEvent, merge, empty, BehaviorSubject, of } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import { mapTo } from 'rxjs/operators';

declare const window: any; //declare window object

@Injectable({
  providedIn: 'root'
})
export class OnlineOfflineService {
  networkConnectivity$ = new BehaviorSubject(true);
  networkStatus = true;
  // private connectionMonitor: Observable<boolean>;

  constructor( @Inject(PLATFORM_ID) platformId: Object) {
    // // console.log(PLATFORM_ID,isPlatformBrowser(PLATFORM_ID))
    this.networkConnectivity$.subscribe(status => {
      console.log({status})
      // this.connectionMonitor = of(false)
      this.networkStatus = status;
    });
    
// if (isPlatformBrowser(platformId)) {
//   const offline$ = fromEvent(window, 'offline').pipe(mapTo(false));
//   const online$ = fromEvent(window, 'online').pipe(mapTo(true));
//   this.connectionMonitor = merge(
//     offline$, online$
//   );
// } else {
//   this.connectionMonitor = empty();
// }
}
get isOnline() {
   return this.networkStatus ? navigator.onLine : false;
}

  // networkConnectivity$: Observable<boolean> {
  //   return this.networkConnectivity$;
  // }
}
