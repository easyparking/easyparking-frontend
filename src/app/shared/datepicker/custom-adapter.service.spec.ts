import { TestBed } from '@angular/core/testing';
import { CustomAdapter } from './custom-adapter.service';

describe('CustomAdapterService', () => {
  let service: CustomAdapter;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CustomAdapter);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
