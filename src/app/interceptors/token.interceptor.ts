// import { AuthService } from './../services/auth.service';
import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../shared/services/auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private router: Router,
    private _authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let newRequest;
    const re = '/userSignIn';
    const headers1 = new HttpHeaders({
      // 'Authorization': `Bearer ${this._authService.getToken().accessToken}`,
      'Cache-Control':  'no-cache, no-store, must-revalidate, post-check=0, pre-check=0',
        'Pragma': 'no-cache',
        'Expires': '0',
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });

    // const forgetPswd = 'employees/get-my-new-password';
    // if ((request.url.search(re) === -1)) {
    //   if (this._authService.isLoggedIn) {
    //     const headers = new HttpHeaders({
    //       'Authorization': `Bearer ${this._authService.getToken().accessToken}`,
    //       'Content-Type': 'application/json',
    //       'Accept': 'application/json'
    //     });

    //     newRequest = request.clone({ headers: headers });
    //     return next.handle(newRequest);

    //   } else {
    //     this.router.navigate(['/login']);
    //   }
    // }
    return next.handle(request.clone({ headers: headers1 }));
  }
}
