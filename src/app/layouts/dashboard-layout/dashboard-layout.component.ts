
import { DataService } from '../../shared/services/data.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { EncryptDecryptService } from 'src/app/shared/services/encrypt-decrypt.service';

@Component({
  selector: 'app-dashboard-layout',
  templateUrl: './dashboard-layout.component.html',
  styleUrls: ['./dashboard-layout.component.scss']
})
export class DashboardLayoutComponent implements OnInit {
  public isCollapsed = false;
  isCollapsableValue: boolean = false
  loginTimeValue: any
  currentYear: any;
  loginData: any;
  isSyncing: any;
  constructor(private _dataServie: DataService,
    private router: Router,
    private _authService: AuthService,
    private _decryptService: EncryptDecryptService) {
    this.currentYear = (new Date()).getFullYear()
  }

  ngOnInit(): void {
    this.loginData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    if(!this.loginData.data){
    this.router.navigateByUrl('/login');
  }
    this.loginTimeValue = moment(localStorage.getItem("currentDate")).fromNow()
    setInterval(() => {
      this.loginTimeValue = moment(localStorage.getItem("currentDate")).fromNow()
    }, 5000)

    this._dataServie.isCollapsableValue.subscribe((result) => {
      this.isCollapsableValue = result
    })
    this._dataServie.isSyncing.subscribe((result: any) => {
      this.isSyncing = result;
    });
    
  }
  sideNavClick() {
    this._dataServie.isCollapsableMethod(this.isCollapsed = !this.isCollapsed)
  }

  logout() {
    let standDetails: any;
    if (localStorage.getItem("standsArray")) {
      standDetails = JSON.parse(localStorage.getItem("standsArray") || "")
    }
    const sessionDetails = localStorage.getItem("sessionStartTime");
    if (sessionDetails) {
      this._dataServie.putApi(environment.endSessionURL, {
        "end_time": new Date(Date.now()).toString(),
        "start_time": JSON.parse(localStorage.getItem("sessionStartTime") || "").start_time,
        "sessionId": JSON.parse(sessionDetails)._id,
        "userName": this._authService.getToken().data.userName,
        "employeeId": this.loginData.data.employeeId,
        "employeeName": this._authService.getToken().data.name,
        "role": this._authService.getToken().data.role,
        "standName": standDetails && standDetails.length > 0 ? standDetails[0].standName : '',
        "standId": standDetails && standDetails.length > 0 ? standDetails[0].standId : '',
        "ownerId": standDetails && standDetails.length > 0 ? standDetails[0].ownerId : ''
      })
        .subscribe(res => {
          // if (res && res.status == 'success') {
          localStorage.clear();
          this._dataServie.setLoginData('')
          this.router.navigateByUrl('/login')
          // }
        })
    }
    else {
      localStorage.clear();
      this._dataServie.setLoginData('')
      this.router.navigateByUrl('/login')
    }

    // })
  }
}
