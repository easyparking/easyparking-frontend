
import { SidenavComponent } from './dashboardComponents/sidenav/sidenav.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
// import { ModalModule } from 'ngb-modal';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './landingPageComponents/footer/footer.component';
import { HeaderComponent } from './landingPageComponents/header/header.component';
import { HelpComponent } from './landingPageComponents/help/help.component';
import { HomeComponent } from './landingPageComponents/home/home.component';
import { OursectorsComponent } from './landingPageComponents/oursectors/oursectors.component';
import { SignUpComponent } from './landingPageComponents/sign-up/sign-up.component';
import { ForgotComponent } from './landingPageComponents/forgot/forgot.component';
import { LandingLayoutComponent } from './layouts/landing-layout/landing-layout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ParkingsComponent } from './dashboardComponents/parkings/parkings.component';
import { ParkingsearchComponent } from './dashboardComponents/parkingsearch/parkingsearch.component';
import { WantedVehiclesComponent } from './dashboardComponents/wanted-vehicles/wanted-vehicles.component';
import { MonthlycardsComponent } from './dashboardComponents/monthlycards/monthlycards.component';
import { OnlinecardsComponent } from './dashboardComponents/onlinecards/onlinecards.component';
import { VehicleNamesComponent } from './dashboardComponents/vehicle-names/vehicle-names.component';
import { ApplicationsComponent } from './dashboardComponents/applications/applications.component';
import { DemoRequestsComponent } from './dashboardComponents/demo-requests/demo-requests.component';
import { SettingsComponent } from './dashboardComponents/settings/settings.component';
import { OnlinePaymentReportComponent } from './dashboardComponents/online-payment-report/online-payment-report.component';
import { OwnerDashboardComponent } from './dashboardComponents/owner-dashboard/owner-dashboard.component';
import { ReportsComponent } from './dashboardComponents/reports/reports.component';
import { MinireportsComponent } from './dashboardComponents/minireports/minireports.component';
import { ForceLogoutComponent } from './dashboardComponents/force-logout/force-logout.component';
import { AdminDashboardComponent } from './dashboardComponents/admin-dashboard/admin-dashboard.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
/*import { MaterialComponentsModule } from './material-components.module';*/
import { NgbModule, NgbActiveModal, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgbdSortableHeader } from './directives/tableDirective';
import { OfflinePaymentsReportComponent } from './dashboardComponents/offline-payments-report/offline-payments-report.component';
import { ViewStandsComponent } from './dashboardComponents/view-stands/view-stands.component';
import { OfflinecardsComponent } from './dashboardComponents/offlinecards/offlinecards.component';
import { AppregistrationsComponent } from './dashboardComponents/appregistrations/appregistrations.component';
import { EmpSessionHistoryComponent } from './dashboardComponents/emp-session-history/emp-session-history.component';
import { SlipLostVehiclesComponent } from './dashboardComponents/slip-lost-vehicles/slip-lost-vehicles.component';
import { AnalyticsComponent } from './dashboardComponents/analytics/analytics.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './shared/interceptors/token.interceptor';
import { MaterialComponentsModule } from './material-components.module';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { EmployeeDashboardComponent } from './dashboardComponents/employee-dashboard/employee-dashboard.component';
import { PriceRulesComponent } from './dashboardComponents/price-rules/price-rules.component';
import { EmploysComponent } from './dashboardComponents/employs/employs.component';
import { SettlementsComponent } from './dashboardComponents/settlements/settlements.component';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { DashboardLayoutComponent } from './layouts/dashboard-layout/dashboard-layout.component';
import { SignInComponent } from './landingPageComponents/sign-in/sign-in.component';
import { AccountsComponent } from './dashboardComponents/accounts/accounts.component';
import { LoginActivityComponent } from './dashboardComponents/login-activity/login-activity.component';
import { ProductsComponent } from './landingPageComponents/products/products.component';
import { OwnerMonthlyCardsComponent } from './dashboardComponents/owner-monthly-cards/owner-monthly-cards.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ShowpdfComponent } from './showpdf/showpdf.component';
import { QRCodeModule } from 'angularx-qrcode';
import { MyAccountComponent } from './customerComponents/my-account/my-account.component';
import { PlanTypeComponent } from './dashboardComponents/plan-type/plan-type.component';
import { ParkingTypeComponent } from './dashboardComponents/parking-type/parking-type.component';
import { ProfileComponent } from './dashboardComponents/profile/profile.component';
import { GetStandComponent } from './landingPageComponents/get-stand/get-stand.component';
import { HelpRequestComponent } from './dashboardComponents/help-request/help-request.component';
import { AgmCoreModule } from '@agm/core';
import { ChartsModule } from 'ng2-charts';
import { ResetPasswordComponent } from './dashboardComponents/reset-password/reset-password.component';
// import { Autofocus } from './directives/autofocus.directive';
// import { AutofocusDirective } from './autofocus.directive';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    OursectorsComponent,
    HelpComponent,
    HeaderComponent,
    FooterComponent,
    SignInComponent,
    SignUpComponent,
    LandingLayoutComponent,
    DashboardLayoutComponent,
    ForgotComponent,
    SidenavComponent,
    ParkingsComponent,
    ParkingsearchComponent,
    WantedVehiclesComponent,
    MonthlycardsComponent,
    OnlinecardsComponent,
    VehicleNamesComponent,
    ApplicationsComponent,
    DemoRequestsComponent,
    SettingsComponent,
    OnlinePaymentReportComponent,
    OwnerDashboardComponent,
    ReportsComponent,
    MinireportsComponent,
    ForceLogoutComponent,
    AdminDashboardComponent,
    NgbdSortableHeader,
    ProfileComponent,
    AccountsComponent,
    ViewStandsComponent,
    OfflinePaymentsReportComponent,
    OfflinecardsComponent,
    EmpSessionHistoryComponent,
    SlipLostVehiclesComponent,
    AnalyticsComponent,
    EmployeeDashboardComponent,
    PriceRulesComponent,
    EmploysComponent,
    SettlementsComponent,
    LoginActivityComponent,
    ProductsComponent,
    OwnerMonthlyCardsComponent,
    ShowpdfComponent,
    MyAccountComponent,
    PlanTypeComponent,
    ParkingTypeComponent,
    AppregistrationsComponent,
    GetStandComponent,
    HelpRequestComponent,
    ResetPasswordComponent
    // Autofocus,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgbModule,
    HttpClientModule,
    MaterialComponentsModule,
    NgIdleKeepaliveModule.forRoot(),
    LoadingBarRouterModule,
    QRCodeModule,
    NgbModule,
    ChartsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD8EtMxFYLAMHufP2ijGrFQ1zwJQoaLn08'
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }, NgbActiveModal,
    // {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent],

})
export class AppModule { }
