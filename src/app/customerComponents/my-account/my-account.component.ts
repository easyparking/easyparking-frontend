import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { EncryptDecryptService } from './../../shared/services/encrypt-decrypt.service';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from 'src/app/shared/services/data.service';
import { environment } from 'src/environments/environment.prod';
import swal from 'sweetalert2';
import { NgbDateParserFormatter, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateFRParserFormatt } from 'src/app/Pipes/Dateparserformat';
import * as moment from 'moment'
import { Router } from '@angular/router';
@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css'],
  providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatt }]
})
export class MyAccountComponent implements OnInit {
  userData: any
  updateCustomerDetailsForm!: FormGroup;
  show: boolean = false
  spinner: boolean = false
  searchVehicleForm: FormGroup;
  file: any;
  fileError: boolean = false;
  fileName: any;
  url: any;
  base64: any = "";
  monthlyCardsData: any = [];
  loader: boolean = false;
  passwordMatched = true;
  currentCard: any;
  vehicleTypes: any = [];
  priceRulesDetails: any;
  amount: any;
  cardOfferPrice: any;
  actualAmount: number = 0;
  cardsForm!: FormGroup;
  hasError: boolean = false;
  constructor(private _decryptService: EncryptDecryptService,
    private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private _fb: FormBuilder,
    private _dataService: DataService,
    private _ngbFormat: NgbDateParserFormatter,
    private router: Router) {
    this.updateCustomerDetailsForm = this._fb.group({
      name: ['', [Validators.required]],
      vehicleNumber: ['', [Validators.required,Validators.pattern("^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$")]],
      phoneNumber: ['', [Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]],
      pwd: [''],
      reTypePwd: ['']
    });
    this.cardsForm! = this._fb.group({
      startDate: [''],
      phone: ['', [Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]],
      vehicalType: ['Bicycle', Validators.required],
      duration: ['1', Validators.required],
      name: ['', Validators.required],
      vehicalNumber: ['', [Validators.required, Validators.pattern("^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$")]],
      amount: ['', Validators.required],
      cardNumber: [''],
      endDate: ''
    })
    this.searchVehicleForm = this._fb.group({
      "vehicleNumber": ['', [Validators.required,Validators.pattern("^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}[0-9]{4}$")]],
      // "type":['', [Validators.required]],
      "phoneNumber": ['', [Validators.required, Validators.pattern("^(?!0+$)[6-9][0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]],
      // "insuranceValidty":['', [Validators.required]],
      "missingDate": ['', [Validators.required]],
      "policeComplaintMade": [''],
      "remarks": [''],
      "isMatched": [false],
      "engineNumber": [''],
      "chassisNumber": [0],
      "insuranceFrom": [''],
      "insuranceTo": [''],
      "cBook": [''],
    });
  }
  test() {
    // // console.log(this.searchVehicleForm)
  }

  ngOnInit(): void {
    this.userData = localStorage.getItem('UserData') ? this._decryptService.decryptData(localStorage.getItem('UserData')) : {};
    if(!this.userData.data){
    this.router.navigateByUrl('/login');
  }
    this.updateCustomerDetailsForm.setValue({
      name: this.userData.data.name,
      vehicleNumber: this.userData.data.vehicleNumber,
      phoneNumber: this.userData.data.phoneNumber,
      pwd: '',
      reTypePwd: ''
    })
    // this.getMonthlyCards()
  }
  openModal(content: any) {
    this.updateCustomerDetailsForm.setValue({
      name: this.userData.data.name,
      vehicleNumber: this.userData.data.vehicleNumber,
      phoneNumber: this.userData.data.phoneNumber,
      pwd: '',
      reTypePwd: ''
    })
    this.activeModal = this.modalService.open(content, { size: 'lg', centered: true });

  }

  showHidePassword() {
    this.show = !this.show
  }
  matchPasswords() {
    this.passwordMatched = false;
    if(!this.updateCustomerDetailsForm.controls.pwd.value && !this.updateCustomerDetailsForm.controls.reTypePwd.value){
      this.passwordMatched = true;
    }
    if (this.updateCustomerDetailsForm.controls.pwd.value && this.updateCustomerDetailsForm.controls.reTypePwd.value) {
      // if (this.updateCustomerDetailsForm.controls.reTypePwd.value.length >= this.updateCustomerDetailsForm.controls.pwd.value.length) {
      if (this.updateCustomerDetailsForm.controls.pwd.value != this.updateCustomerDetailsForm.controls.reTypePwd.value) {
        this.updateCustomerDetailsForm.controls.reTypePwd.setErrors({ noMatch: true })
        this.passwordMatched = false;
        // this.updateCustomerDetailsForm.controls.reTypePwd.markAsDirty()
        // this.updateCustomerDetailsForm.controls.reTypePwd.markAsTouched()
      } else {
        this.updateCustomerDetailsForm.controls.reTypePwd.setErrors(null);
        this.passwordMatched = true;
      }
    } else if (this.updateCustomerDetailsForm.controls.pwd.value) {
      if (this.updateCustomerDetailsForm.controls.pwd.value.indexOf(' ') >= 0) {
        this.updateCustomerDetailsForm.controls.pwd.setErrors({ specialCharacter: true });
        this.passwordMatched = false;
      } else {
        this.updateCustomerDetailsForm.controls.pwd.setErrors(null);
        this.passwordMatched = false;
      }
    }
  }
  searchRequest() {
    let missingDate = new Date(this.searchVehicleForm.controls.missingDate.value.month + '/' + this.searchVehicleForm.controls.missingDate.value.day + '/' + this.searchVehicleForm.controls.missingDate.value.year)
    let insuranceFrom: any = '';
    if (this.searchVehicleForm.controls.insuranceFrom.value && this.searchVehicleForm.controls.insuranceFrom.value.month) {
      insuranceFrom = new Date(this.searchVehicleForm.controls.insuranceFrom.value.month + '/' + this.searchVehicleForm.controls.insuranceFrom.value.day + '/' + this.searchVehicleForm.controls.insuranceFrom.value.year)
    }
    let insuranceTo: any = '';
    if (this.searchVehicleForm.controls.insuranceTo.value && this.searchVehicleForm.controls.insuranceTo.value.month) {
      insuranceTo = new Date(this.searchVehicleForm.controls.insuranceTo.value.month + '/' + this.searchVehicleForm.controls.insuranceTo.value.day + '/' + this.searchVehicleForm.controls.insuranceTo.value.year)
    }
    let data = {
      "vehicleNumber": this.searchVehicleForm.value.vehicleNumber ? this.searchVehicleForm.value.vehicleNumber.toUpperCase() : '',
      // "type":this.searchVehicleForm.value.type,
      "phoneNumber": this.searchVehicleForm.value.phoneNumber,
      // "insuranceValidty":this.searchVehicleForm.value.insuranceValidty,
      "missingDate": this.searchVehicleForm.controls.missingDate.value ? missingDate.toString() : '',
      "requestBy": this.userData.data.name,
      "requestDate": new Date().toString(),
      "policeComplaintMade": this.searchVehicleForm.value.policeComplaintMade,
      "remarks": this.searchVehicleForm.value.remarks,
      "isMatched": this.searchVehicleForm.value.isMatched,
      "engineNumber": this.searchVehicleForm.value.engineNumber,
      "chassisNumber": this.searchVehicleForm.value.chassisNumber,
      "insuranceFrom": this.searchVehicleForm.value.insuranceFrom ? insuranceFrom.toString() : '',
      "insuranceTo": this.searchVehicleForm.value.insuranceTo ? insuranceTo.toString() : '',
      "cBook": this.base64,
      "stolenDate": this.searchVehicleForm.controls.missingDate.value ? missingDate.toString() : '',
    }
    this.spinner = true;
    this._dataService.postApi(environment.addWantedVehicle, data)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.spinner = false;
          // modal.dismiss('Data saved')
          swal.fire("", res.message, "success");
          // this.getVehiclesList(1);
          setTimeout(() => {
            swal.close()
          }, 3000);
          this.searchVehicleForm.reset();
          this.clearFiles();
        }
        else {
          this.spinner = false
          swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
          setTimeout(() => {
            swal.close()
          }, 3000);
        }
      });
  }

  uploadFile(event: any) {
    this.file = event.target.files[0];
    if (this.file.size > 100000) {
      this.fileError = true
      setTimeout(() => {
        this.fileError = false
      }, 10000);
    } else {
      this.fileName = this.file.name;
      this.fileError = false;
      setTimeout(() => {
        this.searchVehicleForm.controls.cBook.setValue(this.file.name);
        this.searchVehicleForm.controls.cBook.markAsDirty();
      }, 1);
      const reader = new FileReader();
      reader.readAsDataURL(this.file);
      reader.onload = (event: any) => {
        this.url = event.target.result
        this.base64 = reader.result
      };
    }
  }
  clearFiles() {
    this.base64 = '';
    this.file = undefined;
    this.url = '';
    this.fileError = false;
    this.fileName = "";
    setTimeout(() => {
      this.searchVehicleForm.controls.cBook.setValue(null);
      this.searchVehicleForm.controls.cBook.markAsDirty();
    }, 1);
  }

  updateProfile() {
    this.spinner = true    
    let dataToAPI: any = {
      "id": this.userData.data._id,
      "userName": this.updateCustomerDetailsForm.value.name,
      "phoneNumber": this.updateCustomerDetailsForm.value.phoneNumber,
      "name": this.updateCustomerDetailsForm.value.name,
      "vehicleNumber": this.updateCustomerDetailsForm.value.vehicleNumber
    }
    if(this.updateCustomerDetailsForm.value.pwd){
      dataToAPI['password'] = this.updateCustomerDetailsForm.value.pwd;
    }
    this._dataService.putApi(environment.updateEmployeeDataURL, dataToAPI)
      .subscribe((res) => {
        if (res && res.status == 'success') {
          swal.fire("", res.message, "success");
          this.spinner = false;
          this.userData = {
            data: res.result
          }
          this.activeModal.close();
          this.updateCustomerDetailsForm.reset()
        }
        else {
          this.spinner = false
          swal.fire("Warning!", res.message, "warning");
        }

      }, (error) => {
        this.spinner = false
        swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
      })
  }
  getMonthlyCards() {
    this.loader = true
    this._dataService.getApi(environment.getCustomerMonthlyCardsURL + '/' + this.userData.data.name)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success') {
          this.loader = false
          this.monthlyCardsData = res.data
        } else {
          this.loader = false
          this.monthlyCardsData = []
        }
      })
  }

  getDate(date: any) {
    // if(date){
    // let newDate = date.split('/')[1] + '/' + date.split('/')[0]  + '/' + date.split('/')[2]
    //   return moment(newDate).format("DD/MM/YYYY HH:mm:ss")
    // } else {
    return moment(date).format("DD/MM/YYYY HH:mm:ss")
    // }
  }
  tabChanged(type: any) {
    if (type == 'monthlyCards') {
      this.getMonthlyCards()
    }
  }

  getPriceRulesDetails() {
    this.vehicleTypes = [];
    this._dataService.getApi(environment.getPriceRulesURL + this.currentCard.standId)
      .subscribe((res) => {
        if (res && res.status.toLowerCase() == 'success' && res.data) {
          this.priceRulesDetails = res.data;
          for (let i = 0; i < res.data[0].vehicleTypes.length; i++) {
            if (!(res.data[0].vehicleTypes[i].amount == 0 && (res.data[0].vehicleTypes[i].fixedPriceFor1stHour == 0 || res.data[0].vehicleTypes[i].forEveryNextHour == 0))) {
              if (res.data[0].vehicleTypes[i].vehicletype != 'bicycle') {
                if (res.data[0].vehicleTypes[i].vehicletype == 'twoWheeler') {

                  res.data[0].vehicleTypes[i].vehicletypeValue = '2 W'
                  this.vehicleTypes.push(res.data[0].vehicleTypes[i])
                } else if (res.data[0].vehicleTypes[i].vehicletype == 'threeWheeler') {
                  // if(!(res.data[0].vehicleTypes[i].amount == 0 && (res.data[0].vehicleTypes[i].fixedPriceFor1stHour == 0 || res.data[0].vehicleTypes[i].forEveryNextHour == 0))){
                  res.data[0].vehicleTypes[i].vehicletypeValue = '3 W'
                  this.vehicleTypes.push(res.data[0].vehicleTypes[i])
                  // }
                } else if (res.data[0].vehicleTypes[i].vehicletype == 'fourWheeler') {
                  // if(!(res.data[0].vehicleTypes[i].amount == 0 && (res.data[0].vehicleTypes[i].fixedPriceFor1stHour == 0 || res.data[0].vehicleTypes[i].forEveryNextHour == 0))){
                  res.data[0].vehicleTypes[i].vehicletypeValue = '4 W'
                  this.vehicleTypes.push(res.data[0].vehicleTypes[i])
                  // }
                } else {
                  // if(!(res.data[0].vehicleTypes[i].amount == 0 && (res.data[0].vehicleTypes[i].fixedPriceFor1stHour == 0 || res.data[0].vehicleTypes[i].forEveryNextHour == 0))){
                  res.data[0].vehicleTypes[i].vehicletypeValue = res.data[0].vehicleTypes[i].vehicletype
                  this.vehicleTypes.push(res.data[0].vehicleTypes[i])
                  // }
                }
              } else {
                // if(!(res.data[0].vehicleTypes[i].amount == 0 && (res.data[0].vehicleTypes[i].fixedPriceFor1stHour == 0 || res.data[0].vehicleTypes[i].forEveryNextHour == 0))){
                res.data[0].vehicleTypes[i].vehicletypeValue = 'Bicycle';
                this.vehicleTypes.push(res.data[0].vehicleTypes[i])
                // }
              }
            }
            if ((res.data[0].vehicleTypes.length == i + 1 )) {
              // this.isPriceRuleAdded = false
              
      this.getAmount(this.currentCard.vehicleType);
            }
          }
        }
      })
  }

  getAmount(event: any) {
    if (this.vehicleTypes) {
      let vehicle = this.vehicleTypes.filter((element: any) => element.vehicletype == event);
      if (vehicle.length > 0) {
        this.amount = vehicle[0].cardActualPrice;
        this.cardOfferPrice = vehicle[0].cardOfferPrice;
        if (this.cardsForm.controls.duration.value) {
          this.actualAmount = vehicle[0].cardActualPrice * Number(this.cardsForm.controls.duration.value)
          setTimeout(() => {
            this.cardsForm.controls.amount.setValue(this.actualAmount)
            // this.cardsForm.controls.amount.setValidators(Validators.max(this.actualAmount))
          }, 1);
        } else {
          this.actualAmount = vehicle[0].cardActualPrice;
          setTimeout(() => {
            this.cardsForm.controls.amount.setValue(this.actualAmount)
            // this.cardsForm.controls.amount.setValidators(Validators.max(this.actualAmount))
          }, 1);
        }
      }
    } else {
      this.actualAmount = 0;
      this.cardsForm.controls.amount.setValue(0)
    }
  }
  changePrice(event: number) {
    if (event < this.cardOfferPrice) {
      this.hasError = true;
    } else {
      this.hasError = false;
    }
  }
  startDateChanged($event: any) {
    if ($event) {
      let endDate = new Date($event.month + '/' + $event.day + '/' + $event.year)
      let date = new Date(endDate.setMonth(($event.month - 1) + parseInt(this.cardsForm.controls.duration.value)))
      let date1 = moment(date).subtract(1, 'day')
      this.cardsForm.controls.endDate.setValue(date1.format('DD') + "/" + (date1.format('MM')) + "/" + date1.format('yyyy'))
    }
  }
  
  durationChanged(duration: any) {
    if (this.cardsForm.controls.startDate.value) {
      let endDate = new Date(this.cardsForm.controls.startDate.value.month + '/' + this.cardsForm.controls.startDate.value.day + '/' + this.cardsForm.controls.startDate.value.year)
      let date = new Date(endDate.setMonth((this.cardsForm.controls.startDate.value.month - 1) + parseInt(duration)))
      let date1 = moment(date).subtract(1, 'day')
      this.cardsForm.controls.endDate.setValue(date1.format('DD') + "/" + (date1.format('MM')) + "/" + date1.format('yyyy'))
    }
    setTimeout(() => {
      this.actualAmount = this.amount * Number(duration);
      this.cardsForm.controls.amount.setValue(this.amount * Number(duration))
    }, 1);
  }

  openRenewalModel(content: any, card: any) {
    this.cardsForm.reset();
    this.spinner = false;
      this.currentCard = card;
      this.getPriceRulesDetails();
      let date = new Date(this.currentCard.endDate);
      let startDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
      let getEndDate = new Date((date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear())
      if (this.currentCard.vehicleType == "bicycle") {
        setTimeout(() => {
          this.cardsForm.controls.vehicalNumber.setErrors(null);
        }, 1);
      } else {
        setTimeout(() => {
          if(!this.currentCard.vehicleNumber){
            this.cardsForm.controls.vehicalNumber.setErrors({ require: true });
          }
        }, 1);
      }
      let endDate = getEndDate.getDate() + '/' + ((getEndDate.getMonth() + 1) + Number(this.currentCard.duration)) + '/' + getEndDate.getFullYear();
      this.cardsForm.setValue({
        startDate: this._ngbFormat.parse(startDate),
        phone: this.currentCard.customerPhone,
        vehicalType: this.currentCard.vehicleType,
        duration: this.currentCard.duration,
        cardNumber: this.currentCard.cardId,
        name: this.currentCard.customerName,
        vehicalNumber: this.currentCard.vehicleNumber,
        amount: this.currentCard.amount,
        endDate: endDate
      });
    this.modalService.open(content, { size: 'lg', centered: true });
  }
  renewalCardAPICall(modal: any) {
    let date = this.cardsForm.controls.startDate.value ? moment(this.cardsForm.controls.startDate.value.month + '/' + this.cardsForm.controls.startDate.value.day + '/' + this.cardsForm.controls.startDate.value.year) : moment();
    this.currentCard.cardHistory.push({
      "startDate": date.toString(),
      "endDate": moment(date).add(parseInt(this.currentCard.duration), 'M').toString(),
      "duration": this.currentCard.duration,
      "amount": Number(this.currentCard.amount)
    })
    let dataToAPI = {
      "cardId": this.currentCard.cardId,
      "startDate": date.toString(),
      "endDate": moment(date).add(parseInt(this.currentCard.duration), 'M').toString(),
      "renewalDate": moment().toString(),
      "cardHistory": this.currentCard.cardHistory,
      "sendSMS": false,
      "cardType": 'Online',
      "amount": Number(this.cardsForm.controls.amount.value)
    }
    this.spinner = true;
      this._dataService.putApi(environment.cardRenewalUrl, dataToAPI)
        .subscribe((res) => {
          if (res && res.status.toLowerCase() == 'success') {
            this.spinner = false;
            modal.dismiss('Data saved')
            swal.fire("", res.message, "success");
            setTimeout(() => {
              swal.close()
            }, 3000);
            this.getMonthlyCards();
            this.cardsForm.reset()
          }
          else {
            this.spinner = false;
            swal.fire('Oops!', 'Something went wrong. Please try again', 'error');
            setTimeout(() => {
              swal.close()
            }, 3000);
          }
        });
  }
}
